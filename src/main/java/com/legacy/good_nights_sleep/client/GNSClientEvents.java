package com.legacy.good_nights_sleep.client;

import com.legacy.good_nights_sleep.capabillity.DreamPlayer;
import com.legacy.good_nights_sleep.client.render.gui.DreamTransitionScreen;
import com.legacy.good_nights_sleep.client.render.gui.layers.WakeOverlayLayer;
import com.legacy.good_nights_sleep.mixin.LevelRendererInvoker;
import com.legacy.good_nights_sleep.registry.GNSDimensions;

import net.minecraft.client.Camera;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.DimensionSpecialEffects;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.material.FogType;
import net.minecraft.world.phys.Vec3;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.client.event.ClientTickEvent;
import net.neoforged.neoforge.client.event.RegisterDimensionSpecialEffectsEvent;
import net.neoforged.neoforge.client.event.RegisterDimensionTransitionScreenEvent;
import net.neoforged.neoforge.client.event.RegisterGuiLayersEvent;
import net.neoforged.neoforge.client.event.ViewportEvent;

@SuppressWarnings("resource")
public class GNSClientEvents
{
	private static final DimensionSpecialEffects DREAM_RENDER_INFO = new DreamRenderInfo();
	private static final DimensionSpecialEffects NIGHTMARE_RENDER_INFO = new NightmareRenderInfo();

	public static Minecraft mc()
	{
		return Minecraft.getInstance();
	}

	public static void registerOverlays(RegisterGuiLayersEvent event)
	{
		event.registerAboveAll(WakeOverlayLayer.KEY, new WakeOverlayLayer());
	}

	@SubscribeEvent
	public static void onClientTick(ClientTickEvent.Post event)
	{
		Level world = mc().level;

		if (world == null || world != null && !world.isClientSide)
			return;

		if (mc().player != null && DreamPlayer.get(mc().player) != null)
			DreamPlayer.get(mc().player).clientTick();
	}

	@SubscribeEvent
	public static void modifyFog(ViewportEvent.RenderFog event)
	{
		// Fog in the nightmare. Slightly reduced from the Nether's numbers, while also
		// keeping the sky rendered, which would not happen if we used isFoggyAt
		if (mc().player != null && mc().player.level().dimensionType().effectsLocation().equals(GNSDimensions.NIGHTMARE_ID))
		{
			Camera cam = event.getCamera();
			FogType fogtype = cam.getFluidInCamera();
			if (fogtype != FogType.POWDER_SNOW && fogtype != FogType.LAVA && !((LevelRendererInvoker) mc().levelRenderer).gns$doesMobEffectBlockSky(cam))
			{
				event.setNearPlaneDistance(mc().gameRenderer.getRenderDistance() * 0.05F);
				event.setFarPlaneDistance(Math.min(mc().gameRenderer.getRenderDistance(), 192.0F) * 0.8F);
				event.setCanceled(true);
			}
		}
	}
	
	public static void registerDimensionTransitions(RegisterDimensionTransitionScreenEvent event)
	{
		event.registerIncomingEffect(GNSDimensions.dreamKey(), DreamTransitionScreen::new);
		event.registerOutgoingEffect(GNSDimensions.dreamKey(), DreamTransitionScreen::new);
		
		event.registerIncomingEffect(GNSDimensions.nightmareKey(), DreamTransitionScreen::new);
		event.registerOutgoingEffect(GNSDimensions.nightmareKey(), DreamTransitionScreen::new);
	}

	public static void initDimensionRenderInfo(RegisterDimensionSpecialEffectsEvent event)
	{
		event.register(GNSDimensions.DREAM_ID, DREAM_RENDER_INFO);
		event.register(GNSDimensions.NIGHTMARE_ID, NIGHTMARE_RENDER_INFO);
	}

	// i am going insane
	public static float calculateSunAngle(long dayTime, float partialTicks)
	{
		long worldTime;
		Player player = mc().player;

		if (player != null && player.level().isClientSide && DreamPlayer.get(player) != null)
		{
			worldTime = mc().level.getGameTime() - DreamPlayer.get(player).getEnteredDreamTime();
		}
		else
			worldTime = dayTime;

		int j = (int) (worldTime % 48000L);
		float angle = ((float) j + partialTicks) / 48000.0F - 0.25F;

		if (player != null && player.level() != null && player.level().dimension().equals(GNSDimensions.nightmareKey()))
			angle += 0.5F;

		if (angle < 0.0F)
		{
			++angle;
		}

		if (angle > 1.0F)
		{
			--angle;
		}

		float f2 = angle;
		angle = 1.0F - (float) ((Math.cos((double) angle * Math.PI) + 1.0D) / 2.0D);
		angle = f2 + (angle - f2) / 3.0F;

		return angle;
	}

	public static class DreamRenderInfo extends DimensionSpecialEffects.OverworldEffects
	{
		public DreamRenderInfo()
		{
			super();
		}

		/*@Override
		public boolean renderSky(ClientLevel level, int ticks, float partialTick, PoseStack poseStack, Camera camera, Matrix4f projectionMatrix, boolean isFoggy, Runnable setupFog)
		{
			DreamSkyRenderer.INSTANCE.render(ticks, partialTick, poseStack, level, camera, projectionMatrix, setupFog);
			return true;
		}*/
	}

	public static class NightmareRenderInfo extends DimensionSpecialEffects.OverworldEffects
	{
		public NightmareRenderInfo()
		{
			super();
		}

		@Override
		public Vec3 getBrightnessDependentFogColor(Vec3 fogColor, float fogBrightness)
		{
			return fogColor;
		}

		// 0.035F
		@Override
		public boolean isFoggyAt(int posX, int posZ)
		{
			return false;
		}

		/*@Override
		public boolean renderSky(ClientLevel level, int ticks, float partialTick, PoseStack poseStack, Camera camera, Matrix4f projectionMatrix, boolean isFoggy, Runnable setupFog)
		{
			DreamSkyRenderer.INSTANCE.render(ticks, partialTick, poseStack, level, camera, projectionMatrix, setupFog);
			return true;
		}*/
	}
}
