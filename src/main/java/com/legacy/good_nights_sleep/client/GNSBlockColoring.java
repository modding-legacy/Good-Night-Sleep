package com.legacy.good_nights_sleep.client;

import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.data.GNSBiomeTags;
import com.legacy.good_nights_sleep.mixin.BiomeColorsMixin;
import com.legacy.good_nights_sleep.registry.GNSBlocks;
import com.legacy.good_nights_sleep.registry.GNSDimensions;
import com.mojang.serialization.MapCodec;

import net.minecraft.client.Minecraft;
import net.minecraft.client.color.item.ItemTintSource;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.renderer.BiomeColors;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockAndTintGetter;
import net.minecraft.world.level.Level;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.client.event.RegisterColorHandlersEvent;

@EventBusSubscriber(modid = GoodNightSleep.MODID, bus = Bus.MOD, value = Dist.CLIENT)
public class GNSBlockColoring
{
	@SubscribeEvent
	public static void registerBlockColors(final RegisterColorHandlersEvent.Block event)
	{
		Minecraft mc = Minecraft.getInstance();

		event.register((state, level, pos, index) ->
		{
			if (mc.level != null && pos != null)
			{
				if (mc.level.dimension().equals(GNSDimensions.dreamKey()) || mc.level.getBiome(pos).is(GNSBiomeTags.IS_DREAM))
					return level != null ? getAverageGrassColor(level, pos) : 0xffffff;
			}

			return 0xffffff;
		}, GNSBlocks.dream_grass_block, GNSBlocks.short_dream_grass);
	}

	@SubscribeEvent
	public static void registerItemColors(final RegisterColorHandlersEvent.ItemTintSources event)
	{
		event.register(GoodNightSleep.locate("dream_grass"), DreamGrassTintSource.CODEC);
		/*event.register((itemStack, index) -> index == 0 ? 0xffffff : -1, GNSBlocks.dream_grass_block, GNSBlocks.short_dream_grass);*/
	}

	public static int getAverageGrassColor(BlockAndTintGetter pLevel, BlockPos pBlockPos)
	{
		return pLevel.getBlockTint(pBlockPos, BiomeColors.GRASS_COLOR_RESOLVER);
	}

	/**
	 * Color of vanilla grass in our dimension(s)
	 * 
	 * {@link BiomeColorsMixin}
	 */
	@SuppressWarnings("resource")
	public static void getVanillaGrassColor(BlockAndTintGetter pLevel, BlockPos pBlockPos, CallbackInfoReturnable<Integer> callback)
	{
		Player player = null;

		if ((player = Minecraft.getInstance().player) != null)
		{
			ResourceKey<Level> dim = player.level().dimension();
			if (dim.equals(GNSDimensions.dreamKey()))
				callback.setReturnValue(0x79eb7e);
		}
	}

	public static final record DreamGrassTintSource() implements ItemTintSource
	{
		static final MapCodec<DreamGrassTintSource> CODEC = MapCodec.unit(DreamGrassTintSource::new);

		@Override
		public int calculate(ItemStack stack, ClientLevel level, LivingEntity entity)
		{
			return 0xffffff;
		}

		@Override
		public MapCodec<? extends ItemTintSource> type()
		{
			return CODEC;
		}

	}
}