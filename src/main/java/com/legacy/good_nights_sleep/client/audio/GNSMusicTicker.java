package com.legacy.good_nights_sleep.client.audio;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.registry.GNSDimensions;
import com.legacy.good_nights_sleep.registry.GNSSounds;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.texture.Tickable;
import net.minecraft.client.resources.sounds.SimpleSoundInstance;
import net.minecraft.client.resources.sounds.SoundInstance;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.client.event.ClientTickEvent;
import net.neoforged.neoforge.client.event.sound.PlaySoundEvent;

@OnlyIn(Dist.CLIENT)
public class GNSMusicTicker implements Tickable
{
	private static final GNSMusicTicker INSTANCE = new GNSMusicTicker();

	// FIXME
	private final RandomSource rand;

	public SoundInstance ambientMusic, playingRecord;

	private int timeUntilNextMusic = 100;

	public GNSMusicTicker()
	{
		this.rand = RandomSource.create();
	}

	@SuppressWarnings("resource")
	public void tick()
	{
		TrackType tracktypeB = this.getRandomTrackDream();
		TrackType tracktypeD = this.getRandomTrackNightmare();

		try
		{
			if (mc().player != null && !mc().getSoundManager().isActive(this.playingRecord) && GNSDimensions.inSleepDimension(mc().player))
			{
				if (mc().player.level().dimension().location().equals(GNSDimensions.DREAM_ID))
				{
					if (this.ambientMusic != null)
					{
						if (!mc().getSoundManager().isActive(this.ambientMusic))
						{
							this.ambientMusic = null;
							this.timeUntilNextMusic = Math.min(Mth.nextInt(this.rand, tracktypeB.getMinDelay(), tracktypeB.getMaxDelay()), this.timeUntilNextMusic);
						}
					}
					this.timeUntilNextMusic = Math.min(this.timeUntilNextMusic, tracktypeB.getMaxDelay());
					if (this.ambientMusic == null && this.timeUntilNextMusic-- <= 0)
					{
						this.playMusic(tracktypeB);
					}
				}
				else if (mc().player.level().dimension().location().equals(GNSDimensions.NIGHTMARE_ID))
				{
					if (this.ambientMusic != null)
					{
						if (!mc().getSoundManager().isActive(this.ambientMusic))
						{
							this.ambientMusic = null;
							this.timeUntilNextMusic = Math.min(Mth.nextInt(this.rand, tracktypeD.getMinDelay(), tracktypeD.getMaxDelay()), this.timeUntilNextMusic);
						}
					}
					this.timeUntilNextMusic = Math.min(this.timeUntilNextMusic, tracktypeD.getMaxDelay());
					if (this.ambientMusic == null && this.timeUntilNextMusic-- <= 0)
					{
						this.playMusic(tracktypeD);
					}
				}
				else
				{
					this.stopMusic();
				}
			}
		}
		catch (NullPointerException e)
		{
			e.printStackTrace();
		}
	}

	public boolean playingMusic()
	{
		return this.ambientMusic != null;
	}

	public boolean playingRecord()
	{
		return this.playingRecord != null;
	}

	public GNSMusicTicker.TrackType getRandomTrackDream()
	{
		return this.rand.nextBoolean() ? TrackType.SKY_BLUE : TrackType.DREAM;
	}

	public GNSMusicTicker.TrackType getRandomTrackNightmare()
	{
		return TrackType.NIGHTMARE;
	}

	public void playMusic(TrackType requestedMusicType)
	{
		this.ambientMusic = SimpleSoundInstance.forMusic(requestedMusicType.getMusicLocation());
		mc().getSoundManager().play(this.ambientMusic);
		this.timeUntilNextMusic = Integer.MAX_VALUE;
	}

	public void stopMusic()
	{
		if (this.ambientMusic != null)
		{
			mc().getSoundManager().stop(this.ambientMusic);
			this.ambientMusic = null;
			this.timeUntilNextMusic = 0;
		}
	}

	private static Minecraft mc()
	{
		return Minecraft.getInstance();
	}

	@OnlyIn(Dist.CLIENT)
	public static enum TrackType
	{
		// @formatter:off
    	DREAM(GNSSounds.MUSIC_GOOD_DREAM, 1200, 1500),
    	SKY_BLUE(GNSSounds.MUSIC_SKY_BLUE, 1200, 1500),
    	NIGHTMARE(GNSSounds.MUSIC_NIGHTMARE, 1200, 1500);
    	// @formatter:on

		private final SoundEvent musicLocation;
		private final int minDelay;
		private final int maxDelay;

		private TrackType(SoundEvent musicLocationIn, int minDelayIn, int maxDelayIn)
		{
			this.musicLocation = musicLocationIn;
			this.minDelay = minDelayIn;
			this.maxDelay = maxDelayIn;
		}

		public SoundEvent getMusicLocation()
		{
			return this.musicLocation;
		}

		public int getMinDelay()
		{
			return this.minDelay;
		}

		public int getMaxDelay()
		{
			return this.maxDelay;
		}
	}

	public static class Handler
	{
		@SubscribeEvent
		public static void onClientTick(ClientTickEvent.Post event) throws Exception
		{
			if (!mc().isPaused())
				INSTANCE.tick();
		}

		@SuppressWarnings("resource")
		@SubscribeEvent
		public static void onMusicControl(PlaySoundEvent event)
		{
			SoundInstance sound = event.getSound();

			if (sound == null)
				return;

			SoundSource category = sound.getSource();

			if (category == SoundSource.MUSIC && mc().player != null && GNSDimensions.inSleepDimension(mc().player) && !sound.getLocation().toString().contains(GoodNightSleep.MODID))
			{
				event.setSound(null);
				return;
			}
		}

		private static Minecraft mc()
		{
			return Minecraft.getInstance();
		}
	}
}