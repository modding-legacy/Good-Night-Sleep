package com.legacy.good_nights_sleep.client.render.entity;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.client.render.GNSRenderRefs;
import com.legacy.good_nights_sleep.client.render.entity.state.UnicornRenderState;
import com.legacy.good_nights_sleep.client.render.models.UnicornModel;
import com.legacy.good_nights_sleep.entity.dream.UnicornEntity;

import net.minecraft.client.renderer.entity.AbstractHorseRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class UnicornRenderer extends AbstractHorseRenderer<UnicornEntity, UnicornRenderState, UnicornModel>
{
	public UnicornRenderer(EntityRendererProvider.Context context)
	{
		super(context, new UnicornModel(context.bakeLayer(GNSRenderRefs.UNICORN)), new UnicornModel(context.bakeLayer(GNSRenderRefs.UNICORN_BABY)));
	}

	@Override
	public ResourceLocation getTextureLocation(UnicornRenderState state)
	{
		String type = state.variant == 1 ? "green" : state.variant == 2 ? "yellow" : state.variant == 3 ? "blue" : "pink";
		return GoodNightSleep.locate("textures/entity/unicorn/unicorn_" + type + ".png");
	}

	@Override
	public void extractRenderState(UnicornEntity entity, UnicornRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);
		state.variant = entity.getUnicornType();
	}

	@Override
	public UnicornRenderState createRenderState()
	{
		return new UnicornRenderState();
	}
}