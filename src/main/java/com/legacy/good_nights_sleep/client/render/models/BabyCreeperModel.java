package com.legacy.good_nights_sleep.client.render.models;

import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.entity.state.CreeperRenderState;
import net.minecraft.util.Mth;

public class BabyCreeperModel extends EntityModel<CreeperRenderState>
{
	public ModelPart root;
	public ModelPart head;
	public ModelPart body;

	public ModelPart rightHindLeg;
	public ModelPart leftHindLeg;

	public ModelPart rightFrontLeg;
	public ModelPart leftFrontLeg;

	public BabyCreeperModel(ModelPart model)
	{
		super(model);
		this.root = model;

		this.head = model.getChild("head");
		this.body = model.getChild("body");

		this.rightFrontLeg = model.getChild("right_front_leg");
		this.leftFrontLeg = model.getChild("left_front_leg");
		this.rightHindLeg = model.getChild("right_hind_leg");
		this.leftHindLeg = model.getChild("left_hind_leg");
	}

	public static LayerDefinition createBodyLayer(CubeDeformation size)
	{
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();
		partdefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0).addBox(-4.0F, -8.0F, -4.0F, 8, 8, 8, size), PartPose.offset(0.0F, 12.0F, 0.0F));
		partdefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(16, 16).addBox(-4.0F, 0.0F, -2.0F, 8, 8, 4, size), PartPose.offset(0.0F, 12.0F, 0.0F));

		CubeListBuilder frontLeg = CubeListBuilder.create().texOffs(0, 16).addBox(-2.0F, 0.0F, -4.0F, 4, 4, 4, size);
		CubeListBuilder backLeg = CubeListBuilder.create().texOffs(0, 16).addBox(-2.0F, 0.0F, 0.0F, 4, 4, 4, size);

		partdefinition.addOrReplaceChild("right_front_leg", frontLeg, PartPose.offset(-2.0F, 20.0F, -2.0F)); // leg 3
		partdefinition.addOrReplaceChild("left_front_leg", frontLeg, PartPose.offset(2.0F, 20.0F, -2.0F)); // leg 4
		partdefinition.addOrReplaceChild("right_hind_leg", backLeg, PartPose.offset(-2.0F, 20.0F, 2.0F)); // leg 1
		partdefinition.addOrReplaceChild("left_hind_leg", backLeg, PartPose.offset(2.0F, 20.0F, 2.0F)); // leg 2

		return LayerDefinition.create(meshdefinition, 64, 32);
	}

	@Override
	public void setupAnim(CreeperRenderState state)
	{
		float limbSwing = state.walkAnimationPos, limbSwingAmount = state.walkAnimationSpeed;
		this.head.yRot = state.yRot / Mth.RAD_TO_DEG;
		this.head.xRot = state.xRot / Mth.RAD_TO_DEG;
		this.rightHindLeg.xRot = Mth.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
		this.leftHindLeg.xRot = Mth.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
		this.rightFrontLeg.xRot = Mth.cos(limbSwing * 0.6662F + (float) Math.PI) * 1.4F * limbSwingAmount;
		this.leftFrontLeg.xRot = Mth.cos(limbSwing * 0.6662F) * 1.4F * limbSwingAmount;
	}
}
