package com.legacy.good_nights_sleep.client.render.models;

import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.ZombieModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.renderer.entity.state.ZombieRenderState;
import net.minecraft.util.Mth;

public class TormenterModel<S extends ZombieRenderState> extends ZombieModel<S>
{
	public TormenterModel(ModelPart model)
	{
		super(model);
	}

	public static LayerDefinition createBodyLayer()
	{
		MeshDefinition meshdefinition = HumanoidModel.createMesh(CubeDeformation.NONE, 0.0F);
		return LayerDefinition.create(meshdefinition, 64, 32);
	}

	@Override
	public void setupAnim(S state)
	{
		super.setupAnim(state);

		float ageInTicks = state.ageInTicks;
		float f6 = Mth.sin(state.attackTime * Mth.PI);
		float f7 = Mth.sin((1.0F - (1.0F - state.attackTime) * (1.0F - state.attackTime)) * Mth.PI);
		this.rightArm.zRot = 0.34906587F;
		this.leftArm.zRot = -0.5235988F;
		this.rightArm.yRot = -(0.1F - f6 * 0.6F) - 0.5235988F;
		this.leftArm.yRot = 0.1F - f6 * 0.6F;
		this.rightArm.xRot = -((float) Math.PI / 2F);
		this.leftArm.xRot = -1.7453293F;
		this.rightArm.xRot -= f6 * 1.2F - f7 * 0.4F;
		this.leftArm.xRot -= f6 * 1.2F - f7 * 0.4F;
		this.rightArm.zRot += Mth.cos(ageInTicks * 0.09F) * 0.05F + 0.05F;
		this.leftArm.zRot -= Mth.cos(ageInTicks * 0.09F) * 0.05F + 0.05F;
		this.rightArm.xRot += Mth.sin(ageInTicks * 0.067F) * 0.05F;
		this.leftArm.xRot -= Mth.sin(ageInTicks * 0.067F) * 0.05F;
		this.head.zRot = -0.5235988F;
		this.head.yRot = state.yRot / Mth.RAD_TO_DEG - 0.17453294F;
		this.head.xRot = state.xRot / Mth.RAD_TO_DEG - 0.34906587F;
	}
}
