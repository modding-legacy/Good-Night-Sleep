package com.legacy.good_nights_sleep.client.render.entity.layer;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.client.render.models.HerobrineModel;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.EyesLayer;
import net.minecraft.client.renderer.entity.state.PlayerRenderState;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class HerobrineEyeLayer extends EyesLayer<PlayerRenderState, HerobrineModel>
{
	private static final RenderType EYES = RenderType.eyes(GoodNightSleep.locate("textures/entity/herobrine_eyes.png"));

	public HerobrineEyeLayer(RenderLayerParent<PlayerRenderState, HerobrineModel> state)
	{
		super(state);
	}

	@Override
	public RenderType renderType()
	{
		return EYES;
	}
}
