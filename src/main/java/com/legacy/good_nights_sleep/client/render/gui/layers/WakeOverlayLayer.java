package com.legacy.good_nights_sleep.client.render.gui.layers;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.capabillity.DreamPlayer;
import com.legacy.good_nights_sleep.registry.GNSDimensions;

import net.minecraft.client.DeltaTracker;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.LayeredDraw;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;

public class WakeOverlayLayer implements LayeredDraw.Layer
{
	public static final ResourceLocation KEY = GoodNightSleep.locate("gns_wake_overlay");

	@Override
	public void render(GuiGraphics graphics, DeltaTracker deltaTracker)
	{
		Minecraft mc = Minecraft.getInstance();

		DreamPlayer dp;
		if (mc.player != null && !mc.options.hideGui && GNSDimensions.inSleepDimension(mc.player) && (dp = DreamPlayer.get(mc.player)) != null/* && !mc.getDebugOverlay().showDebugScreen()*/)
		{
			float total = 25000F, start = total * 0.99F;

			float f = (float) ((mc.level.getGameTime() - dp.getEnteredDreamTime()) - (start));

			// System.out.println("start: " + start + " - current: " + f);

			float amount = -(total - start);

			if (f > 0)
			{
				// System.out.println("active " + f);

				float f1 = f / amount;
				if (f1 > 1.0F)
				{
					f1 = 1.0F - (f - amount) / 10.0F;
				}

				int i = (int) (255.0F * -Mth.clamp(f1, -1, 0)) << 24 | 1052704;
				graphics.fill(RenderType.guiOverlay(), 0, 0, graphics.guiWidth(), graphics.guiHeight(), i);
			}

		}
	}

}
