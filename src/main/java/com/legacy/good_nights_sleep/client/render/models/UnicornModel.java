package com.legacy.good_nights_sleep.client.render.models;

import net.minecraft.client.model.HorseModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.renderer.entity.state.EquineRenderState;

public class UnicornModel extends HorseModel
{
	private final ModelPart horn;

	public UnicornModel(ModelPart model)
	{
		super(model);

		this.horn = model.getChild("horn");
	}

	public static LayerDefinition createUniBodyLayer()
	{
		MeshDefinition meshdefinition = HorseModel.createBodyMesh(CubeDeformation.NONE);
		PartDefinition root = meshdefinition.getRoot();
		root.addOrReplaceChild("horn", CubeListBuilder.create().texOffs(56, 0).addBox(-1.0F, -16.0F, 2.0F, 2, 7, 2), PartPose.offset(0.0F, 4.0F, -10.0F));

		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	private static MeshDefinition createFullScaleBabyUniMesh(CubeDeformation scale)
	{
		MeshDefinition meshdefinition = HorseModel.createFullScaleBabyMesh(scale);
		PartDefinition root = meshdefinition.getRoot();
		root.addOrReplaceChild("horn", CubeListBuilder.create().texOffs(56, 0).addBox(-1.0F, -16.0F, 2.0F, 2, 7, 2), PartPose.offset(0.0F, 4.0F, -10.0F));

		return meshdefinition;
	}

	public static MeshDefinition createBabyUniMesh(CubeDeformation scale)
	{
		return BABY_TRANSFORMER.apply(createFullScaleBabyUniMesh(scale));
	}

	@Override
	public void setupAnim(EquineRenderState state)
	{
		super.setupAnim(state);
		this.horn.y = this.headParts.y;
		this.horn.z = this.headParts.z;

		this.horn.copyFrom(this.headParts);
	}
}
