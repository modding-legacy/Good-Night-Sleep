package com.legacy.good_nights_sleep.client.render.tile;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.client.model.geom.EntityModelSet;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.renderer.special.NoDataSpecialModelRenderer;
import net.minecraft.client.renderer.special.SpecialModelRenderer;
import net.minecraft.client.resources.model.Material;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemDisplayContext;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class GNSBedSpecialRenderer implements NoDataSpecialModelRenderer
{
	private final GNSBedBlockEntityRenderer bedRenderer;
	private final Material material;

	public GNSBedSpecialRenderer(GNSBedBlockEntityRenderer bedRenderer, Material material)
	{
		this.bedRenderer = bedRenderer;
		this.material = material;
	}

	@Override
	public void render(ItemDisplayContext displayContext, PoseStack pose, MultiBufferSource buffer, int packedLight, int packedOverlay, boolean p_387936_)
	{
		this.bedRenderer.renderInHand(pose, buffer, packedLight, packedOverlay, this.material);
	}

	@OnlyIn(Dist.CLIENT)
	public static record Unbaked(ResourceLocation texture) implements SpecialModelRenderer.Unbaked
	{
		public static final MapCodec<GNSBedSpecialRenderer.Unbaked> MAP_CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(ResourceLocation.CODEC.fieldOf("texture").forGetter(GNSBedSpecialRenderer.Unbaked::texture)).apply(instance, GNSBedSpecialRenderer.Unbaked::new));

		public Unbaked(Material mat)
		{
			this(mat.texture());
		}

		@Override
		public MapCodec<GNSBedSpecialRenderer.Unbaked> type()
		{
			return MAP_CODEC;
		}

		@Override
		public SpecialModelRenderer<?> bake(EntityModelSet modelSet)
		{
			return new GNSBedSpecialRenderer(new GNSBedBlockEntityRenderer(modelSet), new Material(Sheets.BED_SHEET, this.texture));
		}
	}
}
