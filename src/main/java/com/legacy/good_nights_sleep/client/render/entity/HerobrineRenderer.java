package com.legacy.good_nights_sleep.client.render.entity;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.client.render.GNSRenderRefs;
import com.legacy.good_nights_sleep.client.render.entity.layer.HerobrineEyeLayer;
import com.legacy.good_nights_sleep.client.render.models.HerobrineModel;
import com.legacy.good_nights_sleep.entity.HerobrineEntity;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.HumanoidMobRenderer;
import net.minecraft.client.renderer.entity.state.PlayerRenderState;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class HerobrineRenderer extends HumanoidMobRenderer<HerobrineEntity, PlayerRenderState, HerobrineModel>
{
	private static final ResourceLocation TEXTURE = GoodNightSleep.locate("textures/entity/herobrine.png");

	public HerobrineRenderer(EntityRendererProvider.Context context)
	{
		super(context, new HerobrineModel(context.bakeLayer(GNSRenderRefs.HEROBRINE)), 0.5F);
		this.addLayer(new HerobrineEyeLayer(this));
	}

	@Override
	public ResourceLocation getTextureLocation(PlayerRenderState entity)
	{
		return TEXTURE;
	}

	@Override
	protected boolean affectedByCulling(HerobrineEntity state)
	{
		return false;
	}

	@Override
	public PlayerRenderState createRenderState()
	{
		return new PlayerRenderState();
	}
}