package com.legacy.good_nights_sleep.client.render.gui;

import java.util.function.BooleanSupplier;

import net.minecraft.client.gui.GuiGraphics;
import net.minecraft.client.gui.screens.ReceivingLevelScreen;
import net.minecraft.client.renderer.RenderType;

public class DreamTransitionScreen extends ReceivingLevelScreen
{
	protected final ReceivingLevelScreen.Reason reason;

	public DreamTransitionScreen(BooleanSupplier levelReceived, Reason reason)
	{
		super(levelReceived, reason);
		this.reason = reason;
	}

	@Override
	public void renderBackground(GuiGraphics guiGraphics, int mouseX, int mouseY, float partialTick)
	{
		if (this.reason != ReceivingLevelScreen.Reason.OTHER)
		{
			super.renderBackground(guiGraphics, mouseX, mouseY, partialTick);
			return;
		}

		guiGraphics.fill(RenderType.guiOverlay(), 0, 0, this.width, this.height, 255 << 24 | 1052704);
	}
}
