package com.legacy.good_nights_sleep.client.render;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.client.render.entity.BabyCreeperRenderer;
import com.legacy.good_nights_sleep.client.render.entity.HerobrineRenderer;
import com.legacy.good_nights_sleep.client.render.entity.TormenterRenderer;
import com.legacy.good_nights_sleep.client.render.entity.UnicornRenderer;
import com.legacy.good_nights_sleep.client.render.models.BabyCreeperModel;
import com.legacy.good_nights_sleep.client.render.models.HerobrineModel;
import com.legacy.good_nights_sleep.client.render.models.TormenterModel;
import com.legacy.good_nights_sleep.client.render.models.UnicornModel;
import com.legacy.good_nights_sleep.client.render.tile.GNSBedBlockEntityRenderer;
import com.legacy.good_nights_sleep.client.render.tile.GNSBedSpecialRenderer;
import com.legacy.good_nights_sleep.registry.GNSBlockEntityTypes;
import com.legacy.good_nights_sleep.registry.GNSEntityTypes;

import net.minecraft.client.model.HumanoidArmorModel;
import net.minecraft.client.model.HumanoidModel;
import net.minecraft.client.model.geom.LayerDefinitions;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshTransformer;
import net.minecraft.client.renderer.blockentity.HangingSignRenderer;
import net.minecraft.client.renderer.blockentity.SignRenderer;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.neoforge.client.event.EntityRenderersEvent;
import net.neoforged.neoforge.client.event.RegisterSpecialBlockModelRendererEvent;
import net.neoforged.neoforge.client.event.RegisterSpecialModelRendererEvent;

public class GNSEntityRendering
{
	public static void init(IEventBus modBus)
	{
		modBus.addListener(GNSEntityRendering::initLayers);
		modBus.addListener(GNSEntityRendering::initRenders);
		modBus.addListener(GNSEntityRendering::initSpecialModelRenderers);
		modBus.addListener(GNSEntityRendering::initSpecialBlockRenderers);
	}

	public static void initLayers(EntityRenderersEvent.RegisterLayerDefinitions event)
	{
		LayerDefinition outerArmor = LayerDefinition.create(HumanoidArmorModel.createBodyLayer(LayerDefinitions.OUTER_ARMOR_DEFORMATION), 64, 32);
		LayerDefinition innerArmor = LayerDefinition.create(HumanoidArmorModel.createBodyLayer(LayerDefinitions.INNER_ARMOR_DEFORMATION), 64, 32);

		MeshTransformer unicornScaler = MeshTransformer.scaling(1.1F);
		event.registerLayerDefinition(GNSRenderRefs.UNICORN, () -> UnicornModel.createUniBodyLayer().apply(unicornScaler));
		event.registerLayerDefinition(GNSRenderRefs.UNICORN_BABY, () -> LayerDefinition.create(UnicornModel.createBabyUniMesh(CubeDeformation.NONE), 64, 64).apply(unicornScaler));
		event.registerLayerDefinition(GNSRenderRefs.BABY_CREEPER, () -> BabyCreeperModel.createBodyLayer(CubeDeformation.NONE));
		/*event.registerLayerDefinition(GNSRenderRefs.GUMMY_BEAR, () -> GummyBearModel.createBodyLayer(CubeDeformation.NONE));*/

		event.registerLayerDefinition(GNSRenderRefs.TORMENTER, TormenterModel::createBodyLayer);
		event.registerLayerDefinition(GNSRenderRefs.TORMENTER_INNER_ARMOR, () -> innerArmor);
		event.registerLayerDefinition(GNSRenderRefs.TORMENTER_OUTER_ARMOR, () -> outerArmor);
		event.registerLayerDefinition(GNSRenderRefs.TORMENTER_BABY, () -> TormenterModel.createBodyLayer().apply(HumanoidModel.BABY_TRANSFORMER));
		event.registerLayerDefinition(GNSRenderRefs.TORMENTER_BABY_INNER_ARMOR, () -> innerArmor.apply(HumanoidModel.BABY_TRANSFORMER));
		event.registerLayerDefinition(GNSRenderRefs.TORMENTER_BABY_OUTER_ARMOR, () -> outerArmor.apply(HumanoidModel.BABY_TRANSFORMER));

		event.registerLayerDefinition(GNSRenderRefs.HEROBRINE, HerobrineModel::createBodyLayer);
	}

	public static void initRenders(EntityRenderersEvent.RegisterRenderers event)
	{
		event.registerEntityRenderer(GNSEntityTypes.UNICORN, UnicornRenderer::new);
		/*event.registerEntityRenderer(GNSEntityTypes.GUMMY_BEAR, GummyBearRenderer::new);*/
		event.registerEntityRenderer(GNSEntityTypes.BABY_CREEPER, BabyCreeperRenderer::new);
		event.registerEntityRenderer(GNSEntityTypes.TORMENTER, TormenterRenderer::new);
		event.registerEntityRenderer(GNSEntityTypes.HEROBRINE, HerobrineRenderer::new);

		event.registerBlockEntityRenderer(GNSBlockEntityTypes.DREAM_BED.get(), GNSBedBlockEntityRenderer::new);
		event.registerBlockEntityRenderer(GNSBlockEntityTypes.SIGN.get(), SignRenderer::new);
		event.registerBlockEntityRenderer(GNSBlockEntityTypes.HANGING_SIGN.get(), HangingSignRenderer::new);

	}

	public static void initSpecialModelRenderers(RegisterSpecialModelRendererEvent event)
	{
		event.register(GoodNightSleep.locate("bed"), GNSBedSpecialRenderer.Unbaked.MAP_CODEC);
	}

	public static void initSpecialBlockRenderers(RegisterSpecialBlockModelRendererEvent event)
	{
		/*event.register(GNSBlocks.strange_bed, new GNSBedSpecialRenderer.Unbaked(GNSRenderRefs.STRANGE_BED_MATERIAL));
		event.register(GNSBlocks.luxurious_bed, new GNSBedSpecialRenderer.Unbaked(GNSRenderRefs.LUXURIOUS_BED_MATERIAL));
		event.register(GNSBlocks.wretched_bed, new GNSBedSpecialRenderer.Unbaked(GNSRenderRefs.WRETCHED_BED_MATERIAL));*/
	}
}