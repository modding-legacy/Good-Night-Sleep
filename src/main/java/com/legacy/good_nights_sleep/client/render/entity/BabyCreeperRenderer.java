package com.legacy.good_nights_sleep.client.render.entity;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.client.render.GNSRenderRefs;
import com.legacy.good_nights_sleep.client.render.models.BabyCreeperModel;
import com.legacy.good_nights_sleep.entity.dream.BabyCreeperEntity;
import com.mojang.blaze3d.vertex.PoseStack;

import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.state.CreeperRenderState;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class BabyCreeperRenderer<T extends BabyCreeperEntity> extends MobRenderer<T, CreeperRenderState, BabyCreeperModel>
{
	private static final ResourceLocation TEXTURE = GoodNightSleep.locate("textures/entity/baby_creeper.png");

	public BabyCreeperRenderer(EntityRendererProvider.Context context)
	{
		super(context, new BabyCreeperModel(context.bakeLayer(GNSRenderRefs.BABY_CREEPER)), 0.5F);
	}

	@Override
	protected void scale(CreeperRenderState state, PoseStack pose)
	{
		float f = state.swelling;
		float f1 = 1.0F + Mth.sin(f * 100.0F) * f * 0.01F;
		f = Mth.clamp(f, 0.0F, 1.0F);
		f *= f;
		f *= f;
		float f2 = (1.0F + f * 0.4F) * f1;
		float f3 = (1.0F + f * 0.1F) / f1;
		pose.scale(f2, f3, f2);
	}

	@Override
	protected float getWhiteOverlayProgress(CreeperRenderState state)
	{
		float f = state.swelling;
		return (int) (f * 10.0F) % 2 == 0 ? 0.0F : Mth.clamp(f, 0.5F, 1.0F);
	}

	@Override
	public void extractRenderState(T entity, CreeperRenderState state, float partialTicks)
	{
		super.extractRenderState(entity, state, partialTicks);
		state.swelling = entity.getSwelling(partialTicks);
		state.isPowered = entity.isPowered();
	}

	@Override
	public ResourceLocation getTextureLocation(CreeperRenderState entity)
	{
		return TEXTURE;
	}

	@Override
	public CreeperRenderState createRenderState()
	{
		return new CreeperRenderState();
	}
}