package com.legacy.good_nights_sleep.client.render;

import com.legacy.good_nights_sleep.GoodNightSleep;

import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.resources.model.Material;

// ModelLayers
public interface GNSRenderRefs
{
	Material LUXURIOUS_BED_MATERIAL = new Material(Sheets.BED_SHEET, GoodNightSleep.locate("entity/bed/luxurious_bed"));
	Material WRETCHED_BED_MATERIAL = new Material(Sheets.BED_SHEET, GoodNightSleep.locate("entity/bed/wretched_bed"));
	Material STRANGE_BED_MATERIAL = new Material(Sheets.BED_SHEET, GoodNightSleep.locate("entity/bed/strange_bed"));

	ModelLayerLocation UNICORN = layer("unicorn");
	ModelLayerLocation UNICORN_BABY = layer("unicorn", "baby");
	ModelLayerLocation GUMMY_BEAR = layer("gummy_bear");
	ModelLayerLocation BABY_CREEPER = layer("baby_creeper");

	ModelLayerLocation TORMENTER = layer("tormenter");
	ModelLayerLocation TORMENTER_INNER_ARMOR = innerArmor("tormenter");
    ModelLayerLocation TORMENTER_OUTER_ARMOR = outerArmor("tormenter");
	ModelLayerLocation TORMENTER_BABY = layer("tormenter_baby");
    ModelLayerLocation TORMENTER_BABY_INNER_ARMOR = innerArmor("tormenter_baby");
    ModelLayerLocation TORMENTER_BABY_OUTER_ARMOR = outerArmor("tormenter_baby");
    
	ModelLayerLocation HEROBRINE = layer("herobrine");

	private static ModelLayerLocation layer(String name)
	{
		return layer(name, "main");
	}

	private static ModelLayerLocation layer(String name, String layer)
	{
		return new ModelLayerLocation(GoodNightSleep.locate(name), layer);
	}

	private static ModelLayerLocation innerArmor(String path)
	{
		return layer(path, "inner_armor");
	}

	private static ModelLayerLocation outerArmor(String path)
	{
		return layer(path, "outer_armor");
	}
}
