package com.legacy.good_nights_sleep.client.render.entity;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.client.render.GNSRenderRefs;
import com.legacy.good_nights_sleep.client.render.models.TormenterModel;
import com.legacy.good_nights_sleep.entity.TormenterEntity;

import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.renderer.entity.AbstractZombieRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.state.ZombieRenderState;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class TormenterRenderer extends AbstractZombieRenderer<TormenterEntity, ZombieRenderState, TormenterModel<ZombieRenderState>>
{
	private static final ResourceLocation TEXTURE = GoodNightSleep.locate("textures/entity/tormenter.png");

	public TormenterRenderer(EntityRendererProvider.Context context)
	{
		this(context, GNSRenderRefs.TORMENTER, GNSRenderRefs.TORMENTER_BABY, GNSRenderRefs.TORMENTER_INNER_ARMOR, GNSRenderRefs.TORMENTER_OUTER_ARMOR, GNSRenderRefs.TORMENTER_BABY_INNER_ARMOR, GNSRenderRefs.TORMENTER_BABY_OUTER_ARMOR);
	}

	public TormenterRenderer(EntityRendererProvider.Context context, ModelLayerLocation zombieLayer, ModelLayerLocation innerArmor, ModelLayerLocation outerArmor, ModelLayerLocation p_362432_, ModelLayerLocation p_361708_, ModelLayerLocation p_365510_)
	{
		super(context, new TormenterModel<>(context.bakeLayer(zombieLayer)), new TormenterModel<>(context.bakeLayer(innerArmor)), new TormenterModel<>(context.bakeLayer(outerArmor)), new TormenterModel<>(context.bakeLayer(p_362432_)), new TormenterModel<>(context.bakeLayer(p_361708_)), new TormenterModel<>(context.bakeLayer(p_365510_)));
	}

	@Override
	public ResourceLocation getTextureLocation(ZombieRenderState entity)
	{
		return TEXTURE;
	}

	@Override
	public ZombieRenderState createRenderState()
	{
		return new ZombieRenderState();
	}
}