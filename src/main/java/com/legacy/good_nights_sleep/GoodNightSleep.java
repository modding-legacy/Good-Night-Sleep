package com.legacy.good_nights_sleep;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.legacy.good_nights_sleep.blocks.util.Flamibility;
import com.legacy.good_nights_sleep.blocks.util.ToolCompat;
import com.legacy.good_nights_sleep.client.GNSClientEvents;
import com.legacy.good_nights_sleep.client.render.GNSEntityRendering;
import com.legacy.good_nights_sleep.client.resource_pack.GNSResourcePackHandler;
import com.legacy.good_nights_sleep.commands.GNSCommand;
import com.legacy.good_nights_sleep.data.GNSBlockTags;
import com.legacy.good_nights_sleep.data.GNSItemTags;
import com.legacy.good_nights_sleep.event.GNSEvents;
import com.legacy.good_nights_sleep.event.GNSPlayerEvents;
import com.legacy.good_nights_sleep.registry.GNSDimensions;
import com.legacy.good_nights_sleep.registry.GNSEntityTypes;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.bus.api.EventPriority;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.ModContainer;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.config.ModConfig;
import net.neoforged.fml.event.lifecycle.FMLClientSetupEvent;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.client.gui.ConfigurationScreen;
import net.neoforged.neoforge.client.gui.IConfigScreenFactory;
import net.neoforged.neoforge.common.NeoForge;
import net.neoforged.neoforge.event.RegisterCommandsEvent;

@Mod(GoodNightSleep.MODID)
public class GoodNightSleep
{
	public static final Logger LOGGER = LogManager.getLogger();
	public static final String NAME = "Good Night's Sleep";
	public static final String MODID = "good_nights_sleep";

	// would disconnect outdated players from servers if this mismatches
	public static final String PACKET_VERSION = "1.21.3-1.4.0";

	// used for remapping
	public static final String OLD_MODID = "goodnightsleep";

	public static ResourceLocation locate(String name)
	{
		return ResourceLocation.fromNamespaceAndPath(MODID, name);
	}

	public static ResourceLocation locateOld(String name)
	{
		return ResourceLocation.fromNamespaceAndPath(OLD_MODID, name);
	}

	public static String find(String name)
	{
		return MODID + ":" + name;
	}

	public GoodNightSleep(IEventBus modBus, ModContainer modContainer)
	{
		/*ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, GNSConfig.COMMON_SPEC);*/
		modContainer.registerConfig(ModConfig.Type.SERVER, GNSConfig.WORLD_SPEC);

		if (FMLEnvironment.dist == Dist.CLIENT)
		{
			GNSEntityRendering.init(modBus);
			modBus.addListener(GoodNightSleep::clientInit);
			modBus.addListener(GNSClientEvents::initDimensionRenderInfo);
			modBus.addListener(GNSClientEvents::registerDimensionTransitions);
			modBus.addListener(GNSResourcePackHandler::packRegistry);
			modBus.addListener(GNSClientEvents::registerOverlays);
		}

		GNSDimensions.init();
		/*RegistrarHandler.registerHandlers(MODID, modBus, GNSBiomes.HANDLER, GNSBiomeModifiers.HANDLER);*/

		modContainer.registerExtensionPoint(IConfigScreenFactory.class, ConfigurationScreen::new);

		modBus.addListener(GoodNightSleep::commonInit);
		modBus.addListener(GNSEntityTypes::onAttributesRegistered);
		modBus.addListener(EventPriority.LOWEST, GNSEntityTypes::registerPlacements);
		modBus.addListener(GNSEntityTypes::registerPlacementOverrides);
	}

	private static void commonInit(final FMLCommonSetupEvent event)
	{
		IEventBus forge = NeoForge.EVENT_BUS;

		forge.register(GNSEvents.class);
		forge.register(GNSPlayerEvents.class);
		forge.addListener((RegisterCommandsEvent cmdEvent) -> GNSCommand.register(cmdEvent.getDispatcher()));
		ToolCompat.init();

		GNSBlockTags.init();
		GNSItemTags.init();

		event.enqueueWork(() ->
		{
			Flamibility.init();

			BuiltInRegistries.BLOCK.addAlias(locate("dream_grass"), locate("short_dream_grass"));
			BuiltInRegistries.BLOCK.addAlias(locate("nightmare_grass"), locate("short_nightmare_grass"));

			BuiltInRegistries.ITEM.addAlias(locate("dream_grass"), locate("short_dream_grass"));
			BuiltInRegistries.ITEM.addAlias(locate("nightmare_grass"), locate("short_nightmare_grass"));
		});
	}

	@OnlyIn(Dist.CLIENT)
	public static void clientInit(FMLClientSetupEvent event)
	{
		NeoForge.EVENT_BUS.register(com.legacy.good_nights_sleep.client.audio.GNSMusicTicker.Handler.class);
		NeoForge.EVENT_BUS.register(GNSClientEvents.class);
	}
}