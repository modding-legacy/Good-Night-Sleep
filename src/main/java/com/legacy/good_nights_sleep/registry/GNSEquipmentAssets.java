package com.legacy.good_nights_sleep.registry;

import com.legacy.good_nights_sleep.GoodNightSleep;

import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.equipment.EquipmentAsset;
import net.minecraft.world.item.equipment.EquipmentAssets;

public interface GNSEquipmentAssets
{
	ResourceKey<EquipmentAsset> CANDY = createId("candy");
	ResourceKey<EquipmentAsset> SPECTRITE = createId("spectrite");
	ResourceKey<EquipmentAsset> POSITITE = createId("positite");
	ResourceKey<EquipmentAsset> ZITRITE = createId("zitrite");
	ResourceKey<EquipmentAsset> NEGATITE = createId("negatite");

	// static final Set<ResourceKey<EquipmentAsset>> ASSETS = new HashSet<>();

	static ResourceKey<EquipmentAsset> createId(String key)
	{
		var rk = ResourceKey.create(EquipmentAssets.ROOT_ID, GoodNightSleep.locate(key));
		/*if (DatagenModLoader.isRunningDataGen())
			ASSETS.add(rk);*/

		return rk;
	}

	static void bootstrap(BootstrapContext<EquipmentAsset> context)
	{
		// ASSETS.forEach(key -> context.register(key, new EquipmentAsset()));
	}
}
