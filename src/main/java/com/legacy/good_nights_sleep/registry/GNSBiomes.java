package com.legacy.good_nights_sleep.registry;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.data.GNSBiomeProv;
import com.legacy.good_nights_sleep.util.Pointer;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.biome.Biome;
import net.neoforged.neoforge.data.loading.DatagenModLoader;
import net.neoforged.neoforge.registries.RegisterEvent;

public class GNSBiomes
{
	/*public static final RegistrarHandler<Biome> HANDLER = RegistrarHandler.getOrCreate(Registries.BIOME, GoodNightSleep.MODID);*/
	private static final Map<ResourceLocation, Pointer<Biome>> POINTERS = new HashMap<>();

	public static final Pointer<Biome> SLEEPY_HILLS = create("sleepy_hills", (c) -> GNSBiomeProv.sleepyHills(c, 0.5F, 0.0F, 4159204, 329011));
	public static final Pointer<Biome> GOOD_DREAM_PLAINS = create("good_dream_plains", (c) -> GNSBiomeProv.goodDreamPlains(c, 0.5F, 0.0F, 4159204, 329011));
	public static final Pointer<Biome> DREAMY_FOREST = create("dreamy_forest", (c) -> GNSBiomeProv.dreamyForest(c, 0.5F, 0.0F, 4159204, 329011));
	public static final Pointer<Biome> LOLLIPOP_LANDS = create("lollipop_lands", (c) -> GNSBiomeProv.lollipopLands(c, 0.5F, 0.0F, 4159204, 329011));

	public static final Pointer<Biome> NIGHTMARE_MOUND = create("nightmare_mound", (c) -> GNSBiomeProv.nightmareMound(c, 0.8F, 0.0F, 4159204, 329011));
	public static final Pointer<Biome> SHAMEFUL_PLAINS = create("shameful_plains", (c) -> GNSBiomeProv.shamefulPlains(c, 0.8F, 0.0F, 4159204, 329011));
	public static final Pointer<Biome> WASTED_FOREST = create("wasted_forest", (c) -> GNSBiomeProv.wastedForest(c, 0.8F, 0.0F, 4159204, 329011));

	public static void bootstrap(BootstrapContext<Biome> bootstrap)
	{
		POINTERS.forEach((key, pointer) -> bootstrap.register(pointer.getKey(), pointer.getInstance().apply(bootstrap)));
	}

	public static void init(RegisterEvent event)
	{
	}

	private static Pointer<Biome> create(String name, Function<BootstrapContext<?>, Biome> biome)
	{
		Pointer<Biome> pointer = new Pointer<>(Registries.BIOME, name, biome);

		if (DatagenModLoader.isRunningDataGen())
			POINTERS.put(GoodNightSleep.locate(name), pointer);

		return pointer;
	}
}
