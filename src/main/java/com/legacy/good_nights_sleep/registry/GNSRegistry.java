package com.legacy.good_nights_sleep.registry;

import java.util.function.Supplier;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.capabillity.DreamPlayer;
import com.legacy.good_nights_sleep.world.biome_provider.DreamBiomeSource;
import com.legacy.good_nights_sleep.world.biome_provider.NightmareBiomeSource;
import com.legacy.good_nights_sleep.world.chunkgen.DreamChunkGenerator;
import com.legacy.good_nights_sleep.world.chunkgen.NightmareChunkGenerator;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.entity.player.Player;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.attachment.AttachmentType;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.NeoForgeRegistries;
import net.neoforged.neoforge.registries.RegisterEvent;

@EventBusSubscriber(modid = GoodNightSleep.MODID, bus = Bus.MOD)
public class GNSRegistry
{
	public static final Lazy<AttachmentType<DreamPlayer>> PLAYER_ATTACHMENT = Lazy.of(() -> AttachmentType.builder(p -> new DreamPlayer((Player) p)).serialize(DreamPlayer.Serializer.INSTANCE).copyOnDeath().build());

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(Registries.ENTITY_TYPE))
			GNSEntityTypes.init(event);
		else if (event.getRegistryKey().equals(Registries.ITEM))
			GNSItems.init(event);
		else if (event.getRegistryKey().equals(Registries.BLOCK))
			GNSBlocks.init(event);
		else if (event.getRegistryKey().equals(Registries.SOUND_EVENT))
			GNSSounds.init();
		else if (event.getRegistryKey().equals(Registries.BLOCK_ENTITY_TYPE))
			GNSBlockEntityTypes.init(event);
		else if (event.getRegistryKey().equals(Registries.BIOME))
			GNSBiomes.init(event);
		else if (event.getRegistryKey().equals(Registries.FEATURE))
		{
			GNSFeatures.init(event);

			// GNSConfiguredCarvers.init();
		}
		else if (event.getRegistryKey().equals(Registries.BIOME_SOURCE))
		{
			event.register(Registries.BIOME_SOURCE, GoodNightSleep.locate("dream"), () -> DreamBiomeSource.CODEC);
			event.register(Registries.BIOME_SOURCE, GoodNightSleep.locate("nightmare"), () -> NightmareBiomeSource.CODEC);
		}
		else if (event.getRegistryKey().equals(Registries.CHUNK_GENERATOR))
		{
			event.register(Registries.CHUNK_GENERATOR, GoodNightSleep.locate("dream_chunk_generator"), () -> DreamChunkGenerator.DREAM_CODEC);
			event.register(Registries.CHUNK_GENERATOR, GoodNightSleep.locate("nightmare_chunk_generator"), () -> NightmareChunkGenerator.NIGHTMARE_CODEC);
		}
		else if (event.getRegistryKey().equals(NeoForgeRegistries.Keys.ATTACHMENT_TYPES))
			event.register(NeoForgeRegistries.Keys.ATTACHMENT_TYPES, GoodNightSleep.locate("player_attachment"), (Supplier) PLAYER_ATTACHMENT);

	}
}