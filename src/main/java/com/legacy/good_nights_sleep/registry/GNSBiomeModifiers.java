package com.legacy.good_nights_sleep.registry;

import com.legacy.good_nights_sleep.data.GNSBiomeTags;
import com.legacy.good_nights_sleep.util.Pointer;

import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.GenerationStep.Decoration;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.neoforged.neoforge.common.world.BiomeModifier;
import net.neoforged.neoforge.common.world.BiomeModifiers;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

public class GNSBiomeModifiers
{
	/*public static final RegistrarHandler<BiomeModifier> HANDLER = RegistrarHandler.getOrCreate(NeoForgeRegistries.Keys.BIOME_MODIFIERS, GoodNightSleep.MODID);*/

	public static final Pointer<BiomeModifier> ADD_HOPE_MUSHROOMS = new Pointer<>(NeoForgeRegistries.Keys.BIOME_MODIFIERS, "add_hope_mushrooms", c -> addFeature(c, Decoration.VEGETAL_DECORATION, GNSBiomeTags.HAS_HOPE_MUSHROOMS, GNSFeatures.Placements.NETHER_HOPE_MUSHROOM_PATCH.getKey()));
	public static final Pointer<BiomeModifier> ADD_DESPAIR_MUSHROOMS = new Pointer<>(NeoForgeRegistries.Keys.BIOME_MODIFIERS, "add_despair_mushrooms", c -> addFeature(c, Decoration.VEGETAL_DECORATION, GNSBiomeTags.HAS_DESPAIR_MUSHROOMS, GNSFeatures.Placements.NETHER_DESPAIR_MUSHROOM_PATCH.getKey()));

	private static BiomeModifier addFeature(BootstrapContext<?> bootstrap, GenerationStep.Decoration step, TagKey<Biome> spawnTag, ResourceKey<PlacedFeature> feature)
	{
		return new BiomeModifiers.AddFeaturesBiomeModifier(bootstrap.lookup(Registries.BIOME).getOrThrow(spawnTag), HolderSet.direct(bootstrap.lookup(Registries.PLACED_FEATURE).getOrThrow(feature)), step);
	}
	
	public static void bootstrap(BootstrapContext<BiomeModifier> bootstrap)
	{
		bootstrap.register(ADD_HOPE_MUSHROOMS.getKey(), ADD_HOPE_MUSHROOMS.getInstance().apply(bootstrap));
		bootstrap.register(ADD_DESPAIR_MUSHROOMS.getKey(), ADD_DESPAIR_MUSHROOMS.getInstance().apply(bootstrap));
	}
}
