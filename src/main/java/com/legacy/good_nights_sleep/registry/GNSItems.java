package com.legacy.good_nights_sleep.registry;

import java.util.Map.Entry;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.item.GNSArmorMaterials;
import com.legacy.good_nights_sleep.item.GNSFoods;
import com.legacy.good_nights_sleep.item.GNSToolMaterials;
import com.legacy.good_nights_sleep.item.SleepyStewItem;

import net.minecraft.core.Direction;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.item.ArmorItem;
import net.minecraft.world.item.AxeItem;
import net.minecraft.world.item.BedItem;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.DoubleHighBlockItem;
import net.minecraft.world.item.HangingSignItem;
import net.minecraft.world.item.HoeItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.ShovelItem;
import net.minecraft.world.item.SignItem;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraft.world.item.SwordItem;
import net.minecraft.world.item.component.Consumables;
import net.minecraft.world.item.equipment.ArmorType;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.registries.RegisterEvent;

public class GNSItems
{
	private static RegisterEvent registryEvent;

	public static Item positite, raw_zitrite, zitrite_ingot, negatite, necrum, raw_spectrite, spectrite_ingot,
			candy_bar;

	public static Item candy_pickaxe, candy_axe, candy_shovel, candy_hoe, candy_sword;

	public static Item necrum_pickaxe, necrum_axe, necrum_shovel, necrum_hoe, necrum_sword;

	public static Item zitrite_pickaxe, zitrite_axe, zitrite_shovel, zitrite_hoe, zitrite_sword;

	public static Item spectrite_pickaxe, spectrite_axe, spectrite_shovel, spectrite_hoe, spectrite_sword;

	public static Item positite_pickaxe, positite_axe, positite_shovel, positite_hoe, positite_sword;

	public static Item negatite_pickaxe, negatite_axe, negatite_shovel, negatite_hoe, negatite_sword;

	public static Item candy_helmet, candy_chestplate, candy_leggings, candy_boots;

	public static Item spectrite_helmet, spectrite_chestplate, spectrite_leggings, spectrite_boots;

	public static Item positite_helmet, positite_chestplate, positite_leggings, positite_boots;

	public static Item zitrite_helmet, zitrite_chestplate, zitrite_leggings, zitrite_boots;

	public static Item negatite_helmet, negatite_chestplate, negatite_leggings, negatite_boots;

	public static Item candy, lollipop, rainbow_berries, luxurious_soup, wretched_soup;

	public static Item powdered_sugar, necrotic_extract, rainbow_seeds;

	public static Item luxurious_bed, wretched_bed, strange_bed;

	public static Item dream_door, white_door, dead_door, blood_door;

	public static Item unicorn_spawn_egg, gummy_bear_spawn_egg, baby_creeper_spawn_egg, tormenter_spawn_egg,
			herobrine_spawn_egg, giant_spawn_egg;

	public static Item dream_sign, white_sign, blood_sign, dead_sign;

	public static Item dream_hanging_sign, white_hanging_sign, blood_hanging_sign, dead_hanging_sign;

	public static void init(RegisterEvent event)
	{
		registryEvent = event;

		// Block Items
		luxurious_bed = register("luxurious_bed", p -> new BedItem(GNSBlocks.luxurious_bed, p.stacksTo(1).useBlockDescriptionPrefix()));
		wretched_bed = register("wretched_bed", p -> new BedItem(GNSBlocks.wretched_bed, p.stacksTo(1).useBlockDescriptionPrefix()));
		strange_bed = register("strange_bed", p -> new BedItem(GNSBlocks.strange_bed, p.stacksTo(1).useBlockDescriptionPrefix()));

		dream_door = register("dream_door", p -> new DoubleHighBlockItem(GNSBlocks.dream_door, p.useBlockDescriptionPrefix()));
		white_door = register("white_door", p -> new DoubleHighBlockItem(GNSBlocks.white_door, p.useBlockDescriptionPrefix()));
		dead_door = register("dead_door", p -> new DoubleHighBlockItem(GNSBlocks.dead_door, p.useBlockDescriptionPrefix()));
		blood_door = register("blood_door", p -> new DoubleHighBlockItem(GNSBlocks.blood_door, p.useBlockDescriptionPrefix()));

		unicorn_spawn_egg = register("unicorn_spawn_egg", p -> new SpawnEggItem(GNSEntityTypes.UNICORN, p));
		/*gummy_bear_spawn_egg = register("gummy_bear_spawn_egg", p -> new SpawnEggItem(GNSEntityTypes.GUMMY_BEAR, 0xffffff, 0xffffff, p));*/
		baby_creeper_spawn_egg = register("baby_creeper_spawn_egg", p -> new SpawnEggItem(GNSEntityTypes.BABY_CREEPER, p));
		tormenter_spawn_egg = register("tormenter_spawn_egg", p -> new SpawnEggItem(GNSEntityTypes.TORMENTER, p));
		herobrine_spawn_egg = register("herobrine_spawn_egg", p -> new SpawnEggItem(GNSEntityTypes.HEROBRINE, p));
		/*giant_spawn_egg = register("giant_spawn_egg", p -> new SpawnEggItem(EntityType.GIANT, 1598464, 30652, p));*/

		candy = register("candy", p -> new Item(p.food(GNSFoods.CANDY, Consumables.DRIED_KELP)));
		lollipop = register("lollipop", p -> new Item(p.food(GNSFoods.LOLLIPOP, Consumables.DRIED_KELP)));
		candy_bar = register("candy_bar", p -> new Item(p));
		raw_spectrite = register("raw_spectrite", p -> new Item(p));
		spectrite_ingot = register("spectrite_ingot", p -> new Item(p));
		positite = register("positite", p -> new Item(p));
		necrum = register("necrum", p -> new Item(p));
		raw_zitrite = register("raw_zitrite", p -> new Item(p));
		zitrite_ingot = register("zitrite_ingot", p -> new Item(p));
		negatite = register("negatite", p -> new Item(p));
		necrotic_extract = register("necrotic_extract", p -> new Item(p));
		powdered_sugar = register("powdered_sugar", p -> new Item(p));

		rainbow_seeds = register("rainbow_seeds", p -> new BlockItem(GNSBlocks.rainbow_berries, p.useItemDescriptionPrefix()));
		rainbow_berries = register("rainbow_berries", p -> new Item(p.food(GNSFoods.RAINBOW_BERRIES)));

		luxurious_soup = register("luxurious_soup", p -> new SleepyStewItem(p.stacksTo(1).food(GNSFoods.SLEEPY_STEW), false));
		wretched_soup = register("wretched_soup", p -> new SleepyStewItem(p.stacksTo(1).food(GNSFoods.SLEEPY_STEW), true));

		candy_sword = register("candy_sword", p -> new SwordItem(GNSToolMaterials.CANDY, 3, -2.4F, (p)));
		candy_pickaxe = register("candy_pickaxe", p -> new PickaxeItem(GNSToolMaterials.CANDY, 1, -2.8F, (p)));
		candy_axe = register("candy_axe", p -> new AxeItem(GNSToolMaterials.CANDY, 6.0F, -3.0F, (p)));
		candy_shovel = register("candy_shovel", p -> new ShovelItem(GNSToolMaterials.CANDY, 1.5F, -3.0F, (p)));
		candy_hoe = register("candy_hoe", p -> new HoeItem(GNSToolMaterials.CANDY, 0, -3.0F, (p)));

		necrum_sword = register("necrum_sword", p -> new SwordItem(GNSToolMaterials.NECRUM, 3, -2.4F, (p)));
		necrum_pickaxe = register("necrum_pickaxe", p -> new PickaxeItem(GNSToolMaterials.NECRUM, 1, -2.8F, (p)));
		necrum_axe = register("necrum_axe", p -> new AxeItem(GNSToolMaterials.NECRUM, 6.0F, -3.0F, (p)));
		necrum_shovel = register("necrum_shovel", p -> new ShovelItem(GNSToolMaterials.NECRUM, 1.5F, -3.0F, (p)));
		necrum_hoe = register("necrum_hoe", p -> new HoeItem(GNSToolMaterials.NECRUM, -1, -2.0F, (p)));

		zitrite_sword = register("zitrite_sword", p -> new SwordItem(GNSToolMaterials.ZITRITE, 3, -2.4F, (p)));
		zitrite_pickaxe = register("zitrite_pickaxe", p -> new PickaxeItem(GNSToolMaterials.ZITRITE, 1, -2.8F, (p)));
		zitrite_axe = register("zitrite_axe", p -> new AxeItem(GNSToolMaterials.ZITRITE, 6.0F, -3.1F, (p)));
		zitrite_shovel = register("zitrite_shovel", p -> new ShovelItem(GNSToolMaterials.ZITRITE, 1.5F, -3.0F, (p)));
		zitrite_hoe = register("zitrite_hoe", p -> new HoeItem(GNSToolMaterials.ZITRITE, -2.0F, -1.0F, (p)));

		spectrite_sword = register("spectrite_sword", p -> new SwordItem(GNSToolMaterials.SPECTRITE, 3, -2.4F, (p)));
		spectrite_pickaxe = register("spectrite_pickaxe", p -> new PickaxeItem(GNSToolMaterials.SPECTRITE, 1, -2.8F, (p)));
		spectrite_axe = register("spectrite_axe", p -> new AxeItem(GNSToolMaterials.SPECTRITE, 6.0F, -3.1F, (p)));
		spectrite_shovel = register("spectrite_shovel", p -> new ShovelItem(GNSToolMaterials.SPECTRITE, 1.5F, -3.0F, (p)));
		spectrite_hoe = register("spectrite_hoe", p -> new HoeItem(GNSToolMaterials.SPECTRITE, -2.0F, -1.0F, (p)));

		positite_sword = register("positite_sword", p -> new SwordItem(GNSToolMaterials.POSITITE, 3, -2.4F, (p)));
		positite_pickaxe = register("positite_pickaxe", p -> new PickaxeItem(GNSToolMaterials.POSITITE, 1, -2.8F, (p)));
		positite_axe = register("positite_axe", p -> new AxeItem(GNSToolMaterials.POSITITE, 5.0F, -3.0F, (p)));
		positite_shovel = register("positite_shovel", p -> new ShovelItem(GNSToolMaterials.POSITITE, 1.5F, -3.0F, (p)));
		positite_hoe = register("positite_hoe", p -> new HoeItem(GNSToolMaterials.POSITITE, -3.0F, 0.0F, (p)));

		negatite_sword = register("negatite_sword", p -> new SwordItem(GNSToolMaterials.NEGATITE, 3.0F, -2.4F, (p)));
		negatite_pickaxe = register("negatite_pickaxe", p -> new PickaxeItem(GNSToolMaterials.NEGATITE, 1.0F, -2.8F, (p)));
		negatite_axe = register("negatite_axe", p -> new AxeItem(GNSToolMaterials.NEGATITE, 5.0F, -3.0F, (p)));
		negatite_shovel = register("negatite_shovel", p -> new ShovelItem(GNSToolMaterials.NEGATITE, 1.5F, -3.0F, (p)));
		negatite_hoe = register("negatite_hoe", p -> new HoeItem(GNSToolMaterials.NEGATITE, -4, 0.0F, (p)));

		ArmorType helmet = ArmorType.HELMET, chestplate = ArmorType.CHESTPLATE, leggings = ArmorType.LEGGINGS,
				boots = ArmorType.BOOTS;
		candy_helmet = register("candy_helmet", p -> new ArmorItem(GNSArmorMaterials.CANDY, helmet, p));
		candy_chestplate = register("candy_chestplate", p -> new ArmorItem(GNSArmorMaterials.CANDY, chestplate, p));
		candy_leggings = register("candy_leggings", p -> new ArmorItem(GNSArmorMaterials.CANDY, leggings, p));
		candy_boots = register("candy_boots", p -> new ArmorItem(GNSArmorMaterials.CANDY, boots, p));

		spectrite_helmet = register("spectrite_helmet", p -> new ArmorItem(GNSArmorMaterials.SPECTRITE, helmet, p));
		spectrite_chestplate = register("spectrite_chestplate", p -> new ArmorItem(GNSArmorMaterials.SPECTRITE, chestplate, p));
		spectrite_leggings = register("spectrite_leggings", p -> new ArmorItem(GNSArmorMaterials.SPECTRITE, leggings, p));
		spectrite_boots = register("spectrite_boots", p -> new ArmorItem(GNSArmorMaterials.SPECTRITE, boots, p));

		positite_helmet = register("positite_helmet", p -> new ArmorItem(GNSArmorMaterials.POSITITE, helmet, p));
		positite_chestplate = register("positite_chestplate", p -> new ArmorItem(GNSArmorMaterials.POSITITE, chestplate, p));
		positite_leggings = register("positite_leggings", p -> new ArmorItem(GNSArmorMaterials.POSITITE, leggings, p));
		positite_boots = register("positite_boots", p -> new ArmorItem(GNSArmorMaterials.POSITITE, boots, p));

		zitrite_helmet = register("zitrite_helmet", p -> new ArmorItem(GNSArmorMaterials.ZITRITE, helmet, p));
		zitrite_chestplate = register("zitrite_chestplate", p -> new ArmorItem(GNSArmorMaterials.ZITRITE, chestplate, p));
		zitrite_leggings = register("zitrite_leggings", p -> new ArmorItem(GNSArmorMaterials.ZITRITE, leggings, p));
		zitrite_boots = register("zitrite_boots", p -> new ArmorItem(GNSArmorMaterials.ZITRITE, boots, p));

		negatite_helmet = register("negatite_helmet", p -> new ArmorItem(GNSArmorMaterials.NEGATITE, helmet, p));
		negatite_chestplate = register("negatite_chestplate", p -> new ArmorItem(GNSArmorMaterials.NEGATITE, chestplate, p));
		negatite_leggings = register("negatite_leggings", p -> new ArmorItem(GNSArmorMaterials.NEGATITE, leggings, p));
		negatite_boots = register("negatite_boots", p -> new ArmorItem(GNSArmorMaterials.NEGATITE, boots, p));

		Supplier<Item.Properties> signProps = () -> new Item.Properties().stacksTo(16).useBlockDescriptionPrefix();

		dream_sign = register("dream_sign", p -> new SignItem(p, GNSBlocks.dream_sign, GNSBlocks.dream_wall_sign, Direction.DOWN), signProps);
		white_sign = register("white_sign", p -> new SignItem(p, GNSBlocks.white_sign, GNSBlocks.white_wall_sign, Direction.DOWN), signProps);
		dead_sign = register("dead_sign", p -> new SignItem(p, GNSBlocks.dead_sign, GNSBlocks.dead_wall_sign, Direction.DOWN), signProps);
		blood_sign = register("blood_sign", p -> new SignItem(p, GNSBlocks.blood_sign, GNSBlocks.blood_wall_sign, Direction.DOWN), signProps);

		dream_hanging_sign = register("dream_hanging_sign", p -> new HangingSignItem(GNSBlocks.dream_hanging_sign, GNSBlocks.dream_wall_hanging_sign, p), signProps);
		white_hanging_sign = register("white_hanging_sign", p -> new HangingSignItem(GNSBlocks.white_hanging_sign, GNSBlocks.white_wall_hanging_sign, p), signProps);
		dead_hanging_sign = register("dead_hanging_sign", p -> new HangingSignItem(GNSBlocks.dead_hanging_sign, GNSBlocks.dead_wall_hanging_sign, p), signProps);
		blood_hanging_sign = register("blood_hanging_sign", p -> new HangingSignItem(GNSBlocks.blood_hanging_sign, GNSBlocks.blood_wall_hanging_sign, p), signProps);

		registerBlockItems();
	}

	private static void registerBlockItems()
	{
		for (Entry<String, Block> block : GNSBlocks.blockItemList.entrySet())
			register(block.getKey(), p -> new BlockItem(block.getValue(), p), () -> new Item.Properties().useBlockDescriptionPrefix());

		GNSBlocks.blockItemList.clear();
	}

	private static Item register(String name, Function<Item.Properties, Item> itemFunc)
	{
		return register(name, itemFunc, null);
	}

	private static Item register(String name, Function<Item.Properties, Item> itemFunc, @Nullable Supplier<Item.Properties> newProps)
	{
		if (newProps == null)
			newProps = () -> new Item.Properties();

		Item.Properties props = newProps.get().setId(key(name));
		Item item = itemFunc.apply(props);

		registryEvent.register(Registries.ITEM, GoodNightSleep.locate(name), () -> item);
		return item;
	}

	private static ResourceKey<Item> key(String path)
	{
		return ResourceKey.create(Registries.ITEM, GoodNightSleep.locate(path));
	}
}