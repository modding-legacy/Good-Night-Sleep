
package com.legacy.good_nights_sleep.registry;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.blocks.GNSBedBlock;
import com.legacy.good_nights_sleep.blocks.GNSOreBlock;
import com.legacy.good_nights_sleep.blocks.PotOfGoldBlock;
import com.legacy.good_nights_sleep.blocks.RainbowBlock;
import com.legacy.good_nights_sleep.blocks.natural.DreamFarmlandBlock;
import com.legacy.good_nights_sleep.blocks.natural.DreamGrassBlock;
import com.legacy.good_nights_sleep.blocks.natural.GNSTallGrassBlock;
import com.legacy.good_nights_sleep.blocks.natural.PricklyGrassBlock;
import com.legacy.good_nights_sleep.blocks.natural.RainbowBerriesBlock;
import com.legacy.good_nights_sleep.blocks.natural.SimpleBushBlock;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FlowerBlock;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.block.MushroomBlock;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.WallBlock;
import net.minecraft.world.level.block.grower.TreeGrower;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

public class GNSBlocks
{
	public static Block dream_grass_block, dream_dirt, dream_farmland, nightmare_grass_block;

	public static Block short_dream_grass, lollipop_bush, short_nightmare_grass, prickly_nightmare_grass;

	public static Block zitrite_ore, candy_ore, spectrite_ore, positite_ore, negatite_ore, fossilized_necrum;

	public static Block raw_spectrite_block, raw_zitrite_block;

	public static Block delusion_coal_ore, delusion_lapis_ore;

	public static Block candy_leaves, dream_leaves, diamond_leaves;

	public static Block hard_candy_block, spectrite_block, positite_block, necrum_block, zitrite_block, negatite_block;

	public static Block dead_planks, blood_planks, white_planks, dream_planks;

	public static Block dream_sapling, candy_sapling;

	public static Block orange_flower, cyan_flower, dead_flower, despair_mushroom, hope_mushroom;

	public static Block hope_mushroom_block, despair_mushroom_block;

	public static Block rainbow_berries;

	public static Block dream_log, white_log, dead_log, blood_log, dream_wood, white_wood, dead_wood, blood_wood;

	public static Block stripped_dream_log, stripped_white_log, stripped_dead_log, stripped_blood_log,
			stripped_dream_wood, stripped_white_wood, stripped_dead_wood, stripped_blood_wood;

	public static Block delusion_stone, delusion_cobblestone, delusion_stone_bricks;

	public static Block dream_fence, white_fence, dead_fence, blood_fence;

	public static Block delusion_cobblestone_wall, delusion_stone_brick_wall;

	public static Block dream_fence_gate, white_fence_gate, dead_fence_gate, blood_fence_gate;

	public static Block dream_button, white_button, dead_button, blood_button, delusion_button;

	public static Block dream_pressure_plate, white_pressure_plate, dead_pressure_plate, blood_pressure_plate,
			delusion_pressure_plate;

	public static Block dream_door, white_door, dead_door, blood_door;

	public static Block dream_trapdoor, white_trapdoor, dead_trapdoor, blood_trapdoor;

	public static Block dream_slab, white_slab, dead_slab, blood_slab;

	public static Block delusion_stone_slab, delusion_cobblestone_slab, delusion_stone_brick_slab;

	public static Block dead_stairs, blood_stairs, white_stairs, dream_stairs;

	public static Block delusion_stone_stairs, delusion_cobblestone_stairs, delusion_stone_brick_stairs;

	public static Block pot_of_gold, present, rainbow;

	public static Block luxurious_bed, wretched_bed, strange_bed;

	public static Block potted_dream_sapling, potted_candy_sapling, potted_hope_mushroom, potted_despair_mushroom,
			potted_orange_flower, potted_cyan_flower, potted_dead_flower, potted_lollipop_bush;

	public static Block potted_dream_grass, potted_nightmare_grass, potted_prickly_nightmare_grass;

	public static Block dream_sign, dream_wall_sign, white_sign, white_wall_sign, blood_sign, blood_wall_sign,
			dead_sign, dead_wall_sign;

	public static Block dream_hanging_sign, dream_wall_hanging_sign, white_hanging_sign, white_wall_hanging_sign,
			blood_hanging_sign, blood_wall_hanging_sign, dead_hanging_sign, dead_wall_hanging_sign;

	private static RegisterEvent registryEvent;

	public static Map<String, Block> blockItemList = new HashMap<>();

	public static final Lazy<TreeGrower> DREAM_GROWER = Lazy.of(() -> new TreeGrower("dream", 0.02F, Optional.empty(), Optional.empty(), Optional.of(GNSFeatures.Configured.BASE_DREAM_TREE.getKey()), Optional.of(GNSFeatures.Configured.BASE_DIAMOND_TREE.getKey()), Optional.empty(), Optional.empty()));
	public static final Lazy<TreeGrower> CANDY_GROWER = Lazy.of(() -> new TreeGrower("candy", 0.3F, Optional.empty(), Optional.empty(), Optional.of(GNSFeatures.Configured.BASE_CANDY_TREE.getKey()), Optional.of(GNSFeatures.Configured.BASE_LARGE_CANDY_TREE.getKey()), Optional.empty(), Optional.empty()));

	public static void init(RegisterEvent event)
	{
		registryEvent = event;

		luxurious_bed = registerBlock("luxurious_bed", GNSBedBlock::new, (copy(Blocks.CYAN_BED)));
		wretched_bed = registerBlock("wretched_bed", GNSBedBlock::new, (copy(Blocks.GRAY_BED)));
		strange_bed = registerBlock("strange_bed", GNSBedBlock::new, (copy(Blocks.RED_BED)));

		short_dream_grass = register("short_dream_grass", GNSTallGrassBlock::new, copy(Blocks.SHORT_GRASS));
		short_nightmare_grass = register("short_nightmare_grass", GNSTallGrassBlock::new, copy(Blocks.SHORT_GRASS));
		prickly_nightmare_grass = register("prickly_nightmare_grass", PricklyGrassBlock::new, copy(Blocks.SHORT_GRASS));
		dream_grass_block = register("dream_grass_block", p -> new DreamGrassBlock(p, () -> dream_dirt, () -> short_dream_grass), copy(Blocks.GRASS_BLOCK));
		dream_dirt = register("dream_dirt", Block::new, copy(Blocks.DIRT));
		dream_farmland = register("dream_farmland", DreamFarmlandBlock::new, copy(Blocks.FARMLAND));
		delusion_stone = register("delusion_stone", Block::new, copy(Blocks.STONE));
		delusion_cobblestone = register("delusion_cobblestone", Block::new, copy(Blocks.COBBLESTONE));
		delusion_stone_bricks = register("delusion_stone_bricks", Block::new, copy(Blocks.STONE_BRICKS));
		nightmare_grass_block = register("nightmare_grass_block", p -> new DreamGrassBlock(p, () -> Blocks.DIRT, () -> short_nightmare_grass), copy(Blocks.GRASS_BLOCK));
		hope_mushroom_block = register("hope_mushroom_block", HugeMushroomBlock::new, copy(Blocks.RED_MUSHROOM_BLOCK));
		despair_mushroom_block = register("despair_mushroom_block", HugeMushroomBlock::new, copy(Blocks.BROWN_MUSHROOM_BLOCK));

		candy_ore = register("candy_ore", p -> new GNSOreBlock(p, 0, 1), copy(Blocks.COAL_ORE));
		spectrite_ore = register("spectrite_ore", p -> new GNSOreBlock(p, 1, 2), copy(Blocks.IRON_ORE));
		positite_ore = register("positite_ore", p -> new GNSOreBlock(p, 3, 7), copy(Blocks.DIAMOND_ORE));
		fossilized_necrum = register("fossilized_necrum", p -> new GNSOreBlock(p, 0, 1), copy(Blocks.COAL_ORE));
		zitrite_ore = register("zitrite_ore", p -> new GNSOreBlock(p, 1, 2), copy(Blocks.IRON_ORE));
		negatite_ore = register("negatite_ore", p -> new GNSOreBlock(p, 3, 7), copy(Blocks.DIAMOND_ORE));

		delusion_coal_ore = register("delusion_coal_ore", p -> new GNSOreBlock(p, 0, 2), copy(Blocks.COAL_ORE));
		delusion_lapis_ore = register("delusion_lapis_ore", p -> new GNSOreBlock(p, 2, 5), copy(Blocks.LAPIS_ORE));

		dream_leaves = BlockReg.leaves("dream_leaves");
		candy_leaves = BlockReg.leaves("candy_leaves");
		diamond_leaves = BlockReg.leaves("diamond_leaves");

		dream_log = BlockReg.log("dream_log");
		white_log = BlockReg.log("white_log");
		dead_log = BlockReg.log("dead_log");
		blood_log = BlockReg.log("blood_log");

		dream_wood = BlockReg.log("dream_wood");
		white_wood = BlockReg.log("white_wood");
		dead_wood = BlockReg.log("dead_wood");
		blood_wood = BlockReg.log("blood_wood");

		stripped_dream_log = BlockReg.log("stripped_dream_log");
		stripped_white_log = BlockReg.log("stripped_white_log");
		stripped_dead_log = BlockReg.log("stripped_dead_log");
		stripped_blood_log = BlockReg.log("stripped_blood_log");

		stripped_dream_wood = BlockReg.log("stripped_dream_wood");
		stripped_white_wood = BlockReg.log("stripped_white_wood");
		stripped_dead_wood = BlockReg.log("stripped_dead_wood");
		stripped_blood_wood = BlockReg.log("stripped_blood_wood");

		dream_planks = BlockReg.planks("dream_planks");
		white_planks = BlockReg.planks("white_planks");
		dead_planks = BlockReg.planks("dead_planks");
		blood_planks = BlockReg.planks("blood_planks");

		dream_sign = BlockReg.sign("dream_sign", GNSWoodTypes.DREAM);
		white_sign = BlockReg.sign("white_sign", GNSWoodTypes.WHITE);
		dead_sign = BlockReg.sign("dead_sign", GNSWoodTypes.DEAD);
		blood_sign = BlockReg.sign("blood_sign", GNSWoodTypes.BLOOD);

		dream_wall_sign = BlockReg.wallSign("dream_wall_sign", GNSWoodTypes.DREAM);
		white_wall_sign = BlockReg.wallSign("white_wall_sign", GNSWoodTypes.WHITE);
		dead_wall_sign = BlockReg.wallSign("dead_wall_sign", GNSWoodTypes.DEAD);
		blood_wall_sign = BlockReg.wallSign("blood_wall_sign", GNSWoodTypes.BLOOD);

		dream_hanging_sign = BlockReg.hangingSign("dream_hanging_sign", GNSWoodTypes.DREAM);
		white_hanging_sign = BlockReg.hangingSign("white_hanging_sign", GNSWoodTypes.WHITE);
		dead_hanging_sign = BlockReg.hangingSign("dead_hanging_sign", GNSWoodTypes.DEAD);
		blood_hanging_sign = BlockReg.hangingSign("blood_hanging_sign", GNSWoodTypes.BLOOD);

		dream_wall_hanging_sign = BlockReg.wallHangingSign("dream_wall_hanging_sign", GNSWoodTypes.DREAM);
		white_wall_hanging_sign = BlockReg.wallHangingSign("white_wall_hanging_sign", GNSWoodTypes.WHITE);
		dead_wall_hanging_sign = BlockReg.wallHangingSign("dead_wall_hanging_sign", GNSWoodTypes.DEAD);
		blood_wall_hanging_sign = BlockReg.wallHangingSign("blood_wall_hanging_sign", GNSWoodTypes.BLOOD);

		dream_sapling = register("dream_sapling", p -> new SaplingBlock(DREAM_GROWER.get(), p), copy(Blocks.OAK_SAPLING));
		candy_sapling = register("candy_sapling", p -> new SaplingBlock(CANDY_GROWER.get(), p), copy(Blocks.OAK_SAPLING));

		hope_mushroom = register("hope_mushroom", p -> new MushroomBlock(GNSFeatures.Configured.BASE_HUGE_HOPE_MUSHROOM.getKey(), p), () -> rawCopy(Blocks.RED_MUSHROOM).sound(SoundType.FUNGUS));
		despair_mushroom = register("despair_mushroom", p -> new MushroomBlock(GNSFeatures.Configured.BASE_HUGE_DESPAIR_MUSHROOM.getKey(), p), () -> rawCopy(Blocks.BROWN_MUSHROOM).sound(SoundType.FUNGUS));
		orange_flower = register("orange_flower", p -> new FlowerBlock(MobEffects.ABSORPTION, 10, p), copy(Blocks.POPPY));
		cyan_flower = register("cyan_flower", p -> new FlowerBlock(MobEffects.LUCK, 20, p), copy(Blocks.POPPY));
		lollipop_bush = register("lollipop_bush", SimpleBushBlock::new, copy(Blocks.POPPY));
		dead_flower = register("dead_flower", p -> new FlowerBlock(MobEffects.WEAKNESS, 5, p), copy(Blocks.POPPY));

		rainbow_berries = registerBlock("rainbow_berries", RainbowBerriesBlock::new, copy(Blocks.BEETROOTS));

		dream_door = BlockReg.door("dream_door", GNSBlockSets.DREAM);
		white_door = BlockReg.door("white_door", GNSBlockSets.WHITE);
		dead_door = BlockReg.door("dead_door", GNSBlockSets.DEAD);
		blood_door = BlockReg.door("blood_door", GNSBlockSets.BLOOD);

		dream_trapdoor = BlockReg.trapdoor("dream_trapdoor", GNSBlockSets.DREAM);
		white_trapdoor = BlockReg.trapdoor("white_trapdoor", GNSBlockSets.WHITE);
		dead_trapdoor = BlockReg.trapdoor("dead_trapdoor", GNSBlockSets.DEAD);
		blood_trapdoor = BlockReg.trapdoor("blood_trapdoor", GNSBlockSets.BLOOD);

		dream_fence = BlockReg.fence("dream_fence");
		white_fence = BlockReg.fence("white_fence");
		dead_fence = BlockReg.fence("dead_fence");
		blood_fence = BlockReg.fence("blood_fence");

		delusion_cobblestone_wall = register("delusion_cobblestone_wall", WallBlock::new, copy(Blocks.COBBLESTONE_WALL));
		delusion_stone_brick_wall = register("delusion_stone_brick_wall", WallBlock::new, copy(Blocks.STONE_BRICK_WALL));

		dream_fence_gate = BlockReg.gate("dream_fence_gate", GNSWoodTypes.DREAM);
		white_fence_gate = BlockReg.gate("white_fence_gate", GNSWoodTypes.WHITE);
		dead_fence_gate = BlockReg.gate("dead_fence_gate", GNSWoodTypes.DEAD);
		blood_fence_gate = BlockReg.gate("blood_fence_gate", GNSWoodTypes.BLOOD);

		dream_button = BlockReg.woodenButton("dream_button", GNSBlockSets.DREAM);
		white_button = BlockReg.woodenButton("white_button", GNSBlockSets.WHITE);
		dead_button = BlockReg.woodenButton("dead_button", GNSBlockSets.DEAD);
		blood_button = BlockReg.woodenButton("blood_button", GNSBlockSets.BLOOD);

		delusion_button = BlockReg.stoneButton("delusion_button", GNSBlockSets.DELUSION_STONE);

		dream_pressure_plate = BlockReg.woodenPressurePlate("dream_pressure_plate", GNSBlockSets.DREAM);
		white_pressure_plate = BlockReg.woodenPressurePlate("white_pressure_plate", GNSBlockSets.WHITE);
		dead_pressure_plate = BlockReg.woodenPressurePlate("dead_pressure_plate", GNSBlockSets.DEAD);
		blood_pressure_plate = BlockReg.woodenPressurePlate("blood_pressure_plate", GNSBlockSets.BLOOD);

		delusion_pressure_plate = BlockReg.stonePressurePlate("delusion_pressure_plate", GNSBlockSets.DELUSION_STONE);

		dream_stairs = BlockReg.stairs("dream_stairs", GNSBlocks.dream_planks);
		white_stairs = BlockReg.stairs("white_stairs", GNSBlocks.white_planks);
		dead_stairs = BlockReg.stairs("dead_stairs", GNSBlocks.dead_planks);
		blood_stairs = BlockReg.stairs("blood_stairs", GNSBlocks.blood_planks);

		delusion_stone_stairs = BlockReg.stairs("delusion_stone_stairs", GNSBlocks.delusion_stone);
		delusion_cobblestone_stairs = BlockReg.stairs("delusion_cobblestone_stairs", GNSBlocks.delusion_cobblestone);
		delusion_stone_brick_stairs = BlockReg.stairs("delusion_stone_brick_stairs", GNSBlocks.delusion_stone_bricks);

		dream_slab = BlockReg.slab("dream_slab", GNSBlocks.dream_planks);
		white_slab = BlockReg.slab("white_slab", GNSBlocks.white_planks);
		dead_slab = BlockReg.slab("dead_slab", GNSBlocks.dead_planks);
		blood_slab = BlockReg.slab("blood_slab", GNSBlocks.blood_planks);

		delusion_stone_slab = BlockReg.slab("delusion_stone_slab", GNSBlocks.delusion_stone);
		delusion_cobblestone_slab = BlockReg.slab("delusion_cobblestone_slab", GNSBlocks.delusion_cobblestone);
		delusion_stone_brick_slab = BlockReg.slab("delusion_stone_brick_slab", GNSBlocks.delusion_stone_bricks);

		raw_spectrite_block = register("raw_spectrite_block", Block::new, () -> rawCopy(Blocks.RAW_IRON_BLOCK).lightLevel((state) -> 6));
		raw_zitrite_block = register("raw_zitrite_block", Block::new, (copy(Blocks.RAW_IRON_BLOCK)));

		hard_candy_block = register("hard_candy_block", Block::new, copy(Blocks.COAL_BLOCK));
		spectrite_block = register("spectrite_block", Block::new, () -> rawCopy(Blocks.IRON_BLOCK).lightLevel((state) -> 15));
		positite_block = register("positite_block", Block::new, copy(Blocks.DIAMOND_BLOCK));
		necrum_block = register("necrum_block", Block::new, () -> rawCopy(Blocks.COARSE_DIRT).sound(SoundType.GRAVEL));
		zitrite_block = register("zitrite_block", Block::new, copy(Blocks.IRON_BLOCK));
		negatite_block = register("negatite_block", Block::new, copy(Blocks.DIAMOND_BLOCK));

		present = register("present", Block::new, copy(Blocks.RED_WOOL));

		pot_of_gold = register("pot_of_gold", PotOfGoldBlock::new, copy(Blocks.CAULDRON));
		rainbow = registerBlock("rainbow", RainbowBlock::new, () -> rawCopy(Blocks.NETHER_PORTAL).sound(SoundType.AMETHYST).emissiveRendering((state, level, pos) -> true));

		potted_dream_sapling = BlockReg.flowerPot("potted_dream_sapling", () -> dream_sapling);
		potted_candy_sapling = BlockReg.flowerPot("potted_candy_sapling", () -> candy_sapling);
		potted_hope_mushroom = BlockReg.flowerPot("potted_hope_mushroom", () -> hope_mushroom);
		potted_despair_mushroom = BlockReg.flowerPot("potted_despair_mushroom", () -> despair_mushroom);
		potted_orange_flower = BlockReg.flowerPot("potted_orange_flower", () -> orange_flower);
		potted_cyan_flower = BlockReg.flowerPot("potted_cyan_flower", () -> cyan_flower);
		potted_dead_flower = BlockReg.flowerPot("potted_dead_flower", () -> dead_flower);
		potted_lollipop_bush = BlockReg.flowerPot("potted_lollipop_bush", () -> lollipop_bush);
		potted_prickly_nightmare_grass = BlockReg.flowerPot("potted_prickly_nightmare_grass", () -> prickly_nightmare_grass);

		/*if (ModList.get().isLoaded("quark"))
		{
			potted_dream_grass = registerBlock("potted_dream_grass", () -> dream_grass));
			potted_nightmare_grass = registerBlock("potted_nightmare_grass", () -> nightmare_grass));
		}*/
	}

	private static Supplier<BlockBehaviour.Properties> copy(Block from)
	{
		return () -> rawCopy(from);
	}

	private static BlockBehaviour.Properties rawCopy(Block from)
	{
		return BlockBehaviour.Properties.ofFullCopy(from);
	}

	public static Block register(String key, Function<BlockBehaviour.Properties, Block> blockFunc, Supplier<BlockBehaviour.Properties> newProps)
	{
		Block block = registerBlock(key, blockFunc, newProps);
		blockItemList.put(key, block);
		return block;
	}

	public static Block registerBlock(String name, Function<BlockBehaviour.Properties, Block> blockFunc, Supplier<BlockBehaviour.Properties> newProps)
	{
		if (registryEvent != null)
		{
			BlockBehaviour.Properties props = newProps.get().setId(key(name));
			Block block = blockFunc.apply(props);

			// System.out.println("registering " + name);
			registryEvent.register(Registries.BLOCK, GoodNightSleep.locate(name), () -> block);

			return block;
		}

		return Blocks.STONE;
	}

	private static ResourceKey<Block> key(String path)
	{
		return ResourceKey.create(Registries.BLOCK, GoodNightSleep.locate(path));
	}
}