package com.legacy.good_nights_sleep.registry;

import java.util.List;
import java.util.OptionalLong;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.mixin.NoiseRouterDataInvoker;
import com.legacy.good_nights_sleep.util.DimensionHolder;
import com.legacy.good_nights_sleep.world.GNSSurfaceRuleData;
import com.legacy.good_nights_sleep.world.chunkgen.DreamChunkGenerator;
import com.legacy.good_nights_sleep.world.chunkgen.NightmareChunkGenerator;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.valueproviders.IntProvider;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.dimension.LevelStem;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.NoiseRouter;
import net.minecraft.world.level.levelgen.NoiseSettings;
import net.minecraft.world.level.levelgen.SurfaceRules;

public class GNSDimensions
{
	public static final ResourceLocation DREAM_ID = GoodNightSleep.locate("dream");
	public static final ResourceLocation NIGHTMARE_ID = GoodNightSleep.locate("nightmare");

	public static final DimensionHolder DREAM = createDream();
	public static final DimensionHolder NIGHTMARE = createNightmare();

	public static void init()
	{
	}

	private static DimensionHolder createDream()
	{
		Function<BootstrapContext<?>, DimensionType> dimType = c -> dimType(OptionalLong.of(6000L), 0, DREAM_ID, false, 0);
		Function<BootstrapContext<?>, NoiseGeneratorSettings> noiseSettings = c -> newDimensionSettings(dummyNoise(), GNSBlocks.delusion_stone.defaultBlockState(), Blocks.WATER.defaultBlockState(), NoiseRouterDataInvoker.overworld(c.lookup(Registries.DENSITY_FUNCTION), c.lookup(Registries.NOISE), false, false), GNSSurfaceRuleData.basicData(false, true, false), 0, false);
		Function<BootstrapContext<?>, ChunkGenerator> chunkGen = c -> new DreamChunkGenerator(c.lookup(Registries.BIOME), c.lookup(Registries.NOISE_SETTINGS));

		return new DimensionHolder(DREAM_ID, dimType, noiseSettings, chunkGen);
	}

	private static DimensionHolder createNightmare()
	{
		Function<BootstrapContext<?>, DimensionType> dimType = c -> dimType(OptionalLong.of(18000L), 0, NIGHTMARE_ID, false, 7);
		Function<BootstrapContext<?>, NoiseGeneratorSettings> noiseSettings = c -> newDimensionSettings(dummyNoise(), Blocks.STONE.defaultBlockState(), Blocks.LAVA.defaultBlockState(), NoiseRouterDataInvoker.overworld(c.lookup(Registries.DENSITY_FUNCTION), c.lookup(Registries.NOISE), false, false), GNSSurfaceRuleData.basicData(false, true, true), 0, false);
		Function<BootstrapContext<?>, ChunkGenerator> chunkGen = c -> new NightmareChunkGenerator(c.lookup(Registries.BIOME), c.lookup(Registries.NOISE_SETTINGS));

		return new DimensionHolder(NIGHTMARE_ID, dimType, noiseSettings, chunkGen);
	}

	private static NoiseSettings dummyNoise()
	{
		return NoiseSettings.create(0, 256, 1, 2);
	}

	public static NoiseGeneratorSettings newDimensionSettings(NoiseSettings noise, BlockState defaultBlock, BlockState defaultFluid, NoiseRouter noiseRouter, SurfaceRules.RuleSource surfaceRule, int seaLevel, boolean useLegacyRandomSource)
	{
		return new NoiseGeneratorSettings(noise, defaultBlock, defaultFluid, noiseRouter, surfaceRule, List.of(), seaLevel, false, false, false, useLegacyRandomSource);
	}

	public static ResourceKey<Level> dreamKey()
	{
		return DREAM.getLevelKey();
	}

	public static ResourceKey<Level> nightmareKey()
	{
		return NIGHTMARE.getLevelKey();
	}

	public static boolean inSleepDimension(@Nullable Entity entity)
	{
		return entity != null && inSleepDimension(entity.level().dimension());
	}

	public static boolean inSleepDimension(ResourceKey<Level> levelKey)
	{
		return levelKey.equals(dreamKey()) || levelKey.equals(nightmareKey());
	}

	public static boolean hasDreamEffects(ResourceLocation effects)
	{
		return effects.equals(DREAM_ID) || effects.equals(NIGHTMARE_ID);
	}

	public static DimensionType dimType(OptionalLong fixedTime, int minY, ResourceLocation effects, boolean bedWorks, int monsterSpawnBlockLightLimit)
	{
		boolean hasSkyLight = true;
		boolean hasCeiling = false;
		boolean ultrawarm = false;
		boolean natural = true;
		double coordinateScale = 1.0;
		boolean respawnAnchorWorks = false;
		int height = 384;
		int logicalHeight = 384;
		TagKey<Block> infiniburn = BlockTags.INFINIBURN_OVERWORLD; // TODO
		float ambientLight = 0.0F;

		boolean piglinSafe = false;
		boolean hasRaids = true;
		IntProvider monsterSpawnLightTest = UniformInt.of(0, 7);

		return new DimensionType(fixedTime, hasSkyLight, hasCeiling, ultrawarm, natural, coordinateScale, bedWorks, respawnAnchorWorks, minY, height, logicalHeight, infiniburn, effects, ambientLight, new DimensionType.MonsterSettings(piglinSafe, hasRaids, monsterSpawnLightTest, monsterSpawnBlockLightLimit));
	}

	public static void bootstrapDimType(BootstrapContext<DimensionType> bootstrap)
	{
		bootstrap.register(DREAM.getType().getKey(), DREAM.getType().getInstance().apply(bootstrap));
		bootstrap.register(NIGHTMARE.getType().getKey(), NIGHTMARE.getType().getInstance().apply(bootstrap));
	}

	public static void bootstrapNoiseSettings(BootstrapContext<NoiseGeneratorSettings> bootstrap)
	{
		bootstrap.register(DREAM.getNoiseSettings().getKey(), DREAM.getNoiseSettings().getInstance().apply(bootstrap));
		bootstrap.register(NIGHTMARE.getNoiseSettings().getKey(), NIGHTMARE.getNoiseSettings().getInstance().apply(bootstrap));
	}

	public static void bootstrapLevelStem(BootstrapContext<LevelStem> bootstrap)
	{
		bootstrap.register(DREAM.getLevelStem().getKey(), DREAM.getLevelStem().getInstance().apply(bootstrap));
		bootstrap.register(NIGHTMARE.getLevelStem().getKey(), NIGHTMARE.getLevelStem().getInstance().apply(bootstrap));
	}
}