package com.legacy.good_nights_sleep.registry;

import com.legacy.good_nights_sleep.GoodNightSleep;

import net.minecraft.core.Holder;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;

public class GNSSounds
{
	// Music
	public static final SoundEvent MUSIC_GOOD_DREAM = create("music.good_dream");
	public static final SoundEvent MUSIC_SKY_BLUE = create("music.sky_blue");

	public static final SoundEvent MUSIC_NIGHTMARE = create("music.tfarcenim");

	// Blocks
	public static final SoundEvent BLOCK_POT_OF_GOLD_USE = create("block.pot_of_gold.use");
	/*public static final SoundEvent BLOCK_RAINBOW_PLACE = create("block.rainbow.place");
	public static final SoundEvent BLOCK_RAINBOW_BREAK = create("block.rainbow.break");*/
	public static final SoundEvent BLOCK_RAINBOW_APPEAR = create("block.rainbow.appear");

	// Mobs
	public static final SoundEvent ENTITY_UNICORN_IDLE = create("entity.unicorn.idle");
	public static final SoundEvent ENTITY_UNICORN_HURT = create("entity.unicorn.hurt");
	public static final SoundEvent ENTITY_UNICORN_DEATH = create("entity.unicorn.death");
	public static final SoundEvent ENTITY_UNICORN_ANGRY = create("entity.unicorn.angry");
	public static final SoundEvent ENTITY_UNICORN_EAT = create("entity.unicorn.eat");
	public static final SoundEvent ENTITY_UNICORN_BREATHE = create("entity.unicorn.breathe");

	public static final SoundEvent ENTITY_BABY_CREEPER_HURT = create("entity.baby_creeper.hurt");
	public static final SoundEvent ENTITY_BABY_CREEPER_DEATH = create("entity.baby_creeper.death");

	public static final SoundEvent ENTITY_TORMENTER_IDLE = create("entity.tormenter.idle");
	public static final SoundEvent ENTITY_TORMENTER_HURT = create("entity.tormenter.hurt");
	public static final SoundEvent ENTITY_TORMENTER_DEATH = create("entity.tormenter.death");
	public static final SoundEvent ENTITY_TORMENTER_TORMENT = create("entity.tormenter.torment");

	public static final SoundEvent ENTITY_HEROBRINE_HURT = create("entity.herobrine.hurt");
	public static final SoundEvent ENTITY_HEROBRINE_DEATH = create("entity.herobrine.death");
	public static final SoundEvent ENTITY_HEROBRINE_TELEPORT = create("entity.herobrine.teleport");

	public static final Holder<SoundEvent> ITEM_ARMOR_EQUIP_CANDY = createForHolder("item.armor.equip_candy");
	public static final Holder<SoundEvent> ITEM_ARMOR_EQUIP_SPECTRITE = createForHolder("item.armor.equip_spectrite");
	public static final Holder<SoundEvent> ITEM_ARMOR_EQUIP_POSITITE = createForHolder("item.armor.equip_positite");
	public static final Holder<SoundEvent> ITEM_ARMOR_EQUIP_ZITRITE = createForHolder("item.armor.equip_zitrite");
	public static final Holder<SoundEvent> ITEM_ARMOR_EQUIP_NEGATITE = createForHolder("item.armor.equip_negatite");

	public static void init()
	{
	}

	private static SoundEvent create(String name)
	{
		ResourceLocation location = GoodNightSleep.locate(name);
		return Registry.register(BuiltInRegistries.SOUND_EVENT, location, SoundEvent.createVariableRangeEvent(location));
	}

	private static Holder.Reference<SoundEvent> createForHolder(String name)
	{
		ResourceLocation location = GoodNightSleep.locate(name);
		return Registry.registerForHolder(BuiltInRegistries.SOUND_EVENT, location, SoundEvent.createVariableRangeEvent(location));
	}

}