package com.legacy.good_nights_sleep.registry;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.entity.HerobrineEntity;
import com.legacy.good_nights_sleep.entity.TormenterEntity;
import com.legacy.good_nights_sleep.entity.dream.BabyCreeperEntity;
import com.legacy.good_nights_sleep.entity.dream.UnicornEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.entity.SpawnPlacementTypes;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.level.levelgen.Heightmap;
import net.neoforged.neoforge.event.entity.EntityAttributeCreationEvent;
import net.neoforged.neoforge.event.entity.RegisterSpawnPlacementsEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

public class GNSEntityTypes
{
	public static final EntityType<UnicornEntity> UNICORN = buildEntity("unicorn", EntityType.Builder.<UnicornEntity>of(UnicornEntity::new, MobCategory.CREATURE).sized(1.3964844F, 1.6F));
	/*public static final EntityType<GummyBearEntity> GUMMY_BEAR = buildEntity("gummy_bear", EntityType.Builder.<GummyBearEntity>of(GummyBearEntity::new, MobCategory.CREATURE).sized(0.6F, 0.7F).noSummon());*/
	public static final EntityType<BabyCreeperEntity> BABY_CREEPER = buildEntity("baby_creeper", EntityType.Builder.<BabyCreeperEntity>of(BabyCreeperEntity::new, MobCategory.MONSTER).sized(0.6F, 1.3F));
	public static final EntityType<TormenterEntity> TORMENTER = buildEntity("tormenter", EntityType.Builder.of(TormenterEntity::new, MobCategory.MONSTER));
	public static final EntityType<HerobrineEntity> HEROBRINE = buildEntity("herobrine", EntityType.Builder.of(HerobrineEntity::new, MobCategory.MONSTER).eyeHeight(1.75F));

	private static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		register("unicorn", UNICORN);
		/*register("gummy_bear", GNSEntityTypes.GUMMY_BEAR);*/
		register("baby_creeper", BABY_CREEPER);
		register("tormenter", TORMENTER);
		register("herobrine", HEROBRINE);
	}

	private static void register(String name, EntityType<?> type)
	{
		if (registerEvent == null)
			return;

		registerEvent.register(Registries.ENTITY_TYPE, GoodNightSleep.locate(name), () -> type);
	}

	public static void onAttributesRegistered(EntityAttributeCreationEvent event)
	{
		event.put(UNICORN, UnicornEntity.createBaseHorseAttributes().build());
		/*event.put(GUMMY_BEAR, Mob.createMobAttributes().build());*/
		event.put(BABY_CREEPER, Creeper.createAttributes().build());
		event.put(TORMENTER, TormenterEntity.registerAttributes().build());
		event.put(HEROBRINE, HerobrineEntity.registerAttributes().build());
	}

	private static <T extends Entity> EntityType<T> buildEntity(String key, EntityType.Builder<T> builder)
	{
		return builder.build(ResourceKey.create(Registries.ENTITY_TYPE, GoodNightSleep.locate(key)));
	}

	public static void registerPlacements(RegisterSpawnPlacementsEvent event)
	{
		event.register(TORMENTER, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Monster::checkMonsterSpawnRules, RegisterSpawnPlacementsEvent.Operation.REPLACE);
		event.register(HEROBRINE, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Monster::checkMonsterSpawnRules, RegisterSpawnPlacementsEvent.Operation.REPLACE);
		event.register(BABY_CREEPER, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Monster::checkMonsterSpawnRules, RegisterSpawnPlacementsEvent.Operation.REPLACE);

		event.register(UNICORN, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Animal::checkAnimalSpawnRules, RegisterSpawnPlacementsEvent.Operation.REPLACE);
		/*event.register(GUMMY_BEAR, SpawnPlacementTypes.ON_GROUND, Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Animal::checkAnimalSpawnRules, RegisterSpawnPlacementsEvent.Operation.REPLACE);*/
	}

	public static void registerPlacementOverrides(RegisterSpawnPlacementsEvent event)
	{
		/*event.register(EntityType.ZOMBIE_HORSE, null, null, (type, level, reason, pos, rand) ->
		{
			if (level.getLevel() != null && level.getLevel().dimension().equals(GNSDimensions.NIGHTMARE_DIM) && (reason == MobSpawnType.NATURAL || reason == MobSpawnType.CHUNK_GENERATION))
			{
				return level.getBlockState(pos.below()).is(BlockTags.ANIMALS_SPAWNABLE_ON) && level.getBrightness(LightLayer.SKY, pos) > 8;
			}
		
			return true;
		}, Operation.AND);*/
	}
}