package com.legacy.good_nights_sleep.registry;

import com.legacy.good_nights_sleep.GoodNightSleep;

import net.minecraft.world.level.block.state.properties.BlockSetType;
import net.minecraft.world.level.block.state.properties.WoodType;

public class GNSWoodTypes
{
	public static final WoodType DREAM = register("dream", GNSBlockSets.DREAM);
	public static final WoodType WHITE = register("white", GNSBlockSets.WHITE);
	public static final WoodType DEAD = register("dead", GNSBlockSets.DEAD);
	public static final WoodType BLOOD = register("blood", GNSBlockSets.BLOOD);

	public static WoodType register(String key, BlockSetType blockSet)
	{	
		return WoodType.register(new WoodType(GoodNightSleep.find(key), blockSet));
	}

	public static void init()
	{
	}
}