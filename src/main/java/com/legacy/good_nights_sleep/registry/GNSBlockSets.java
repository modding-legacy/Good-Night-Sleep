package com.legacy.good_nights_sleep.registry;

import com.legacy.good_nights_sleep.GoodNightSleep;

import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.level.block.state.properties.BlockSetType;

public class GNSBlockSets
{
	public static final BlockSetType DREAM = register("dream");
	public static final BlockSetType WHITE = register("white");
	public static final BlockSetType DEAD = register("dead");
	public static final BlockSetType BLOOD = register("blood");

	public static final BlockSetType DELUSION_STONE = register(new BlockSetType(GoodNightSleep.find("delusion_stone"), true, true, false, BlockSetType.PressurePlateSensitivity.MOBS, SoundType.STONE, SoundEvents.IRON_DOOR_CLOSE, SoundEvents.IRON_DOOR_OPEN, SoundEvents.IRON_TRAPDOOR_CLOSE, SoundEvents.IRON_TRAPDOOR_OPEN, SoundEvents.STONE_PRESSURE_PLATE_CLICK_OFF, SoundEvents.STONE_PRESSURE_PLATE_CLICK_ON, SoundEvents.STONE_BUTTON_CLICK_OFF, SoundEvents.STONE_BUTTON_CLICK_ON));

	public static BlockSetType register(String key)
	{
		return register(new BlockSetType(GoodNightSleep.find(key)));
	}

	public static BlockSetType register(BlockSetType type)
	{
		return BlockSetType.register(type);
	}

	public static void init()
	{
	}
}
