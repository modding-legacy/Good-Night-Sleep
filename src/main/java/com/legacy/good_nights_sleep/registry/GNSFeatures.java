package com.legacy.good_nights_sleep.registry;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.OptionalInt;
import java.util.function.Function;
import java.util.function.Supplier;

import org.apache.commons.lang3.tuple.Pair;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.data.GNSBiomeProv;
import com.legacy.good_nights_sleep.util.Pointer;
import com.legacy.good_nights_sleep.world.dream.features.DreamSpongeFeature;
import com.legacy.good_nights_sleep.world.dream.features.FatHopeMushroomFeature;
import com.legacy.good_nights_sleep.world.dream.features.TallHopeMushroomFeature;
import com.legacy.good_nights_sleep.world.general_features.DreamScatteredPlantFeature;
import com.legacy.good_nights_sleep.world.nightmare.features.NetherSplashFeature;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.data.worldgen.Carvers;
import net.minecraft.data.worldgen.features.FeatureUtils;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.data.worldgen.placement.VegetationPlacements;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.random.SimpleWeightedRandomList;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.HugeMushroomBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.VerticalAnchor;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.FeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.HugeMushroomFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.OreConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.RandomPatchConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.SimpleBlockConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.levelgen.feature.featuresize.TwoLayersFeatureSize;
import net.minecraft.world.level.levelgen.feature.foliageplacers.BlobFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.FancyFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.stateproviders.WeightedStateProvider;
import net.minecraft.world.level.levelgen.feature.trunkplacers.FancyTrunkPlacer;
import net.minecraft.world.level.levelgen.feature.trunkplacers.StraightTrunkPlacer;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.CountPlacement;
import net.minecraft.world.level.levelgen.placement.HeightRangePlacement;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.NoiseThresholdCountPlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraft.world.level.levelgen.placement.PlacementModifier;
import net.minecraft.world.level.levelgen.placement.RarityFilter;
import net.minecraft.world.level.levelgen.structure.templatesystem.BlockMatchTest;
import net.minecraft.world.level.levelgen.structure.templatesystem.TagMatchTest;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.data.loading.DatagenModLoader;
import net.neoforged.neoforge.registries.RegisterEvent;

public class GNSFeatures
{
	public static final Feature<HugeMushroomFeatureConfiguration> TALL_HOPE_MUSHROOM = new TallHopeMushroomFeature(HugeMushroomFeatureConfiguration.CODEC);
	public static final Feature<HugeMushroomFeatureConfiguration> FAT_HOPE_MUSHROOM = new FatHopeMushroomFeature(HugeMushroomFeatureConfiguration.CODEC);

	public static final Feature<NoneFeatureConfiguration> SPONGE_GROWTH = new DreamSpongeFeature(NoneFeatureConfiguration.CODEC);
	public static final Feature<NoneFeatureConfiguration> SCATTERED_PRESENTS = new DreamScatteredPlantFeature(NoneFeatureConfiguration.CODEC, Lazy.of(() -> GNSBlocks.present.defaultBlockState()));

	public static final Feature<NoneFeatureConfiguration> NETHER_SPLASH = new NetherSplashFeature(NoneFeatureConfiguration.CODEC);
	public static final Feature<NoneFeatureConfiguration> SCATTERED_PUMPKINS = new DreamScatteredPlantFeature(NoneFeatureConfiguration.CODEC, Lazy.of(() -> Blocks.PUMPKIN.defaultBlockState()));

	public static RegisterEvent registerEvent;

	public static void init(RegisterEvent event)
	{
		registerEvent = event;

		/*register("tall_hope_mushroom", TALL_HOPE_MUSHROOM);
		register("fat_hope_mushroom", FAT_HOPE_MUSHROOM);*/
		register("sponge_growth", SPONGE_GROWTH);
		register("scattered_presents", SCATTERED_PRESENTS);

		register("nether_splash", NETHER_SPLASH);
		register("scattered_pumpkins", SCATTERED_PUMPKINS);

		Configured.init();
	}

	private static void register(String key, Feature<?> feature)
	{
		registerEvent.register(Registries.FEATURE, GoodNightSleep.locate(key), () -> feature);
	}

	public static void addDreamOres(GNSBiomeProv.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.DREAM_DIRT_BLOB);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.DREAM_COAL_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.DREAM_CANDY_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.DREAM_SPECTRITE_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.DREAM_POSITITE_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.DREAM_LAPIS_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.DREAM_GLOWSTONE_BLOB);
	}

	public static void addDreamTrees(GNSBiomeProv.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.HILLS_DREAM_TREE);
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.HILLS_CANDY_TREE);
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.HILLS_DIAMOND_TREE);
	}

	public static void addScatteredDreamFeatures(GNSBiomeProv.RegistrarBuilder biomeIn)
	{
		addDreamSponges(biomeIn);
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.SCATTERED_PRESENTS);
	}

	public static void addDreamSponges(GNSBiomeProv.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.RAW_GENERATION, Placements.SPONGE_GROWTH);
	}

	public static void addHugeHopeMushrooms(GNSBiomeProv.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.HUGE_HOPE_MUSHROOM);
	}

	/**
	 * Unused, was originally planned for the Hopeful Fields biome.
	 */
	public static void addHopeMushroomFields(GNSBiomeProv.RegistrarBuilder biomeIn)
	{
		/*biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.FIELDS_HOPE_MUSHROOM);
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.FIELDS_LARGER_HOPE_MUSHROOM);
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.FIELDS_VERY_LARGE_HOPE_MUSHROOM);*/
	}

	public static void addNightmareOres(GNSBiomeProv.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.NIGHTMARE_DIRT_BLOB);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.NIGHTMARE_GRAVEL_BLOB);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.NIGHTMARE_COAL_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.NIGHTMARE_NECRUM_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.NIGHTMARE_ZITRITE_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.NIGHTMARE_IRON_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.NIGHTMARE_NEGATITE_ORE);
		biomeIn.addFeature(GenerationStep.Decoration.UNDERGROUND_ORES, Placements.NIGHTMARE_LAPIS_ORE);
	}

	public static void addScatteredNightmareFeatures(GNSBiomeProv.RegistrarBuilder biomeIn)
	{
		biomeIn.addFeature(GenerationStep.Decoration.RAW_GENERATION, Placements.NETHER_SPLASH);
		biomeIn.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.SCATTERED_PUMPKINS);
	}

	public static void addCarvers(GNSBiomeProv.RegistrarBuilder biomeIn)
	{
		biomeIn.addCarver(Carvers.CAVE);
		biomeIn.addCarver(Carvers.CAVE_EXTRA_UNDERGROUND);
		biomeIn.addCarver(Carvers.CANYON);

		/*BiomeDefaultFeatures.addDefaultCrystalFormations(p_194870_);
		BiomeDefaultFeatures.addDefaultMonsterRoom(p_194870_);
		BiomeDefaultFeatures.addDefaultUndergroundVariety(p_194870_);*/
		// BiomeDefaultFeatures.addDefaultSprings(biomeIn);

		/*biomeIn.addCarver(Placements.DELUSION_CAVE_CARVER);
		biomeIn.addCarver(Placements.DELUSION_CANYON_CARVER);*/
	}

	// FIXME: add biome modifiers
	/*public static void addMushrooms(BiomeLoadingEvent eventIn)
	{
		if (eventIn.getName().toString().contains(GoodNightSleep.MODID) || eventIn.getCategory() == Biome.BiomeCategory.THEEND || eventIn.getCategory() == Biome.BiomeCategory.NONE)
			return;
	
		if (eventIn.getCategory() == Biome.BiomeCategory.NETHER)
		{
			eventIn.getGeneration().addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Configured.NETHER_HOPE_MUSHROOM_PATCH);
			eventIn.getGeneration().addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Configured.NETHER_DESPAIR_MUSHROOM_PATCH);
		}
		else
		{
			eventIn.getGeneration().addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Configured.HOPE_MUSHROOM_PATCH);
			eventIn.getGeneration().addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Configured.DESPAIR_MUSHROOM_PATCH);
		}
	}*/

	// new RangeDecoratorConfiguration(UniformHeight.of(VerticalAnchor.bottom(),
	// VerticalAnchor.absolute(32)))
	public static class Configured
	{

		/*public static final RegistrarHandler<ConfiguredFeature<?, ?>> HANDLER = RegistrarHandler.getOrCreate(Registries.CONFIGURED_FEATURE, GoodNightSleep.MODID);*/
		private static final Map<ResourceLocation, Pointer<ConfiguredFeature<?, ?>>> POINTERS = new HashMap<>();

		public static final TreeConfiguration DUMMY_CONFIG = (new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(Blocks.OAK_LOG.defaultBlockState()), new StraightTrunkPlacer(4, 2, 0), BlockStateProvider.simple(Blocks.OAK_LEAVES.defaultBlockState()), new BlobFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), 3), new TwoLayersFeatureSize(1, 0, 1))).ignoreVines().build(); // DefaultBiomeFeatures.OAK_TREE_CONFIG;

		public static final Pointer<ConfiguredFeature<?, ?>> BASE_DREAM_TREE = createBasicTree("dream_tree", GNSBlocks.dream_log.defaultBlockState(), GNSBlocks.dream_leaves.defaultBlockState(), 4, 1);
		public static final Pointer<ConfiguredFeature<?, ?>> BASE_CANDY_TREE = createBasicTree("candy_tree", GNSBlocks.white_log.defaultBlockState(), GNSBlocks.candy_leaves.defaultBlockState(), 5);
		// 5, 7
		public static final Pointer<ConfiguredFeature<?, ?>> BASE_LARGE_CANDY_TREE = register("large_candy_tree", Feature.TREE, () -> (new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(GNSBlocks.white_log.defaultBlockState()), new StraightTrunkPlacer(7, 3, 0), BlockStateProvider.simple(GNSBlocks.candy_leaves.defaultBlockState()), new FancyFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), 4), new TwoLayersFeatureSize(0, 0, 0, OptionalInt.of(4)))).ignoreVines().build());

		public static final Pointer<ConfiguredFeature<?, ?>> BASE_DIAMOND_TREE = register("diamond_tree", Feature.TREE, () -> (new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(GNSBlocks.dream_log.defaultBlockState()), new FancyTrunkPlacer(3, 11, 0), BlockStateProvider.simple(GNSBlocks.diamond_leaves.defaultBlockState()), new FancyFoliagePlacer(ConstantInt.of(2), ConstantInt.of(4), 4), new TwoLayersFeatureSize(0, 0, 0, OptionalInt.of(4)))).ignoreVines().build());
		public static final Pointer<ConfiguredFeature<?, ?>> BASE_DEAD_TREE = createBasicTree("dead_tree", GNSBlocks.dead_log.defaultBlockState(), Blocks.AIR.defaultBlockState(), 5);
		public static final Pointer<ConfiguredFeature<?, ?>> BASE_BLOOD_TREE = createBasicTree("blood_tree", GNSBlocks.blood_log.defaultBlockState(), Blocks.AIR.defaultBlockState(), 5);
		public static final Pointer<ConfiguredFeature<?, ?>> BASE_LARGE_DEAD_TREE = register("large_deaf_tree", Feature.TREE, () -> (new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(GNSBlocks.dead_log.defaultBlockState()), new FancyTrunkPlacer(3, 11, 0), BlockStateProvider.simple(Blocks.AIR.defaultBlockState()), new FancyFoliagePlacer(ConstantInt.of(2), ConstantInt.of(4), 4), new TwoLayersFeatureSize(0, 0, 0, OptionalInt.of(4)))).ignoreVines().build());
		public static final Pointer<ConfiguredFeature<?, ?>> BASE_LARGE_BLOOD_TREE = register("large_blood_tree", Feature.TREE, () -> (new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(GNSBlocks.blood_log.defaultBlockState()), new FancyTrunkPlacer(3, 11, 0), BlockStateProvider.simple(Blocks.AIR.defaultBlockState()), new FancyFoliagePlacer(ConstantInt.of(2), ConstantInt.of(4), 4), new TwoLayersFeatureSize(0, 0, 0, OptionalInt.of(8)))).ignoreVines().build());

		public static final Pointer<ConfiguredFeature<?, ?>> BASE_HUGE_HOPE_MUSHROOM = register("huge_hope_mushroom", Feature.HUGE_RED_MUSHROOM, () -> new HugeMushroomFeatureConfiguration(BlockStateProvider.simple(GNSBlocks.hope_mushroom_block.defaultBlockState().setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false))), BlockStateProvider.simple(Blocks.MUSHROOM_STEM.defaultBlockState().setValue(HugeMushroomBlock.UP, Boolean.valueOf(false)).setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false))), 2));
		public static final Pointer<ConfiguredFeature<?, ?>> BASE_HUGE_DESPAIR_MUSHROOM = register("huge_despair_mushroom", Feature.HUGE_BROWN_MUSHROOM, () -> new HugeMushroomFeatureConfiguration(BlockStateProvider.simple(GNSBlocks.despair_mushroom_block.defaultBlockState().setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false))), BlockStateProvider.simple(Blocks.MUSHROOM_STEM.defaultBlockState().setValue(HugeMushroomBlock.UP, Boolean.valueOf(false)).setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false))), 3));

		/*public static final ConfiguredFeature<?, ?> TALLER_HOPE_MUSHROOM = GNSFeatures.TALL_HOPE_MUSHROOM.configured(new HugeMushroomFeatureConfiguration(BlockStateProvider.simple(GNSBlocks.hope_mushroom_block.defaultBlockState().setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false))), BlockStateProvider.simple(Blocks.MUSHROOM_STEM.defaultBlockState().setValue(HugeMushroomBlock.UP, Boolean.valueOf(false)).setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false))), 2));
		public static final ConfiguredFeature<?, ?> SLIGHTLY_LARGER_HOPE_MUSHROOM = GNSFeatures.TALL_HOPE_MUSHROOM.configured(new HugeMushroomFeatureConfiguration(BlockStateProvider.simple(GNSBlocks.hope_mushroom_block.defaultBlockState().setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false))), BlockStateProvider.simple(Blocks.MUSHROOM_STEM.defaultBlockState().setValue(HugeMushroomBlock.UP, Boolean.valueOf(false)).setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false))), 3));
		public static final ConfiguredFeature<?, ?> VERY_LARGE_HOPE_MUSHROOM = GNSFeatures.FAT_HOPE_MUSHROOM.configured(new HugeMushroomFeatureConfiguration(BlockStateProvider.simple(GNSBlocks.hope_mushroom_block.defaultBlockState().setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false))), BlockStateProvider.simple(Blocks.MUSHROOM_STEM.defaultBlockState().setValue(HugeMushroomBlock.UP, Boolean.valueOf(false)).setValue(HugeMushroomBlock.DOWN, Boolean.valueOf(false))), 5));*/

		private static final Supplier<BlockMatchTest> DELUSION_RULE_TEST = () -> new BlockMatchTest(GNSBlocks.delusion_stone);
		private static final Supplier<TagMatchTest> STONE_RULE_TEST = () -> new TagMatchTest(BlockTags.STONE_ORE_REPLACEABLES);

		public static final Pointer<ConfiguredFeature<?, ?>> DREAM_DIRT_BLOB = register("dream_dirt_blob", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(DELUSION_RULE_TEST.get(), GNSBlocks.dream_dirt.defaultBlockState())), 33));
		public static final Pointer<ConfiguredFeature<?, ?>> DREAM_CANDY_ORE = register("dream_candy_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(DELUSION_RULE_TEST.get(), GNSBlocks.candy_ore.defaultBlockState())), 15));
		public static final Pointer<ConfiguredFeature<?, ?>> DREAM_SPECTRITE_ORE = register("dream_spectrite_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(DELUSION_RULE_TEST.get(), GNSBlocks.spectrite_ore.defaultBlockState())), 9));
		public static final Pointer<ConfiguredFeature<?, ?>> DREAM_POSITITE_ORE = register("dream_positite_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(DELUSION_RULE_TEST.get(), GNSBlocks.positite_ore.defaultBlockState())), 8));
		public static final Pointer<ConfiguredFeature<?, ?>> DREAM_COAL_ORE = register("dream_coal_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(DELUSION_RULE_TEST.get(), GNSBlocks.delusion_coal_ore.defaultBlockState())), 17));
		public static final Pointer<ConfiguredFeature<?, ?>> DREAM_LAPIS_ORE = register("dream_lapis_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(DELUSION_RULE_TEST.get(), GNSBlocks.delusion_lapis_ore.defaultBlockState())), 7));
		public static final Pointer<ConfiguredFeature<?, ?>> DREAM_GLOWSTONE_BLOB = register("dream_glowstone_blob", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(DELUSION_RULE_TEST.get(), Blocks.GLOWSTONE.defaultBlockState())), 8));

		public static final Pointer<ConfiguredFeature<?, ?>> SCATTERED_PRESENTS = register("scattered_presents", GNSFeatures.SCATTERED_PRESENTS, () -> FeatureConfiguration.NONE);
		public static final Pointer<ConfiguredFeature<?, ?>> SPONGE_GROWTH = register("sponge_growth", GNSFeatures.SPONGE_GROWTH, () -> FeatureConfiguration.NONE);

		public static final Pointer<ConfiguredFeature<?, ?>> SCATTERED_PUMPKINS = register("scattered_pumpkins", GNSFeatures.SCATTERED_PUMPKINS, () -> FeatureConfiguration.NONE);
		public static final Pointer<ConfiguredFeature<?, ?>> NETHER_SPLASH = register("nether_splash", GNSFeatures.NETHER_SPLASH, () -> FeatureConfiguration.NONE);

		public static final Pointer<ConfiguredFeature<?, ?>> NIGHTMARE_DIRT_BLOB = register("nightmare_dirt_blob", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), Blocks.DIRT.defaultBlockState())), 33));
		public static final Pointer<ConfiguredFeature<?, ?>> NIGHTMARE_GRAVEL_BLOB = register("nightmare_gravel_blob", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), Blocks.GRAVEL.defaultBlockState())), 33));
		public static final Pointer<ConfiguredFeature<?, ?>> NIGHTMARE_NECRUM_ORE = register("nightmare_necrum_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), GNSBlocks.fossilized_necrum.defaultBlockState())), 15));
		public static final Pointer<ConfiguredFeature<?, ?>> NIGHTMARE_ZITRITE_ORE = register("nightmare_zitrite_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), GNSBlocks.zitrite_ore.defaultBlockState())), 8));
		public static final Pointer<ConfiguredFeature<?, ?>> NIGHTMARE_NEGATITE_ORE = register("nightmare_negatite_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), GNSBlocks.negatite_ore.defaultBlockState())), 7));
		public static final Pointer<ConfiguredFeature<?, ?>> NIGHTMARE_COAL_ORE = register("nightmare_coal_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), Blocks.COAL_ORE.defaultBlockState())), 17));
		public static final Pointer<ConfiguredFeature<?, ?>> NIGHTMARE_LAPIS_ORE = register("nightmare_lapis_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), Blocks.LAPIS_ORE.defaultBlockState())), 7));
		public static final Pointer<ConfiguredFeature<?, ?>> NIGHTMARE_IRON_ORE = register("nightmare_iron_ore", Feature.ORE, () -> new OreConfiguration(List.of(OreConfiguration.target(STONE_RULE_TEST.get(), Blocks.IRON_ORE.defaultBlockState())), 9));

		public static final Pointer<ConfiguredFeature<?, ?>> DREAM_GRASS_PATCH = register("dream_grass_patch", Feature.RANDOM_PATCH, () -> Configs.DREAM_GRASS);
		public static final Pointer<ConfiguredFeature<?, ?>> NIGHTMARE_GRASS_PATCH = register("nightmare_grass_patch", Feature.RANDOM_PATCH, () -> Configs.NIGHTMARE_GRASS);
		public static final Pointer<ConfiguredFeature<?, ?>> PRICKLY_NIGHTMARE_GRASS_PATCH = register("prickly_nightmare_grass_patch", Feature.RANDOM_PATCH, () -> Configs.PRICKLY_NIGHTMARE_GRASS);
		public static final Pointer<ConfiguredFeature<?, ?>> DREAM_FLOWER_PATCH = register("dream_flower_patch", Feature.RANDOM_PATCH, () -> Configs.DREAM_FLOWERS);
		public static final Pointer<ConfiguredFeature<?, ?>> NIGHTMARE_FLOWER_PATCH = register("nightmare_flower_patch", Feature.RANDOM_PATCH, () -> Configs.NIGHTMARE_FLOWERS);
		public static final Pointer<ConfiguredFeature<?, ?>> LOLLIPOP_BUSH_PATCH = register("lollipop_bush_patch", Feature.RANDOM_PATCH, () -> Configs.LOLLIPOP_BUSHES);
		public static final Pointer<ConfiguredFeature<?, ?>> HOPE_MUSHROOM_PATCH = register("hope_mushroom_patch", Feature.RANDOM_PATCH, () -> Configs.HOPE_MUSHROOMS);
		public static final Pointer<ConfiguredFeature<?, ?>> DESPAIR_MUSHROOM_PATCH = register("despair_mushroom_patch", Feature.RANDOM_PATCH, () -> Configs.DESPAIR_MUSHROOMS);

		/*public static final ConfiguredWorldCarver<?> DELUSION_CAVE_CARVER = GNSFeatures.Carvers.DELUSION_CAVE_CARVER.configured(new CaveCarverConfiguration(0.14285715F, BiasedToBottomHeight.of(VerticalAnchor.absolute(0), VerticalAnchor.absolute(127), 8), ConstantFloat.of(0.5F), VerticalAnchor.aboveBottom(10), false, CarverDebugSettings.of(false, GNSBlocks.dream_button.defaultBlockState()), ConstantFloat.of(1.0F), ConstantFloat.of(1.0F), ConstantFloat.of(-0.7F)));
		public static final ConfiguredWorldCarver<?> DELUSION_CANYON_CARVER = GNSFeatures.Carvers.DELUSION_CANYON_CARVER.configured(new CanyonCarverConfiguration(0.02F, BiasedToBottomHeight.of(VerticalAnchor.absolute(20), VerticalAnchor.absolute(67), 8), ConstantFloat.of(3.0F), VerticalAnchor.aboveBottom(10), false, CarverDebugSettings.of(false, GNSBlocks.white_button.defaultBlockState()), UniformFloat.of(-0.125F, 0.125F), new CanyonCarverConfiguration.CanyonShapeConfiguration(UniformFloat.of(0.75F, 1.0F), TrapezoidFloat.of(0.0F, 6.0F, 2.0F), 3, UniformFloat.of(0.75F, 1.0F), 1.0F, 0.0F)));*/

		public static void bootstrap(BootstrapContext<ConfiguredFeature<?, ?>> bootstrap)
		{
			POINTERS.forEach((key, pointer) ->
			{
				System.out.println("Registering " + pointer.getKey().toString());
				bootstrap.register(pointer.getKey(), pointer.getInstance().apply(bootstrap));
			});
		}

		public static void init()
		{
		}

		public static void initCarvers()
		{
			/*registerCarver("delusion_cave_carver", DELUSION_CAVE_CARVER);
			registerCarver("delusion_canyon_carver", DELUSION_CANYON_CARVER);*/
		}

		private static <FC extends FeatureConfiguration, F extends Feature<FC>> Pointer<ConfiguredFeature<?, ?>> register(String key, F feature, Supplier<FC> config)
		{
			/*return HANDLER.createPointer(key, () -> new ConfiguredFeature<>(feature, config.get()));*/

			Pointer<ConfiguredFeature<?, ?>> pointer = new Pointer<>(Registries.CONFIGURED_FEATURE, key, () -> new ConfiguredFeature<>(feature, config.get()));

			if (DatagenModLoader.isRunningDataGen())
				POINTERS.put(GoodNightSleep.locate(key), pointer);

			return pointer;
		}

		/*private static <C extends CarverConfiguration> ConfiguredWorldCarver<C> registerCarver(String nameIn, ConfiguredWorldCarver<C> featureIn)
		{
			return Registry.register(BuiltinRegistries.CONFIGURED_CARVER, nameIn, featureIn);
		}*/

		public static Pointer<ConfiguredFeature<?, ?>> createBasicTree(String key, BlockState log, BlockState leaves, int height)
		{
			return createBasicTree(key, log, leaves, height, 2);
		}

		public static Pointer<ConfiguredFeature<?, ?>> createBasicTree(String key, BlockState log, BlockState leaves, int height, int randHeight)
		{
			return register(key, Feature.TREE, () -> (new TreeConfiguration.TreeConfigurationBuilder(BlockStateProvider.simple(log), new StraightTrunkPlacer(height, randHeight, 0), BlockStateProvider.simple(leaves), new BlobFoliagePlacer(ConstantInt.of(2), ConstantInt.of(0), 3), new TwoLayersFeatureSize(1, 0, 1))).ignoreVines().build());
		}

		protected static class Configs
		{

			public static final RandomPatchConfiguration DREAM_GRASS = simplePatch("dream_grass", GNSBlocks.short_dream_grass.defaultBlockState(), 32);
			public static final RandomPatchConfiguration NIGHTMARE_GRASS = simplePatch("nightmare_grass", GNSBlocks.short_nightmare_grass.defaultBlockState(), 32);
			public static final RandomPatchConfiguration DREAM_FLOWERS = weightedPatch("dream_flowers", weighted().add(GNSBlocks.cyan_flower.defaultBlockState(), 1).add(GNSBlocks.orange_flower.defaultBlockState(), 1).add(GNSBlocks.lollipop_bush.defaultBlockState(), 2), 64);
			public static final RandomPatchConfiguration LOLLIPOP_BUSHES = simplePatch("lollipop_bushes", GNSBlocks.lollipop_bush.defaultBlockState(), 32);
			public static final RandomPatchConfiguration PRICKLY_NIGHTMARE_GRASS = simplePatch("prickly_nightmare_grass", GNSBlocks.prickly_nightmare_grass.defaultBlockState(), 32);
			public static final RandomPatchConfiguration NIGHTMARE_FLOWERS = simplePatch("nightmare_flowers", GNSBlocks.dead_flower.defaultBlockState(), 64);

			public static final RandomPatchConfiguration HOPE_MUSHROOMS = simplePatch("hope_mushrooms", GNSBlocks.hope_mushroom.defaultBlockState(), 64);
			public static final RandomPatchConfiguration DESPAIR_MUSHROOMS = simplePatch("despair_mushrooms", GNSBlocks.despair_mushroom.defaultBlockState(), 64);

			public static RandomPatchConfiguration simplePatch(String name, BlockState state, int tries)
			{
				return simplePatch(name, state, tries, p -> PlacementUtils.onlyWhenEmpty(p.getLeft(), p.getRight()));
			}

			public static RandomPatchConfiguration filteredPatch(String name, BlockState state, int tries, Block notBlock)
			{
				return simplePatch(name, state, tries, p -> PlacementUtils.filtered(p.getLeft(), p.getRight(), BlockPredicate.allOf(BlockPredicate.matchesBlocks(Blocks.AIR), BlockPredicate.not(BlockPredicate.matchesBlocks(BlockPos.ZERO.below(), notBlock)))));
			}

			@SuppressWarnings("unchecked")
			public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration simplePatch(String name, BlockState state, int tries, Function<Pair<F, FC>, Holder<PlacedFeature>> cons)
			{
				return randomPatchConfig(name, tries, cons.apply(Pair.of((F) Feature.SIMPLE_BLOCK, (FC) new SimpleBlockConfiguration(BlockStateProvider.simple(state)))));
			}

			public static RandomPatchConfiguration weightedPatch(String name, SimpleWeightedRandomList.Builder<BlockState> builder, int tries)
			{
				return weightedPatch(name, builder, tries, p -> PlacementUtils.onlyWhenEmpty(p.getLeft(), p.getRight()));
			}

			@SuppressWarnings("unchecked")
			public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration weightedPatch(String name, SimpleWeightedRandomList.Builder<BlockState> builder, int tries, Function<Pair<F, FC>, Holder<PlacedFeature>> cons)
			{
				return randomPatchConfig(name, tries, cons.apply(Pair.of((F) Feature.SIMPLE_BLOCK, (FC) new SimpleBlockConfiguration(new WeightedStateProvider(builder)))));
			}

			public static <FC extends FeatureConfiguration, F extends Feature<FC>> RandomPatchConfiguration randomPatchConfig(String name, int tries, Holder<PlacedFeature> placedHolder)
			{
				return FeatureUtils.simpleRandomPatchConfiguration(tries, placedHolder);
			}

			public static SimpleWeightedRandomList.Builder<BlockState> weighted()
			{
				return SimpleWeightedRandomList.<BlockState>builder();
			}
		}
	}

	public static class Placements
	{
		/*public static final RegistrarHandler<PlacedFeature> HANDLER = RegistrarHandler.getOrCreate(Registries.PLACED_FEATURE, GoodNightSleep.MODID);*/
		private static final Map<ResourceLocation, Pointer<PlacedFeature>> POINTERS = new HashMap<>();

		public static final Pointer<PlacedFeature> HOPE_MUSHROOM_PATCH = register("hope_mushroom_patch", Configured.HOPE_MUSHROOM_PATCH, flower(8, true));
		public static final Pointer<PlacedFeature> DESPAIR_MUSHROOM_PATCH = register("despair_mushroom_patch", Configured.DESPAIR_MUSHROOM_PATCH, flower(4, true));

		public static final Pointer<PlacedFeature> NETHER_HOPE_MUSHROOM_PATCH = register("nether_hope_mushroom_patch", Configured.HOPE_MUSHROOM_PATCH, List.of(RarityFilter.onAverageOnceEvery(8), InSquarePlacement.spread(), HeightRangePlacement.uniform(VerticalAnchor.bottom(), VerticalAnchor.absolute(127)), BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> NETHER_DESPAIR_MUSHROOM_PATCH = register("nether_despair_mushroom_patch", Configured.DESPAIR_MUSHROOM_PATCH, List.of(RarityFilter.onAverageOnceEvery(4), InSquarePlacement.spread(), HeightRangePlacement.uniform(VerticalAnchor.bottom(), VerticalAnchor.absolute(127)), BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> HILLS_DREAM_TREE = register("hills_dream_tree", GNSFeatures.Configured.BASE_DREAM_TREE, VegetationPlacements.treePlacement(CountPlacement.of(UniformInt.of(0, 2)), GNSBlocks.dream_sapling));
		public static final Pointer<PlacedFeature> HILLS_CANDY_TREE = register("hills_candy_tree", GNSFeatures.Configured.BASE_CANDY_TREE, VegetationPlacements.treePlacement(CountPlacement.of(UniformInt.of(0, 1)), GNSBlocks.candy_sapling));
		public static final Pointer<PlacedFeature> HILLS_DIAMOND_TREE = register("hills_diamond_tree", GNSFeatures.Configured.BASE_DIAMOND_TREE, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(5), GNSBlocks.dream_sapling));

		public static final Pointer<PlacedFeature> DREAM_DIRT_BLOB = register("dream_dirt_blob", Configured.DREAM_DIRT_BLOB, countRange(127, 10));
		public static final Pointer<PlacedFeature> DREAM_CANDY_ORE = register("dream_candy_ore", Configured.DREAM_CANDY_ORE, countRange(63, 20));
		public static final Pointer<PlacedFeature> DREAM_SPECTRITE_ORE = register("dream_spectrite_ore", Configured.DREAM_SPECTRITE_ORE, countRange(63, 20));
		public static final Pointer<PlacedFeature> DREAM_POSITITE_ORE = register("dream_positite_ore", Configured.DREAM_POSITITE_ORE, countRange(15, 2));
		public static final Pointer<PlacedFeature> DREAM_COAL_ORE = register("dream_coal_ore", Configured.DREAM_COAL_ORE, countRange(127, 20));
		public static final Pointer<PlacedFeature> DREAM_LAPIS_ORE = register("dream_lapis_ore", Configured.DREAM_LAPIS_ORE, countRange(HeightRangePlacement.triangle(VerticalAnchor.absolute(0), VerticalAnchor.absolute(30)), 1));
		public static final Pointer<PlacedFeature> DREAM_GLOWSTONE_BLOB = register("dream_glowstone_blob", Configured.DREAM_GLOWSTONE_BLOB, countRange(7, 2));

		public static final Pointer<PlacedFeature> SCATTERED_PRESENTS = register("scattered_presents", Configured.SCATTERED_PRESENTS, List.of(RarityFilter.onAverageOnceEvery(32), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_TOP_SOLID, BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> SPONGE_GROWTH = register("sponge_growth", Configured.SPONGE_GROWTH, List.of(CountPlacement.of(1), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_OCEAN_FLOOR, BiomeFilter.biome()));

		public static final Pointer<PlacedFeature> HUGE_HOPE_MUSHROOM = register("huge_hope_mushroom", Configured.BASE_HUGE_HOPE_MUSHROOM, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(4), GNSBlocks.hope_mushroom));

		/*public static final ConfiguredFeature<?, ?> FIELDS_HOPE_MUSHROOM = GNSFeatures.Configured.BASE_HUGE_HOPE_MUSHROOM, VegetationPlacements.treePlacement(CountPlacement.of(1), GNSBlocks.hope_mushroom)));
		public static final ConfiguredFeature<?, ?> FIELDS_LARGER_HOPE_MUSHROOM = GNSFeatures.Configured.SLIGHTLY_LARGER_HOPE_MUSHROOM.decorated(Features.Decorators.HEIGHTMAP_SQUARE).decorated(FeatureDecorator.COUNT_EXTRA.configured(new FrequencyWithExtraChanceDecoratorConfiguration(0, 1.0F, 2)));
		public static final ConfiguredFeature<?, ?> FIELDS_VERY_LARGE_HOPE_MUSHROOM = GNSFeatures.Configured.VERY_LARGE_HOPE_MUSHROOM, VegetationPlacements.treePlacement(PlacementUtils.countExtra(0, 0.1F, 1), GNSBlocks.hope_mushroom));*/

		public static final Pointer<PlacedFeature> NIGHTMARE_DIRT_BLOB = register("nightmare_dirt_blob", Configured.NIGHTMARE_DIRT_BLOB, countRange(127, 10));
		public static final Pointer<PlacedFeature> NIGHTMARE_GRAVEL_BLOB = register("nightmare_gravel_blob", Configured.NIGHTMARE_GRAVEL_BLOB, countRange(127, 8));
		public static final Pointer<PlacedFeature> NIGHTMARE_NECRUM_ORE = register("nightmare_necrum_ore", Configured.NIGHTMARE_NECRUM_ORE, countRange(127, 8));
		public static final Pointer<PlacedFeature> NIGHTMARE_ZITRITE_ORE = register("nightmare_zitrite_ore", Configured.NIGHTMARE_ZITRITE_ORE, countRange(15, 25));
		public static final Pointer<PlacedFeature> NIGHTMARE_NEGATITE_ORE = register("nightmare_negatite_ore", Configured.NIGHTMARE_NEGATITE_ORE, countRange(15, 1));
		public static final Pointer<PlacedFeature> NIGHTMARE_COAL_ORE = register("nightmare_coal_ore", Configured.NIGHTMARE_COAL_ORE, countRange(127, 20));
		public static final Pointer<PlacedFeature> NIGHTMARE_LAPIS_ORE = register("nightmare_lapis_ore", Configured.NIGHTMARE_LAPIS_ORE, countRange(HeightRangePlacement.triangle(VerticalAnchor.absolute(0), VerticalAnchor.absolute(30)), 1));
		public static final Pointer<PlacedFeature> NIGHTMARE_IRON_ORE = register("nightmare_iron_ore", Configured.NIGHTMARE_IRON_ORE, countRange(63, 20));

		public static final Pointer<PlacedFeature> HILLS_DEAD_TREE = register("hills_dead_tree", GNSFeatures.Configured.BASE_LARGE_DEAD_TREE, VegetationPlacements.treePlacement(CountPlacement.of(UniformInt.of(0, 1)), Blocks.OAK_SAPLING));
		public static final Pointer<PlacedFeature> HILLS_BLOOD_TREE = register("hills_blood_tree", GNSFeatures.Configured.BASE_BLOOD_TREE, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(3), Blocks.OAK_SAPLING));

		public static final Pointer<PlacedFeature> NETHER_SPLASH = register("nether_splash", Configured.NETHER_SPLASH, List.of(RarityFilter.onAverageOnceEvery(3), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP, BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> SCATTERED_PUMPKINS = register("scattered_pumpkins", Configured.SCATTERED_PUMPKINS, List.of(RarityFilter.onAverageOnceEvery(35), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_TOP_SOLID, BiomeFilter.biome()));
		public static final Pointer<PlacedFeature> HUGE_DESPAIR_MUSHROOM = register("huge_despair_mushroom", GNSFeatures.Configured.BASE_HUGE_DESPAIR_MUSHROOM, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(3), GNSBlocks.despair_mushroom));

		public static final Pointer<PlacedFeature> PLAINS_DIAMOND_TREE = register("plains_diamond_tree", GNSFeatures.Configured.BASE_DIAMOND_TREE, VegetationPlacements.treePlacement(PlacementUtils.countExtra(0, 0.1F, 1), GNSBlocks.dream_sapling));
		public static final Pointer<PlacedFeature> PLAINS_HOPE_MUSHROOM = register("plains_hope_mushroom", GNSFeatures.Configured.BASE_HUGE_HOPE_MUSHROOM, VegetationPlacements.treePlacement(PlacementUtils.countExtra(0, 0.1F, 1), GNSBlocks.hope_mushroom));

		public static final Pointer<PlacedFeature> FOREST_DREAM_TREE = register("forest_dream_tree", GNSFeatures.Configured.BASE_DREAM_TREE, VegetationPlacements.treePlacement(CountPlacement.of(5), GNSBlocks.dream_sapling));
		public static final Pointer<PlacedFeature> FOREST_CANDY_TREE = register("forest_candy_tree", GNSFeatures.Configured.BASE_CANDY_TREE, VegetationPlacements.treePlacement(CountPlacement.of(1), GNSBlocks.candy_sapling));

		public static final Pointer<PlacedFeature> LANDS_CANDY_TREE = register("lands_candy_tree", GNSFeatures.Configured.BASE_LARGE_CANDY_TREE, VegetationPlacements.treePlacement(CountPlacement.of(5), GNSBlocks.candy_sapling));
		public static final Pointer<PlacedFeature> LANDS_DREAM_TREE = register("lands_dream_tree", GNSFeatures.Configured.BASE_DREAM_TREE, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(3), GNSBlocks.dream_sapling));

		public static final Pointer<PlacedFeature> PLAINS_BLOOD_TREE = register("plains_blood_tree", GNSFeatures.Configured.BASE_LARGE_BLOOD_TREE, VegetationPlacements.treePlacement(RarityFilter.onAverageOnceEvery(5), Blocks.OAK_SAPLING));
		public static final Pointer<PlacedFeature> PLAINS_DESPAIR_MUSHROOM = register("plains_despair_mushroom", GNSFeatures.Configured.BASE_HUGE_DESPAIR_MUSHROOM, VegetationPlacements.treePlacement(PlacementUtils.countExtra(0, 0.1F, 1), GNSBlocks.despair_mushroom));

		public static final Pointer<PlacedFeature> FOREST_DEAD_TREE = register("forest_dead_tree", GNSFeatures.Configured.BASE_DEAD_TREE, VegetationPlacements.treePlacement(CountPlacement.of(5), Blocks.OAK_SAPLING));
		public static final Pointer<PlacedFeature> FOREST_BLOOD_TREE = register("forest_blood_tree", GNSFeatures.Configured.BASE_LARGE_BLOOD_TREE, VegetationPlacements.treePlacement(CountPlacement.of(1), Blocks.OAK_SAPLING));

		public static final Pointer<PlacedFeature> DREAM_GRASS_NOISE = register("dream_grass_noise", Configured.DREAM_GRASS_PATCH, countNoise(-0.8D, 5, 10));
		public static final Pointer<PlacedFeature> NIGHTMARE_GRASS_NOISE = register("nightmare_grass_noise", Configured.NIGHTMARE_GRASS_PATCH, countNoise(-0.8D, 5, 10));
		public static final Pointer<PlacedFeature> DREAM_FLOWERS_3 = register("dream_flowers_3", Configured.DREAM_FLOWER_PATCH, flower(3, true));
		public static final Pointer<PlacedFeature> NIGHTMARE_FLOWERS_5 = register("nightmare_flowers_5", Configured.NIGHTMARE_FLOWER_PATCH, flower(5, true));
		public static final Pointer<PlacedFeature> DREAM_LOLLIPOPS_5 = register("dream_lollipops_5", Configured.LOLLIPOP_BUSH_PATCH, flower(5, true));
		public static final Pointer<PlacedFeature> DREAM_MUSHROOMS_10 = register("hope_mushrooms_10", Configured.HOPE_MUSHROOM_PATCH, flower(10, true));
		public static final Pointer<PlacedFeature> NIGHTMARE_GRASS_5 = register("nightmare_grass_5", Configured.NIGHTMARE_GRASS_PATCH, flower(5, false));
		public static final Pointer<PlacedFeature> PRICKLY_NIGHTMARE_GRASS_3 = register("prickly_nightmare_grass_3", Configured.PRICKLY_NIGHTMARE_GRASS_PATCH, flower(3, true));
		public static final Pointer<PlacedFeature> PRICKLY_NIGHTMARE_GRASS_5 = register("prickly_nightmare_grass_5", Configured.PRICKLY_NIGHTMARE_GRASS_PATCH, flower(5, true));

		public static void bootstrap(BootstrapContext<PlacedFeature> bootstrap)
		{
			POINTERS.forEach((key, pointer) -> bootstrap.register(pointer.getKey(), pointer.getInstance().apply(bootstrap)));
		}

		public static void init()
		{
		}

		private static Pointer<PlacedFeature> register(String key, Pointer<ConfiguredFeature<?, ?>> feature, List<PlacementModifier> mods)
		{
			/*return HANDLER.createPointer(key, (b) -> new PlacedFeature(feature.getHolder(b).get(), List.copyOf(mods)));*/

			Pointer<PlacedFeature> pointer = new Pointer<>(Registries.PLACED_FEATURE, key, (c) -> new PlacedFeature(feature.getHolder(c).get(), List.copyOf(mods)));

			if (DatagenModLoader.isRunningDataGen())
				POINTERS.put(GoodNightSleep.locate(key), pointer);

			return pointer;
		}

		private static List<PlacementModifier> countRange(int height, int count)
		{
			return List.of(CountPlacement.of(count), InSquarePlacement.spread(), HeightRangePlacement.uniform(VerticalAnchor.absolute(0), VerticalAnchor.absolute(height)), BiomeFilter.biome());
		}

		private static List<PlacementModifier> countRange(HeightRangePlacement heightRange, int count)
		{
			return List.of(CountPlacement.of(count), InSquarePlacement.spread(), heightRange, BiomeFilter.biome());
		}

		/*private static PlacementModifier range(int height)
		{
			// still from 0
			return HeightRangePlacement.uniform(VerticalAnchor.absolute(0), VerticalAnchor.absolute(height));
		}
		
		private static List<PlacementModifier> count(int count)
		{
			return flower(count, false);
		}*/

		private static List<PlacementModifier> flower(int count, boolean average)
		{
			PlacementModifier modifier = average ? RarityFilter.onAverageOnceEvery(count) : CountPlacement.of(count);
			return List.of(modifier, InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome());
		}

		private static List<PlacementModifier> countNoise(double noise, int x, int y)
		{
			return countNoise(noise, x, y, PlacementUtils.HEIGHTMAP);
		}

		private static List<PlacementModifier> countNoise(double noise, int x, int y, PlacementModifier heightmap)
		{
			return List.of(NoiseThresholdCountPlacement.of(noise, x, y), InSquarePlacement.spread(), PlacementUtils.HEIGHTMAP_WORLD_SURFACE, BiomeFilter.biome());
		}
	}
}
