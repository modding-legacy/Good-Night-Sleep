package com.legacy.good_nights_sleep.registry;

import java.util.Set;
import java.util.function.Supplier;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.tile_entity.DreamBedBlockEntity;
import com.legacy.good_nights_sleep.tile_entity.GNSHangingSignBlockEntity;
import com.legacy.good_nights_sleep.tile_entity.GNSSignBlockEntity;

import net.minecraft.core.registries.Registries;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.registries.RegisterEvent;

public class GNSBlockEntityTypes
{
	public static final Supplier<BlockEntityType<DreamBedBlockEntity>> DREAM_BED = Lazy.of(() -> create(DreamBedBlockEntity::new, GNSBlocks.luxurious_bed, GNSBlocks.wretched_bed, GNSBlocks.strange_bed));
	public static final Supplier<BlockEntityType<GNSSignBlockEntity>> SIGN = Lazy.of(() -> create(GNSSignBlockEntity::new, GNSBlocks.dream_sign, GNSBlocks.dream_wall_sign, GNSBlocks.white_sign, GNSBlocks.white_wall_sign, GNSBlocks.blood_sign, GNSBlocks.blood_wall_sign, GNSBlocks.dead_sign, GNSBlocks.dead_wall_sign));
	public static final Supplier<BlockEntityType<GNSHangingSignBlockEntity>> HANGING_SIGN = Lazy.of(() -> create(GNSHangingSignBlockEntity::new, GNSBlocks.dream_hanging_sign, GNSBlocks.dream_wall_hanging_sign, GNSBlocks.white_hanging_sign, GNSBlocks.white_wall_hanging_sign, GNSBlocks.blood_hanging_sign, GNSBlocks.blood_wall_hanging_sign, GNSBlocks.dead_hanging_sign, GNSBlocks.dead_wall_hanging_sign));

	public static void init(RegisterEvent event)
	{
		register(event, "dream_bed", DREAM_BED);
		register(event, "sign", SIGN);
		register(event, "hanging_sign", HANGING_SIGN);
	}

	@SuppressWarnings("unchecked")
	private static <T extends BlockEntity> void register(RegisterEvent event, String name, Object blockEntity)
	{
		event.register(Registries.BLOCK_ENTITY_TYPE, GoodNightSleep.locate(name), (Supplier<BlockEntityType<?>>) blockEntity);
	}

	private static <T extends BlockEntity> BlockEntityType<T> create(BlockEntityType.BlockEntitySupplier<? extends T> p_362578_, Block... p_364748_)
	{
		return new BlockEntityType<>(p_362578_, Set.of(p_364748_));
	}
}
