package com.legacy.good_nights_sleep;

import org.apache.commons.lang3.tuple.Pair;

import net.neoforged.neoforge.common.ModConfigSpec;
import net.neoforged.neoforge.common.ModConfigSpec.ConfigValue;

public class GNSConfig
{
	/*public static final ModConfigSpec CLIENT_SPEC;
	public static final ClientConfig CLIENT;*/

	/*public static final ModConfigSpec COMMON_SPEC;
	public static final CommonConfig COMMON;*/

	public static final ModConfigSpec WORLD_SPEC;
	public static final WorldConfig WORLD;

	static
	{
		{
			/*Pair<ClientConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(ClientConfig::new);
			CLIENT = pair.getLeft();
			CLIENT_SPEC = pair.getRight();*/

			/*Pair<CommonConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(CommonConfig::new);
			COMMON = pair.getLeft();
			COMMON_SPEC = pair.getRight();*/

			Pair<WorldConfig, ModConfigSpec> pair = new ModConfigSpec.Builder().configure(WorldConfig::new);
			WORLD = pair.getLeft();
			WORLD_SPEC = pair.getRight();
		}
	}

	public static class ClientConfig
	{

		public ClientConfig(ModConfigSpec.Builder builder)
		{
		}

		public static String key(String key)
		{
			return translationKey("client") + "." + key;
		}
	}

	public static class CommonConfig
	{
		public CommonConfig(ModConfigSpec.Builder builder)
		{
		}

		public static String key(String key)
		{
			return translationKey("common") + "." + key;
		}
	}

	public static class WorldConfig
	{
		private final ConfigValue<Boolean> allowNightmarePhantoms;
		private final ConfigValue<Boolean> limitSleepTime;

		public WorldConfig(ModConfigSpec.Builder builder)
		{
			allowNightmarePhantoms = builder.translation(key("allow_nightmare_phantoms")).comment("Allows Phantoms to spawn in the Nightmare dimension.").define("allow_nightmare_phantoms", true);
			limitSleepTime = builder.translation(key("limit_sleep_time")).comment("Limits use of the mod's beds to night-time.").define("limit_sleep_time", false);
		}

		public boolean allowNightmarePhantoms()
		{
			return this.allowNightmarePhantoms.get();
		}

		public boolean limitSleepTime()
		{
			return this.limitSleepTime.get();
		}

		public static String key(String key)
		{
			return translationKey("server") + "." + key;
		}
	}

	protected static String translationKey(String key)
	{
		return GoodNightSleep.MODID + "." + key;
	}
}