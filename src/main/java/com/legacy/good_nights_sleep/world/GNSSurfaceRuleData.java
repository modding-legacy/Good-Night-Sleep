package com.legacy.good_nights_sleep.world;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableList.Builder;
import com.legacy.good_nights_sleep.registry.GNSBlocks;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.SurfaceRules;
import net.minecraft.world.level.levelgen.SurfaceRules.RuleSource;
import net.minecraft.world.level.levelgen.VerticalAnchor;

public class GNSSurfaceRuleData
{
	/*private static final SurfaceRules.RuleSource RED_WOOL = stateRule(Blocks.RED_WOOL);
	private static final SurfaceRules.RuleSource BLUE_WOOL = stateRule(Blocks.BLUE_WOOL);*/

	private static final SurfaceRules.RuleSource BEDROCK = stateRule(Blocks.BEDROCK);

	/*private static final SurfaceRules.RuleSource SAND = stateRule(Blocks.SAND);
	private static final SurfaceRules.RuleSource GRAVEL = stateRule(Blocks.GRAVEL);*/

	private static final SurfaceRules.RuleSource DREAM_GRASS = stateRule(GNSBlocks.dream_grass_block);
	private static final SurfaceRules.RuleSource DREAM_DIRT = stateRule(GNSBlocks.dream_dirt);

	private static final SurfaceRules.RuleSource NIGHTMARE_GRASS = stateRule(GNSBlocks.nightmare_grass_block);
	private static final SurfaceRules.RuleSource NIGHTMARE_DIRT = stateRule(Blocks.DIRT);

	public static SurfaceRules.RuleSource basicData(boolean preliminarySurface, boolean bedrockFloor, boolean nightmare)
	{
		SurfaceRules.ConditionSource aboveWater = SurfaceRules.waterBlockCheck(-1, 0);

		SurfaceRules.RuleSource belowSurfaceSource = SurfaceRules.sequence(nightmare ? NIGHTMARE_DIRT : DREAM_DIRT);
		SurfaceRules.RuleSource onSurfaceSource = SurfaceRules.sequence(SurfaceRules.ifTrue(aboveWater, nightmare ? NIGHTMARE_GRASS : DREAM_GRASS), belowSurfaceSource);

		// @formatter:off
		SurfaceRules.RuleSource floorModifiers = SurfaceRules.sequence(
				SurfaceRules.ifTrue(SurfaceRules.ON_FLOOR, onSurfaceSource),
				SurfaceRules.ifTrue(SurfaceRules.UNDER_FLOOR, belowSurfaceSource));
		// @formatter:on

		Builder<SurfaceRules.RuleSource> builder = ImmutableList.builder();

		builder.add(preliminarySurface ? SurfaceRules.ifTrue(SurfaceRules.abovePreliminarySurface(), floorModifiers) : floorModifiers);

		if (bedrockFloor)
			builder.add(SurfaceRules.ifTrue(SurfaceRules.verticalGradient("bedrock_floor", VerticalAnchor.bottom(), VerticalAnchor.aboveBottom(5)), BEDROCK));

		return SurfaceRules.sequence(builder.build().toArray((source) -> new SurfaceRules.RuleSource[source]));
	}

	private static RuleSource stateRule(Block block)
	{
		return stateRule(block.defaultBlockState());
	}

	private static RuleSource stateRule(BlockState state)
	{
		return SurfaceRules.state(state);
	}
}
