package com.legacy.good_nights_sleep.world.chunkgen;

import com.legacy.good_nights_sleep.registry.GNSDimensions;
import com.legacy.good_nights_sleep.world.biome_provider.NightmareBiomeSource;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.RegistryOps;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;

public class NightmareChunkGenerator extends GNSChunkGenerator
{
	public static final MapCodec<NightmareChunkGenerator> NIGHTMARE_CODEC = RecordCodecBuilder.mapCodec(instance ->
	{
		return instance.group(RegistryOps.retrieveGetter(Registries.BIOME), RegistryOps.retrieveGetter(Registries.NOISE_SETTINGS)).apply(instance, NightmareChunkGenerator::new);
	});

	public static final TerrainLayout NIGHTMARE_LAYOUT = new TerrainLayout(130.0D, 600.0D, 0.1D, 10.0D, 0.05, 2, 1.0D);

	public NightmareChunkGenerator(HolderGetter<Biome> biomeRegistry, HolderGetter<NoiseGeneratorSettings> noiseSettingsRegistry)
	{
		super(new NightmareBiomeSource(biomeRegistry), noiseSettingsRegistry.getOrThrow(GNSDimensions.NIGHTMARE.getNoiseSettings().getKey()));
	}

	@Override
	protected long getSeedOffset()
	{
		return 737;
	}

	@Override
	protected TerrainLayout getTerrainLayout()
	{
		return NIGHTMARE_LAYOUT;
	}

	@Override
	protected MapCodec<NightmareChunkGenerator> codec()
	{
		return NIGHTMARE_CODEC;
	}
}