package com.legacy.good_nights_sleep.world.chunkgen;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Predicate;

import javax.annotation.Nullable;

import com.legacy.good_nights_sleep.world.biome_provider.GNSBiomeSource;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderLookup;
import net.minecraft.world.level.LevelHeightAccessor;
import net.minecraft.world.level.NoiseColumn;
import net.minecraft.world.level.StructureManager;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.ChunkGeneratorStructureState;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.levelgen.NoiseBasedChunkGenerator;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;
import net.minecraft.world.level.levelgen.RandomState;
import net.minecraft.world.level.levelgen.WorldgenRandom;
import net.minecraft.world.level.levelgen.WorldgenRandom.Algorithm;
import net.minecraft.world.level.levelgen.blending.Blender;
import net.minecraft.world.level.levelgen.structure.StructureSet;
import net.minecraft.world.level.levelgen.synth.SimplexNoise;

public class GNSChunkGenerator extends NoiseBasedChunkGenerator
{
	public static final MapCodec<GNSChunkGenerator> GNS_CODEC = RecordCodecBuilder.mapCodec(instance ->
	{
		return instance.group(BiomeSource.CODEC.fieldOf("biome_source").forGetter(chunkGen ->
		{
			return chunkGen.biomeSource;
		}), NoiseGeneratorSettings.CODEC.fieldOf("settings").forGetter(chunkGen ->
		{
			return chunkGen.generatorSettings();
		})).apply(instance, instance.stable(GNSChunkGenerator::new));
	});

	public static final TerrainLayout DEFAULT_LAYOUT = new TerrainLayout(70.0D, 350.0D, 0.1D, 8.0D, 0.1D, 1, 1);

	protected static final BlockState AIR = Blocks.AIR.defaultBlockState();

	protected SimplexNoise noiseA = null, noiseB = null, noiseC = null;

	public GNSChunkGenerator(BiomeSource biomeProvider, Holder<NoiseGeneratorSettings> settings)
	{
		super(biomeProvider, settings);
	}

	@Override
	protected MapCodec<? extends GNSChunkGenerator> codec()
	{
		return GNS_CODEC;
	}

	@Override
	public ChunkGeneratorStructureState createState(HolderLookup<StructureSet> structureRegistry, RandomState rand, long serverSeed)
	{
		serverSeed = serverSeed + this.getSeedOffset();

		this.noiseA = new SimplexNoise(new WorldgenRandom(Algorithm.XOROSHIRO.newInstance(serverSeed)));
		this.noiseB = new SimplexNoise(new WorldgenRandom(Algorithm.XOROSHIRO.newInstance(serverSeed + 7)));
		this.noiseC = new SimplexNoise(new WorldgenRandom(Algorithm.XOROSHIRO.newInstance(serverSeed + 23)));

		if (this.biomeSource instanceof GNSBiomeSource b)
			b.setNoises(noiseA, noiseB, noiseC);

		return super.createState(structureRegistry, rand, serverSeed);
	}

	@Override
	public CompletableFuture<ChunkAccess> fillFromNoise(Blender blender, RandomState randomState, StructureManager structureManager, ChunkAccess chunkAccess)
	{
		BlockPos cpos = chunkAccess.getPos().getWorldPosition();
		int chunkWidth = 16;

		Heightmap oceanHM = chunkAccess.getOrCreateHeightmapUnprimed(Heightmap.Types.OCEAN_FLOOR_WG);
		Heightmap surfaceHM = chunkAccess.getOrCreateHeightmapUnprimed(Heightmap.Types.WORLD_SURFACE_WG);

		for (int x = 0; x < chunkWidth; ++x)
		{
			int posX = cpos.getX() + x;

			for (int z = 0; z < chunkWidth; ++z)
			{
				int posZ = cpos.getZ() + z;

				for (int posY = 0; posY <= 200; ++posY)
				{
					BlockState state = this.getStateAt(posX, posY, posZ);

					if (state != AIR)
					{
						BlockPos newPos = new BlockPos(x, posY, z);
						chunkAccess.setBlockState(newPos, state, false);
						oceanHM.update(x, posY, z, state);
						surfaceHM.update(x, posY, z, state);

						if (!state.getFluidState().isEmpty())
							chunkAccess.markPosForPostprocessing(newPos);
					}
				}
			}
		}

		return CompletableFuture.completedFuture(chunkAccess);
	}

	private BlockState getStateAt(int worldX, int worldY, int worldZ)
	{
		TerrainLayout layout = this.getTerrainLayout();
		BlockState state = AIR;

		float seaLevel = 63;
		if (worldY <= seaLevel)
			state = this.generatorSettings().value().defaultFluid();

		double roughScale = layout.terrainRoughness(),
				roughness = (this.noiseA.getValue(worldX / 60.0, worldZ / 60.0) + 1) / 2 * roughScale;

		double hillWideness = layout.hillWideness(), hillScaleDiff = 400;

		double hillDif = (this.noiseC.getValue(worldX / hillScaleDiff, worldZ / hillScaleDiff)) / 2 * 2;

		if (hillDif <= 0)
			hillDif = hillDif * layout.hillInversionScale();

		double hillScale = layout.hillScale() * hillDif;

		double hills = (this.noiseB.getValue(worldX / hillWideness, worldZ / hillWideness)) / 2 * hillScale;

		double groundAddition = roughness - (hills - (hillScale / 2));

		double roughPass1 = ((this.noiseA.getValue(worldX / 20.0, worldZ / 20.0)) * 20) / 4;
		double roughPass2 = ((this.noiseB.getValue(worldX / 80.0, worldZ / 80.0)) * 50) / 4;
		double roughPass3 = ((this.noiseC.getValue(worldX / 40.0, worldZ / 40.0)) * (20 + roughPass1 + roughPass2)) / 4;

		double highCuts = (this.noiseC.getValue(worldX / 60.0, worldZ / 60.0)) / 4;

		double blurScale = 0.03D;
		double plainsBlurring = (this.noiseC.getValue(worldX * blurScale, worldZ * blurScale)) * 0.1D;

		boolean isHill = hillDif >= 0 && hills < -0.5D;
		boolean isBlurredHill = isHill && hills >= 0 - (2 + (plainsBlurring * 4));

		roughPass3 *= layout.roughnessScale();

		// half the roughness in plains biomes so they stay flatter
		if (getBiomeNoise(this.noiseA, worldX, worldZ) <= plainsBlurring && (isBlurredHill && isHill || !isHill))
			roughPass3 *= 0.5F;

		// if it ends up succeeding in the check, it will take chunks of terrain and
		// make them go higher than normal, otherwise it will just add the roughness raw
		groundAddition += roughPass3 * ((highCuts > layout.highCutRange() ? 2 : 1) * layout.cutScale());

		double groundHeight = seaLevel + groundAddition;
		if (worldY < groundHeight)
			state = this.generatorSettings().value().defaultBlock();

		return state;
	}

	public static double getBiomeNoise(SimplexNoise noise, double worldX, double worldZ)
	{
		double biomeScale = 650.0D;
		return noise.getValue(worldX / biomeScale, worldZ / biomeScale);
	}

	protected TerrainLayout getTerrainLayout()
	{
		return DEFAULT_LAYOUT;
	}

	protected long getSeedOffset()
	{
		return 0L;
	}

	@Override
	public int getBaseHeight(int x, int z, Types pType, LevelHeightAccessor level, RandomState pRandom)
	{
		return level.getMinY() + this.iterateColumn(x, z, new ArrayList<>(80), pType.isOpaque(), level);
	}

	@Override
	public NoiseColumn getBaseColumn(int x, int z, LevelHeightAccessor level, RandomState randomState)
	{
		List<BlockState> states = new ArrayList<>(60);
		this.iterateColumn(x, z, states, null, level);
		return new NoiseColumn(level.getMinY(), states.toArray(BlockState[]::new));
	}

	private int iterateColumn(int worldX, int worldZ, List<BlockState> states, @Nullable Predicate<BlockState> stateTest, LevelHeightAccessor level)
	{
		int maxY = level.getMaxY();

		int worldY = level.getMinY();
		while (worldY < maxY)
		{
			BlockState state = this.getStateAt(worldX, worldY, worldZ);

			if (stateTest == null || stateTest.test(state))
			{
				states.add(state);
			}
			else
				break;

			worldY++;
		}

		return states.size();
	}

	@Override
	public void addDebugScreenInfo(List<String> info, RandomState rand, BlockPos pos)
	{
		TerrainLayout layout = this.getTerrainLayout();
		info.add("Hill Scale: " + layout.hillScale);
		info.add("Hill Wideness: " + layout.hillWideness);
		info.add("Hill Inversion Scale: " + layout.hillInversionScale);
		info.add("Terrain Roughness: " + layout.terrainRoughness);
		info.add("High Cut Range: " + layout.highCutRange);
		info.add("Cut Scale: " + layout.cutScale);
		info.add("Roughness Scale: " + layout.roughnessScale);
	}

	/*@Override
	public void applyCarvers(WorldGenRegion pLevel, long pSeed, RandomState pRandom, BiomeManager pBiomeManager, StructureManager pStructureManager, ChunkAccess pChunk, Carving pStep)
	{
	}*/

	/**
	 * @formatter:off
	 * 
	 * @field hillScale (Blocks tall a hill can be)
	 * @field hillWideness
	 * @field hillInversionScale (Depth functionally, how deep should "hills" be when they go under sea level)
	 * @field terrainRoughness
	 * @field highCutRange (Noise range the higher terrain cuts should be taken from. Smaller = more)
	 * @field cutScale (How much should the noise be multiplied within the cut range)
	 * @field roughnessScale (General scale on the roughness passes)
	 * 
	 * @formatter:on
	 */
	public record TerrainLayout(double hillScale, double hillWideness, double hillInversionScale, double terrainRoughness, double highCutRange, double cutScale, double roughnessScale)
	{
	}
}