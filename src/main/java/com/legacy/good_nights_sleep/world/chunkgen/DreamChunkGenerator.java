package com.legacy.good_nights_sleep.world.chunkgen;

import com.legacy.good_nights_sleep.registry.GNSDimensions;
import com.legacy.good_nights_sleep.world.biome_provider.DreamBiomeSource;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.RegistryOps;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;

public class DreamChunkGenerator extends GNSChunkGenerator
{
	public static final MapCodec<DreamChunkGenerator> DREAM_CODEC = RecordCodecBuilder.mapCodec(instance ->
	{
		return instance.group(RegistryOps.retrieveGetter(Registries.BIOME), RegistryOps.retrieveGetter(Registries.NOISE_SETTINGS)).apply(instance, DreamChunkGenerator::new);
	});

	public DreamChunkGenerator(HolderGetter<Biome> biomeRegistry, HolderGetter<NoiseGeneratorSettings> noiseSettingsRegistry)
	{
		super(new DreamBiomeSource(biomeRegistry), noiseSettingsRegistry.getOrThrow(GNSDimensions.DREAM.getNoiseSettings().getKey()));
	}

	@Override
	protected long getSeedOffset()
	{
		return 273;
	}
	
	@Override
	protected MapCodec<DreamChunkGenerator> codec()
	{
		return DREAM_CODEC;
	}
}