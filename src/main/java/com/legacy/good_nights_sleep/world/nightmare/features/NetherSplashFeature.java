package com.legacy.good_nights_sleep.world.nightmare.features;

import org.joml.SimplexNoise;

import com.mojang.serialization.Codec;

import net.minecraft.core.BlockPos;
import net.minecraft.tags.BlockTags;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.WorldGenLevel;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.FeaturePlaceContext;
import net.minecraft.world.level.levelgen.feature.configurations.NoneFeatureConfiguration;
import net.minecraft.world.phys.Vec3;

public class NetherSplashFeature extends Feature<NoneFeatureConfiguration>
{
	public NetherSplashFeature(Codec<NoneFeatureConfiguration> configFactoryIn)
	{
		super(configFactoryIn);
	}

	@Override
	public boolean place(FeaturePlaceContext<NoneFeatureConfiguration> context)
	{
		WorldGenLevel level = context.level();
		BlockPos origin = context.origin();
		Vec3 originVec = origin.getCenter();
		RandomSource rand = context.random();
		int radius = 8 + rand.nextInt(4);

		final Block netherrack = Blocks.NETHERRACK;

		for (int x = -radius; x <= radius; x++)
		{
			for (int y = -radius; y <= radius; y++)
			{
				for (int z = -radius; z <= radius; z++)
				{
					BlockPos pos = origin.offset(x, y, z);
					Vec3 angle = originVec.subtract(pos.getCenter()).normalize().add(originVec);
					float noiseScale = 0.4F;
					// 0-1 value
					float noise = (SimplexNoise.noise((float) angle.x * noiseScale, (float) angle.y * noiseScale, (float) angle.z * noiseScale) + 1.0F) / 2.0F;
					float dist = (float) Math.sqrt(pos.distSqr(origin));
					float r = radius * noise;

					if (dist <= r)
					{
						BlockState replace = level.getBlockState(pos);
						if (replace.is(BlockTags.SCULK_REPLACEABLE_WORLD_GEN))
						{
							Block block = null;
							if (dist >= r - 2.0F)
								block = null;
							else if (dist >= r - 3.5F)
								block = rand.nextFloat() < 0.5F ? null : netherrack;
							else
								block = netherrack;

							if (block != null)
							{
								level.setBlock(pos, block.defaultBlockState(), Block.UPDATE_ALL);

								if (block == netherrack && (dist < 2 && rand.nextFloat() < 0.6F || rand.nextFloat() < 0.2F) && level.isEmptyBlock(pos.above()))
									level.setBlock(pos.above(), Blocks.FIRE.defaultBlockState(), Block.UPDATE_ALL);
							}
						}
					}
				}
			}
		}

		return true;
	}
}
