package com.legacy.good_nights_sleep.world.biome_provider;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import com.legacy.good_nights_sleep.world.chunkgen.GNSChunkGenerator;
import com.mojang.datafixers.util.Pair;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.HolderSet;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.RandomSource;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeSource;
import net.minecraft.world.level.biome.Climate;
import net.minecraft.world.level.biome.Climate.Sampler;
import net.minecraft.world.level.levelgen.synth.SimplexNoise;

public abstract class GNSBiomeSource extends BiomeSource
{
	protected final HolderGetter<Biome> biomeRegistry;
	protected final Map<ResourceLocation, Holder<Biome>> allBiomes;

	protected SimplexNoise noiseA = null, noiseB = null, noiseC = null;

	public GNSBiomeSource(HolderGetter<Biome> biomeRegistry, HolderSet<Biome> biomes)
	{
		super();
		this.biomeRegistry = biomeRegistry;
		this.allBiomes = biomes.stream().collect(Collectors.toMap(h -> h.unwrapKey().orElseThrow(() -> new IllegalArgumentException("A biome in GNSBiomeSource did not have a ResourceKey")).location(), Function.identity()));
	}

	public HolderSet<Biome> biomeHolderSet()
	{
		return HolderSet.direct(List.copyOf(this.possibleBiomes()));
	}

	@Override
	protected Stream<Holder<Biome>> collectPossibleBiomes()
	{
		return this.allBiomes.values().stream();
	}

	@Override
	public Holder<Biome> getNoiseBiome(int x, int y, int z, @Nullable Climate.Sampler climateSampler)
	{
		float worldX = x * 4;
		float worldZ = z * 4;

		double hillWideness = this.getTerrainLayout().hillWideness(), hillScaleDiff = 400;
		double hillDif = (this.noiseC.getValue(worldX / hillScaleDiff, worldZ / hillScaleDiff)) / 2 * 2;

		if (hillDif <= 0)
			hillDif = hillDif * this.getTerrainLayout().hillInversionScale();

		double hillScale = this.getTerrainLayout().hillScale() * hillDif,
				hills = (this.noiseB.getValue(worldX / hillWideness, worldZ / hillWideness)) / 2 * hillScale;

		if (hillDif >= 0 && hills < -0.5D)
			return this.biomeRegistry.getOrThrow(this.getHill());

		double biomeNoise = GNSChunkGenerator.getBiomeNoise(this.noiseA, worldX, worldZ);

		return this.getDeepForest() != null && biomeNoise > 0.5 ? this.biomeRegistry.getOrThrow(this.getDeepForest()) : biomeNoise > 0 ? this.biomeRegistry.getOrThrow(this.getForest()) : this.biomeRegistry.getOrThrow(this.getPlains());

		/*return this.provider.getBiome(this.biomeRegistry, this.allBiomes, x * 4, y * 4, z * 4);*/
	}

	abstract ResourceKey<Biome> getHill();

	abstract ResourceKey<Biome> getPlains();

	abstract ResourceKey<Biome> getForest();

	@Nullable
	abstract ResourceKey<Biome> getDeepForest();

	public void setNoises(SimplexNoise noiseA, SimplexNoise noiseB, SimplexNoise noiseC)
	{
		this.noiseA = noiseA;
		this.noiseB = noiseB;
		this.noiseC = noiseC;
	}

	abstract GNSChunkGenerator.TerrainLayout getTerrainLayout();

	@Override
	@Nullable
	public Pair<BlockPos, Holder<Biome>> findBiomeHorizontal(int originX, int originY, int originZ, int range, int p_207824_, Predicate<Holder<Biome>> predicate, RandomSource rand, boolean p_207827_, Sampler climateSampler)
	{
		return this.possibleBiomes().stream().anyMatch(predicate::test) ? super.findBiomeHorizontal(originX, originY, originZ, range, p_207824_, predicate, rand, p_207827_, climateSampler) : null;
	}
}
