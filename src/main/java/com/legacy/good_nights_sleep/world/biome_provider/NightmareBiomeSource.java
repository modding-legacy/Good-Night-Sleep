package com.legacy.good_nights_sleep.world.biome_provider;

import java.util.List;

import com.legacy.good_nights_sleep.registry.GNSBiomes;
import com.legacy.good_nights_sleep.world.chunkgen.GNSChunkGenerator.TerrainLayout;
import com.legacy.good_nights_sleep.world.chunkgen.NightmareChunkGenerator;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.RegistryOps;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.biome.Biome;

public class NightmareBiomeSource extends GNSBiomeSource
{
	public static final MapCodec<NightmareBiomeSource> CODEC = RecordCodecBuilder.mapCodec(instance ->
	{
		return instance.group(RegistryOps.retrieveGetter(Registries.BIOME), Biome.LIST_CODEC.fieldOf("biomes").stable().forGetter(biomeSource -> biomeSource.biomeHolderSet())).apply(instance, NightmareBiomeSource::new);
	});

	public NightmareBiomeSource(HolderGetter<Biome> biomeRegistry)
	{
		this(biomeRegistry, HolderSet.direct(r -> biomeRegistry.getOrThrow(r.getKey()), List.of(GNSBiomes.NIGHTMARE_MOUND, GNSBiomes.SHAMEFUL_PLAINS, GNSBiomes.WASTED_FOREST)));
	}

	public NightmareBiomeSource(HolderGetter<Biome> biomeRegistry, HolderSet<Biome> biomes)
	{
		super(biomeRegistry, biomes);
	}

	@Override
	ResourceKey<Biome> getHill()
	{
		return GNSBiomes.NIGHTMARE_MOUND.getKey();
	}

	@Override
	ResourceKey<Biome> getPlains()
	{
		return GNSBiomes.SHAMEFUL_PLAINS.getKey();
	}

	@Override
	ResourceKey<Biome> getForest()
	{
		return GNSBiomes.WASTED_FOREST.getKey();
	}

	@Override
	ResourceKey<Biome> getDeepForest()
	{
		return null;
	}

	@Override
	TerrainLayout getTerrainLayout()
	{
		return NightmareChunkGenerator.NIGHTMARE_LAYOUT;
	}

	@Override
	protected MapCodec<NightmareBiomeSource> codec()
	{
		return CODEC;
	}
}
