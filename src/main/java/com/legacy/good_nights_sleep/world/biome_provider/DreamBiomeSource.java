package com.legacy.good_nights_sleep.world.biome_provider;

import java.util.List;

import com.legacy.good_nights_sleep.registry.GNSBiomes;
import com.legacy.good_nights_sleep.world.chunkgen.GNSChunkGenerator;
import com.legacy.good_nights_sleep.world.chunkgen.GNSChunkGenerator.TerrainLayout;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.Registries;
import net.minecraft.resources.RegistryOps;
import net.minecraft.resources.ResourceKey;
import net.minecraft.world.level.biome.Biome;

public class DreamBiomeSource extends GNSBiomeSource
{
	public static final MapCodec<DreamBiomeSource> CODEC = RecordCodecBuilder.mapCodec(instance ->
	{
		return instance.group(RegistryOps.retrieveGetter(Registries.BIOME), Biome.LIST_CODEC.fieldOf("biomes").stable().forGetter(biomeSource -> biomeSource.biomeHolderSet())).apply(instance, DreamBiomeSource::new);
	});

	public DreamBiomeSource(HolderGetter<Biome> biomeRegistry)
	{
		this(biomeRegistry, HolderSet.direct(r -> biomeRegistry.getOrThrow(r.getKey()), List.of(GNSBiomes.SLEEPY_HILLS, GNSBiomes.DREAMY_FOREST, GNSBiomes.GOOD_DREAM_PLAINS, GNSBiomes.LOLLIPOP_LANDS)));
	}

	public DreamBiomeSource(HolderGetter<Biome> biomeRegistry, HolderSet<Biome> biomes)
	{
		super(biomeRegistry, biomes);
	}

	@Override
	ResourceKey<Biome> getHill()
	{
		return GNSBiomes.SLEEPY_HILLS.getKey();
	}

	@Override
	ResourceKey<Biome> getPlains()
	{
		return GNSBiomes.GOOD_DREAM_PLAINS.getKey();
	}

	@Override
	ResourceKey<Biome> getForest()
	{
		return GNSBiomes.DREAMY_FOREST.getKey();
	}

	@Override
	ResourceKey<Biome> getDeepForest()
	{
		return GNSBiomes.LOLLIPOP_LANDS.getKey();
	}

	@Override
	TerrainLayout getTerrainLayout()
	{
		return GNSChunkGenerator.DEFAULT_LAYOUT;
	}

	@Override
	protected MapCodec<DreamBiomeSource> codec()
	{
		return CODEC;
	}
}
