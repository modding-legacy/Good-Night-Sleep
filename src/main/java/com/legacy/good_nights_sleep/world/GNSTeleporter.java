package com.legacy.good_nights_sleep.world;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction.Axis;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.Heightmap.Types;
import net.minecraft.world.level.portal.TeleportTransition;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.common.CommonHooks;
import net.neoforged.neoforge.server.ServerLifecycleHooks;

public class GNSTeleporter
{
	public static void changeDimension(ResourceKey<Level> type, Entity entity, BlockPos pos)
	{
		MinecraftServer server = ServerLifecycleHooks.getCurrentServer();

		if (server == null)
			return;

		ResourceKey<Level> transferDimension = entity.level().dimension() == type ? Level.OVERWORLD : type;
		ServerLevel transferWorld = server.getLevel(transferDimension);

		if (!CommonHooks.onTravelToDimension(entity, transferDimension))
			return;

		Entity teleportedEntity = teleportEntity(entity, transferWorld, pos);

		teleportedEntity.fallDistance = 0.0F;
	}

	private static Entity teleportEntity(Entity entity, ServerLevel transferWorld, BlockPos pos)
	{
		transferWorld.getBlockState(pos);
		var height = transferWorld.getHeightmapPos(Types.MOTION_BLOCKING_NO_LEAVES, pos).getY();
		Vec3 endpointPos = Vec3.atBottomCenterOf(pos).with(Axis.Y, height);

		var transition = new TeleportTransition(transferWorld, endpointPos, Vec3.ZERO, entity.getYRot(), entity.getXRot(), TeleportTransition.DO_NOTHING.then(e -> e.placePortalTicket(pos)));

		if (entity instanceof ServerPlayer player)
		{
			// if (transferWorld.dimension() != Level.OVERWORLD)
			for (int x = -1; x < 2; ++x)
			{
				for (int z = -1; z < 2; ++z)
				{
					BlockPos newPos = BlockPos.containing(endpointPos.add(x, -1, z));
					if (transferWorld.getBlockState(newPos).getBlock() == Blocks.LAVA || transferWorld.getBlockState(newPos).getBlock() == Blocks.LAVA)
						transferWorld.setBlockAndUpdate(newPos, Blocks.GRASS_BLOCK.defaultBlockState());
				}
			}

			entity.teleport(transition);

			return player;
		}

		entity.unRide();
		entity.teleport(transition);

		return entity;
	}
}
