package com.legacy.good_nights_sleep.item;

import java.util.List;
import java.util.stream.Collectors;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.registry.GNSBlocks;
import com.legacy.good_nights_sleep.registry.GNSItems;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.CreativeModeTabs;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SpawnEggItem;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.util.Lazy;
import net.neoforged.neoforge.event.BuildCreativeModeTabContentsEvent;
import net.neoforged.neoforge.registries.RegisterEvent;

@EventBusSubscriber(modid = GoodNightSleep.MODID, bus = Bus.MOD)
public class GNSCreativeTabs
{
	public static final Lazy<List<Item>> SPAWN_EGGS = Lazy.of(() -> BuiltInRegistries.ITEM.stream().filter(item -> BuiltInRegistries.ITEM.getKey(item).getNamespace().equals(GoodNightSleep.MODID) && item instanceof SpawnEggItem).sorted((o, n) -> BuiltInRegistries.ITEM.getKey(o).compareTo(BuiltInRegistries.ITEM.getKey(n))).collect(Collectors.toList()));

	@SubscribeEvent
	public static void modifyExisting(BuildCreativeModeTabContentsEvent event)
	{
		if (event.getTabKey() == CreativeModeTabs.SPAWN_EGGS)
		{
			for (Item item : SPAWN_EGGS.get())
				event.accept(item);
		}
	}

	@SubscribeEvent
	public static void init(RegisterEvent event)
	{
		// @formatter:off
		event.register(Registries.CREATIVE_MODE_TAB, GoodNightSleep.locate("all_items"), () -> CreativeModeTab.builder().icon(() -> GNSItems.strange_bed.getDefaultInstance()).title(Component.translatable("advancements.good_nights_sleep.root.title"))
				.displayItems((params, output) ->
				{
					output.accept(GNSItems.strange_bed);
					output.accept(GNSItems.luxurious_bed);
					output.accept(GNSItems.wretched_bed);

					output.accept(GNSBlocks.dream_sapling);
					output.accept(GNSBlocks.dream_leaves);
					output.accept(GNSBlocks.diamond_leaves);
					output.accept(GNSBlocks.dream_log);
					output.accept(GNSBlocks.dream_wood);
					output.accept(GNSBlocks.stripped_dream_log);
					output.accept(GNSBlocks.stripped_dream_wood);
					output.accept(GNSBlocks.dream_planks);
					output.accept(GNSBlocks.dream_stairs);
					output.accept(GNSBlocks.dream_slab);
					output.accept(GNSBlocks.dream_fence);
					output.accept(GNSBlocks.dream_fence_gate);
					output.accept(GNSItems.dream_door);
					output.accept(GNSBlocks.dream_trapdoor);
					output.accept(GNSBlocks.dream_pressure_plate);
					output.accept(GNSBlocks.dream_button);
					output.accept(GNSBlocks.dream_sign);
					output.accept(GNSBlocks.dream_hanging_sign);
					
					output.accept(GNSBlocks.candy_sapling);
					output.accept(GNSBlocks.candy_leaves);
					output.accept(GNSBlocks.white_log);
					output.accept(GNSBlocks.white_wood);
					output.accept(GNSBlocks.stripped_white_log);
					output.accept(GNSBlocks.stripped_white_wood);
					output.accept(GNSBlocks.white_planks);
					output.accept(GNSBlocks.white_stairs);
					output.accept(GNSBlocks.white_slab);
					output.accept(GNSBlocks.white_fence);
					output.accept(GNSBlocks.white_fence_gate);
					output.accept(GNSItems.white_door);
					output.accept(GNSBlocks.white_trapdoor);
					output.accept(GNSBlocks.white_pressure_plate);
					output.accept(GNSBlocks.white_button);
					output.accept(GNSBlocks.white_sign);
					output.accept(GNSBlocks.white_hanging_sign);

					output.accept(GNSBlocks.dead_log);
					output.accept(GNSBlocks.dead_wood);
					output.accept(GNSBlocks.stripped_dead_log);
					output.accept(GNSBlocks.stripped_dead_wood);
					output.accept(GNSBlocks.dead_planks);
					output.accept(GNSBlocks.dead_stairs);
					output.accept(GNSBlocks.dead_slab);
					output.accept(GNSBlocks.dead_fence);
					output.accept(GNSBlocks.dead_fence_gate);
					output.accept(GNSItems.dead_door);
					output.accept(GNSBlocks.dead_trapdoor);
					output.accept(GNSBlocks.dead_pressure_plate);
					output.accept(GNSBlocks.dead_button);
					output.accept(GNSBlocks.dead_sign);
					output.accept(GNSBlocks.dead_hanging_sign);

					output.accept(GNSBlocks.blood_log);
					output.accept(GNSBlocks.blood_wood);
					output.accept(GNSBlocks.stripped_blood_log);
					output.accept(GNSBlocks.stripped_blood_wood);
					output.accept(GNSBlocks.blood_planks);
					output.accept(GNSBlocks.blood_stairs);
					output.accept(GNSBlocks.blood_slab);
					output.accept(GNSBlocks.blood_fence);
					output.accept(GNSBlocks.blood_fence_gate);
					output.accept(GNSItems.blood_door);
					output.accept(GNSBlocks.blood_trapdoor);
					output.accept(GNSBlocks.blood_pressure_plate);
					output.accept(GNSBlocks.blood_button);
					output.accept(GNSBlocks.blood_sign);
					output.accept(GNSBlocks.blood_hanging_sign);

					output.accept(GNSBlocks.short_dream_grass);
					output.accept(GNSBlocks.dream_grass_block);
					output.accept(GNSBlocks.dream_dirt);
					output.accept(GNSBlocks.dream_farmland);
					output.accept(GNSBlocks.short_nightmare_grass);
					output.accept(GNSBlocks.prickly_nightmare_grass);
					output.accept(GNSBlocks.nightmare_grass_block);

					output.accept(GNSBlocks.cyan_flower);
					output.accept(GNSBlocks.orange_flower);
					output.accept(GNSBlocks.lollipop_bush);
					output.accept(GNSBlocks.dead_flower);
					output.accept(GNSBlocks.hope_mushroom);
					output.accept(GNSBlocks.hope_mushroom_block);
					output.accept(GNSBlocks.despair_mushroom);
					output.accept(GNSBlocks.despair_mushroom_block);

					output.accept(GNSBlocks.delusion_stone);
					output.accept(GNSBlocks.delusion_stone_stairs);
					output.accept(GNSBlocks.delusion_stone_slab);
					output.accept(GNSBlocks.delusion_pressure_plate);
					output.accept(GNSBlocks.delusion_button);
					output.accept(GNSBlocks.delusion_cobblestone);
					output.accept(GNSBlocks.delusion_cobblestone_stairs);
					output.accept(GNSBlocks.delusion_cobblestone_slab);
					output.accept(GNSBlocks.delusion_cobblestone_wall);
					output.accept(GNSBlocks.delusion_stone_bricks);
					output.accept(GNSBlocks.delusion_stone_brick_stairs);
					output.accept(GNSBlocks.delusion_stone_brick_slab);
					output.accept(GNSBlocks.delusion_stone_brick_wall);

					output.accept(GNSBlocks.pot_of_gold);
					output.accept(GNSBlocks.present);

					output.accept(GNSItems.rainbow_seeds);
					output.accept(GNSItems.rainbow_berries);
					output.accept(GNSItems.lollipop);
					output.accept(GNSItems.powdered_sugar);
					output.accept(GNSItems.necrotic_extract);
					output.accept(GNSItems.luxurious_soup);
					output.accept(GNSItems.wretched_soup);

					output.accept(GNSItems.candy);
					output.accept(GNSItems.raw_spectrite);
					output.accept(GNSItems.spectrite_ingot);
					output.accept(GNSItems.positite);
					output.accept(GNSItems.necrum);
					output.accept(GNSItems.raw_zitrite);
					output.accept(GNSItems.zitrite_ingot);
					output.accept(GNSItems.negatite);

					output.accept(GNSBlocks.candy_ore);
					output.accept(GNSBlocks.hard_candy_block);
					output.accept(GNSBlocks.spectrite_ore);
					output.accept(GNSBlocks.raw_spectrite_block);
					output.accept(GNSBlocks.spectrite_block);
					output.accept(GNSBlocks.positite_ore);
					output.accept(GNSBlocks.positite_block);
					output.accept(GNSBlocks.fossilized_necrum);
					output.accept(GNSBlocks.necrum_block);
					output.accept(GNSBlocks.zitrite_ore);
					output.accept(GNSBlocks.raw_zitrite_block);
					output.accept(GNSBlocks.zitrite_block);
					output.accept(GNSBlocks.negatite_ore);
					output.accept(GNSBlocks.negatite_block);

					output.accept(GNSBlocks.delusion_coal_ore);
					output.accept(GNSBlocks.delusion_lapis_ore);

					output.accept(GNSItems.candy_sword);
					output.accept(GNSItems.candy_shovel);
					output.accept(GNSItems.candy_pickaxe);
					output.accept(GNSItems.candy_axe);
					output.accept(GNSItems.candy_hoe);
					
					output.accept(GNSItems.spectrite_sword);
					output.accept(GNSItems.spectrite_shovel);
					output.accept(GNSItems.spectrite_pickaxe);
					output.accept(GNSItems.spectrite_axe);
					output.accept(GNSItems.spectrite_hoe);
					
					output.accept(GNSItems.positite_sword);
					output.accept(GNSItems.positite_shovel);
					output.accept(GNSItems.positite_pickaxe);
					output.accept(GNSItems.positite_axe);
					output.accept(GNSItems.positite_hoe);
					
					output.accept(GNSItems.necrum_sword);
					output.accept(GNSItems.necrum_shovel);
					output.accept(GNSItems.necrum_pickaxe);
					output.accept(GNSItems.necrum_axe);
					output.accept(GNSItems.necrum_hoe);
					
					output.accept(GNSItems.zitrite_sword);
					output.accept(GNSItems.zitrite_shovel);
					output.accept(GNSItems.zitrite_pickaxe);
					output.accept(GNSItems.zitrite_axe);
					output.accept(GNSItems.zitrite_hoe);
					
					output.accept(GNSItems.negatite_sword);
					output.accept(GNSItems.negatite_shovel);
					output.accept(GNSItems.negatite_pickaxe);
					output.accept(GNSItems.negatite_axe);
					output.accept(GNSItems.negatite_hoe);
					
					output.accept(GNSItems.candy_helmet);
					output.accept(GNSItems.candy_chestplate);
					output.accept(GNSItems.candy_leggings);
					output.accept(GNSItems.candy_boots);
					
					output.accept(GNSItems.spectrite_helmet);
					output.accept(GNSItems.spectrite_chestplate);
					output.accept(GNSItems.spectrite_leggings);
					output.accept(GNSItems.spectrite_boots);
					
					output.accept(GNSItems.positite_helmet);
					output.accept(GNSItems.positite_chestplate);
					output.accept(GNSItems.positite_leggings);
					output.accept(GNSItems.positite_boots);
					
					/*output.accept(GNSItems.necrum_helmet);
					output.accept(GNSItems.necrum_chestplate);
					output.accept(GNSItems.necrum_leggings);
					output.accept(GNSItems.necrum_boots);*/
					
					output.accept(GNSItems.zitrite_helmet);
					output.accept(GNSItems.zitrite_chestplate);
					output.accept(GNSItems.zitrite_leggings);
					output.accept(GNSItems.zitrite_boots);
					
					output.accept(GNSItems.negatite_helmet);
					output.accept(GNSItems.negatite_chestplate);
					output.accept(GNSItems.negatite_leggings);
					output.accept(GNSItems.negatite_boots);

					for (Item item : SPAWN_EGGS.get())
						output.accept(item);
				}).build());
		// @formatter:on
	}

	/*private static void insertAfter(BuildCreativeModeTabContentsEvent event, List<ItemLike> items, ItemLike target)
	{
		ItemStack currentStack = null;
	
		for (ItemStack e : event.getParentEntries())
		{
			if (e.getItem() == target)
			{
				currentStack = e;
				break;
			}
		}
	
		for (var item : items)
			event.insertAfter(currentStack, currentStack = item.asItem().getDefaultInstance(), TabVisibility.PARENT_AND_SEARCH_TABS);
	}*/

	/*protected static void printMissingItems(BuildCreativeModeTabContentsEvent event)
	{
		try
		{
			Set<Item> items = new HashSet<>();
	
			for (var e : event.getEntries())
				items.add(e.getKey().getItem());
	
			List<Item> missing = BuiltInRegistries.ITEM.stream().filter(block -> BuiltInRegistries.ITEM.getKey(block).getNamespace().equals(GoodNightSleep.MODID) && !items.contains(block)).collect(Collectors.toList());
	
			for (var e : missing)
				System.out.println(BuiltInRegistries.ITEM.getKey(e));
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}*/
}
