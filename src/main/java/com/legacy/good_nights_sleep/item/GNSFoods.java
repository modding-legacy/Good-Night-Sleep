package com.legacy.good_nights_sleep.item;

import net.minecraft.world.food.FoodProperties;

public class GNSFoods
{

	public static final FoodProperties RAINBOW_BERRIES = (new FoodProperties.Builder()).nutrition(6).saturationModifier(0.6F).build();

	public static final FoodProperties CANDY = (new FoodProperties.Builder()).nutrition(2).saturationModifier(0.2F).build();
	public static final FoodProperties LOLLIPOP = (new FoodProperties.Builder()).nutrition(3).saturationModifier(0.2F).build();

	public static final FoodProperties SLEEPY_STEW = (new FoodProperties.Builder()).nutrition(6).saturationModifier(0.6F).alwaysEdible().build();
}