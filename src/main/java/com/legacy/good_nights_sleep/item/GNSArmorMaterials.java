package com.legacy.good_nights_sleep.item;

import java.util.EnumMap;

import com.legacy.good_nights_sleep.data.GNSItemTags;
import com.legacy.good_nights_sleep.registry.GNSEquipmentAssets;
import com.legacy.good_nights_sleep.registry.GNSSounds;

import net.minecraft.Util;
import net.minecraft.world.item.equipment.ArmorMaterial;
import net.minecraft.world.item.equipment.ArmorType;

public interface GNSArmorMaterials
{
	ArmorMaterial CANDY = new ArmorMaterial(7, Util.make(new EnumMap<>(ArmorType.class), values ->
	{
		values.put(ArmorType.BOOTS, 1);
		values.put(ArmorType.LEGGINGS, 2);
		values.put(ArmorType.CHESTPLATE, 2);
		values.put(ArmorType.HELMET, 1);
		values.put(ArmorType.BODY, 3);
	}), 5, GNSSounds.ITEM_ARMOR_EQUIP_CANDY, 0.0F, 0.0F, GNSItemTags.CANDY_MATERIALS, GNSEquipmentAssets.CANDY);

	ArmorMaterial SPECTRITE = new ArmorMaterial(22, Util.make(new EnumMap<>(ArmorType.class), values ->
	{
		values.put(ArmorType.BOOTS, 2);
		values.put(ArmorType.LEGGINGS, 5);
		values.put(ArmorType.CHESTPLATE, 6);
		values.put(ArmorType.HELMET, 2);
		values.put(ArmorType.BODY, 5);
	}), 14, GNSSounds.ITEM_ARMOR_EQUIP_SPECTRITE, 0.0F, 0.0F, GNSItemTags.SPECTRITE_INGOTS, GNSEquipmentAssets.SPECTRITE);

	ArmorMaterial POSITITE = new ArmorMaterial(49, Util.make(new EnumMap<>(ArmorType.class), values ->
	{
		values.put(ArmorType.BOOTS, 3);
		values.put(ArmorType.LEGGINGS, 6);
		values.put(ArmorType.CHESTPLATE, 8);
		values.put(ArmorType.HELMET, 3);
		values.put(ArmorType.BODY, 11);
	}), 10, GNSSounds.ITEM_ARMOR_EQUIP_POSITITE, 2.0F, 0.0F, GNSItemTags.POSITITE, GNSEquipmentAssets.POSITITE);

	/*ArmorMaterial NECRUM = new ArmorMaterial(5, Util.make(new EnumMap<>(ArmorType.class), values ->
	{
		values.put(ArmorType.BOOTS, 2);
		values.put(ArmorType.LEGGINGS, 3);
		values.put(ArmorType.CHESTPLATE, 3);
		values.put(ArmorType.HELMET, 2);
		values.put(ArmorType.BODY, 6);
	}), 2, GNSSounds.ITEM_ARMOR_EQUIP_NECRUM, 0.0F, 0.0F, GNSItemTags.NECRUM, GNSEquipmentAssets.NECRUM);*/

	ArmorMaterial ZITRITE = new ArmorMaterial(18, Util.make(new EnumMap<>(ArmorType.class), values ->
	{
		values.put(ArmorType.BOOTS, 2);
		values.put(ArmorType.LEGGINGS, 5);
		values.put(ArmorType.CHESTPLATE, 6);
		values.put(ArmorType.HELMET, 3);
		values.put(ArmorType.BODY, 9);
	}), 3, GNSSounds.ITEM_ARMOR_EQUIP_ZITRITE, 0.0F, 0.15F, GNSItemTags.ZITRITE_INGOTS, GNSEquipmentAssets.ZITRITE);

	ArmorMaterial NEGATITE = new ArmorMaterial(29, Util.make(new EnumMap<>(ArmorType.class), values ->
	{
		values.put(ArmorType.BOOTS, 3);
		values.put(ArmorType.LEGGINGS, 6);
		values.put(ArmorType.CHESTPLATE, 8);
		values.put(ArmorType.HELMET, 3);
		values.put(ArmorType.BODY, 11);
	}), 1, GNSSounds.ITEM_ARMOR_EQUIP_NEGATITE, 6.0F, 0.0F, GNSItemTags.NEGATITE, GNSEquipmentAssets.NEGATITE);
}