package com.legacy.good_nights_sleep.item;

import java.util.List;

import com.legacy.good_nights_sleep.capabillity.DreamPlayer;
import com.legacy.good_nights_sleep.registry.GNSDimensions;

import net.minecraft.ChatFormatting;
import net.minecraft.network.chat.Component;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.TooltipFlag;
import net.minecraft.world.level.Level;

public class SleepyStewItem extends Item
{
	private final boolean isEvil;

	public SleepyStewItem(Properties props, boolean isEvil)
	{
		super(props);
		this.isEvil = isEvil;
	}

	@Override
	public ItemStack finishUsingItem(ItemStack stack, Level level, LivingEntity entity)
	{
		if (!level.isClientSide && entity instanceof Player player && GNSDimensions.inSleepDimension(entity))
		{
			DreamPlayer dp = DreamPlayer.get(player);

			if (!this.isEvil && level.dimension().equals(GNSDimensions.dreamKey()) || this.isEvil && level.dimension().equals(GNSDimensions.nightmareKey()))
				dp.setEnteredDreamTime(level.getGameTime());
			else
				dp.setEnteredDreamTime(dp.getEnteredDreamTime() - 25200L);

			// dp.getEnteredDreamTime() - 4000L

			dp.syncDataToClient();
		}

		return super.finishUsingItem(stack, level, entity);
	}

	@Override
	public void appendHoverText(ItemStack stack, Item.TooltipContext context, List<Component> tooltipComponents, TooltipFlag tooltipFlag)
	{
		tooltipComponents.add(Component.translatable(stack.getItem().getDescriptionId() + ".description").withStyle(ChatFormatting.GRAY));	
	}
}
