package com.legacy.good_nights_sleep.item;

import com.legacy.good_nights_sleep.data.GNSItemTags;

import net.minecraft.tags.BlockTags;
import net.minecraft.world.item.ToolMaterial;

public interface GNSToolMaterials
{
	ToolMaterial CANDY = new ToolMaterial(BlockTags.INCORRECT_FOR_WOODEN_TOOL, 196, 4.0F, 1.0F, 5, GNSItemTags.CANDY_MATERIALS);
	ToolMaterial SPECTRITE = new ToolMaterial(BlockTags.INCORRECT_FOR_IRON_TOOL, 375, 6.0F, 2.0F, 14, GNSItemTags.SPECTRITE_INGOTS);
	ToolMaterial POSITITE = new ToolMaterial(BlockTags.INCORRECT_FOR_DIAMOND_TOOL, 2341, 8.0F, 3.0F, 10, GNSItemTags.POSITITE);

	ToolMaterial NECRUM = new ToolMaterial(BlockTags.INCORRECT_FOR_WOODEN_TOOL, 131, 6.0F, 2.0F, 1, GNSItemTags.NECRUM);
	ToolMaterial ZITRITE = new ToolMaterial(BlockTags.INCORRECT_FOR_DIAMOND_TOOL, 975, 8.0F, 2.5F, 10, GNSItemTags.ZITRITE_INGOTS);
	ToolMaterial NEGATITE = new ToolMaterial(BlockTags.INCORRECT_FOR_NETHERITE_TOOL, 1100, 9.0F, 4.0F, 1, GNSItemTags.NEGATITE);
}