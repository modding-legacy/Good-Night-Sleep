package com.legacy.good_nights_sleep.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.good_nights_sleep.registry.GNSDimensions;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.util.Mth;

@Mixin(ClientLevel.class)
public class ClientLevelMixin
{
	/**
	 * Simpler than making a whole sky renderer just for this minor aesthetic
	 * change.
	 */
	@SuppressWarnings("resource")
	@Inject(at = @At("RETURN"), method = "getStarBrightness", cancellable = true)
	private void getStarBrightness(float partialTick, CallbackInfoReturnable<Float> callback)
	{
		ClientLevel self = ((ClientLevel) (Object) this);
		if (GNSDimensions.inSleepDimension(self.dimension()))
			callback.setReturnValue(self.dimension().equals(GNSDimensions.nightmareKey()) ? Mth.clamp(callback.getReturnValue() + 0.5F, 0, 1) : 1.0F);
	}
}
