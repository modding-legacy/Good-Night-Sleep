package com.legacy.good_nights_sleep.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.good_nights_sleep.client.GNSClientEvents;
import com.legacy.good_nights_sleep.registry.GNSDimensions;

import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.world.level.LevelTimeAccess;

@Mixin(LevelTimeAccess.class)
public interface LevelTimeAccessMixin
{
	/**
	 * Exists in place of a sky renderer, which wouldn't fully cause the desired
	 * visual. If anyone has a problem with this, do mention it, and suggest a
	 * solution.
	 */
	@Inject(at = @At("HEAD"), method = "getTimeOfDay", cancellable = true)
	private void getTimeOfDay(float partialTick, CallbackInfoReturnable<Float> callback)
	{
		LevelTimeAccess self = ((LevelTimeAccess) (Object) this);
		if (self instanceof ClientLevel && GNSDimensions.hasDreamEffects(self.dimensionType().effectsLocation()))
			callback.setReturnValue(GNSClientEvents.calculateSunAngle(self.dayTime(), partialTick));
	}
}
