package com.legacy.good_nights_sleep.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import com.legacy.good_nights_sleep.client.GNSBlockColoring;

import net.minecraft.client.renderer.BiomeColors;
import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockAndTintGetter;

@Mixin(BiomeColors.class)
public class BiomeColorsMixin
{
	@Inject(at = @At("HEAD"), method = "getAverageGrassColor", cancellable = true)
	private static void getAverageGrassColor(BlockAndTintGetter pLevel, BlockPos pBlockPos, CallbackInfoReturnable<Integer> callback)
	{
		GNSBlockColoring.getVanillaGrassColor(pLevel, pBlockPos, callback);
	}
}
