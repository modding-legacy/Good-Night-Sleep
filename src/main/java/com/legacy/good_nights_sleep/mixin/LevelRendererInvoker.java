package com.legacy.good_nights_sleep.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

import net.minecraft.client.Camera;
import net.minecraft.client.renderer.LevelRenderer;

@Mixin(LevelRenderer.class)
public interface LevelRendererInvoker
{
	@Invoker("doesMobEffectBlockSky(Lnet/minecraft/client/Camera;)Z")
	abstract boolean gns$doesMobEffectBlockSky(Camera camera);
}
