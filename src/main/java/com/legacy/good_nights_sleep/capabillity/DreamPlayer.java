package com.legacy.good_nights_sleep.capabillity;

import com.legacy.good_nights_sleep.network.PacketHandler;
import com.legacy.good_nights_sleep.network.SyncPlayerPayload;
import com.legacy.good_nights_sleep.registry.GNSDimensions;
import com.legacy.good_nights_sleep.registry.GNSRegistry;
import com.legacy.good_nights_sleep.world.GNSTeleporter;

import net.minecraft.core.BlockPos;
import net.minecraft.core.HolderLookup;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.network.codec.StreamCodec;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.neoforge.attachment.IAttachmentHolder;
import net.neoforged.neoforge.attachment.IAttachmentSerializer;

public class DreamPlayer
{
	private final Player player;
	private long enteredGameTime = 0L;

	public DreamPlayer(Player player)
	{
		this.player = player;
	}

	public static DreamPlayer get(Player player)
	{
		return player.getData(GNSRegistry.PLAYER_ATTACHMENT);
	}

	public void serverTick()
	{
		if (this.player instanceof ServerPlayer sp)
		{
			long worldTime = sp.level().getGameTime() - this.getEnteredDreamTime();

			if (worldTime > 25000L && GNSDimensions.inSleepDimension(player))
			{
				if (sp.level() instanceof ServerLevel sl)
				{
					// try for bed spawn, otherwise go to the world spawn
					BlockPos pos = sp.getRespawnPosition() != null ? sp.getRespawnPosition() : sl.getSharedSpawnPos();
					GNSTeleporter.changeDimension(Level.OVERWORLD, sp, pos);
				}
			}
			else if (!GNSDimensions.inSleepDimension(player))
				this.setEnteredDreamTime(sp.level().getGameTime());
		}
	}

	@OnlyIn(Dist.CLIENT)
	public void clientTick()
	{
		/*if (player != null && player instanceof net.minecraft.client.entity.player.ClientPlayerEntity && player.getEntityWorld().isRemote())
		{
		}*/
	}

	public Player getPlayer()
	{
		return this.player;
	}

	public long getEnteredDreamTime()
	{
		return this.enteredGameTime;
	}

	public void setEnteredDreamTime(long timeIn)
	{
		this.enteredGameTime = timeIn;
	}

	public void onDeath()
	{
	}

	public void syncDataToClient()
	{
		if (this.player instanceof ServerPlayer s)
			PacketHandler.sendToClient(new SyncPlayerPayload(this), s);
	}

	public void copyFrom(DreamPlayer player)
	{
		this.setEnteredDreamTime(player.getEnteredDreamTime());
	}

	public static class Serializer implements IAttachmentSerializer<CompoundTag, DreamPlayer>
	{
		public static final Serializer INSTANCE = new Serializer();

		static final String ENTERED_TIME_KEY = "EnteredDreamTime";

		@Override
		public CompoundTag write(DreamPlayer attachment, HolderLookup.Provider provider)
		{
			CompoundTag compound = new CompoundTag();
			compound.putLong(ENTERED_TIME_KEY, attachment.getEnteredDreamTime());
			return compound;
		}

		@Override
		public DreamPlayer read(IAttachmentHolder holder, CompoundTag compound, HolderLookup.Provider provider)
		{
			DreamPlayer attachment = new DreamPlayer((Player) holder);
			attachment.setEnteredDreamTime(compound.getLong(ENTERED_TIME_KEY));
			return attachment;
		}
	}

	public static final StreamCodec<RegistryFriendlyByteBuf, DreamPlayer> STREAM_CODEC = StreamCodec.of(DreamPlayer::writeToStream, DreamPlayer::createFromStream);

	private static void writeToStream(RegistryFriendlyByteBuf buffer, DreamPlayer player)
	{
		CompoundTag tag = DreamPlayer.Serializer.INSTANCE.write(player, null);
		buffer.writeNbt(tag);
	}

	public static DreamPlayer createFromStream(RegistryFriendlyByteBuf buffer)
	{
		DreamPlayer attachment = DreamPlayer.Serializer.INSTANCE.read(null, buffer.readNbt(), null);
		return attachment;
	}
}
