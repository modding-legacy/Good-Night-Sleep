package com.legacy.good_nights_sleep.entity;

import com.legacy.good_nights_sleep.registry.GNSEntityTypes;
import com.legacy.good_nights_sleep.registry.GNSSounds;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.monster.Zombie;
import net.minecraft.world.level.Level;

public class TormenterEntity extends Zombie
{
	public TormenterEntity(EntityType<? extends TormenterEntity> type, Level level)
	{
		super(type, level);
	}

	public TormenterEntity(Level level)
	{
		this(GNSEntityTypes.TORMENTER, level);
	}

	public static AttributeSupplier.Builder registerAttributes()
	{
		return TormenterEntity.createAttributes().add(Attributes.MAX_HEALTH, 30.0D);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return GNSSounds.ENTITY_TORMENTER_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return GNSSounds.ENTITY_TORMENTER_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return GNSSounds.ENTITY_TORMENTER_DEATH;
	}

	@Override
	public boolean doHurtTarget(ServerLevel level, Entity entity)
	{
		if (entity instanceof LivingEntity living && random.nextBoolean() && !living.hasEffect(MobEffects.BLINDNESS) && !living.hasEffect(MobEffects.CONFUSION))
		{
			this.playSound(GNSSounds.ENTITY_TORMENTER_TORMENT, 0.5F, 1.0F);
			((LivingEntity) entity).addEffect(new MobEffectInstance(MobEffects.BLINDNESS, 80));
			((LivingEntity) entity).addEffect(new MobEffectInstance(MobEffects.CONFUSION, 160));

			if (this.random.nextBoolean())
				((LivingEntity) entity).addEffect(new MobEffectInstance(MobEffects.WITHER, 60, level.getDifficulty().getId()));
		}

		return super.doHurtTarget(level, entity);
	}
}
