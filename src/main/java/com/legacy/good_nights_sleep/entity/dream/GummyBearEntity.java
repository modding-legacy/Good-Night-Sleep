package com.legacy.good_nights_sleep.entity.dream;

import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;

public class GummyBearEntity extends Animal
{
	public GummyBearEntity(EntityType<? extends GummyBearEntity> type, Level level)
	{
		super(type, level);
	}

	@Override
	public AgeableMob getBreedOffspring(ServerLevel level, AgeableMob otherParent)
	{
		return null;
	}

	@Override
	public boolean isFood(ItemStack stack)
	{
		// TODO Auto-generated method stub
		return false;
	}
}