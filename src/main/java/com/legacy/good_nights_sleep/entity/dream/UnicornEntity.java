package com.legacy.good_nights_sleep.entity.dream;

import javax.annotation.Nullable;

import com.legacy.good_nights_sleep.registry.GNSEntityTypes;
import com.legacy.good_nights_sleep.registry.GNSSounds;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.util.RandomSource;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.AgeableMob;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityDimensions;
import net.minecraft.world.entity.EntitySpawnReason;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.SpawnGroupData;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.animal.horse.AbstractHorse;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.SoundType;
import net.minecraft.world.phys.Vec3;

public class UnicornEntity extends AbstractHorse
{
	public static final EntityDataAccessor<Integer> UNICORN_TYPE = SynchedEntityData.<Integer>defineId(UnicornEntity.class, EntityDataSerializers.INT);

	public UnicornEntity(EntityType<? extends UnicornEntity> type, Level level)
	{
		super(type, level);
	}

	@Override
	protected void defineSynchedData(SynchedEntityData.Builder builder)
	{
		super.defineSynchedData(builder);
		builder.define(UNICORN_TYPE, 0);
	}

	@Override
	public SpawnGroupData finalizeSpawn(ServerLevelAccessor level, DifficultyInstance difficulty, EntitySpawnReason reason, @Nullable SpawnGroupData data)
	{
		this.setUnicornType(level.getRandom().nextInt(4));
		return super.finalizeSpawn(level, difficulty, reason, data);
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	public void addAdditionalSaveData(CompoundTag compound)
	{
		super.addAdditionalSaveData(compound);
		compound.putInt("UnicornType", this.getUnicornType());
	}

	@Override
	public void load(CompoundTag compound)
	{
		super.load(compound);
		this.setUnicornType(compound.getInt("UnicornType"));
	}

	@Override
	protected void randomizeAttributes(RandomSource rand)
	{
		this.getAttribute(Attributes.MAX_HEALTH).setBaseValue((double) generateMaxHealth(rand::nextInt));
		this.getAttribute(Attributes.MOVEMENT_SPEED).setBaseValue(generateSpeed(rand::nextDouble));
		this.getAttribute(Attributes.JUMP_STRENGTH).setBaseValue(generateJumpStrength(rand::nextDouble));
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return GNSSounds.ENTITY_UNICORN_IDLE;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSourceIn)
	{
		return GNSSounds.ENTITY_UNICORN_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return GNSSounds.ENTITY_UNICORN_DEATH;
	}

	@Override
	protected SoundEvent getAngrySound()
	{
		return GNSSounds.ENTITY_UNICORN_ANGRY;
	}

	@Override
	protected SoundEvent getEatingSound()
	{
		return GNSSounds.ENTITY_UNICORN_EAT;
	}

	@Override
	protected void playGallopSound(SoundType soundType)
	{
		super.playGallopSound(soundType);

		if (this.random.nextInt(10) == 0)
			this.playSound(GNSSounds.ENTITY_UNICORN_BREATHE, soundType.getVolume() * 0.6F, soundType.getPitch());
	}

	@Override
	protected Vec3 getPassengerAttachmentPoint(Entity entity, EntityDimensions dimensions, float partialTicks)
	{
		return super.getPassengerAttachmentPoint(entity, dimensions, partialTicks).subtract(0, 0.2F, 0);
	}

	@Override
	public InteractionResult mobInteract(Player player, InteractionHand hand)
	{
		boolean flag = !this.isBaby() && this.isTamed() && player.isSecondaryUseActive();

		if (!this.isVehicle() && !flag)
		{
			ItemStack itemstack = player.getItemInHand(hand);
			if (!itemstack.isEmpty())
			{
				if (this.isFood(itemstack))
					return this.fedFood(player, itemstack);

				if (!this.isTamed())
				{
					this.makeMad();
					return InteractionResult.SUCCESS;
				}
			}

			return super.mobInteract(player, hand);
		}

		return super.mobInteract(player, hand);
	}

	@Override
	public boolean canMate(Animal otherAnimal)
	{
		if (otherAnimal == this || !(otherAnimal instanceof UnicornEntity unicorn))
			return false;

		return this.canParent() && unicorn.canParent();
	}

	@Nullable
	@Override
	public AgeableMob getBreedOffspring(ServerLevel level, AgeableMob otherParent)
	{
		UnicornEntity mate = (UnicornEntity) otherParent;
		UnicornEntity child = GNSEntityTypes.UNICORN.create(level, EntitySpawnReason.BREEDING);

		if (child != null)
		{
			int aType = this.getUnicornType();
			int bType = mate.getUnicornType();

			// TODO: Mix types where possible
			child.setUnicornType(child.getRandom().nextBoolean() ? bType : aType);
			this.setOffspringAttributes(otherParent, child);

			return child;
		}

		return null;
	}

	public void setUnicornType(int type)
	{
		this.entityData.set(UNICORN_TYPE, type);
	}

	public int getUnicornType()
	{
		return this.entityData.get(UNICORN_TYPE);
	}
}