package com.legacy.good_nights_sleep.entity.dream;

import com.legacy.good_nights_sleep.registry.GNSSounds;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.Difficulty;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.level.Level;

public class BabyCreeperEntity extends Creeper
{
	public BabyCreeperEntity(EntityType<? extends BabyCreeperEntity> type, Level level)
	{
		super(type, level);
		this.explosionRadius = level.getDifficulty() == Difficulty.PEACEFUL ? 0 : 2;

		this.maxSwell = 20;
	}

	@Override
	protected boolean shouldDespawnInPeaceful()
	{
		return this.explosionRadius > 0;
	}

	@Override
	public void tick()
	{
		super.tick();
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource damageSource)
	{
		return GNSSounds.ENTITY_BABY_CREEPER_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return GNSSounds.ENTITY_BABY_CREEPER_DEATH;
	}
}
