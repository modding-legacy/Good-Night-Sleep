package com.legacy.good_nights_sleep.entity;

import com.legacy.good_nights_sleep.registry.GNSSounds;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.tags.FluidTags;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.phys.Vec3;
import net.neoforged.neoforge.event.EventHooks;
import net.neoforged.neoforge.event.entity.EntityTeleportEvent;

public class HerobrineEntity extends Monster
{
	public HerobrineEntity(EntityType<? extends HerobrineEntity> type, Level worldIn)
	{
		super(type, worldIn);
	}

	@Override
	protected void registerGoals()
	{
		super.registerGoals();

		this.goalSelector.addGoal(0, new FloatGoal(this));
		this.goalSelector.addGoal(2, new MeleeAttackGoal(this, 1.0D, false));
		this.goalSelector.addGoal(7, new WaterAvoidingRandomStrollGoal(this, 0.8D, 0.0F));
		this.goalSelector.addGoal(8, new LookAtPlayerGoal(this, Player.class, 127.0F, 99999.0F));
		this.goalSelector.addGoal(8, new RandomLookAroundGoal(this));
		this.targetSelector.addGoal(1, new HurtByTargetGoal(this));
	}

	public static AttributeSupplier.Builder registerAttributes()
	{
		return Monster.createMonsterAttributes().add(Attributes.FOLLOW_RANGE, 64.0D).add(Attributes.MOVEMENT_SPEED, 0.3F).add(Attributes.ATTACK_DAMAGE, 7.0D).add(Attributes.MAX_HEALTH, 80.0D);
	}

	@Override
	protected SoundEvent getAmbientSound()
	{
		return null;
	}

	@Override
	protected SoundEvent getHurtSound(DamageSource source)
	{
		return GNSSounds.ENTITY_HEROBRINE_HURT;
	}

	@Override
	protected SoundEvent getDeathSound()
	{
		return GNSSounds.ENTITY_HEROBRINE_DEATH;
	}
	
	@Override
	public void tick()
	{
		super.tick();
	}
	
	@Override
	protected void customServerAiStep(ServerLevel level)
	{
		super.customServerAiStep(level);
	}

	@Override
	public boolean hurtServer(ServerLevel level, DamageSource source, float amount)
	{
		if (this.isInvulnerableTo(level, source))
		{
			return false;
		}
		else if (!source.is(DamageTypeTags.IS_PROJECTILE))
		{
			boolean flag = super.hurtServer(level, source, amount);

			if (source.is(DamageTypeTags.BYPASSES_ARMOR) && this.random.nextInt(10) != 0)
				this.teleportRandomly();

			return flag;
		}
		else
		{
			for (int i = 0; i < 64; ++i)
			{
				if (this.teleportRandomly())
				{
					return true;
				}
			}

			return false;
		}
	}

	protected boolean teleportRandomly()
	{
		double d0 = this.getX() + (this.random.nextDouble() - 0.5D) * 64.0D;
		double d1 = this.getY() + (double) (this.random.nextInt(64) - 32);
		double d2 = this.getZ() + (this.random.nextDouble() - 0.5D) * 64.0D;
		return this.teleportToPos(d0, d1, d2);
	}

	@SuppressWarnings("unused")
	private boolean teleportToEntity(Entity p_70816_1_)
	{
		Vec3 vec = new Vec3(this.getX() - p_70816_1_.getX(), this.getBoundingBox().minY + (double) (this.getBbHeight() / 2.0F) - p_70816_1_.getY() + (double) p_70816_1_.getEyeHeight(), this.getZ() - p_70816_1_.getZ());
		vec = vec.normalize();
		double d0 = 16.0D;
		double d1 = this.getX() + (this.random.nextDouble() - 0.5D) * 8.0D - vec.x * 16.0D;
		double d2 = this.getY() + (double) (this.random.nextInt(16) - 8) - vec.y * 16.0D;
		double d3 = this.getZ() + (this.random.nextDouble() - 0.5D) * 8.0D - vec.z * 16.0D;
		return this.teleportToPos(d1, d2, d3);
	}

	@SuppressWarnings("deprecation")
	private boolean teleportToPos(double x, double y, double z)
	{
		BlockPos.MutableBlockPos tpPos = new BlockPos.MutableBlockPos(x, y, z);

		while (tpPos.getY() > this.level().getMinY() && !this.level().getBlockState(tpPos).blocksMotion())
		{
			tpPos.move(Direction.DOWN);
		}

		BlockState blockstate = this.level().getBlockState(tpPos);
		boolean flag = blockstate.blocksMotion();
		boolean flag1 = blockstate.getFluidState().is(FluidTags.WATER);
		if (flag && !flag1)
		{
			EntityTeleportEvent.EnderEntity event = EventHooks.onEnderTeleport(this, x, y, z);
			if (event.isCanceled())
				return false;
			Vec3 vec3 = this.position();
			boolean flag2 = this.randomTeleport(event.getTargetX(), event.getTargetY(), event.getTargetZ(), true);
			if (flag2)
			{
				this.level().gameEvent(GameEvent.TELEPORT, vec3, GameEvent.Context.of(this));
				if (!this.isSilent())
				{
					this.level().playSound(null, this.xo, this.yo, this.zo, GNSSounds.ENTITY_HEROBRINE_TELEPORT, this.getSoundSource(), 1.0F, 1.0F);
					this.playSound(GNSSounds.ENTITY_HEROBRINE_TELEPORT, 1.0F, 1.0F);
				}
			}

			return flag2;
		}
		else
		{
			return false;
		}
	}
}
