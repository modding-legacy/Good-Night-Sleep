package com.legacy.good_nights_sleep.data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.registry.GNSBlocks;
import com.legacy.good_nights_sleep.registry.GNSItems;
import com.legacy.good_nights_sleep.registry.GNSSounds;

import net.minecraft.Util;
import net.minecraft.advancements.AdvancementHolder;
import net.minecraft.client.KeyMapping;
import net.minecraft.core.Holder;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.Registry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.contents.TranslatableContents;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.world.damagesource.DamageType;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.ai.attributes.Attribute;
import net.minecraft.world.entity.decoration.PaintingVariant;
import net.minecraft.world.entity.npc.VillagerProfession;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.neoforged.neoforge.common.data.LanguageProvider;
import net.neoforged.neoforge.common.util.Lazy;

public class GNSLangProv extends LanguageProvider
{
	private final CompletableFuture<HolderLookup.Provider> lookup;

	public GNSLangProv(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookup)
	{
		super(packOutput, GoodNightSleep.MODID, "en_us");
		this.lookup = lookup;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void addTranslations()
	{		
		this.add("death.attack." + GoodNightSleep.MODID + ":prickly_nightmare_grass", "%1$s was pricked to death by nightmare grass");
		this.add("death.attack." + GoodNightSleep.MODID + ":prickly_nightmare_grass.player", "%1$s was pricked to death by nightmare grass while trying to escape %2$s");

		this.add(GNSItems.luxurious_soup.getDescriptionId() + ".description", "Enhances sleep while dreaming, but wakes from nightmares");
		this.add(GNSItems.wretched_soup.getDescriptionId() + ".description", "Intensifies sleep during nightmares, but wakes from dreams");

		String mm = "sounds.musicmanager.good_nights_sleep.music.";
		this.add(mm + "good_dream", "tippyfoo - Good Dream");
		this.add(mm + "sky_blue", "tippyfoo - Sky Blue");
		this.add(mm + "tfarcenim", "tippyfoo - Tfarcenim");

		Set<Block> ignoredBlocks = new HashSet<>();

		// futureproofing
		String blockBase = "block." + GoodNightSleep.MODID + ".";
		this.add(blockBase + "dream_chest", "Dream Chest");
		this.add(blockBase + "white_chest", "White Chest");
		this.add(blockBase + "dead_chest", "Dead Chest");
		this.add(blockBase + "blood_chest", "Blood Chest");

		this.add(blockBase + "dream_ladder", "Dream Ladder");
		this.add(blockBase + "white_ladder", "White Ladder");
		this.add(blockBase + "dead_ladder", "Dead Ladder");
		this.add(blockBase + "blood_ladder", "Blood Ladder");

		Map<Block, String> blockOverrides = Util.make(new HashMap<>(), m ->
		{
			m.put(GNSBlocks.hard_candy_block, "Block of Hard Candy");
			m.put(GNSBlocks.spectrite_block, "Block of Spectrite");
			m.put(GNSBlocks.positite_block, "Block of Positite");
			m.put(GNSBlocks.necrum_block, "Block of Necrum");
			m.put(GNSBlocks.zitrite_block, "Block of Zitrite");
			m.put(GNSBlocks.negatite_block, "Block of Negatite");

			m.put(GNSBlocks.raw_spectrite_block, "Block of Raw Spectrite");
			m.put(GNSBlocks.raw_zitrite_block, "Block of Raw Zitrite");

			m.put(GNSBlocks.delusion_button, "Delusion Stone Button");
			m.put(GNSBlocks.delusion_pressure_plate, "Delusion Stone Pressure Plate");

			for (var ignored : ignoredBlocks)
				m.put(ignored, "");
		});

		this.addDefault(Registries.BLOCK, blockOverrides.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().builtInRegistryHolder().key().location(), Map.Entry::getValue)));

		// this.addDefault(Registries.STRUCTURE, Map.of());

		// futureproofing
		this.add("item.good_nights_sleep.gummy_bear_spawn_egg", "Gummy Bear Spawn Egg");

		Set<Item> ignoredItems = new HashSet<>();

		Map<Item, String> itemOverrides = Util.make(new HashMap<>(), m ->
		{
			// m.put(GNSItems.aaa, "");

			for (var ignored : ignoredItems)
				m.put(ignored, "");
		});

		this.addDefault(Registries.ITEM, itemOverrides.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().builtInRegistryHolder().key().location(), Map.Entry::getValue)));

		Map<EntityType<?>, String> entityOverrides = Util.make(new HashMap<>(), m ->
		{
		});

		// futureproofing
		this.add("entity.good_nights_sleep.gummy_bear", "Gummy Bear");

		this.addDefault(Registries.ENTITY_TYPE, entityOverrides.entrySet().stream().collect(Collectors.toMap(e -> e.getKey().builtInRegistryHolder().key().location(), Map.Entry::getValue)));
		this.addDefault(Registries.BIOME, Map.of());
		this.addDefault(Registries.CHUNK_GENERATOR, Map.of());
		// this.addDefault(Registries.ENCHANTMENT, Map.of());

		/*this.addDefault(Registries.MOB_EFFECT, Map.of());
		this.addDefault(Registries.TRIM_PATTERN, Map.of());
		this.addDefault(Registries.TRIM_MATERIAL, Map.of());*/

		this.addAdvancement(GNSAdvancementProv.root, "Good Night's Sleep", "A Dreamy Mod");
		this.addAdvancement(GNSAdvancementProv.enterDream, "Rainbows and Unicorns!", "Have a Good Dream");
		this.addAdvancement(GNSAdvancementProv.killBabyCreeper, "How Could You...", "Kill a Baby Creeper");
		this.addAdvancement(GNSAdvancementProv.obtainSpectriteIngot, "Prismatic!", "Obtain a Spectrite Ingot");
		this.addAdvancement(GNSAdvancementProv.obtainPositite, "Positively Delightful!", "Obtain a Positite");
		this.addAdvancement(GNSAdvancementProv.obtainPotOfGold, "The End of the Rainbow", "Craft a Pot of Gold");
		this.addAdvancement(GNSAdvancementProv.obtainPresent, "You Shouldn't Have!", "Find a way to pick up a Present");
		this.addAdvancement(GNSAdvancementProv.tameUnicorn, "It's So Fluffy!", "Tame a Unicorn");
		this.addAdvancement(GNSAdvancementProv.eatRainbowBerries, "Taste of the Rainbow!", "Grow and eat Rainbow Berries");

		this.addAdvancement(GNSAdvancementProv.enterNightmare, "A Night of Torment", "Have a Nightmare");
		this.addAdvancement(GNSAdvancementProv.killTormenter, "Face Your Fears", "Kill a Tormenter");
		this.addAdvancement(GNSAdvancementProv.killHerobrine, "Removed Herobrine", "Slay Herobrine");
		this.addAdvancement(GNSAdvancementProv.obtainZitriteIngot, "Black Metal", "Obtain a Zitrite Ingot");
		this.addAdvancement(GNSAdvancementProv.obtainZitriteSword, "Really Edgy", "Craft a Zitrite Sword");
		this.addAdvancement(GNSAdvancementProv.obtainNegatite, "Nightmare Fuel", "Obtain a Negatite");

		this.add(GNSSounds.BLOCK_POT_OF_GOLD_USE, "Pot of Gold splashes");

		this.add(GNSSounds.BLOCK_RAINBOW_APPEAR, "Rainbow shimmers");

		this.add(GNSSounds.ENTITY_BABY_CREEPER_HURT, "Baby Creeper hurts");
		this.add(GNSSounds.ENTITY_BABY_CREEPER_DEATH, "Baby Creeper dies");

		this.add(GNSSounds.ENTITY_UNICORN_IDLE, "Unicorn neighs");
		this.add(GNSSounds.ENTITY_UNICORN_HURT, "Unicorn hurts");
		this.add(GNSSounds.ENTITY_UNICORN_DEATH, "Unicorn dies");
		this.add(GNSSounds.ENTITY_UNICORN_ANGRY, "Unicorn rears");
		this.add(GNSSounds.ENTITY_UNICORN_EAT, "Unicorn eats");
		this.add(GNSSounds.ENTITY_UNICORN_BREATHE, "Unicorn huffs");

		this.add(GNSSounds.ENTITY_TORMENTER_IDLE, "Tormenter echos");
		this.add(GNSSounds.ENTITY_TORMENTER_HURT, "Tormenter hurts");
		this.add(GNSSounds.ENTITY_TORMENTER_DEATH, "Tormenter dies");
		this.add(GNSSounds.ENTITY_TORMENTER_TORMENT, "Tormenter torments");

		this.add(GNSSounds.ENTITY_HEROBRINE_HURT, "Herobrine hurts");
		this.add(GNSSounds.ENTITY_HEROBRINE_DEATH, "Herobrine dies");
		this.add(GNSSounds.ENTITY_HEROBRINE_TELEPORT, "Herobrine teleports");

		this.add(GNSSounds.ITEM_ARMOR_EQUIP_CANDY, "Candy armor shuffles");
		this.add(GNSSounds.ITEM_ARMOR_EQUIP_SPECTRITE, "Spectrite armor clangs");
		this.add(GNSSounds.ITEM_ARMOR_EQUIP_POSITITE, "Positite armor clashes");
		this.add(GNSSounds.ITEM_ARMOR_EQUIP_ZITRITE, "Zitrite armor clangs");
		this.add(GNSSounds.ITEM_ARMOR_EQUIP_NEGATITE, "Negatite armor clashes");

		this.add(GoodNightSleep.MODID + ".configuration.section.good.nights.sleep.server.toml.title", "Good Night's Sleep World Configuration");
		this.add(GoodNightSleep.MODID + ".configuration.section.good.nights.sleep.server.toml", "World Settings");

		this.serverConf("allow_nightmare_phantoms", "Allow Nightmare Phantoms", "Allows Phantoms to spawn in the Nightmare dimension when applicable.");
		this.serverConf("limit_sleep_time", "Limit Sleep Time", "Limits use of the Luxurious, Wretched and Strange beds to night time like normal beds. This does not prevent them from following other rules beds follow.");
	}

	public void serverConf(String key, String title, String desc)
	{
		String serverConf = GoodNightSleep.MODID + ".server.";
		this.add(serverConf.concat(key), title);
		this.add(serverConf.concat(key + ".tooltip"), desc);
	}

	public void addKeybind(Lazy<KeyMapping> key, String translation)
	{
		this.add(key.get().getName(), translation);
	}

	public void addPainting(ResourceKey<PaintingVariant> painting, String title, String author)
	{
		ResourceLocation key = painting.location();
		String s = "painting." + key.getNamespace() + "." + key.getPath() + ".";
		this.add(s + "title", title);
		this.add(s + "author", author);
	}

	public void addAttribute(Supplier<Attribute> attribute, String name)
	{
		this.add(attribute.get().getDescriptionId(), name);
	}

	public void addPotion(Supplier<MobEffect> potion)
	{
		ResourceLocation key = BuiltInRegistries.MOB_EFFECT.getKey(potion.get());
		String path = key.getPath();
		String name = this.toName(path);
		this.add("item.minecraft.potion.effect." + path, "Potion of " + name);
		this.add("item.minecraft.splash_potion.effect." + path, "Splash Potion of " + name);
		this.add("item.minecraft.lingering_potion.effect." + path, "Lingering Potion of " + name);
		this.add("item.minecraft.tipped_arrow.effect." + path, "Arrow of " + name);
	}

	private void addDamageType(ResourceKey<DamageType> damageType, String deathMessage, @Nullable String playerKillMessage)
	{
		try
		{
			String messageID = "death.attack." + this.lookup.get().lookupOrThrow(Registries.DAMAGE_TYPE).getOrThrow(damageType).value().msgId();
			this.add(messageID, deathMessage);

			if (playerKillMessage != null && !playerKillMessage.isBlank())
				this.add(messageID + ".player", playerKillMessage);
		}
		catch (InterruptedException | ExecutionException e)
		{
			e.printStackTrace();
		}
	}

	private void addAdvancement(AdvancementHolder advancement, String title, String desc)
	{
		advancement.value().display().ifPresent(display ->
		{
			this.add(display.getTitle().getString(), title);
			this.add(display.getDescription().getString(), desc);
		});
	}

	private <T> void addDefault(ResourceKey<Registry<T>> registry, Map<ResourceLocation, String> overrides)
	{
		this.addDefault_(registry, overrides.entrySet().stream().collect(Collectors.toMap(e -> ResourceKey.create(registry, e.getKey()), e -> e.getValue())));
	}

	private <T> void addDefault_(ResourceKey<Registry<T>> registry, Map<ResourceKey<T>, String> overrides)
	{
		try
		{
			this.lookup.get().lookupOrThrow(registry).listElementIds().distinct().filter(key -> GoodNightSleep.MODID.equals(key.location().getNamespace())).filter(key ->
			{
				return !overrides.containsKey(key) && !(registry.location().equals(Registries.ITEM.location()) && BuiltInRegistries.ITEM.getValue(key.location()) instanceof BlockItem bi && (bi.getDescriptionId().startsWith("block")));
			}).forEach(this::add);
		}
		catch (InterruptedException | ExecutionException e)
		{
			e.printStackTrace();
		}
		overrides.forEach(this::add);
	}

	private void add(ResourceKey<?> key)
	{
		// temp brick thing, remove when we actually remap that stuff
		this.add(key, this.toName(key).replace("Stonebrick ", "Stone Brick ").replace("Stonebrick", "Stone Bricks"));
	}

	private void add(ResourceKey<?> key, String translation)
	{
		this.add(this.makeDescriptionID(key), translation);
	}

	private void add(Component key, String translation)
	{
		if (key.getContents() instanceof TranslatableContents trans)
			this.add(trans.getKey(), translation);
		else
			LOGGER.error("Tried to add non translatable component to GNS lang prov. Contents: {}", translation);
	}

	private void add(Supplier<SoundEvent> sound, String translation)
	{
		this.add(sound.get(), translation);
	}

	private void add(Holder<SoundEvent> sound, String translation)
	{
		this.add(sound.value(), translation);
	}

	private void add(SoundEvent sound, String translation)
	{
		this.add("subtitles." + GoodNightSleep.MODID + "." + sound.location().getPath(), translation);
	}

	private void addItemInfo(Supplier<Item> item, String key, String translation)
	{
		var resourceKey = BuiltInRegistries.ITEM.getResourceKey(item.get()).get();
		ResourceLocation location = resourceKey.location();
		this.add(Util.makeDescriptionId(resourceKey.registry().getPath().replace('/', '.'), ResourceLocation.fromNamespaceAndPath(location.getNamespace(), location.getPath() + "." + key)), translation);
	}

	// Converts camel case to a proper name. snowy_temple -> Snowy Temple
	private String toName(ResourceKey<?> key)
	{
		String suffix;
		if (key.registry().equals(Registries.TRIM_MATERIAL.location()))
			suffix = " Material";
		else if (key.registry().equals(Registries.TRIM_PATTERN.location()))
			suffix = " Armor Trim";
		else
			suffix = "";

		return this.toName(key.location().getPath()) + suffix;
	}

	private String toName(String key)
	{
		String[] words = key.split("_");
		for (int i = words.length - 1; i > -1; i--)
		{
			if (!words[i].equals("of")) // don't capitalize "of"
				words[i] = words[i].substring(0, 1).toUpperCase(Locale.ENGLISH) + words[i].substring(1).toLowerCase(Locale.ENGLISH);
		}
		return String.join(" ", words);
	}

	private String makeDescriptionID(ResourceKey<?> resourceKey)
	{
		String registryPath = resourceKey.registry().getPath();
		if (registryPath.equals("custom_stat"))
			registryPath = "stat";
		return Util.makeDescriptionId(registryPath.replace('/', '.').replace("worldgen.", ""), resourceKey.location()).replace("entity_type", "entity").replace("mob_effect", "effect");
	}

	private Set<String> existing = new HashSet<>();

	public void add(String key, String value)
	{
		if (!value.isBlank() && existing.add(key))
			super.add(key, value);
	}

	public void add(String key, Lazy<VillagerProfession> profession, String value)
	{
		ResourceLocation profLoc = BuiltInRegistries.VILLAGER_PROFESSION.getKey(profession.get());
		this.add(key + "." + profLoc.getNamespace() + "." + profLoc.getPath(), value);
	}
}
