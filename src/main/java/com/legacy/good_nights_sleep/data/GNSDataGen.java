package com.legacy.good_nights_sleep.data;

import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.registry.GNSBiomeModifiers;
import com.legacy.good_nights_sleep.registry.GNSBiomes;
import com.legacy.good_nights_sleep.registry.GNSDimensions;
import com.legacy.good_nights_sleep.registry.GNSFeatures;

import net.minecraft.DetectedVersion;
import net.minecraft.core.RegistrySetBuilder;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.data.metadata.PackMetadataGenerator;
import net.minecraft.network.chat.Component;
import net.minecraft.server.packs.PackType;
import net.minecraft.server.packs.metadata.pack.PackMetadataSection;
import net.minecraft.util.InclusiveRange;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.data.BlockTagsProvider;
import net.neoforged.neoforge.common.data.DatapackBuiltinEntriesProvider;
import net.neoforged.neoforge.data.event.GatherDataEvent;
import net.neoforged.neoforge.registries.NeoForgeRegistries;

@EventBusSubscriber(modid = GoodNightSleep.MODID, bus = Bus.MOD)
public class GNSDataGen
{
	// @formatter:off
	private static final RegistrySetBuilder BUILDER = new RegistrySetBuilder()
			.add(Registries.DIMENSION_TYPE, GNSDimensions::bootstrapDimType)
			.add(Registries.NOISE_SETTINGS, GNSDimensions::bootstrapNoiseSettings)
			.add(Registries.LEVEL_STEM, GNSDimensions::bootstrapLevelStem)
			.add(Registries.CONFIGURED_FEATURE, GNSFeatures.Configured::bootstrap)
			.add(Registries.PLACED_FEATURE, GNSFeatures.Placements::bootstrap)
			.add(Registries.BIOME, GNSBiomes::bootstrap)
			.add(NeoForgeRegistries.Keys.BIOME_MODIFIERS, GNSBiomeModifiers::bootstrap);
	// @formatter:on

	@SubscribeEvent
	public static void gatherData(GatherDataEvent.Client event)
	{
		DataGenerator gen = event.getGenerator();
		PackOutput output = gen.getPackOutput();
		boolean run = true;

		GNSFeatures.Configured.init();
		GNSFeatures.Placements.init();

		DatapackBuiltinEntriesProvider provider = new DatapackBuiltinEntriesProvider(gen.getPackOutput(), event.getLookupProvider(), BUILDER, Set.of(GoodNightSleep.MODID));
		;
		var lookup = provider.getRegistryProvider();
		gen.addProvider(run, provider);

		BlockTagsProvider blockTagProv = new GNSTagProv.BlockTagProv(gen, lookup);
		gen.addProvider(run, blockTagProv);
		gen.addProvider(run, new GNSTagProv.ItemTagProv(gen, blockTagProv.contentsGetter(), lookup));
		gen.addProvider(run, new GNSTagProv.BiomeTagProv(gen, lookup));

		/*gen.addProvider(server, new GNSTagProv.EntityTagProv(gen, lookup));
		gen.addProvider(server, new GNSTagProv.StructureTagProv(gen, lookup));
		gen.addProvider(server, new GNSTagProv.BiomeTagProv(gen, lookup));
		gen.addProvider(server, new GNSTagProv.PoiTagProv(gen, lookup));
		gen.addProvider(server, new GNSTagProv.DamageTypeProv(output, lookup));*/

		gen.addProvider(run, new GNSRecipeProv.Runner(output, lookup));

		gen.addProvider(run, new GNSAdvancementProv(gen, event.getLookupProvider()));
		gen.addProvider(run, new GNSLootProv(gen, event.getLookupProvider()));

		gen.addProvider(run, new GNSDataMapProv(output, lookup));

		gen.addProvider(run, new GNSModelProv(output));
		gen.addProvider(run, new GNSModelProv.Equipment(output));

		gen.addProvider(run, packMcmeta(output, "Good Night's Sleep resources"));
		gen.addProvider(run, new GNSLangProv(output, lookup));
		gen.addProvider(run, new GNSSoundProv(output));

		PackOutput legacyPackOutput = gen.getPackOutput("assets/good_nights_sleep/legacy_pack");
		gen.addProvider(run, packMcmeta(legacyPackOutput, "Good Night's Sleep's old textures from pre-1.14"));
	}

	private static final DataProvider packMcmeta(PackOutput output, String description)
	{
		int serverVersion = DetectedVersion.BUILT_IN.getPackVersion(PackType.SERVER_DATA);
		return NestedDataProvider.of(new PackMetadataGenerator(output).add(PackMetadataSection.TYPE, new PackMetadataSection(Component.literal(description), serverVersion, Optional.of(new InclusiveRange<>(0, Integer.MAX_VALUE)))), description);
	}

	private record NestedDataProvider<D extends DataProvider>(D provider, String namePrefix) implements DataProvider
	{

		public static <D extends DataProvider> NestedDataProvider<D> of(D provider, String namePrefix)
		{
			return new NestedDataProvider<>(provider, namePrefix);
		}

		@Override
		public CompletableFuture<?> run(CachedOutput cachedOutput)
		{
			return this.provider.run(cachedOutput);
		}

		@Override
		public String getName()
		{
			return this.namePrefix + "/" + this.provider.getName();
		}
	}
}
