package com.legacy.good_nights_sleep.data;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.registry.GNSBlocks;
import com.legacy.good_nights_sleep.registry.GNSItems;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.HolderSet;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.PackOutput;
import net.minecraft.data.recipes.RecipeCategory;
import net.minecraft.data.recipes.RecipeOutput;
import net.minecraft.data.recipes.RecipeProvider;
import net.minecraft.data.recipes.ShapedRecipeBuilder;
import net.minecraft.data.recipes.ShapelessRecipeBuilder;
import net.minecraft.data.recipes.SimpleCookingRecipeBuilder;
import net.minecraft.data.recipes.SingleItemRecipeBuilder;
import net.minecraft.data.recipes.packs.VanillaRecipeProvider;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.AbstractCookingRecipe;
import net.minecraft.world.item.crafting.BlastingRecipe;
import net.minecraft.world.item.crafting.CampfireCookingRecipe;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.SmeltingRecipe;
import net.minecraft.world.item.crafting.SmokingRecipe;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.SlabBlock;
import net.neoforged.neoforge.common.Tags;

@SuppressWarnings("unused")
public class GNSRecipeProv extends VanillaRecipeProvider
{
	public static class Runner extends RecipeProvider.Runner
	{
		public Runner(PackOutput output, CompletableFuture<HolderLookup.Provider> registries)
		{
			super(output, registries);
		}

		@Override
		protected RecipeProvider createRecipeProvider(HolderLookup.Provider lookup, RecipeOutput output)
		{
			return new GNSRecipeProv(lookup, output);
		}

		@Override
		public String getName()
		{
			return GoodNightSleep.NAME + " Recipe Gen";
		}
	}

	private String hasItem = "has_item";

	private HolderGetter<Item> items;

	private GNSRecipeProv(HolderLookup.Provider lookupProvider, RecipeOutput output)
	{
		super(lookupProvider, output);
		this.items = lookupProvider.lookupOrThrow(Registries.ITEM);
	}

	@Override
	protected void buildRecipes()
	{
		//this.con = cons;

		// Stone based
		simple2x2(RecipeCategory.BUILDING_BLOCKS, GNSBlocks.delusion_stone, GNSBlocks.delusion_stone_bricks, 4);
		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, GNSBlocks.delusion_cobblestone, GNSBlocks.delusion_cobblestone_slab, GNSBlocks.delusion_cobblestone_stairs, GNSBlocks.delusion_cobblestone_wall);
		slabsStairsWalls(RecipeCategory.BUILDING_BLOCKS, GNSBlocks.delusion_stone_bricks, GNSBlocks.delusion_stone_brick_slab, GNSBlocks.delusion_stone_brick_stairs, GNSBlocks.delusion_stone_brick_wall);
		slabsStairs(RecipeCategory.BUILDING_BLOCKS, GNSBlocks.delusion_stone, GNSBlocks.delusion_stone_slab, GNSBlocks.delusion_stone_stairs);
		stoneCutting(GNSBlocks.delusion_cobblestone, ImmutableList.of(GNSBlocks.delusion_cobblestone_slab, GNSBlocks.delusion_cobblestone_stairs, GNSBlocks.delusion_cobblestone_wall));
		stoneCutting(GNSBlocks.delusion_stone_bricks, ImmutableList.of(GNSBlocks.delusion_stone_brick_slab, GNSBlocks.delusion_stone_brick_stairs, GNSBlocks.delusion_stone_brick_wall));

		ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.REDSTONE, GNSBlocks.delusion_button).requires(GNSBlocks.delusion_stone).unlockedBy(hasItem, has(GNSBlocks.delusion_stone)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.REDSTONE, GNSBlocks.delusion_pressure_plate).define('#', GNSBlocks.delusion_stone).pattern("##").unlockedBy(hasItem, has(GNSBlocks.delusion_stone)).save(this.output);

		// Wood based
		//@formatter:off
		List<WoodMap> woodMapping = ImmutableList.of(
			new WoodMap(GNSItemTags.DREAM_LOGS, GNSBlocks.dream_log, GNSBlocks.dream_wood, GNSBlocks.stripped_dream_log, GNSBlocks.stripped_dream_wood, GNSBlocks.dream_planks, GNSBlocks.dream_slab, GNSBlocks.dream_stairs, GNSBlocks.dream_pressure_plate, GNSBlocks.dream_button, GNSBlocks.dream_door, GNSBlocks.dream_trapdoor, GNSBlocks.dream_fence, GNSBlocks.dream_fence_gate, GNSBlocks.dream_sign, GNSBlocks.dream_hanging_sign),
			new WoodMap(GNSItemTags.WHITE_LOGS, GNSBlocks.white_log, GNSBlocks.white_wood, GNSBlocks.stripped_white_log, GNSBlocks.stripped_white_wood, GNSBlocks.white_planks, GNSBlocks.white_slab, GNSBlocks.white_stairs, GNSBlocks.white_pressure_plate, GNSBlocks.white_button, GNSBlocks.white_door, GNSBlocks.white_trapdoor, GNSBlocks.white_fence, GNSBlocks.white_fence_gate, GNSBlocks.white_sign, GNSBlocks.white_hanging_sign),
			new WoodMap(GNSItemTags.DEAD_LOGS, GNSBlocks.dead_log, GNSBlocks.dead_wood, GNSBlocks.stripped_dead_log, GNSBlocks.stripped_dead_wood, GNSBlocks.dead_planks, GNSBlocks.dead_slab, GNSBlocks.dead_stairs, GNSBlocks.dead_pressure_plate, GNSBlocks.dead_button, GNSBlocks.dead_door, GNSBlocks.dead_trapdoor, GNSBlocks.dead_fence, GNSBlocks.dead_fence_gate, GNSBlocks.dead_sign, GNSBlocks.dead_hanging_sign),
			new WoodMap(GNSItemTags.BLOOD_LOGS, GNSBlocks.blood_log, GNSBlocks.blood_wood, GNSBlocks.stripped_blood_log, GNSBlocks.stripped_blood_wood, GNSBlocks.blood_planks, GNSBlocks.blood_slab, GNSBlocks.blood_stairs, GNSBlocks.blood_pressure_plate, GNSBlocks.blood_button, GNSBlocks.blood_door, GNSBlocks.blood_trapdoor, GNSBlocks.blood_fence, GNSBlocks.blood_fence_gate, GNSBlocks.blood_sign, GNSBlocks.blood_hanging_sign)
				);
		//@formatter:on
		woodMapping.forEach(wood ->
		{
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, wood.strippedWood, 3).define('#', wood.strippedLog).pattern("##").pattern("##").group("stripped_bark").unlockedBy(hasItem, has(wood.strippedLog)).save(this.output);
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, wood.wood, 3).define('#', wood.log).pattern("##").pattern("##").group("bark").unlockedBy(hasItem, has(wood.wood)).save(this.output);
			ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.BUILDING_BLOCKS, wood.plank, 4).requires(wood.logTag).group("planks").unlockedBy(hasItem, has(wood.logTag)).save(this.output);
			slabs(RecipeCategory.BUILDING_BLOCKS, wood.plank, wood.slab).group("wooden_slab").save(this.output);
			stairs(RecipeCategory.BUILDING_BLOCKS, wood.plank, wood.stair).group("wooden_stairs").save(this.output);
			fencesGates(wood.plank, wood.fence, wood.gate);
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.REDSTONE, wood.door, 3).define('#', wood.plank).pattern("##").pattern("##").pattern("##").group("wooden_door").unlockedBy(hasItem, has(wood.plank)).save(this.output);
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.REDSTONE, wood.trapdoor, 2).define('#', wood.plank).pattern("###").pattern("###").group("wooden_trapdoor").unlockedBy(hasItem, has(wood.plank)).save(this.output);
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.REDSTONE, wood.pressurePlate).define('#', wood.plank).pattern("##").group("wooden_pressure_plate").unlockedBy(hasItem, has(wood.plank)).save(this.output);
			ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.REDSTONE, wood.button).requires(wood.plank).unlockedBy(hasItem, has(wood.plank)).save(this.output);
			signBuilder(wood.sign, Ingredient.of(wood.plank)).unlockedBy(hasItem, has(wood.plank)).save(this.output);
			hangingSign(wood.hangingSign, wood.strippedLog);
		});

		// Armor/Storage blocks
		//@formatter:off
		List<OreMap> materialMapping = ImmutableList.of(
			new OreMap(GNSItemTags.CANDY_BLOCKS, GNSBlocks.hard_candy_block, GNSItemTags.CANDY_BLOCKS, GNSBlocks.hard_candy_block, GNSItems.candy_chestplate, GNSItems.candy_leggings, GNSItems.candy_boots, GNSItems.candy_helmet, GNSItems.candy_sword, GNSItems.candy_pickaxe, GNSItems.candy_axe, GNSItems.candy_shovel, GNSItems.candy_hoe),
			new OreMap(GNSItemTags.SPECTRITE_INGOTS, GNSItems.spectrite_ingot, GNSItemTags.SPECTRITE_BLOCKS, GNSBlocks.spectrite_block, GNSItems.spectrite_chestplate, GNSItems.spectrite_leggings, GNSItems.spectrite_boots, GNSItems.spectrite_helmet, GNSItems.spectrite_sword, GNSItems.spectrite_pickaxe, GNSItems.spectrite_axe, GNSItems.spectrite_shovel, GNSItems.spectrite_hoe),
			new OreMap(GNSItemTags.POSITITE, GNSItems.positite, GNSItemTags.POSITITE_BLOCKS, GNSBlocks.positite_block, GNSItems.positite_chestplate, GNSItems.positite_leggings, GNSItems.positite_boots, GNSItems.positite_helmet, GNSItems.positite_sword, GNSItems.positite_pickaxe, GNSItems.positite_axe, GNSItems.positite_shovel, GNSItems.positite_hoe),
			new OreMap(GNSItemTags.ZITRITE_INGOTS, GNSItems.zitrite_ingot, GNSItemTags.ZITRITE_BLOCKS, GNSBlocks.zitrite_block, GNSItems.zitrite_chestplate, GNSItems.zitrite_leggings, GNSItems.zitrite_boots, GNSItems.zitrite_helmet, GNSItems.zitrite_sword, GNSItems.zitrite_pickaxe, GNSItems.zitrite_axe, GNSItems.zitrite_shovel, GNSItems.zitrite_hoe),
			new OreMap(GNSItemTags.NEGATITE, GNSItems.negatite, GNSItemTags.NEGATITE_BLOCKS, GNSBlocks.negatite_block, GNSItems.negatite_chestplate, GNSItems.negatite_leggings, GNSItems.negatite_boots, GNSItems.negatite_helmet, GNSItems.negatite_sword, GNSItems.negatite_pickaxe, GNSItems.negatite_axe, GNSItems.negatite_shovel, GNSItems.negatite_hoe)
		);
		//@formatter:on
		materialMapping.forEach(mat ->
		{
			if (mat.material != GNSBlocks.hard_candy_block)
			{
				ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.BUILDING_BLOCKS, mat.material, 9).requires(mat.blockTag).unlockedBy(hasItem, has(mat.blockTag)).save(this.output, GoodNightSleep.find(BuiltInRegistries.ITEM.getKey(mat.material.asItem()).getPath() + "_from_block"));
				ShapedRecipeBuilder.shaped(this.items, RecipeCategory.MISC, mat.block).define('#', mat.materialTag).pattern("###").pattern("###").pattern("###").unlockedBy(hasItem, has(mat.materialTag)).save(this.output);
			}
			else
			{
				simple3x3(RecipeCategory.BUILDING_BLOCKS, GNSItems.candy, GNSBlocks.hard_candy_block, 1);
				ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.MISC, GNSItems.candy, 9).requires(mat.blockTag).unlockedBy(hasItem, has(mat.blockTag)).save(this.output, GoodNightSleep.find(BuiltInRegistries.ITEM.getKey(mat.material.asItem()).getPath() + "_from_block"));
			}

			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, mat.helmet).define('#', mat.materialTag).pattern("###").pattern("# #").unlockedBy(hasItem, has(mat.materialTag)).save(this.output);
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, mat.chestplate).define('#', mat.materialTag).pattern("# #").pattern("###").pattern("###").unlockedBy(hasItem, has(mat.materialTag)).save(this.output);
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, mat.leggings).define('#', mat.materialTag).pattern("###").pattern("# #").pattern("# #").unlockedBy(hasItem, has(mat.materialTag)).save(this.output);
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, mat.boots).define('#', mat.materialTag).pattern("# #").pattern("# #").unlockedBy(hasItem, has(mat.materialTag)).save(this.output);

			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, mat.sword).define('#', Items.STICK).define('X', mat.materialTag).pattern("X").pattern("X").pattern("#").unlockedBy(hasItem, has(mat.materialTag)).save(this.output);
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, mat.pickaxe).define('#', Items.STICK).define('X', mat.materialTag).pattern("XXX").pattern(" # ").pattern(" # ").unlockedBy(hasItem, has(mat.materialTag)).save(this.output);
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, mat.axe).define('#', Items.STICK).define('X', mat.materialTag).pattern("XX").pattern("X#").pattern(" #").unlockedBy(hasItem, has(mat.materialTag)).save(this.output);
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, mat.shovel).define('#', Items.STICK).define('X', mat.materialTag).pattern("X").pattern("#").pattern("#").unlockedBy(hasItem, has(mat.materialTag)).save(this.output);
			ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, mat.hoe).define('#', Items.STICK).define('X', mat.materialTag).pattern("XX").pattern(" #").pattern(" #").unlockedBy(hasItem, has(mat.materialTag)).save(this.output);
		});

		ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.BUILDING_BLOCKS, GNSItems.necrum, 9).requires(GNSItemTags.NECRUM_BLOCKS).unlockedBy(hasItem, has(GNSItemTags.NECRUM_BLOCKS)).save(this.output, GoodNightSleep.find("necrum_from_block"));
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, GNSBlocks.necrum_block).define('#', GNSItemTags.NECRUM).pattern("###").pattern("###").pattern("###").unlockedBy(hasItem, has(GNSItemTags.NECRUM)).save(this.output);

		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.COMBAT, GNSItems.necrum_sword).define('#', Items.STICK).define('X', GNSItemTags.NECRUM_BLOCKS).pattern("X").pattern("X").pattern("#").unlockedBy(hasItem, has(GNSItemTags.NECRUM_BLOCKS)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, GNSItems.necrum_pickaxe).define('#', Items.STICK).define('X', GNSItemTags.NECRUM_BLOCKS).pattern("XXX").pattern(" # ").pattern(" # ").unlockedBy(hasItem, has(GNSItemTags.NECRUM_BLOCKS)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, GNSItems.necrum_axe).define('#', Items.STICK).define('X', GNSItemTags.NECRUM_BLOCKS).pattern("XX").pattern("X#").pattern(" #").unlockedBy(hasItem, has(GNSItemTags.NECRUM_BLOCKS)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, GNSItems.necrum_shovel).define('#', Items.STICK).define('X', GNSItemTags.NECRUM_BLOCKS).pattern("X").pattern("#").pattern("#").unlockedBy(hasItem, has(GNSItemTags.NECRUM_BLOCKS)).save(this.output);
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.TOOLS, GNSItems.necrum_hoe).define('#', Items.STICK).define('X', GNSItemTags.NECRUM_BLOCKS).pattern("XX").pattern(" #").pattern(" #").unlockedBy(hasItem, has(GNSItemTags.NECRUM_BLOCKS)).save(this.output);

		// Misc
		ImmutableMap<ItemLike, ItemLike> flowerDyeMap = ImmutableMap.of(GNSBlocks.cyan_flower, Items.CYAN_DYE, GNSBlocks.orange_flower, Items.ORANGE_DYE, GNSBlocks.dead_flower, Items.GRAY_DYE);
		flowerDyeMap.forEach((flower, dye) -> ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.MISC, dye).requires(flower).unlockedBy(hasItem, has(flower)).save(this.output, GoodNightSleep.find(BuiltInRegistries.ITEM.getKey(dye.asItem()).getPath() + "_from_" + BuiltInRegistries.ITEM.getKey(flower.asItem()).getPath())));
		ImmutableMap<ItemLike, ItemLike> cropSeedMap = ImmutableMap.of(GNSItems.rainbow_seeds, GNSItems.rainbow_berries);
		cropSeedMap.forEach((seed, crop) -> ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.MISC, seed).requires(crop).unlockedBy(hasItem, has(crop)).save(this.output));
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, Blocks.FURNACE).define('#', GNSItemTags.COBBLESTONES).pattern("###").pattern("# #").pattern("###").unlockedBy(hasItem, has(GNSItemTags.COBBLESTONES)).save(this.output, GoodNightSleep.find("furnace_compat"));

		ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.BUILDING_BLOCKS, GNSItems.strange_bed).requires(ItemTags.BEDS).requires(GNSBlocks.hope_mushroom).requires(GNSBlocks.despair_mushroom).unlockedBy(hasItem, has(ItemTags.BEDS)).save(this.output);

		ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.BUILDING_BLOCKS, GNSItems.luxurious_bed).requires(GNSItems.strange_bed).requires(GNSItems.positite).unlockedBy(hasItem, has(GNSItems.strange_bed)).save(this.output);
		ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.BUILDING_BLOCKS, GNSItems.wretched_bed).requires(GNSItems.strange_bed).requires(GNSItems.negatite).unlockedBy(hasItem, has(GNSItems.strange_bed)).save(this.output);

		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, GNSBlocks.pot_of_gold.asItem()).define('I', GNSItems.spectrite_ingot).define('G', Blocks.GOLD_BLOCK.asItem()).define('C', Blocks.CAULDRON.asItem()).pattern("III").pattern("IGI").pattern("ICI").unlockedBy(hasItem, has(GNSItems.spectrite_ingot)).save(this.output);

		ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.MISC, GNSItems.powdered_sugar).requires(GNSItemTags.SUGARY).unlockedBy(hasItem, has(GNSItemTags.SUGARY)).save(this.output);
		ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.MISC, GNSItems.necrotic_extract).requires(GNSItemTags.NECROTIC_EXTRACTABLES).unlockedBy(hasItem, has(GNSItemTags.NECROTIC_EXTRACTABLES)).save(this.output);

		ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.FOOD, GNSItems.luxurious_soup).requires(Items.BOWL).requires(GNSBlocks.hope_mushroom).requires(GNSItems.powdered_sugar).unlockedBy(hasItem, has(GNSBlocks.hope_mushroom)).save(this.output);
		ShapelessRecipeBuilder.shapeless(this.items, RecipeCategory.FOOD, GNSItems.wretched_soup).requires(Items.BOWL).requires(GNSBlocks.despair_mushroom).requires(GNSItems.necrotic_extract).unlockedBy(hasItem, has(GNSBlocks.despair_mushroom)).save(this.output);

		nineBlockStorageRecipes(RecipeCategory.MISC, GNSItems.raw_spectrite, RecipeCategory.BUILDING_BLOCKS, GNSBlocks.raw_spectrite_block);
		nineBlockStorageRecipes(RecipeCategory.MISC, GNSItems.raw_zitrite, RecipeCategory.BUILDING_BLOCKS, GNSBlocks.raw_zitrite_block);

		// Cooking
		/*blasting(GNSItemTags.CANDY_ORES, GNSItems.candy_bar, 0.2F);*/
		blasting(RecipeCategory.MISC, GNSItemTags.SPECTRITE_SMELTABLES, GNSItems.spectrite_ingot, 0.7F);
		blasting(RecipeCategory.MISC, GNSItemTags.POSITITE_SMELTABLES, GNSItems.positite, 0.9F);
		blasting(RecipeCategory.MISC, GNSItemTags.NECRUM_SMELTABLES, GNSItems.necrum, 0.2F);
		blasting(RecipeCategory.MISC, GNSItemTags.ZITRITE_SMELTABLES, GNSItems.zitrite_ingot, 0.9F);
		blasting(RecipeCategory.MISC, GNSItemTags.NEGATITE_SMELTABLES, GNSItems.negatite, 1.0F);

		cooking(RecipeCategory.BUILDING_BLOCKS, GNSBlocks.delusion_cobblestone, GNSBlocks.delusion_stone, 0.1F);
	}

	private void simple2x2(RecipeCategory cat, ItemLike item, ItemLike output, int amount)
	{
		ShapedRecipeBuilder.shaped(this.items, cat, output, amount).define('#', item).pattern("##").pattern("##").unlockedBy(hasItem, has(item)).save(this.output);
	}

	private void simple2x2(RecipeCategory cat, ItemLike item, ItemLike output)
	{
		simple2x2(cat, item, output, 1);
	}

	private void simple3x3(RecipeCategory cat, ItemLike item, ItemLike output, int amount)
	{
		ShapedRecipeBuilder.shaped(this.items, cat, output, amount).define('#', item).pattern("###").pattern("###").pattern("###").unlockedBy(hasItem, has(item)).save(this.output);
	}

	private void simple3x3(RecipeCategory cat, ItemLike item, ItemLike output)
	{
		simple3x3(cat, item, output, 1);
	}

	private void slabsStairs(RecipeCategory cat, ItemLike block, ItemLike slab, ItemLike stair)
	{
		slabs(cat, block, slab).save(this.output);
		stairs(cat, block, stair).save(this.output);
	}

	private void slabsStairsWalls(RecipeCategory cat, ItemLike block, ItemLike slab, ItemLike stair, ItemLike wall)
	{
		slabsStairs(cat, block, slab, stair);
		walls(block, wall);
	}

	private void slabsStairs(RecipeCategory cat, ItemLike block, ItemLike slab, ItemLike stair, boolean withStoneCutting)
	{
		slabsStairs(cat, block, slab, stair);
		stoneCutting(block, ImmutableList.of(slab, stair));
	}

	private void slabsStairsWalls(RecipeCategory cat, ItemLike block, ItemLike slab, ItemLike stair, ItemLike wall, boolean withStoneCutting)
	{
		slabsStairsWalls(cat, block, slab, stair, wall);
		stoneCutting(block, ImmutableList.of(slab, stair, wall));
	}

	private ShapedRecipeBuilder slabs(RecipeCategory cat, ItemLike ingredient, ItemLike slab)
	{
		return ShapedRecipeBuilder.shaped(this.items, cat, slab, 6).define('#', ingredient).pattern("###").unlockedBy(hasItem, has(ingredient));
	}

	private ShapedRecipeBuilder stairs(RecipeCategory cat, ItemLike ingredient, ItemLike stair)
	{
		return ShapedRecipeBuilder.shaped(this.items, cat, stair, 4).define('#', ingredient).pattern("#  ").pattern("## ").pattern("###").unlockedBy(hasItem, has(ingredient));
	}

	private void walls(ItemLike ingredient, ItemLike wall)
	{
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, wall, 6).define('#', ingredient).pattern("###").pattern("###").unlockedBy(hasItem, has(ingredient)).save(this.output);
	}

	private void fencesGates(ItemLike plank, ItemLike fence, ItemLike gate)
	{
		fences(plank, fence);
		gates(plank, gate);
	}

	private void fences(ItemLike plank, ItemLike fence)
	{
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, fence, 3).define('P', plank).define('S', Ingredient.of(this.items.getOrThrow(Tags.Items.RODS_WOODEN))).pattern("PSP").pattern("PSP").group("wooden_fence").unlockedBy(hasItem, has(plank)).save(this.output);
	}

	private void gates(ItemLike plank, ItemLike gate)
	{
		ShapedRecipeBuilder.shaped(this.items, RecipeCategory.BUILDING_BLOCKS, gate).define('P', plank).define('S', Ingredient.of(this.items.getOrThrow(Tags.Items.RODS_WOODEN))).pattern("SPS").pattern("SPS").group("wooden_fence_gate").unlockedBy(hasItem, has(plank)).save(this.output);
	}

	private void stoneCutting(ItemLike ingredient, ImmutableList<ItemLike> results)
	{
		results.forEach(result ->
		{
			SingleItemRecipeBuilder.stonecutting(Ingredient.of(ingredient), RecipeCategory.BUILDING_BLOCKS, result, result instanceof SlabBlock ? 2 : 1).unlockedBy(hasItem, has(ingredient)).save(this.output, GoodNightSleep.find(BuiltInRegistries.ITEM.getKey(result.asItem()).getPath() + "_stonecutting_" + BuiltInRegistries.ITEM.getKey(ingredient.asItem()).getPath()));
		});
	}

	private void cooking(RecipeCategory cat, ItemLike ingredient, ItemLike result, float exp)
	{
		cooking(cat, ingredient, result, exp, 200, RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new);
	}

	private <T extends AbstractCookingRecipe> void cooking(RecipeCategory cat, ItemLike ingredient, ItemLike result, float exp, int time, RecipeSerializer<T> smeltingRecipe, AbstractCookingRecipe.Factory<T> recipeFactory)
	{
		SimpleCookingRecipeBuilder.generic(Ingredient.of(ingredient), cat, result, exp, time, smeltingRecipe, recipeFactory).unlockedBy(hasItem, has(ingredient)).save(this.output, GoodNightSleep.find(BuiltInRegistries.ITEM.getKey(result.asItem()).getPath() + "_from_" + BuiltInRegistries.ITEM.getKey(ingredient.asItem()).getPath()));
	}

	private void smoking(TagKey<Item> ingredient, ItemLike result, float exp)
	{
		cooking(RecipeCategory.FOOD, ingredient, result, exp);
		cooking(RecipeCategory.FOOD, ingredient, result, exp, 100, RecipeSerializer.SMOKING_RECIPE, SmokingRecipe::new);
		cooking(RecipeCategory.FOOD, ingredient, result, exp, 600, RecipeSerializer.CAMPFIRE_COOKING_RECIPE, CampfireCookingRecipe::new);
	}

	private void blasting(RecipeCategory cat, TagKey<Item> ingredient, ItemLike result, float exp)
	{
		cooking(cat, ingredient, result, exp);
		cooking(cat, ingredient, result, exp, 100, RecipeSerializer.BLASTING_RECIPE, BlastingRecipe::new);
	}

	private void cooking(RecipeCategory cat, TagKey<Item> ingredient, ItemLike result, float exp)
	{
		cooking(cat, ingredient, result, exp, 200, RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new);
	}

	private <T extends AbstractCookingRecipe> void cooking(RecipeCategory cat, TagKey<Item> ingredient, ItemLike result, float exp, int time, RecipeSerializer<T> smeltingRecipe, AbstractCookingRecipe.Factory<T> recipeFactory)
	{
		SimpleCookingRecipeBuilder.generic(Ingredient.of(this.items.getOrThrow(ingredient)), cat, result, exp, time, smeltingRecipe, recipeFactory).unlockedBy(hasItem, has(ingredient)).save(this.output, GoodNightSleep.find(BuiltInRegistries.ITEM.getKey(result.asItem()).getPath() + "_from_" + BuiltInRegistries.RECIPE_SERIALIZER.getKey(smeltingRecipe).getPath()));
	}

	private void smoking(RecipeCategory cat, ItemLike ingredient, ItemLike result, float exp)
	{
		cooking(cat, ingredient, result, exp, 200, RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new);
		cooking(cat, ingredient, result, exp, 100, RecipeSerializer.SMOKING_RECIPE, SmokingRecipe::new);
		cooking(cat, ingredient, result, exp, 600, RecipeSerializer.CAMPFIRE_COOKING_RECIPE, CampfireCookingRecipe::new);
	}

	private void blasting(RecipeCategory cat, ItemLike ingredient, ItemLike result, float exp)
	{
		cooking(cat, ingredient, result, exp, 200, RecipeSerializer.SMELTING_RECIPE, SmeltingRecipe::new);
		cooking(cat, ingredient, result, exp, 100, RecipeSerializer.BLASTING_RECIPE, BlastingRecipe::new);
	}

	/*@Override
	public String getName()
	{
		return "Good Night's Sleep Recipes";
	}*/

	/**
	 * Collection of wood and wood related blocks.
	 * 
	 * @author David
	 *
	 */
	protected static record WoodMap(TagKey<Item> logTag, ItemLike log, ItemLike wood, ItemLike strippedLog, ItemLike strippedWood, ItemLike plank, ItemLike slab, ItemLike stair, ItemLike pressurePlate, ItemLike button, ItemLike door, ItemLike trapdoor, ItemLike fence, ItemLike gate, ItemLike sign, ItemLike hangingSign)
	{
	}

	/**
	 * Collection of armor materials
	 * 
	 * @author David
	 *
	 */
	protected static record OreMap(TagKey<Item> materialTag, ItemLike material, TagKey<Item> blockTag, ItemLike block, ItemLike chestplate, ItemLike leggings, ItemLike boots, ItemLike helmet, ItemLike sword, ItemLike pickaxe, ItemLike axe, ItemLike shovel, ItemLike hoe)
	{
	}
}