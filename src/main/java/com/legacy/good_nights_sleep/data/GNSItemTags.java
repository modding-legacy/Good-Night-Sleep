package com.legacy.good_nights_sleep.data;

import com.legacy.good_nights_sleep.GoodNightSleep;

import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;

public interface GNSItemTags
{
	public static void init()
	{
	}

	TagKey<Item> SUGARY = tag("sugary");
	TagKey<Item> NECROTIC_EXTRACTABLES = tag("necrotic_extractables");

	TagKey<Item> PLANKS = tag("planks");

	TagKey<Item> DREAM_LOGS = tag("dream_logs");
	TagKey<Item> WHITE_LOGS = tag("white_logs");
	TagKey<Item> DEAD_LOGS = tag("dead_logs");
	TagKey<Item> BLOOD_LOGS = tag("blood_logs");

	TagKey<Item> COBBLESTONES = tag("cobblestone");
	TagKey<Item> STONES = tag("stone");
	TagKey<Item> MUSHROOMS = tag("mushrooms");

	TagKey<Item> CANDY_ORES = tag("ores/candy");
	TagKey<Item> SPECTRITE_ORES = tag("ores/spectrite");
	TagKey<Item> POSITITE_ORES = tag("ores/positite");
	TagKey<Item> NECRUM_ORES = tag("ores/necrum");
	TagKey<Item> ZITRITE_ORES = tag("ores/zitrite");
	TagKey<Item> NEGATITE_ORES = tag("ores/negatite");

	TagKey<Item> CANDY_SMELTABLES = tag("smeltables/candy");
	TagKey<Item> SPECTRITE_SMELTABLES = tag("smeltables/spectrite");
	TagKey<Item> POSITITE_SMELTABLES = tag("smeltables/positite");
	TagKey<Item> NECRUM_SMELTABLES = tag("smeltables/necrum");
	TagKey<Item> ZITRITE_SMELTABLES = tag("smeltables/zitrite");
	TagKey<Item> NEGATITE_SMELTABLES = tag("smeltables/negatite");

	TagKey<Item> CANDY_MATERIALS = tag("materials/candy");
	TagKey<Item> SPECTRITE_INGOTS = tag("ingots/spectrite");
	TagKey<Item> POSITITE = tag("gems/positite");
	TagKey<Item> NECRUM = tag("materials/necrum");
	TagKey<Item> ZITRITE_INGOTS = tag("ingots/zitrite");
	TagKey<Item> NEGATITE = tag("gems/negatite");

	TagKey<Item> CANDY_BLOCKS = tag("storage_blocks/candy");
	TagKey<Item> SPECTRITE_BLOCKS = tag("storage_blocks/spectrite");
	TagKey<Item> POSITITE_BLOCKS = tag("storage_blocks/positite");
	TagKey<Item> NECRUM_BLOCKS = tag("storage_blocks/necrum");
	TagKey<Item> ZITRITE_BLOCKS = tag("storage_blocks/zitrite");
	TagKey<Item> NEGATITE_BLOCKS = tag("storage_blocks/negatite");

	private static TagKey<Item> tag(String key)
	{
		return ItemTags.create(GoodNightSleep.locate(key));
	}
}