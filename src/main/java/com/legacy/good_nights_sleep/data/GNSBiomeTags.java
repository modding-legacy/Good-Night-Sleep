package com.legacy.good_nights_sleep.data;

import com.legacy.good_nights_sleep.GoodNightSleep;

import net.minecraft.core.registries.Registries;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.biome.Biome;

public interface GNSBiomeTags
{
	public static void init()
	{
	}

	TagKey<Biome> IS_DREAM = tag("is_dream");
	TagKey<Biome> IS_NIGHTMARE = tag("is_nightmare");

	TagKey<Biome> HAS_HOPE_MUSHROOMS = tag("has_hope_mushrooms");
	TagKey<Biome> HAS_DESPAIR_MUSHROOMS = tag("has_despair_mushrooms");

	private static TagKey<Biome> tag(String key)
	{
		return TagKey.create(Registries.BIOME, GoodNightSleep.locate(key));
	}
}