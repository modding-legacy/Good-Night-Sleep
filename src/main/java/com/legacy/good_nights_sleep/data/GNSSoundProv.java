package com.legacy.good_nights_sleep.data;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.registry.GNSSounds;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.data.PackOutput;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.neoforged.neoforge.common.data.SoundDefinition;
import net.neoforged.neoforge.common.data.SoundDefinition.Sound;
import net.neoforged.neoforge.common.data.SoundDefinition.SoundType;
import net.neoforged.neoforge.common.data.SoundDefinitionsProvider;

public class GNSSoundProv extends SoundDefinitionsProvider
{
	/**
	 * If disabled, a mixin will prevent Forge from throwing an exception while
	 * running datagen if a sound file doesn't exist in the resources folder. This
	 * exists since usually we make our events before the sound files themselves
	 * have even been designed.
	 */
	public static final boolean VALIDATE = false;

	public GNSSoundProv(PackOutput output)
	{
		super(output, GoodNightSleep.MODID);
	}

	@Override
	public void registerSounds()
	{
		this.add(GNSSounds.MUSIC_GOOD_DREAM, musicDef("good_dream"));
		this.add(GNSSounds.MUSIC_SKY_BLUE, musicDef("sky_blue"));
		this.add(GNSSounds.MUSIC_NIGHTMARE, musicDef("tfarcenim"));

		String armorSub = itemSub("armor.equip_");
		this.add(GNSSounds.ITEM_ARMOR_EQUIP_CANDY.value(), definition().with(event(SoundEvents.ARMOR_EQUIP_LEATHER.value())).subtitle(armorSub.concat("candy")));
		this.add(GNSSounds.ITEM_ARMOR_EQUIP_SPECTRITE.value(), definition().with(event(SoundEvents.ARMOR_EQUIP_IRON.value())).subtitle(armorSub.concat("spectrite")));
		this.add(GNSSounds.ITEM_ARMOR_EQUIP_POSITITE.value(), definition().with(event(SoundEvents.ARMOR_EQUIP_DIAMOND.value())).subtitle(armorSub.concat("positite")));

		this.add(GNSSounds.ITEM_ARMOR_EQUIP_ZITRITE.value(), definition().with(event(SoundEvents.ARMOR_EQUIP_DIAMOND.value())).subtitle(armorSub.concat("zitrite")));
		this.add(GNSSounds.ITEM_ARMOR_EQUIP_NEGATITE.value(), definition().with(event(SoundEvents.ARMOR_EQUIP_GOLD.value())).subtitle(armorSub.concat("negatite")));

		// itemDef(item, "name", 1)

		/*String breakSub = subMc("block.generic.break");
		String placeSub = subMc("block.generic.place");
		String hitSub = subMc("block.generic.hit");
		String stepSub = subMc("block.generic.footsteps");*/

		String block = "pot_of_gold";
		this.add(GNSSounds.BLOCK_POT_OF_GOLD_USE, definition().with(event(SoundEvents.BOTTLE_FILL)).subtitle(blockSub(block + ".use")));

		block = "rainbow";
		/*this.add(GNSSounds.BLOCK_RAINBOW_PLACE, definition().with(event(SoundEvents.AMETHYST_BLOCK_PLACE)).subtitle(placeSub));
		this.add(GNSSounds.BLOCK_RAINBOW_BREAK, definition().with(event(SoundEvents.AMETHYST_BLOCK_BREAK)).subtitle(breakSub));*/
		this.add(GNSSounds.BLOCK_RAINBOW_APPEAR, definition().with(event(SoundEvents.AMETHYST_BLOCK_RESONATE)).subtitle(blockSub(block + ".appear")));

		String entity = "baby_creeper";
		this.add(GNSSounds.ENTITY_BABY_CREEPER_HURT, definition().with(event(SoundEvents.CREEPER_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(GNSSounds.ENTITY_BABY_CREEPER_DEATH, definition().with(event(SoundEvents.CREEPER_DEATH)).subtitle(entitySub(entity + ".death")));

		entity = "tormenter";
		/*this.add(GNSSounds.ENTITY_TORMENTER_IDLE, entityDef(entity, "idle", 5));
		this.add(GNSSounds.ENTITY_TORMENTER_HURT, entityDef(entity, "hurt", 3));
		this.add(GNSSounds.ENTITY_TORMENTER_DEATH, entityDef(entity, "death", 1));*/

		this.add(GNSSounds.ENTITY_TORMENTER_IDLE, definition().with(event(SoundEvents.AMBIENT_CAVE.value())).subtitle(entitySub(entity + ".idle")));
		this.add(GNSSounds.ENTITY_TORMENTER_HURT, definition().with(event(SoundEvents.AMBIENT_CAVE.value())).subtitle(entitySub(entity + ".hurt")));
		this.add(GNSSounds.ENTITY_TORMENTER_DEATH, definition().with(event(SoundEvents.ZOMBIE_HORSE_DEATH)).subtitle(entitySub(entity + ".death")));
		this.add(GNSSounds.ENTITY_TORMENTER_TORMENT, definition().with(event(SoundEvents.ELDER_GUARDIAN_CURSE)).subtitle(entitySub(entity + ".torment")));

		entity = "unicorn";
		this.add(GNSSounds.ENTITY_UNICORN_IDLE, definition().with(event(SoundEvents.HORSE_AMBIENT)).subtitle(entitySub(entity + ".idle")));
		this.add(GNSSounds.ENTITY_UNICORN_HURT, definition().with(event(SoundEvents.HORSE_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(GNSSounds.ENTITY_UNICORN_DEATH, definition().with(event(SoundEvents.HORSE_DEATH)).subtitle(entitySub(entity + ".death")));
		this.add(GNSSounds.ENTITY_UNICORN_ANGRY, definition().with(event(SoundEvents.HORSE_ANGRY)).subtitle(entitySub(entity + ".angry")));
		this.add(GNSSounds.ENTITY_UNICORN_EAT, definition().with(event(SoundEvents.HORSE_EAT)).subtitle(entitySub(entity + ".eat")));
		this.add(GNSSounds.ENTITY_UNICORN_BREATHE, definition().with(event(SoundEvents.HORSE_BREATHE)).subtitle(entitySub(entity + ".breathe")));

		entity = "herobrine";
		this.add(GNSSounds.ENTITY_HEROBRINE_HURT, definition().with(event(SoundEvents.WITHER_HURT)).subtitle(entitySub(entity + ".hurt")));
		this.add(GNSSounds.ENTITY_HEROBRINE_DEATH, definition().with(event(SoundEvents.WITHER_DEATH)).subtitle(entitySub(entity + ".death")));
		this.add(GNSSounds.ENTITY_HEROBRINE_TELEPORT, definition().with(event(SoundEvents.PLAYER_TELEPORT)).subtitle(entitySub(entity + ".teleport")));
	}

	protected static SoundDefinition def(Dir dir, String name, String subtitle)
	{
		return def(dir, name, 1, subtitle);
	}

	protected static SoundDefinition def(Dir dir, String name, int count, String subtitle)
	{
		return def(dir, name, count, subtitle, s ->
		{
		});
	}

	protected static SoundDefinition def(Dir dir, String name, int count, String subtitle, Consumer<Sound> extra)
	{
		var def = count > 1 ? definition().with(gns(dir.folderName.concat(name), count, extra)) : definition().with(gns(dir.folderName.concat(name), extra));
		return def.subtitle(dir == Dir.ENTITY ? entitySub(subtitle) : dir == Dir.BLOCKS ? blockSub(subtitle) : dir == Dir.ITEMS ? itemSub(subtitle) : subtitle);
	}

	protected static SoundDefinition.Sound gns(final String name)
	{
		return gns(name, s ->
		{
		});
	}

	protected static SoundDefinition.Sound gns(final String name, Consumer<Sound> extra)
	{
		var sound = sound(GoodNightSleep.locate(name));
		extra.accept(sound);
		return sound;
	}

	protected static Sound[] gns(String name, int count)
	{
		return gns(name, count, s ->
		{
		});
	}

	protected static Sound[] gns(String name, int count, Consumer<Sound> extra)
	{
		List<Sound> sounds = new ArrayList<>();

		for (int i = 1; i <= count; ++i)
		{
			Sound sound = gns(name.concat("_" + i));
			extra.accept(sound);
			sounds.add(sound);
		}
		return sounds.toArray(new Sound[count]);
	}

	protected static SoundDefinition.Sound event(final SoundEvent event)
	{
		return sound(BuiltInRegistries.SOUND_EVENT.getKey(event), SoundType.EVENT);
	}

	protected static String subMc(final String name)
	{
		return "subtitles.".concat(name);
	}

	protected static String sub(final String name)
	{
		return subMc(GoodNightSleep.MODID + "." + name);
	}

	protected static String blockSub(final String name)
	{
		return sub("block.".concat(name));
	}

	protected static String itemSub(final String name)
	{
		return sub("item.".concat(name));
	}

	protected static String entitySub(final String name)
	{
		return sub("entity.".concat(name));
	}

	protected static SoundDefinition musicDef(final String name)
	{
		return musicDef(name, false);
	}

	protected static SoundDefinition musicDef(final String name, boolean preload)
	{
		String music = "music/";
		return definition().with(gns(music.concat(name)).stream().preload(preload));
	}

	protected static SoundDefinition entityDef(String entityName, String soundName, int count, String subtitle)
	{
		return def(Dir.ENTITY, entityName + "/" + soundName, count, entityName + "." + subtitle);
	}

	protected static SoundDefinition entityDef(String entityName, String soundName, int count)
	{
		return entityDef(entityName, soundName, count, soundName);
	}

	protected static SoundDefinition blockDef(String blockName, String soundName, int count)
	{
		return blockDef(blockName, soundName, count, 0);
	}

	protected static SoundDefinition blockDef(String blockName, String soundName, int count, final int attenuationDistance)
	{
		String defName = (blockName + "/" + soundName);
		String defSubtitle = blockName + "." + soundName;

		return def(Dir.BLOCKS, defName, count, defSubtitle, s ->
		{
			if (attenuationDistance > 0)
				s.attenuationDistance(attenuationDistance);
		});
	}

	protected static SoundDefinition itemDef(String itemName, String soundName, int count, String subtitle)
	{
		return def(Dir.ITEMS, itemName + "/" + soundName, count, itemName + "." + subtitle);
	}

	protected static SoundDefinition itemDef(String itemName, String soundName, int count)
	{
		return def(Dir.ITEMS, itemName + "/" + soundName, count, itemName);
	}

	private enum Dir
	{
		BLOCKS("block/"), ITEMS("item/"), ENTITY("entity/"), MUSIC("music/");

		public final String folderName;

		Dir(String folderName)
		{
			this.folderName = folderName;
		}
	}
}
