package com.legacy.good_nights_sleep.data;

import com.legacy.good_nights_sleep.GoodNightSleep;

import net.minecraft.tags.BlockTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.level.block.Block;

public interface GNSBlockTags
{
	public static void init()
	{
	}

	TagKey<Block> PLANKS = tag("planks");

	TagKey<Block> DREAM_LOGS = tag("dream_logs");
	TagKey<Block> WHITE_LOGS = tag("white_logs");
	TagKey<Block> DEAD_LOGS = tag("dead_logs");
	TagKey<Block> BLOOD_LOGS = tag("blood_logs");

	TagKey<Block> COBBLESTONES = tag("cobblestone");
	TagKey<Block> STONES = tag("stone");
	TagKey<Block> MUSHROOMS = tag("mushrooms");

	TagKey<Block> CANDY_ORES = tag("ores/candy");
	TagKey<Block> SPECTRITE_ORES = tag("ores/spectrite");
	TagKey<Block> POSITITE_ORES = tag("ores/positite");
	TagKey<Block> NECRUM_ORES = tag("ores/necrum");
	TagKey<Block> ZITRITE_ORES = tag("ores/zitrite");
	TagKey<Block> NEGATITE_ORES = tag("ores/negatite");

	TagKey<Block> CANDY_BLOCKS = tag("storage_blocks/candy");
	TagKey<Block> SPECTRITE_BLOCKS = tag("storage_blocks/spectrite");
	TagKey<Block> POSITITE_BLOCKS = tag("storage_blocks/positite");
	TagKey<Block> NECRUM_BLOCKS = tag("storage_blocks/necrum");
	TagKey<Block> ZITRITE_BLOCKS = tag("storage_blocks/zitrite");
	TagKey<Block> NEGATITE_BLOCKS = tag("storage_blocks/negatite");

	private static TagKey<Block> tag(String key)
	{
		return BlockTags.create(GoodNightSleep.locate(key));
	}
}