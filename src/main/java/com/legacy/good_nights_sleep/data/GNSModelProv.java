package com.legacy.good_nights_sleep.data;

import static com.legacy.good_nights_sleep.registry.GNSBlocks.*;
import static com.legacy.good_nights_sleep.registry.GNSItems.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.annotation.Nullable;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.blocks.RainbowBlock;
import com.legacy.good_nights_sleep.client.GNSBlockColoring;
import com.legacy.good_nights_sleep.client.render.GNSRenderRefs;
import com.legacy.good_nights_sleep.client.render.tile.GNSBedSpecialRenderer;
import com.legacy.good_nights_sleep.registry.GNSBlocks;
import com.legacy.good_nights_sleep.registry.GNSEquipmentAssets;
import com.legacy.good_nights_sleep.registry.GNSItems;

import net.minecraft.client.color.item.ItemTintSource;
import net.minecraft.client.data.models.BlockModelGenerators;
import net.minecraft.client.data.models.EquipmentAssetProvider;
import net.minecraft.client.data.models.ItemModelGenerators;
import net.minecraft.client.data.models.ItemModelOutput;
import net.minecraft.client.data.models.ModelProvider;
import net.minecraft.client.data.models.blockstates.BlockStateGenerator;
import net.minecraft.client.data.models.blockstates.Condition;
import net.minecraft.client.data.models.blockstates.MultiPartGenerator;
import net.minecraft.client.data.models.blockstates.MultiVariantGenerator;
import net.minecraft.client.data.models.blockstates.PropertyDispatch;
import net.minecraft.client.data.models.blockstates.Variant;
import net.minecraft.client.data.models.blockstates.VariantProperties;
import net.minecraft.client.data.models.model.ItemModelUtils;
import net.minecraft.client.data.models.model.ModelInstance;
import net.minecraft.client.data.models.model.ModelLocationUtils;
import net.minecraft.client.data.models.model.ModelTemplate;
import net.minecraft.client.data.models.model.ModelTemplates;
import net.minecraft.client.data.models.model.TextureMapping;
import net.minecraft.client.data.models.model.TextureSlot;
import net.minecraft.client.data.models.model.TexturedModel;
import net.minecraft.client.resources.model.EquipmentClientInfo;
import net.minecraft.client.resources.model.Material;
import net.minecraft.core.Direction;
import net.minecraft.data.BlockFamily;
import net.minecraft.data.CachedOutput;
import net.minecraft.data.DataProvider;
import net.minecraft.data.PackOutput;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.equipment.EquipmentAsset;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.neoforged.neoforge.client.model.generators.template.ExtendedModelTemplate;
import net.neoforged.neoforge.client.model.generators.template.ExtendedModelTemplateBuilder;

public class GNSModelProv extends ModelProvider
{
	public GNSModelProv(PackOutput output)
	{
		super(output, GoodNightSleep.MODID);
	}

	@Override
	public CompletableFuture<?> run(CachedOutput output)
	{
		return super.run(output);
	}

	@Override
	protected void registerModels(BlockModelGenerators blockModels, ItemModelGenerators itemModels)
	{
		var items = new Items(itemModels.itemModelOutput, itemModels.modelOutput);
		items.run();

		var states = new States(blockModels.blockStateOutput, blockModels.itemModelOutput, blockModels.modelOutput);
		states.run();
	}

	public static class Items extends ItemModelGenerators
	{
		public Items(ItemModelOutput output, BiConsumer<ResourceLocation, ModelInstance> models)
		{

			super(output, models);
		}

		@Override
		public void run()
		{
			this.basicItem(candy);
			this.basicItem(lollipop);
			this.basicItem(powdered_sugar);
			this.basicItem(spectrite_ingot);
			this.basicItem(positite);
			this.basicItem(necrum);
			this.basicItem(necrotic_extract);
			this.basicItem(zitrite_ingot);
			this.basicItem(negatite);

			this.basicItem(candy_bar);

			this.basicItem(raw_spectrite);
			this.basicItem(raw_zitrite);

			this.basicItem(luxurious_soup);
			this.basicItem(wretched_soup);

			// this.basicItem(rainbow_seeds);
			this.basicItem(GNSItems.rainbow_berries);

			this.handheldItem(candy_sword);
			this.handheldItem(candy_pickaxe);
			this.handheldItem(candy_axe);
			this.handheldItem(candy_shovel);
			this.handheldItem(candy_hoe);

			this.handheldItem(spectrite_sword);
			this.handheldItem(spectrite_pickaxe);
			this.handheldItem(spectrite_axe);
			this.handheldItem(spectrite_shovel);
			this.handheldItem(spectrite_hoe);

			this.handheldItem(positite_sword);
			this.handheldItem(positite_pickaxe);
			this.handheldItem(positite_axe);
			this.handheldItem(positite_shovel);
			this.handheldItem(positite_hoe);

			this.handheldItem(necrum_sword);
			this.handheldItem(necrum_pickaxe);
			this.handheldItem(necrum_axe);
			this.handheldItem(necrum_shovel);
			this.handheldItem(necrum_hoe);

			this.handheldItem(zitrite_sword);
			this.handheldItem(zitrite_pickaxe);
			this.handheldItem(zitrite_axe);
			this.handheldItem(zitrite_shovel);
			this.handheldItem(zitrite_hoe);

			this.handheldItem(negatite_sword);
			this.handheldItem(negatite_pickaxe);
			this.handheldItem(negatite_axe);
			this.handheldItem(negatite_shovel);
			this.handheldItem(negatite_hoe);

			this.generateTrimmableItem(candy_helmet, GNSEquipmentAssets.CANDY, SLOT_HELMET, false);
			this.generateTrimmableItem(candy_chestplate, GNSEquipmentAssets.CANDY, SLOT_CHESTPLATE, false);
			this.generateTrimmableItem(candy_leggings, GNSEquipmentAssets.CANDY, SLOT_LEGGINS, false);
			this.generateTrimmableItem(candy_boots, GNSEquipmentAssets.CANDY, SLOT_BOOTS, false);

			this.generateTrimmableItem(spectrite_helmet, GNSEquipmentAssets.CANDY, SLOT_HELMET, false);
			this.generateTrimmableItem(spectrite_chestplate, GNSEquipmentAssets.CANDY, SLOT_CHESTPLATE, false);
			this.generateTrimmableItem(spectrite_leggings, GNSEquipmentAssets.CANDY, SLOT_LEGGINS, false);
			this.generateTrimmableItem(spectrite_boots, GNSEquipmentAssets.CANDY, SLOT_BOOTS, false);

			this.generateTrimmableItem(positite_helmet, GNSEquipmentAssets.CANDY, SLOT_HELMET, false);
			this.generateTrimmableItem(positite_chestplate, GNSEquipmentAssets.CANDY, SLOT_CHESTPLATE, false);
			this.generateTrimmableItem(positite_leggings, GNSEquipmentAssets.CANDY, SLOT_LEGGINS, false);
			this.generateTrimmableItem(positite_boots, GNSEquipmentAssets.CANDY, SLOT_BOOTS, false);

			this.generateTrimmableItem(zitrite_helmet, GNSEquipmentAssets.CANDY, SLOT_HELMET, false);
			this.generateTrimmableItem(zitrite_chestplate, GNSEquipmentAssets.CANDY, SLOT_CHESTPLATE, false);
			this.generateTrimmableItem(zitrite_leggings, GNSEquipmentAssets.CANDY, SLOT_LEGGINS, false);
			this.generateTrimmableItem(zitrite_boots, GNSEquipmentAssets.CANDY, SLOT_BOOTS, false);

			this.generateTrimmableItem(negatite_helmet, GNSEquipmentAssets.CANDY, SLOT_HELMET, false);
			this.generateTrimmableItem(negatite_chestplate, GNSEquipmentAssets.CANDY, SLOT_CHESTPLATE, false);
			this.generateTrimmableItem(negatite_leggings, GNSEquipmentAssets.CANDY, SLOT_LEGGINS, false);
			this.generateTrimmableItem(negatite_boots, GNSEquipmentAssets.CANDY, SLOT_BOOTS, false);

			this.generateSpawnEgg(unicorn_spawn_egg, 0xffffff, 0xdf8cf8);
			// this.generateSpawnEgg(gummy_bear_spawn_egg, 0xffffff, 0xffffff);
			this.generateSpawnEgg(baby_creeper_spawn_egg, 45079, 16711901);
			this.generateSpawnEgg(tormenter_spawn_egg, 10516796, 5525034);
			this.generateSpawnEgg(herobrine_spawn_egg, 1598464, 30652);
			// this.generateSpawnEgg(giant_spawn_egg, 1598464, 30652);
		}

		private void basicItem(Item item)
		{
			this.generateFlatItem(item, ModelTemplates.FLAT_ITEM);
		}

		private void handheldItem(Item item)
		{
			this.generateFlatItem(item, ModelTemplates.FLAT_HANDHELD_ITEM);
		}
	}

	public static class Equipment extends EquipmentAssetProvider
	{
		private final PackOutput.PathProvider pathProvider;

		public Equipment(PackOutput output)
		{
			super(output);
			this.pathProvider = output.createPathProvider(PackOutput.Target.RESOURCE_PACK, "equipment");
		}

		@Override
		public CompletableFuture<?> run(CachedOutput p_387304_)
		{
			Map<ResourceKey<EquipmentAsset>, EquipmentClientInfo> map = new HashMap<>();
			bootstrap((asset, info) ->
			{
				if (map.putIfAbsent(asset, info) != null)
					throw new IllegalStateException("Tried to register equipment asset twice for id: " + asset);
			});

			return DataProvider.saveAll(p_387304_, EquipmentClientInfo.CODEC, this.pathProvider::json, map);
		}

		static void bootstrap(BiConsumer<ResourceKey<EquipmentAsset>, EquipmentClientInfo> output)
		{
			output.accept(GNSEquipmentAssets.CANDY, onlyHumanoid(GoodNightSleep.find("candy")));
			output.accept(GNSEquipmentAssets.SPECTRITE, onlyHumanoid(GoodNightSleep.find("spectrite")));
			output.accept(GNSEquipmentAssets.POSITITE, onlyHumanoid(GoodNightSleep.find("positite")));

			output.accept(GNSEquipmentAssets.ZITRITE, onlyHumanoid(GoodNightSleep.find("zitrite")));
			output.accept(GNSEquipmentAssets.NEGATITE, onlyHumanoid(GoodNightSleep.find("negatite")));
		}
	}

	public static class States extends BlockModelGenerators
	{
		public States(Consumer<BlockStateGenerator> blockStateOutput, ItemModelOutput itemModelOutput, BiConsumer<ResourceLocation, ModelInstance> modelOutput)
		{
			super(blockStateOutput, itemModelOutput, modelOutput);
		}

		@Override
		public void run()
		{
			fullBlockModelCustomGenerators = new HashMap<Block, BlockModelGenerators.BlockStateGeneratorSupplier>(fullBlockModelCustomGenerators);
			fullBlockModelCustomGenerators.put(delusion_stone, BlockModelGenerators::createMirroredCubeGenerator);

			GNSBlockFamilies.getFamilies().stream().filter(BlockFamily::shouldGenerateModel).forEach(fam -> this.family(fam.getBaseBlock()).generateFor(fam));

			this.woodProvider(dream_log).logWithHorizontal(dream_log).wood(dream_wood);
			this.woodProvider(stripped_dream_log).logWithHorizontal(stripped_dream_log).wood(stripped_dream_wood);
			this.createHangingSign(stripped_dream_log, GNSBlocks.dream_hanging_sign, dream_wall_hanging_sign);
			this.createPlantWithDefaultItem(dream_sapling, potted_dream_sapling, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createTrivialBlock(dream_leaves, TexturedModel.LEAVES);
			this.createTrivialBlock(diamond_leaves, TexturedModel.LEAVES);

			this.woodProvider(white_log).logWithHorizontal(white_log).wood(white_wood);
			this.woodProvider(stripped_white_log).logWithHorizontal(stripped_white_log).wood(stripped_white_wood);
			this.createHangingSign(stripped_white_log, GNSBlocks.white_hanging_sign, white_wall_hanging_sign);
			this.createPlantWithDefaultItem(candy_sapling, potted_candy_sapling, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createTrivialBlock(candy_leaves, TexturedModel.LEAVES);

			this.woodProvider(dead_log).logWithHorizontal(dead_log).wood(dead_wood);
			this.woodProvider(stripped_dead_log).logWithHorizontal(stripped_dead_log).wood(stripped_dead_wood);
			this.createHangingSign(stripped_dead_log, GNSBlocks.dead_hanging_sign, dead_wall_hanging_sign);

			this.woodProvider(blood_log).logWithHorizontal(blood_log).wood(blood_wood);
			this.woodProvider(stripped_blood_log).logWithHorizontal(stripped_blood_log).wood(stripped_blood_wood);
			this.createHangingSign(stripped_blood_log, GNSBlocks.blood_hanging_sign, blood_wall_hanging_sign);

			this.createRotatedMirroredVariantBlock(dream_dirt);
			this.createGrassBlock(dream_grass_block, dream_dirt, new GNSBlockColoring.DreamGrassTintSource());
			this.createTexturedFarmland(dream_dirt, dream_farmland);

			this.createGrassBlock(nightmare_grass_block, Blocks.DIRT, null);

			this.createTintedCrossWithOverlay(short_dream_grass);
			this.createPlantWithItemNoPot(short_nightmare_grass);

			this.createPlantWithDefaultItem(lollipop_bush, potted_lollipop_bush, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createPlantWithDefaultItem(prickly_nightmare_grass, potted_prickly_nightmare_grass, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createPlantWithDefaultItem(cyan_flower, potted_cyan_flower, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createPlantWithDefaultItem(orange_flower, potted_orange_flower, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createPlantWithDefaultItem(dead_flower, potted_dead_flower, BlockModelGenerators.PlantType.NOT_TINTED);

			this.createPlantWithDefaultItem(hope_mushroom, potted_hope_mushroom, BlockModelGenerators.PlantType.NOT_TINTED);
			this.createPlantWithDefaultItem(despair_mushroom, potted_despair_mushroom, BlockModelGenerators.PlantType.NOT_TINTED);

			this.createMushroomBlock(hope_mushroom_block);
			this.createMushroomBlock(despair_mushroom_block);

			this.simpleBlock(delusion_coal_ore);
			this.simpleBlock(delusion_lapis_ore);

			this.simpleBlock(candy_ore);
			this.simpleBlock(hard_candy_block);

			this.simpleBlock(spectrite_ore);
			this.simpleBlock(raw_spectrite_block);
			this.simpleBlock(spectrite_block);

			this.simpleBlock(positite_ore);
			this.simpleBlock(positite_block);

			this.simpleBlock(fossilized_necrum);
			this.simpleBlock(necrum_block);

			this.simpleBlock(zitrite_ore);
			this.simpleBlock(raw_zitrite_block);
			this.simpleBlock(zitrite_block);

			this.simpleBlock(negatite_ore);
			this.simpleBlock(negatite_block);

			this.createPotOfGold();
			this.createRainbow();

			this.createTrivialBlock(present, TexturedModel.CUBE_TOP_BOTTOM);

			this.createCropBlock(GNSBlocks.rainbow_berries, BlockStateProperties.AGE_3, 0, 1, 2, 3);

			this.createDreamBed(GNSBlocks.strange_bed, GNSRenderRefs.STRANGE_BED_MATERIAL);
			this.createDreamBed(GNSBlocks.luxurious_bed, GNSRenderRefs.LUXURIOUS_BED_MATERIAL);
			this.createDreamBed(GNSBlocks.wretched_bed, GNSRenderRefs.WRETCHED_BED_MATERIAL);
		}

		public static final TextureSlot OVERLAY_SLOT = TextureSlot.create("overlay"),
				PORTAL_SLOT = TextureSlot.create("portal");
		public static final ModelTemplate TINTED_CROSS_OVERLAY = ModelTemplates.create(GoodNightSleep.find("tinted_cross_overlay"), TextureSlot.CROSS, OVERLAY_SLOT).extend().renderType("cutout").build();

		public void createPlantWithItemNoPot(Block block)
		{
			this.createCrossBlock(block, BlockModelGenerators.PlantType.NOT_TINTED);
			this.registerSimpleItemModel(block, this.createFlatItemModelWithBlockTexture(block.asItem(), block));
		}

		public void createTintedCrossWithOverlay(Block block)
		{
			this.blockStateOutput.accept(createSimpleBlock(block, TINTED_CROSS_OVERLAY.create(block, TextureMapping.cross(block).put(OVERLAY_SLOT, TextureMapping.getBlockTexture(block, "_overlay")), this.modelOutput)));
			this.registerSimpleItemModel(block, this.createFlatItemModelWithBlockTextureAndOverlay(block.asItem(), block, "_overlay"));
		}

		public void createPotOfGold()
		{
			Block block = pot_of_gold;
			this.registerSimpleItemModel(block, this.createFlatItemModel(block.asItem()));
			this.blockStateOutput.accept(createSimpleBlock(block, ModelTemplates.CAULDRON_FULL.create(block, TextureMapping.cauldron(TextureMapping.getBlockTexture(block)), this.modelOutput)));
		}

		private static final ExtendedModelTemplateBuilder frontBackTemplate()
		{
			return ModelTemplates.create(TextureSlot.FRONT, TextureSlot.BACK).extend().parent(ResourceLocation.withDefaultNamespace("block/block"));
		}

		private static final ExtendedModelTemplate EW_RAINBOW = frontBackTemplate().element(e ->
		{
			e.from(6, 0, 0).to(10, 16, 16);

			e.face(Direction.EAST, f -> f.uvs(0, 0, 16, 16).texture(TextureSlot.FRONT));
			e.face(Direction.WEST, f -> f.uvs(0, 0, 16, 16).texture(TextureSlot.BACK));

		}).renderType("translucent").build();

		private static final ExtendedModelTemplate NS_RAINBOW = frontBackTemplate().element(e ->
		{
			e.from(0, 0, 6).to(16, 16, 10);

			e.face(Direction.NORTH, f -> f.uvs(0, 0, 16, 16).texture(TextureSlot.BACK));
			e.face(Direction.SOUTH, f -> f.uvs(0, 0, 16, 16).texture(TextureSlot.FRONT));

		}).renderType("translucent").build();

		public void createRainbow()
		{
			Block block = rainbow;

			Variant start = this.rainbowVariant("start", "end", false),
					startCorner = this.rainbowVariant("start_corner", "end_corner", false),
					top = this.rainbowVariant("top", false),
					endCorner = this.rainbowVariant("end_corner", "start_corner", false),
					end = this.rainbowVariant("end", "start", false);

			Variant startEW = this.rainbowVariant("start", "end", true),
					startCornerEW = this.rainbowVariant("start_corner", "end_corner", true),
					topEW = this.rainbowVariant("top", true),
					endCornerEW = this.rainbowVariant("end_corner", "start_corner", true),
					endEW = this.rainbowVariant("end", "start", true);

			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(block).with(PropertyDispatch.properties(BlockStateProperties.HORIZONTAL_AXIS, RainbowBlock.CORNER_TYPE, RainbowBlock.SIDE_TYPE).generate((dir, cornerType, sideType) ->
			{
				boolean ew = dir == Direction.Axis.Z;
				return cornerType == 1 ? (ew ? startCornerEW : startCorner) : cornerType == 2 ? (ew ? endCornerEW : endCorner) : sideType == 1 ? (ew ? topEW : top) : sideType == 2 ? (ew ? endEW : end) : (ew ? startEW : start);
			})));
		}

		private final Variant rainbowVariant(String suffix, boolean ew)
		{
			return rainbowVariant(suffix, suffix, ew);
		}

		private final Variant rainbowVariant(String suffix, String backTexSuffix, boolean ew)
		{
			var nsTemplate = NS_RAINBOW;
			var ewTemplate = EW_RAINBOW;

			ResourceLocation tex = TextureMapping.getBlockTexture(rainbow, "_" + suffix),
					backTex = TextureMapping.getBlockTexture(rainbow, "_" + backTexSuffix);

			return Variant.variant().with(VariantProperties.MODEL, (ew ? ewTemplate : nsTemplate).createWithSuffix(rainbow, "_" + suffix + "_" + (ew ? "ew" : "ns"), new TextureMapping().put(TextureSlot.FRONT, tex).put(TextureSlot.BACK, backTex).copyForced(TextureSlot.FRONT, TextureSlot.PARTICLE), this.modelOutput));
		}

		public void createBush(Block block)
		{
			this.createCrossBlock(block, BlockModelGenerators.PlantType.NOT_TINTED);
			this.registerSimpleItemModel(block, this.createFlatItemModelWithBlockTexture(block.asItem(), block));
		}

		public void createDreamBed(Block block, Material mat)
		{
			@SuppressWarnings("deprecation")
			ResourceLocation resourcelocation = ModelLocationUtils.decorateBlockModelLocation("bed");
			this.blockStateOutput.accept(createSimpleBlock(block, resourcelocation));
			Item item = block.asItem();
			ResourceLocation resourcelocation1 = ModelTemplates.BED_INVENTORY.create(ModelLocationUtils.getModelLocation(item), TextureMapping.particle(Blocks.OAK_PLANKS), this.modelOutput);
			this.itemModelOutput.accept(item, ItemModelUtils.specialModel(resourcelocation1, new GNSBedSpecialRenderer.Unbaked(mat)));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createPlant(Block flower, Block pot, BlockModelGenerators.PlantType type)
		{
			this.createCrossBlock(flower, type);
			TextureMapping texturemapping = type.getPlantTextureMapping(flower);
			ResourceLocation resourcelocation = type.getCrossPot().extend().renderType(ResourceLocation.parse("cutout")).build().create(pot, texturemapping, this.modelOutput);
			this.blockStateOutput.accept(createSimpleBlock(pot, resourcelocation));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createCrossBlock(Block flower, BlockModelGenerators.PlantType type, TextureMapping mapping)
		{
			ResourceLocation resourcelocation = type.getCross().extend().renderType(ResourceLocation.parse("cutout")).build().create(flower, mapping, this.modelOutput);
			this.blockStateOutput.accept(createSimpleBlock(flower, resourcelocation));
		}

		public void ladder(Block block)
		{
			ResourceLocation modelName = ModelTemplates.create("ladder", TextureSlot.TEXTURE, TextureSlot.PARTICLE).extend().renderType("cutout").build().create(block, new TextureMapping().put(TextureSlot.TEXTURE, TextureMapping.getBlockTexture(block)).put(TextureSlot.PARTICLE, TextureMapping.getBlockTexture(block)), modelOutput);
			MultiVariantGenerator gen = MultiVariantGenerator.multiVariant(block, Variant.variant().with(VariantProperties.MODEL, modelName));
			this.blockStateOutput.accept(gen);

			this.registerSimpleFlatItemModel(block);
		}

		public void createTexturedFarmland(Block dirt, Block farmland)
		{
			TextureMapping texturemapping = new TextureMapping().put(TextureSlot.DIRT, TextureMapping.getBlockTexture(dirt)).put(TextureSlot.TOP, TextureMapping.getBlockTexture(farmland));
			TextureMapping texturemapping1 = new TextureMapping().put(TextureSlot.DIRT, TextureMapping.getBlockTexture(dirt)).put(TextureSlot.TOP, TextureMapping.getBlockTexture(farmland, "_moist"));
			ResourceLocation resourcelocation = ModelTemplates.FARMLAND.create(farmland, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation1 = ModelTemplates.FARMLAND.create(TextureMapping.getBlockTexture(farmland, "_moist"), texturemapping1, this.modelOutput);
			this.blockStateOutput.accept(MultiVariantGenerator.multiVariant(farmland).with(createEmptyOrFullDispatch(BlockStateProperties.MOISTURE, 7, resourcelocation1, resourcelocation)));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createDoor(Block door)
		{
			String renderType = "cutout";
			TextureMapping texturemapping = TextureMapping.door(door);
			ResourceLocation resourcelocation = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_LEFT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation1 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_LEFT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation2 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_RIGHT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation3 = extendWithType(renderType, ModelTemplates.DOOR_BOTTOM_RIGHT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation4 = extendWithType(renderType, ModelTemplates.DOOR_TOP_LEFT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation5 = extendWithType(renderType, ModelTemplates.DOOR_TOP_LEFT_OPEN).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation6 = extendWithType(renderType, ModelTemplates.DOOR_TOP_RIGHT).create(door, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation7 = extendWithType(renderType, ModelTemplates.DOOR_TOP_RIGHT_OPEN).create(door, texturemapping, this.modelOutput);
			this.registerSimpleFlatItemModel(door.asItem());
			this.blockStateOutput.accept(createDoor(door, resourcelocation, resourcelocation1, resourcelocation2, resourcelocation3, resourcelocation4, resourcelocation5, resourcelocation6, resourcelocation7));
		}

		/**
		 * Copy of super, but with added render type arg
		 */
		@Override
		public void createGlassBlocks(Block block, Block pane)
		{
			this.createTrivialCube(block);

			String renderType = "cutout";
			TextureMapping texturemapping = TextureMapping.pane(block, pane);
			ResourceLocation resourcelocation = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_POST).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation1 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_SIDE).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation2 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_SIDE_ALT).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation3 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_NOSIDE).create(pane, texturemapping, this.modelOutput);
			ResourceLocation resourcelocation4 = extendWithType(renderType, ModelTemplates.STAINED_GLASS_PANE_NOSIDE_ALT).create(pane, texturemapping, this.modelOutput);
			Item item = pane.asItem();
			this.registerSimpleItemModel(item, this.createFlatItemModelWithBlockTexture(item, block));
			this.blockStateOutput.accept(MultiPartGenerator.multiPart(pane).with(Variant.variant().with(VariantProperties.MODEL, resourcelocation)).with(Condition.condition().term(BlockStateProperties.NORTH, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation1)).with(Condition.condition().term(BlockStateProperties.EAST, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation1).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90)).with(Condition.condition().term(BlockStateProperties.SOUTH, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation2)).with(Condition.condition().term(BlockStateProperties.WEST, true), Variant.variant().with(VariantProperties.MODEL, resourcelocation2).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90)).with(Condition.condition().term(BlockStateProperties.NORTH, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation3)).with(Condition.condition().term(BlockStateProperties.EAST, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation4)).with(Condition.condition().term(BlockStateProperties.SOUTH, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation4).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R90)).with(Condition.condition().term(BlockStateProperties.WEST, false), Variant.variant().with(VariantProperties.MODEL, resourcelocation3).with(VariantProperties.Y_ROT, VariantProperties.Rotation.R270)));
		}

		private static ModelTemplate extendWithType(String renderType, ModelTemplate template)
		{
			return template.extend().renderType(ResourceLocation.parse(renderType)).build();
		}

		public void simpleBlock(Block block)
		{
			this.simpleBlock(block, (String) null);
		}

		public void simpleBlock(Block block, @Nullable String renderType)
		{
			this.createTrivialBlock(block, renderType != null ? TexturedModel.createDefault(TextureMapping::cube, ModelTemplates.CUBE_ALL.extend().renderType(ResourceLocation.parse(renderType)).build()) : TexturedModel.CUBE);
		}

		/**
		 * Giving crops the cutout rendertype
		 */
		@Override
		public ResourceLocation createSuffixedVariant(Block block, String suffix, ModelTemplate modelTemplate, Function<ResourceLocation, TextureMapping> textureMappingGetter)
		{
			return modelTemplate == ModelTemplates.CROP ? super.createSuffixedVariant(block, suffix, extendWithType("cutout", modelTemplate), textureMappingGetter) : super.createSuffixedVariant(block, suffix, modelTemplate, textureMappingGetter);
		}

		public void createGrassBlock(Block grass, Block dirt, @Nullable ItemTintSource tint)
		{
			ResourceLocation resourcelocation = TextureMapping.getBlockTexture(dirt);
			TextureMapping snowMapping = new TextureMapping().put(TextureSlot.BOTTOM, resourcelocation).copyForced(TextureSlot.BOTTOM, TextureSlot.PARTICLE).put(TextureSlot.TOP, TextureMapping.getBlockTexture(grass, "_top")).put(TextureSlot.SIDE, TextureMapping.getBlockTexture(grass, "_snow"));

			if (tint == null)
			{
				Variant variant = Variant.variant().with(VariantProperties.MODEL, ModelTemplates.CUBE_BOTTOM_TOP.createWithSuffix(grass, "_snow", snowMapping, this.modelOutput));
				ResourceLocation resourcelocation1 = TexturedModel.CUBE_TOP_BOTTOM.get(grass).updateTextures(p_388599_ -> p_388599_.put(TextureSlot.BOTTOM, resourcelocation)).create(grass, this.modelOutput);
				this.createGrassLikeBlock(grass, resourcelocation1, variant);

				return;
			}

			var grassTemplate = ModelTemplates.create(TextureSlot.TOP, TextureSlot.BOTTOM, TextureSlot.SIDE, OVERLAY_SLOT).extend().parent(ResourceLocation.withDefaultNamespace("block/grass_block")).renderType("cutout_mipped").build();
			TextureMapping texturemapping = new TextureMapping().put(TextureSlot.BOTTOM, resourcelocation).copyForced(TextureSlot.BOTTOM, TextureSlot.PARTICLE).put(TextureSlot.TOP, TextureMapping.getBlockTexture(grass, "_top")).put(TextureSlot.SIDE, TextureMapping.getBlockTexture(grass, "_side")).put(OVERLAY_SLOT, TextureMapping.getBlockTexture(grass, "_side_overlay"));

			Variant normalVariant = Variant.variant().with(VariantProperties.MODEL, grassTemplate.create(grass, texturemapping, this.modelOutput));
			Variant snowVariant = Variant.variant().with(VariantProperties.MODEL, ModelTemplates.CUBE_BOTTOM_TOP.createWithSuffix(grass, "_snow", snowMapping, this.modelOutput));
			this.createGrassLikeBlock(grass, ModelLocationUtils.getModelLocation(grass), Variant.merge(normalVariant, snowVariant));
			this.registerSimpleTintedItemModel(grass, ModelLocationUtils.getModelLocation(grass), tint);
		}
	}
}
