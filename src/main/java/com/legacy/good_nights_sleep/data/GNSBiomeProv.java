package com.legacy.good_nights_sleep.data;

import com.legacy.good_nights_sleep.registry.GNSEntityTypes;
import com.legacy.good_nights_sleep.registry.GNSFeatures;
import com.legacy.good_nights_sleep.registry.GNSFeatures.Placements;
import com.legacy.good_nights_sleep.util.Pointer;

import net.minecraft.core.HolderGetter;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BiomeDefaultFeatures;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import net.minecraft.world.level.biome.AmbientMoodSettings;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.biome.BiomeGenerationSettings;
import net.minecraft.world.level.biome.BiomeSpecialEffects;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.carver.ConfiguredWorldCarver;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;

public class GNSBiomeProv
{
	/**
	 * The Sleepy Hills biome. The OG biome from the original, renamed to fit its
	 * hilly nature.
	 */
	public static Biome sleepyHills(BootstrapContext<?> bootstrap, float tempIn, float downfallIn, int waterColorIn, int waterFogColorIn)
	{
		MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

		BiomeDefaultFeatures.farmAnimals(spawns);
		spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(GNSEntityTypes.UNICORN, 5, 1, 4));

		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(GNSEntityTypes.BABY_CREEPER, 10, 1, 4));

		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

		GNSFeatures.addDreamTrees(builder);
		GNSFeatures.addHugeHopeMushrooms(builder);
		GNSFeatures.addScatteredDreamFeatures(builder);
		GNSFeatures.addDreamOres(builder);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_GRASS_NOISE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_FLOWERS_3);

		GNSFeatures.addCarvers(builder);

		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(tempIn).downfall(downfallIn).specialEffects((new BiomeSpecialEffects.Builder()).grassColorOverride(0xffffff).waterColor(waterColorIn).waterFogColor(waterFogColorIn).fogColor(12638463).skyColor(calculateSkyColor(tempIn)).ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}

	/**
	 * The Good Dream Plains biome.
	 */
	public static Biome goodDreamPlains(BootstrapContext<?> bootstrap, float tempIn, float downfallIn, int waterColorIn, int waterFogColorIn)
	{
		MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

		BiomeDefaultFeatures.farmAnimals(spawns);
		spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(GNSEntityTypes.UNICORN, 5, 1, 4));

		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(GNSEntityTypes.BABY_CREEPER, 10, 1, 4));

		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.PLAINS_DIAMOND_TREE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.PLAINS_HOPE_MUSHROOM);

		GNSFeatures.addScatteredDreamFeatures(builder);
		GNSFeatures.addDreamOres(builder);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_GRASS_NOISE);

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_FLOWERS_3);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_LOLLIPOPS_5);

		GNSFeatures.addCarvers(builder);

		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(tempIn).downfall(downfallIn).specialEffects((new BiomeSpecialEffects.Builder()).grassColorOverride(0xffffff).waterColor(waterColorIn).waterFogColor(waterFogColorIn).fogColor(12638463).skyColor(calculateSkyColor(tempIn)).ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}

	/**
	 * The Dreamy Forest biome.
	 */
	public static Biome dreamyForest(BootstrapContext<?> bootstrap, float tempIn, float downfallIn, int waterColorIn, int waterFogColorIn)
	{
		MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

		BiomeDefaultFeatures.farmAnimals(spawns);
		// spawns.withSpawner(EntityClassification.CREATURE, new
		// MobSpawnInfo.Spawners(GNSEntityTypes.UNICORN, 90, 1, 4));

		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(GNSEntityTypes.BABY_CREEPER, 10, 1, 4));

		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.FOREST_DREAM_TREE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.FOREST_CANDY_TREE);

		GNSFeatures.addDreamSponges(builder);
		GNSFeatures.addDreamOres(builder);

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_GRASS_NOISE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_FLOWERS_3);

		GNSFeatures.addCarvers(builder);

		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(tempIn).downfall(downfallIn).specialEffects((new BiomeSpecialEffects.Builder()).grassColorOverride(0xffffff).waterColor(waterColorIn).waterFogColor(waterFogColorIn).fogColor(12638463).skyColor(calculateSkyColor(tempIn)).ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}

	/**
	 * The Lollipop Lands biome.
	 */
	public static Biome lollipopLands(BootstrapContext<?> bootstrap, float tempIn, float downfallIn, int waterColorIn, int waterFogColorIn)
	{
		MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

		BiomeDefaultFeatures.farmAnimals(spawns);

		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(GNSEntityTypes.BABY_CREEPER, 10, 1, 4));

		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.LANDS_CANDY_TREE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.LANDS_DREAM_TREE);

		GNSFeatures.addDreamSponges(builder);
		GNSFeatures.addDreamOres(builder);

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_GRASS_NOISE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_FLOWERS_3);

		GNSFeatures.addCarvers(builder);
		// TODO CCFF99
		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(tempIn).downfall(downfallIn).specialEffects((new BiomeSpecialEffects.Builder()).grassColorOverride(0xFFFFCC).waterColor(waterColorIn).waterFogColor(waterFogColorIn).fogColor(12638463).skyColor(calculateSkyColor(tempIn)).ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}

	/**
	 * The Hopeful Fields biome. (UNUSED DUE TO UGLY)
	 */
	/*public static Biome createHopefulFieldsBiome(BootstrapContext<?> bootstrap, float tempIn, float downfallIn, int waterColorIn, int waterFogColorIn)
	{
		MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();
	
		BiomeDefaultFeatures.farmAnimals(spawns);
		spawns.addSpawn(EntityClassification.CREATURE, new MobSpawnInfo.Spawners(GNSEntityTypes.SPAWNER_ENTITY, 140, 1, 1));
		spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(GNSEntityTypes.UNICORN, 5, 1, 4)); // 90
	
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(GNSEntityTypes.BABY_CREEPER, 10, 1, 4));
	
		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));
	
		GNSFeatures.addHopeMushroomFields(builder);
		GNSFeatures.addDreamSponges(builder);
		GNSFeatures.addDreamOres(builder);
	
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_GRASS_NOISE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_FLOWERS_5);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.DREAM_MUSHROOMS_25);
	
		GNSFeatures.addCarvers(builder);
	
		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(tempIn).downfall(downfallIn).specialEffects((new BiomeSpecialEffects.Builder()).grassColorOverride(0xffffff).waterColor(waterColorIn).waterFogColor(waterFogColorIn).fogColor(12638463).skyColor(calculateSkyColor(tempIn)).ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}*/

	private static final int NIGHTMARE_SKY = 0x8c4949;//0x752828;

	public static Biome nightmareMound(BootstrapContext<?> bootstrap, float tempIn, float downfallIn, int waterColorIn, int waterFogColorIn)
	{
		MobSpawnSettings.Builder spawns = getDefaultNightmareSpawns();

		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.HILLS_DEAD_TREE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.HILLS_BLOOD_TREE);

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, Placements.HUGE_DESPAIR_MUSHROOM);

		GNSFeatures.addScatteredNightmareFeatures(builder);
		GNSFeatures.addNightmareOres(builder);

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.NIGHTMARE_GRASS_NOISE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.PRICKLY_NIGHTMARE_GRASS_3);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.NIGHTMARE_FLOWERS_5);

		GNSFeatures.addCarvers(builder);
		BiomeDefaultFeatures.addDefaultMonsterRoom(builder); // monster rooms

		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(tempIn).downfall(downfallIn).specialEffects((new BiomeSpecialEffects.Builder()).waterColor(waterColorIn).waterFogColor(waterFogColorIn).fogColor(3344392).skyColor(NIGHTMARE_SKY).ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}

	public static Biome shamefulPlains(BootstrapContext<?> bootstrap, float tempIn, float downfallIn, int waterColorIn, int waterFogColorIn)
	{
		MobSpawnSettings.Builder spawns = getDefaultNightmareSpawns();

		// undead horses
		/*spawns.addSpawn(EntityClassification.CREATURE, new MobSpawnInfo.Spawners(GNSEntityTypes.SPAWNER_ENTITY, 1, 1, 1));*/

		spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(EntityType.ZOMBIE_HORSE, 1, 1, 3));
		spawns.addSpawn(MobCategory.CREATURE, new MobSpawnSettings.SpawnerData(EntityType.SKELETON_HORSE, 1, 1, 3));

		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.PLAINS_BLOOD_TREE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.PLAINS_DESPAIR_MUSHROOM);

		GNSFeatures.addScatteredNightmareFeatures(builder);
		GNSFeatures.addNightmareOres(builder);

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.NIGHTMARE_GRASS_NOISE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.PRICKLY_NIGHTMARE_GRASS_5);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.NIGHTMARE_FLOWERS_5);

		GNSFeatures.addCarvers(builder);
		BiomeDefaultFeatures.addDefaultMonsterRoom(builder); // monster rooms

		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(tempIn).downfall(downfallIn).specialEffects((new BiomeSpecialEffects.Builder()).waterColor(waterColorIn).waterFogColor(waterFogColorIn).fogColor(3344392).skyColor(NIGHTMARE_SKY).ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}

	public static Biome wastedForest(BootstrapContext<?> bootstrap, float tempIn, float downfallIn, int waterColorIn, int waterFogColorIn)
	{
		MobSpawnSettings.Builder spawns = getDefaultNightmareSpawns();

		RegistrarBuilder builder = new RegistrarBuilder(bootstrap.lookup(Registries.PLACED_FEATURE), bootstrap.lookup(Registries.CONFIGURED_CARVER));

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.FOREST_DEAD_TREE);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.FOREST_BLOOD_TREE);

		GNSFeatures.addScatteredNightmareFeatures(builder); // 20
		GNSFeatures.addNightmareOres(builder);

		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.NIGHTMARE_GRASS_5);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.PRICKLY_NIGHTMARE_GRASS_5);
		builder.addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, GNSFeatures.Placements.NIGHTMARE_FLOWERS_5);

		GNSFeatures.addCarvers(builder);
		BiomeDefaultFeatures.addDefaultMonsterRoom(builder); // monster rooms

		return (new Biome.BiomeBuilder()).hasPrecipitation(false).temperature(tempIn).downfall(downfallIn).specialEffects((new BiomeSpecialEffects.Builder()).waterColor(waterColorIn).waterFogColor(waterFogColorIn).fogColor(3344392).skyColor(NIGHTMARE_SKY).ambientMoodSound(AmbientMoodSettings.LEGACY_CAVE_SETTINGS).build()).mobSpawnSettings(spawns.build()).generationSettings(builder.build()).build();
	}

	private static MobSpawnSettings.Builder getDefaultNightmareSpawns()
	{
		MobSpawnSettings.Builder spawns = new MobSpawnSettings.Builder();

		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(GNSEntityTypes.TORMENTER, 100, 4, 4));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(GNSEntityTypes.HEROBRINE, 10, 1, 1));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.SPIDER, 100, 4, 4));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.ZOMBIE, 95, 4, 4));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.ZOMBIE_VILLAGER, 5, 1, 1));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.SKELETON, 100, 4, 4));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.CREEPER, 100, 4, 4));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.SLIME, 100, 4, 4));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.ENDERMAN, 10, 1, 4));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.WITCH, 5, 1, 1));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.GHAST, 30, 4, 4));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.ZOMBIFIED_PIGLIN, 30, 4, 4));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.MAGMA_CUBE, 2, 4, 4));
		spawns.addSpawn(MobCategory.MONSTER, new MobSpawnSettings.SpawnerData(EntityType.SILVERFISH, 5, 4, 4));

		return spawns;
	}

	public static class RegistrarBuilder extends BiomeGenerationSettings.Builder
	{
		public RegistrarBuilder(HolderGetter<PlacedFeature> featureGetter, HolderGetter<ConfiguredWorldCarver<?>> carverGetter)
		{
			super(featureGetter, carverGetter);
		}

		public BiomeGenerationSettings.Builder addFeature(GenerationStep.Decoration step, Pointer<PlacedFeature> registrar)
		{
			return super.addFeature(step, registrar.getKey());
		}
	}

	private static int calculateSkyColor(float tempIn)
	{
		float lvt_1_1_ = tempIn / 3.0F;
		lvt_1_1_ = Mth.clamp(lvt_1_1_, -1.0F, 1.0F);
		return Mth.hsvToRgb(0.62222224F - lvt_1_1_ * 0.05F, 0.5F + lvt_1_1_ * 0.1F, 1.0F);
	}
}
