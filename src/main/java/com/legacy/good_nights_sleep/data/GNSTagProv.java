package com.legacy.good_nights_sleep.data;

import static com.legacy.good_nights_sleep.registry.GNSBlocks.*;
import static com.legacy.good_nights_sleep.registry.GNSItems.*;

import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Stream;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.registry.GNSBiomes;
import com.legacy.good_nights_sleep.registry.GNSBlocks;
import com.legacy.good_nights_sleep.registry.GNSItems;
import com.legacy.good_nights_sleep.util.Pointer;

import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BiomeTagsProvider;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.BiomeTags;
import net.minecraft.tags.BlockTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.BushBlock;
import net.minecraft.world.level.block.CeilingHangingSignBlock;
import net.minecraft.world.level.block.FlowerBlock;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.GrassBlock;
import net.minecraft.world.level.block.LeavesBlock;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.StairBlock;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.WallBlock;
import net.minecraft.world.level.block.WallHangingSignBlock;
import net.minecraft.world.level.block.WallSignBlock;
import net.neoforged.neoforge.common.Tags;
import net.neoforged.neoforge.common.data.BlockTagsProvider;

@SuppressWarnings("unchecked")
public class GNSTagProv
{
	public static class BlockTagProv extends BlockTagsProvider
	{
		public BlockTagProv(DataGenerator generatorIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, GoodNightSleep.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			goodNightsSleep(prov);
			vanilla(prov);
			forge(prov);
		}

		void goodNightsSleep(Provider prov)
		{
			this.tag(GNSBlockTags.PLANKS).add(GNSBlocks.dream_planks, GNSBlocks.white_planks, GNSBlocks.dead_planks, GNSBlocks.blood_planks);

			this.tag(GNSBlockTags.DREAM_LOGS).add(GNSBlocks.dream_log, GNSBlocks.dream_wood, GNSBlocks.stripped_dream_log, GNSBlocks.stripped_dream_wood);
			this.tag(GNSBlockTags.WHITE_LOGS).add(GNSBlocks.white_log, GNSBlocks.white_wood, GNSBlocks.stripped_white_log, GNSBlocks.stripped_white_wood);
			this.tag(GNSBlockTags.DEAD_LOGS).add(GNSBlocks.dead_log, GNSBlocks.dead_wood, GNSBlocks.stripped_dead_log, GNSBlocks.stripped_dead_wood);
			this.tag(GNSBlockTags.BLOOD_LOGS).add(GNSBlocks.blood_log, GNSBlocks.blood_wood, GNSBlocks.stripped_blood_log, GNSBlocks.stripped_blood_wood);

			this.tag(GNSBlockTags.COBBLESTONES).add(GNSBlocks.delusion_cobblestone);
			this.tag(GNSBlockTags.STONES).add(GNSBlocks.delusion_stone);
			this.tag(GNSBlockTags.MUSHROOMS).add(GNSBlocks.hope_mushroom, GNSBlocks.despair_mushroom);

			this.tag(GNSBlockTags.CANDY_ORES).add(GNSBlocks.candy_ore);
			this.tag(GNSBlockTags.SPECTRITE_ORES).add(GNSBlocks.spectrite_ore);
			this.tag(GNSBlockTags.POSITITE_ORES).add(GNSBlocks.positite_ore);
			this.tag(GNSBlockTags.NECRUM_ORES).add(GNSBlocks.fossilized_necrum);
			this.tag(GNSBlockTags.ZITRITE_ORES).add(GNSBlocks.zitrite_ore);
			this.tag(GNSBlockTags.NEGATITE_ORES).add(GNSBlocks.negatite_ore);

			this.tag(GNSBlockTags.CANDY_BLOCKS).add(GNSBlocks.hard_candy_block);
			this.tag(GNSBlockTags.SPECTRITE_BLOCKS).add(GNSBlocks.spectrite_block);
			this.tag(GNSBlockTags.POSITITE_BLOCKS).add(GNSBlocks.positite_block);
			this.tag(GNSBlockTags.NECRUM_BLOCKS).add(GNSBlocks.necrum_block);
			this.tag(GNSBlockTags.ZITRITE_BLOCKS).add(GNSBlocks.zitrite_block);
			this.tag(GNSBlockTags.NEGATITE_BLOCKS).add(GNSBlocks.negatite_block);
		}

		void vanilla(Provider prov)
		{
			addMatching(block -> block instanceof FlowerPotBlock, BlockTags.FLOWER_POTS);
			addMatching(block -> block instanceof LeavesBlock, BlockTags.LEAVES);
			addMatching(block -> block instanceof SaplingBlock, BlockTags.SAPLINGS);
			addMatching(block -> block instanceof SlabBlock, BlockTags.SLABS);
			addMatching(block -> block instanceof FlowerBlock, BlockTags.SMALL_FLOWERS);
			addMatching(block -> block instanceof StairBlock, BlockTags.STAIRS);
			addMatching(block -> block instanceof GrassBlock, BlockTags.VALID_SPAWN);
			addMatching(block -> block instanceof WallBlock, BlockTags.WALLS);
			this.tag(BlockTags.WOODEN_BUTTONS).add(dream_button, white_button, dead_button, blood_button);
			this.tag(BlockTags.WOODEN_PRESSURE_PLATES).add(dream_pressure_plate, white_pressure_plate, dead_pressure_plate, blood_pressure_plate);
			this.tag(BlockTags.WOODEN_DOORS).add(GNSBlocks.dream_door, GNSBlocks.white_door, GNSBlocks.dead_door, GNSBlocks.blood_door);
			this.tag(BlockTags.WOODEN_TRAPDOORS).add(dream_trapdoor, white_trapdoor, dead_trapdoor, blood_trapdoor);
			this.tag(BlockTags.WOODEN_SLABS).add(dream_slab, white_slab, dead_slab, blood_slab);
			this.tag(BlockTags.WOODEN_STAIRS).add(dream_stairs, white_stairs, dead_stairs, blood_stairs);
			this.tag(BlockTags.WOODEN_FENCES).add(dream_fence, white_fence, dead_fence, blood_fence);
			this.tag(BlockTags.FENCE_GATES).add(dream_fence_gate, white_fence_gate, dead_fence_gate, blood_fence_gate);
			this.tag(BlockTags.PLANKS).addTag(GNSBlockTags.PLANKS);
			this.tag(BlockTags.LOGS).addTags(GNSBlockTags.DEAD_LOGS, GNSBlockTags.BLOOD_LOGS);
			this.tag(BlockTags.LOGS_THAT_BURN).addTags(GNSBlockTags.DREAM_LOGS, GNSBlockTags.WHITE_LOGS);
			addMatching(BlockTags.STANDING_SIGNS, b -> b instanceof StandingSignBlock);
			addMatching(BlockTags.WALL_SIGNS, b -> b instanceof WallSignBlock);
			addMatching(BlockTags.CEILING_HANGING_SIGNS, b -> b instanceof CeilingHangingSignBlock);
			addMatching(BlockTags.WALL_HANGING_SIGNS, b -> b instanceof WallHangingSignBlock);

			this.tag(BlockTags.STONE_BUTTONS).add(delusion_button);
			this.tag(BlockTags.STONE_PRESSURE_PLATES).add(delusion_pressure_plate);

			this.tag(BlockTags.BEACON_BASE_BLOCKS).addTags(GNSBlockTags.SPECTRITE_BLOCKS, GNSBlockTags.POSITITE_BLOCKS, GNSBlockTags.ZITRITE_BLOCKS, GNSBlockTags.NEGATITE_BLOCKS);

			this.tag(BlockTags.FLOWER_POTS).addOptional(GoodNightSleep.locate("potted_dream_grass")).addOptional(GoodNightSleep.locate("potted_nightmare_grass")).addOptional(GoodNightSleep.locate("potted_prickly_nightmare_grass"));

			this.tag(BlockTags.ANIMALS_SPAWNABLE_ON).add(GNSBlocks.dream_grass_block, GNSBlocks.nightmare_grass_block);
			this.tag(BlockTags.DIRT).add(GNSBlocks.dream_grass_block, GNSBlocks.dream_dirt, GNSBlocks.nightmare_grass_block);
			this.tag(BlockTags.BEDS).add(GNSBlocks.luxurious_bed, GNSBlocks.wretched_bed, GNSBlocks.strange_bed);
			this.tag(BlockTags.MOSS_REPLACEABLE).add(delusion_stone);
			this.tag(BlockTags.SCULK_REPLACEABLE).addTag(GNSBlockTags.STONES);

			this.tag(BlockTags.OVERWORLD_CARVER_REPLACEABLES).add(GNSBlocks.delusion_stone);

			this.tag(BlockTags.GUARDED_BY_PIGLINS).add(GNSBlocks.pot_of_gold);

			this.tag(BlockTags.MUSHROOM_GROW_BLOCK).add(GNSBlocks.dream_grass_block, GNSBlocks.dream_dirt, GNSBlocks.nightmare_grass_block);

			this.tag(BlockTags.COAL_ORES).add(GNSBlocks.delusion_coal_ore);
			this.tag(BlockTags.LAPIS_ORES).add(GNSBlocks.delusion_lapis_ore);

			this.tag(BlockTags.STONE_BRICKS).add(GNSBlocks.delusion_stone_bricks);

			/**
			 * Go to GNSEvents.onPlayerRightClickBlock to enable debug output for blocks
			 * without tools
			 */
			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(delusion_button, delusion_pressure_plate, pot_of_gold);
			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(delusion_stone, delusion_stone_slab, delusion_stone_stairs, delusion_stone_bricks, delusion_stone_brick_slab, delusion_stone_brick_stairs, delusion_stone_brick_wall, delusion_cobblestone, delusion_cobblestone_slab, delusion_cobblestone_stairs, delusion_cobblestone_wall);
			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(candy_ore, spectrite_ore, positite_ore, fossilized_necrum, zitrite_ore, negatite_ore, delusion_coal_ore, delusion_lapis_ore);
			this.tag(BlockTags.MINEABLE_WITH_PICKAXE).add(hard_candy_block, spectrite_block, raw_spectrite_block, positite_block, necrum_block, zitrite_block, raw_zitrite_block, negatite_block);

			this.tag(BlockTags.MINEABLE_WITH_AXE).add(short_dream_grass, short_nightmare_grass, prickly_nightmare_grass, hope_mushroom, hope_mushroom_block, despair_mushroom, despair_mushroom_block);
			this.addMatching(BlockTags.MINEABLE_WITH_AXE, b -> b instanceof FlowerBlock || b instanceof BushBlock);

			this.tag(BlockTags.MINEABLE_WITH_SHOVEL).add(dream_grass_block, dream_dirt, dream_farmland, nightmare_grass_block);

			this.tag(BlockTags.MINEABLE_WITH_HOE).add(present, necrum_block, dream_leaves, candy_leaves, diamond_leaves);

			this.tag(BlockTags.NEEDS_STONE_TOOL).addTags(GNSBlockTags.SPECTRITE_BLOCKS, GNSBlockTags.SPECTRITE_ORES, GNSBlockTags.ZITRITE_BLOCKS, GNSBlockTags.ZITRITE_ORES);
			this.tag(BlockTags.NEEDS_IRON_TOOL).addTags(GNSBlockTags.POSITITE_BLOCKS, GNSBlockTags.POSITITE_ORES, GNSBlockTags.NEGATITE_BLOCKS, GNSBlockTags.NEGATITE_ORES);

			this.tag(BlockTags.SNIFFER_DIGGABLE_BLOCK).add(dream_dirt, dream_grass_block, nightmare_grass_block);

			this.tag(BlockTags.REPLACEABLE_BY_TREES).add(short_dream_grass, short_nightmare_grass, hope_mushroom, despair_mushroom);
			var appender = this.tag(BlockTags.REPLACEABLE);
			prov.lookupOrThrow(Registries.BLOCK).filterElements(b -> b.defaultBlockState().canBeReplaced()).listElementIds().filter(r -> r.location().getNamespace().equals(GoodNightSleep.MODID)).forEach(appender::add);
		}

		void forge(Provider prov)
		{
			this.tag(Tags.Blocks.FENCES_WOODEN).add(dream_fence, white_fence, dead_fence, blood_fence);
			this.tag(Tags.Blocks.FENCE_GATES_WOODEN).add(dream_fence_gate, white_fence_gate, dead_fence_gate, blood_fence_gate);
			this.tag(Tags.Blocks.COBBLESTONES_NORMAL).addTag(GNSBlockTags.COBBLESTONES);
			this.tag(Tags.Blocks.STONES).addTag(GNSBlockTags.STONES);
			this.tag(Tags.Blocks.ORES).addTags(GNSBlockTags.CANDY_ORES, GNSBlockTags.SPECTRITE_ORES, GNSBlockTags.POSITITE_ORES, GNSBlockTags.NECRUM_ORES, GNSBlockTags.ZITRITE_ORES, GNSBlockTags.NEGATITE_ORES);
			this.tag(Tags.Blocks.STORAGE_BLOCKS).addTags(GNSBlockTags.CANDY_BLOCKS, GNSBlockTags.SPECTRITE_BLOCKS, GNSBlockTags.POSITITE_BLOCKS, GNSBlockTags.NECRUM_BLOCKS, GNSBlockTags.ZITRITE_BLOCKS, GNSBlockTags.NEGATITE_BLOCKS);

			this.tag(Tags.Blocks.ORES_COAL).add(GNSBlocks.delusion_coal_ore);
			this.tag(Tags.Blocks.ORES_LAPIS).add(GNSBlocks.delusion_lapis_ore);
		}

		private Stream<Block> getMatching(Function<Block, Boolean> condition)
		{
			return BuiltInRegistries.BLOCK.stream().filter(block -> BuiltInRegistries.BLOCK.getKey(block).getNamespace().equals(GoodNightSleep.MODID) && condition.apply(block));
		}

		@SafeVarargs
		private void addMatching(Function<Block, Boolean> condition, TagKey<Block>... tags)
		{
			for (TagKey<Block> tag : tags)
				getMatching(condition).forEach(this.tag(tag)::add);
		}

		private void addMatching(TagKey<Block> blockTag, Function<Block, Boolean> condition)
		{
			getMatching(condition).forEach(this.tag(blockTag)::add);
		}

		@Override
		public String getName()
		{
			return "Good Night's Sleep Block Tags";
		}
	}

	public static class ItemTagProv extends ItemTagsProvider
	{
		public ItemTagProv(DataGenerator generatorIn, CompletableFuture<TagLookup<Block>> blocktagProvIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, blocktagProvIn, GoodNightSleep.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			goodNightsSleep();
			vanilla();
			forge();
		}

		void goodNightsSleep()
		{
			this.copy(GNSBlockTags.PLANKS, GNSItemTags.PLANKS);

			this.copy(GNSBlockTags.DREAM_LOGS, GNSItemTags.DREAM_LOGS);
			this.copy(GNSBlockTags.WHITE_LOGS, GNSItemTags.WHITE_LOGS);
			this.copy(GNSBlockTags.DEAD_LOGS, GNSItemTags.DEAD_LOGS);
			this.copy(GNSBlockTags.BLOOD_LOGS, GNSItemTags.BLOOD_LOGS);

			this.copy(GNSBlockTags.COBBLESTONES, GNSItemTags.COBBLESTONES);
			this.copy(GNSBlockTags.STONES, GNSItemTags.STONES);
			this.copy(GNSBlockTags.MUSHROOMS, GNSItemTags.MUSHROOMS);

			this.copy(GNSBlockTags.CANDY_ORES, GNSItemTags.CANDY_ORES);
			this.copy(GNSBlockTags.SPECTRITE_ORES, GNSItemTags.SPECTRITE_ORES);
			this.copy(GNSBlockTags.POSITITE_ORES, GNSItemTags.POSITITE_ORES);
			this.copy(GNSBlockTags.NECRUM_ORES, GNSItemTags.NECRUM_ORES);
			this.copy(GNSBlockTags.ZITRITE_ORES, GNSItemTags.ZITRITE_ORES);
			this.copy(GNSBlockTags.NEGATITE_ORES, GNSItemTags.NEGATITE_ORES);

			this.copy(GNSBlockTags.CANDY_BLOCKS, GNSItemTags.CANDY_BLOCKS);
			this.copy(GNSBlockTags.SPECTRITE_BLOCKS, GNSItemTags.SPECTRITE_BLOCKS);
			this.copy(GNSBlockTags.POSITITE_BLOCKS, GNSItemTags.POSITITE_BLOCKS);
			this.copy(GNSBlockTags.NECRUM_BLOCKS, GNSItemTags.NECRUM_BLOCKS);
			this.copy(GNSBlockTags.ZITRITE_BLOCKS, GNSItemTags.ZITRITE_BLOCKS);
			this.copy(GNSBlockTags.NEGATITE_BLOCKS, GNSItemTags.NEGATITE_BLOCKS);

			this.tag(GNSItemTags.CANDY_MATERIALS).add(GNSBlocks.hard_candy_block.asItem());
			this.tag(GNSItemTags.SPECTRITE_INGOTS).add(GNSItems.spectrite_ingot);
			this.tag(GNSItemTags.POSITITE).add(GNSItems.positite);
			this.tag(GNSItemTags.NECRUM).add(GNSItems.necrum);
			this.tag(GNSItemTags.ZITRITE_INGOTS).add(GNSItems.zitrite_ingot);
			this.tag(GNSItemTags.NEGATITE).add(GNSItems.negatite);

			this.tag(GNSItemTags.SUGARY).add(Items.SUGAR, GNSItems.candy);
			this.tag(GNSItemTags.NECROTIC_EXTRACTABLES).add(Items.ROTTEN_FLESH).addTag(GNSItemTags.NECRUM);

			this.tag(GNSItemTags.CANDY_SMELTABLES).addTag(GNSItemTags.CANDY_ORES);
			this.tag(GNSItemTags.NECRUM_SMELTABLES).addTag(GNSItemTags.NECRUM_ORES);
			this.tag(GNSItemTags.SPECTRITE_SMELTABLES).add(GNSItems.raw_spectrite).addTag(GNSItemTags.SPECTRITE_ORES);
			this.tag(GNSItemTags.ZITRITE_SMELTABLES).add(GNSItems.raw_zitrite).addTag(GNSItemTags.ZITRITE_ORES);
			this.tag(GNSItemTags.POSITITE_SMELTABLES).addTag(GNSItemTags.POSITITE_ORES);
			this.tag(GNSItemTags.NEGATITE_SMELTABLES).addTag(GNSItemTags.NEGATITE_ORES);
		}

		void vanilla()
		{
			this.copy(BlockTags.BEDS, ItemTags.BEDS);
			/*this.copy(BlockTags.SAND, ItemTags.SAND);*/
			this.copy(BlockTags.SAPLINGS, ItemTags.SAPLINGS);
			this.copy(BlockTags.SLABS, ItemTags.SLABS);
			this.copy(BlockTags.SMALL_FLOWERS, ItemTags.SMALL_FLOWERS);
			this.copy(BlockTags.STAIRS, ItemTags.STAIRS);
			this.copy(BlockTags.WALLS, ItemTags.WALLS);
			this.copy(BlockTags.WOODEN_BUTTONS, ItemTags.WOODEN_BUTTONS);
			this.copy(BlockTags.WOODEN_PRESSURE_PLATES, ItemTags.WOODEN_PRESSURE_PLATES);
			this.copy(BlockTags.PLANKS, ItemTags.PLANKS);
			this.copy(BlockTags.WOODEN_SLABS, ItemTags.WOODEN_SLABS);
			this.copy(BlockTags.WOODEN_STAIRS, ItemTags.WOODEN_STAIRS);
			this.copy(BlockTags.WOODEN_DOORS, ItemTags.WOODEN_DOORS);
			this.copy(BlockTags.WOODEN_TRAPDOORS, ItemTags.WOODEN_TRAPDOORS);
			this.copy(BlockTags.WOODEN_FENCES, ItemTags.WOODEN_FENCES);
			this.copy(BlockTags.FENCE_GATES, ItemTags.FENCE_GATES);
			this.copy(BlockTags.LOGS, ItemTags.LOGS);
			this.copy(BlockTags.LOGS_THAT_BURN, ItemTags.LOGS_THAT_BURN);
			this.copy(BlockTags.LEAVES, ItemTags.LEAVES);
			this.copy(BlockTags.STONE_BRICKS, ItemTags.STONE_BRICKS);

			this.tag(ItemTags.STONE_TOOL_MATERIALS).addTag(GNSItemTags.COBBLESTONES);
			this.tag(ItemTags.STONE_CRAFTING_MATERIALS).addTag(GNSItemTags.COBBLESTONES);

			this.tag(ItemTags.PIGLIN_LOVED).add(GNSBlocks.pot_of_gold.asItem());

			this.copy(BlockTags.STANDING_SIGNS, ItemTags.SIGNS);
			this.copy(BlockTags.CEILING_HANGING_SIGNS, ItemTags.HANGING_SIGNS);

			this.tag(ItemTags.PICKAXES).add(candy_pickaxe, positite_pickaxe, spectrite_pickaxe, necrum_pickaxe, negatite_pickaxe, zitrite_pickaxe);
			this.tag(ItemTags.AXES).add(candy_axe, positite_axe, spectrite_axe, necrum_axe, negatite_axe, zitrite_axe);
			this.tag(ItemTags.SHOVELS).add(candy_shovel, positite_shovel, spectrite_shovel, necrum_shovel, negatite_shovel, zitrite_shovel);
			this.tag(ItemTags.HOES).add(candy_hoe, positite_hoe, spectrite_hoe, necrum_hoe, negatite_hoe, zitrite_hoe);
			this.tag(ItemTags.SWORDS).add(candy_sword, positite_sword, spectrite_sword, necrum_sword, negatite_sword, zitrite_sword);

			this.tag(ItemTags.CLUSTER_MAX_HARVESTABLES).add(candy_pickaxe, positite_pickaxe, spectrite_pickaxe, necrum_pickaxe, negatite_pickaxe, zitrite_pickaxe);

			this.tag(ItemTags.TRIMMABLE_ARMOR).add(positite_helmet, positite_chestplate, positite_leggings, positite_boots);
			this.tag(ItemTags.TRIMMABLE_ARMOR).add(candy_helmet, candy_chestplate, candy_leggings, candy_boots);
			this.tag(ItemTags.TRIMMABLE_ARMOR).add(spectrite_helmet, spectrite_chestplate, spectrite_leggings, spectrite_boots);
			this.tag(ItemTags.TRIMMABLE_ARMOR).add(negatite_helmet, negatite_chestplate, negatite_leggings, negatite_boots);
			this.tag(ItemTags.TRIMMABLE_ARMOR).add(zitrite_helmet, zitrite_chestplate, zitrite_leggings, zitrite_boots);

			this.tag(ItemTags.HEAD_ARMOR).add(candy_helmet, spectrite_helmet, positite_helmet, zitrite_helmet, negatite_helmet);
			this.tag(ItemTags.CHEST_ARMOR).add(candy_chestplate, spectrite_chestplate, positite_chestplate, zitrite_chestplate, negatite_chestplate);
			this.tag(ItemTags.LEG_ARMOR).add(candy_leggings, spectrite_leggings, positite_leggings, zitrite_leggings, negatite_leggings);
			this.tag(ItemTags.FOOT_ARMOR).add(candy_boots, spectrite_boots, positite_boots, zitrite_boots, negatite_boots);

			/*ForgeRegistries.ITEMS.getValues().stream().filter(item -> item.getRegistryName().getNamespace().equals(GoodNightSleep.MODID) && item instanceof MusicDiscItem).forEach(this.getOrCreateBuilder(ItemTags.MUSIC_DISCS)::add);*/
		}

		void forge()
		{
			/*this.copy(Tags.Blocks.CHESTS_WOODEN, Tags.Items.CHESTS_WOODEN);*/
			/*this.copy(Tags.Blocks.COBBLESTONE, Tags.Items.COBBLESTONE);
			this.copy(Tags.Blocks.STONE, Tags.Items.STONE);*/
			this.copy(Tags.Blocks.ORES, Tags.Items.ORES);
			this.copy(Tags.Blocks.ORES_COAL, Tags.Items.ORES_COAL);
			this.copy(Tags.Blocks.ORES_LAPIS, Tags.Items.ORES_LAPIS);
			/*this.copy(Tags.Blocks.SANDSTONE, Tags.Items.SANDSTONE);*/
			this.copy(Tags.Blocks.STORAGE_BLOCKS, Tags.Items.STORAGE_BLOCKS);
			this.tag(Tags.Items.CROPS).add(GNSItems.rainbow_berries);
			this.tag(Tags.Items.FOODS_BERRY).add(GNSItems.rainbow_berries);
			this.tag(Tags.Items.FOODS_CANDY).add(GNSItems.candy, GNSItems.lollipop);
			this.tag(Tags.Items.FOODS_SOUP).add(GNSItems.luxurious_soup, GNSItems.wretched_soup);
			this.tag(Tags.Items.GEMS).addTags(GNSItemTags.POSITITE, GNSItemTags.NEGATITE);
			this.tag(Tags.Items.INGOTS).addTags(GNSItemTags.SPECTRITE_INGOTS, GNSItemTags.ZITRITE_INGOTS);
			this.tag(Tags.Items.SEEDS).add(GNSItems.rainbow_seeds);

			this.tag(Tags.Items.RAW_MATERIALS).add(GNSItems.raw_spectrite, GNSItems.raw_zitrite);

			this.tag(Tags.Items.MELEE_WEAPON_TOOLS).add(candy_sword, positite_sword, spectrite_sword, necrum_sword, negatite_sword, zitrite_sword, candy_axe, positite_axe, spectrite_axe, necrum_axe, negatite_axe, zitrite_axe);
			this.tag(Tags.Items.MINING_TOOL_TOOLS).add(candy_pickaxe, positite_pickaxe, spectrite_pickaxe, necrum_pickaxe, negatite_pickaxe, zitrite_pickaxe);

		}

		@Override
		public String getName()
		{
			return "Good Night's Sleep Item Tags";
		}
	}

	public static class BiomeTagProv extends BiomeTagsProvider
	{
		public BiomeTagProv(DataGenerator generatorIn, CompletableFuture<Provider> lookup)
		{
			super(generatorIn.getPackOutput(), lookup, GoodNightSleep.MODID);
		}

		@Override
		protected void addTags(Provider prov)
		{
			goodNightsSleep();
			vanilla();
			forge();
		}

		void goodNightsSleep()
		{
			this.add(GNSBiomeTags.IS_DREAM, GNSBiomes.GOOD_DREAM_PLAINS, GNSBiomes.SLEEPY_HILLS, GNSBiomes.DREAMY_FOREST, GNSBiomes.LOLLIPOP_LANDS);
			this.add(GNSBiomeTags.IS_NIGHTMARE, GNSBiomes.SHAMEFUL_PLAINS, GNSBiomes.NIGHTMARE_MOUND, GNSBiomes.WASTED_FOREST);

			this.tag(GNSBiomeTags.HAS_HOPE_MUSHROOMS).addTag(BiomeTags.IS_NETHER);
			this.tag(GNSBiomeTags.HAS_DESPAIR_MUSHROOMS).addTag(BiomeTags.IS_NETHER);
		}

		void vanilla()
		{
		}

		void forge()
		{
		}

		/*private void add(TagKey<Biome> tag, Collection<Pointer<Biome>> list)
		{
			this.tag(tag).add(list.stream().map((r) -> r.getKey()).toArray(ResourceKey[]::new));
		}*/

		private void add(TagKey<Biome> tag, Pointer<Biome>... registrars)
		{
			this.tag(tag).add(Arrays.stream(registrars).map((r) -> r.getKey()).toArray(ResourceKey[]::new));
		}

		@Override
		public String getName()
		{
			return "Good Night's Sleep Biome Tags";
		}
	}
}
