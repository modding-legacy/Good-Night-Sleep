package com.legacy.good_nights_sleep.data;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.registry.GNSBlocks;
import com.legacy.good_nights_sleep.registry.GNSDimensions;
import com.legacy.good_nights_sleep.registry.GNSEntityTypes;
import com.legacy.good_nights_sleep.registry.GNSItems;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementHolder;
import net.minecraft.advancements.AdvancementRequirements;
import net.minecraft.advancements.AdvancementType;
import net.minecraft.advancements.Criterion;
import net.minecraft.advancements.critereon.ChangeDimensionTrigger;
import net.minecraft.advancements.critereon.ConsumeItemTrigger;
import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.EntityTypePredicate;
import net.minecraft.advancements.critereon.InventoryChangeTrigger;
import net.minecraft.advancements.critereon.KilledTrigger;
import net.minecraft.advancements.critereon.TameAnimalTrigger;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.advancements.AdvancementProvider;
import net.minecraft.data.advancements.AdvancementSubProvider;
import net.minecraft.network.chat.Component;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.Level;

@SuppressWarnings("unused")
public class GNSAdvancementProv extends AdvancementProvider
{
	private static final Gson GSON = (new GsonBuilder()).setPrettyPrinting().create();

	public static AdvancementHolder root;
	public static AdvancementHolder enterDream, eatRainbowBerries, killBabyCreeper, obtainSpectriteIngot, obtainPositite,
			obtainPotOfGold, obtainPresent, tameUnicorn;
	public static AdvancementHolder enterNightmare, killTormenter, obtainZitriteIngot, obtainZitriteSword,
			obtainNegatite, killHerobrine;

	public GNSAdvancementProv(DataGenerator gen, CompletableFuture<HolderLookup.Provider> lookup)
	{
		super(gen.getPackOutput(), lookup, List.of(new Adv()));
	}

	private static class Adv implements AdvancementSubProvider
	{
		@Override
		public void generate(Provider lookup, Consumer<AdvancementHolder> saver)
		{
			var blockReg = lookup.lookupOrThrow(Registries.BLOCK);
			var itemReg = lookup.lookupOrThrow(Registries.ITEM);
			var entityReg = lookup.lookupOrThrow(Registries.ENTITY_TYPE);

			root = builder(GNSItems.strange_bed, "root", GoodNightSleep.locate("textures/block/dream_dirt.png"), AdvancementType.TASK, false, false, false).requirements(AdvancementRequirements.Strategy.OR).addCriterion("dream", changeDim(GNSDimensions.dreamKey())).addCriterion("nightmare", changeDim(GNSDimensions.nightmareKey())).save(saver, GoodNightSleep.find("root"));

			enterDream = builder(GNSItems.luxurious_bed, "dream", AdvancementType.TASK, true, true, false).parent(root).addCriterion("dream", changeDim(GNSDimensions.dreamKey())).save(saver, GoodNightSleep.find("enter_dream"));
			eatRainbowBerries = builder(GNSItems.rainbow_berries, "eat_rainbow_berry", AdvancementType.TASK, true, true, false).parent(enterDream).addCriterion("eat", ConsumeItemTrigger.TriggerInstance.usedItem(itemReg, GNSItems.rainbow_berries)).save(saver, GoodNightSleep.find("eat_rainbow_berries"));
			killBabyCreeper = builder(GNSItems.baby_creeper_spawn_egg, "kill_baby_creeper", AdvancementType.TASK, true, true, false).parent(enterDream).addCriterion("kill_creeper", KilledTrigger.TriggerInstance.playerKilledEntity(EntityPredicate.Builder.entity().of(entityReg, GNSEntityTypes.BABY_CREEPER))).save(saver, GoodNightSleep.find("kill_baby_creeper"));
			obtainSpectriteIngot = builder(GNSItems.spectrite_ingot, "obtain_spectrite_ingot", AdvancementType.TASK, true, true, false).parent(killBabyCreeper).addCriterion("collect_item", InventoryChangeTrigger.TriggerInstance.hasItems(GNSItems.spectrite_ingot)).save(saver, GoodNightSleep.find("obtain_rainbow_ingot"));
			obtainPositite = builder(GNSItems.positite, "obtain_positite", AdvancementType.GOAL, true, true, false).parent(obtainSpectriteIngot).addCriterion("collect_item", InventoryChangeTrigger.TriggerInstance.hasItems(GNSItems.positite)).save(saver, GoodNightSleep.find("obtain_positite"));
			obtainPotOfGold = builder(GNSBlocks.pot_of_gold, "obtain_pot_of_gold", AdvancementType.TASK, true, true, false).parent(obtainSpectriteIngot).addCriterion("collect_item", InventoryChangeTrigger.TriggerInstance.hasItems(GNSBlocks.pot_of_gold)).save(saver, GoodNightSleep.find("obtain_pot_of_gold"));
			obtainPresent = builder(GNSBlocks.present, "obtain_present", AdvancementType.TASK, true, true, false).parent(obtainSpectriteIngot).addCriterion("collect_item", InventoryChangeTrigger.TriggerInstance.hasItems(GNSBlocks.present)).save(saver, GoodNightSleep.find("obtain_present"));
			tameUnicorn = builder(Items.SADDLE, "tame_unicorn", AdvancementType.TASK, true, true, false).parent(enterDream).addCriterion("tame", TameAnimalTrigger.TriggerInstance.tamedAnimal(EntityPredicate.Builder.entity().entityType(EntityTypePredicate.of(entityReg, GNSEntityTypes.UNICORN)))).save(saver, GoodNightSleep.find("tame_unicorn"));

			enterNightmare = builder(GNSItems.wretched_bed, "nightmare", AdvancementType.TASK, true, true, false).parent(root).addCriterion("nightmare", changeDim(GNSDimensions.nightmareKey())).save(saver, GoodNightSleep.find("enter_nightmare"));
			killTormenter = builder(GNSItems.necrum, "kill_tormenter", AdvancementType.TASK, true, true, false).parent(enterNightmare).addCriterion("kill_tormenter", KilledTrigger.TriggerInstance.playerKilledEntity(EntityPredicate.Builder.entity().of(entityReg, GNSEntityTypes.TORMENTER))).save(saver, GoodNightSleep.find("kill_tormeter"));
			obtainZitriteIngot = builder(GNSItems.zitrite_ingot, "obtain_zitrite_ingot", AdvancementType.TASK, true, true, false).parent(killTormenter).addCriterion("collect_item", InventoryChangeTrigger.TriggerInstance.hasItems(GNSItems.zitrite_ingot)).save(saver, GoodNightSleep.find("obtain_zitrite_ingot"));
			obtainZitriteSword = builder(GNSItems.zitrite_sword, "obtain_zitrite_sword", AdvancementType.TASK, true, true, false).parent(obtainZitriteIngot).addCriterion("collect_item", InventoryChangeTrigger.TriggerInstance.hasItems(GNSItems.zitrite_sword)).save(saver, GoodNightSleep.find("obtain_zitrite_sword"));
			obtainNegatite = builder(GNSItems.negatite, "obtain_negatite", AdvancementType.GOAL, true, true, false).parent(obtainZitriteIngot).addCriterion("collect_item", InventoryChangeTrigger.TriggerInstance.hasItems(GNSItems.negatite)).save(saver, GoodNightSleep.find("obtain_negatite"));
			killHerobrine = builder(GNSItems.negatite_sword, "kill_herobrine", AdvancementType.CHALLENGE, true, true, false).parent(killTormenter).addCriterion("kill_herobrine", KilledTrigger.TriggerInstance.playerKilledEntity(EntityPredicate.Builder.entity().of(entityReg, GNSEntityTypes.HEROBRINE))).save(saver, GoodNightSleep.find("kill_herobrine"));
		}

		/*private Advancement.Builder enterAnyStructure(Advancement.Builder builder, List<ResourceKey<Structure>> structures)
		{
			structures.forEach(structure -> builder.addCriterion("entered_" + structure.location().getPath(), enterStructure(structure)));
			return builder;
		}
		
		private Criterion<PlayerTrigger.TriggerInstance> enterStructure(ResourceKey<Structure> type)
		{
			return PlayerTrigger.TriggerInstance.located(LocationPredicate.Builder.inStructure(type));
		}*/

		private Criterion<ChangeDimensionTrigger.TriggerInstance> changeDim(ResourceKey<Level> type)
		{
			return ChangeDimensionTrigger.TriggerInstance.changedDimensionTo(type);
		}

		private Component translate(String key)
		{
			return Component.translatable("advancements." + GoodNightSleep.MODID + "." + key);
		}

		private Advancement.Builder builder(ItemStack displayItem, String name, ResourceLocation background, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return Advancement.Builder.advancement().display(displayItem, translate(name + ".title"), translate(name + ".desc"), background, AdvancementType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemLike displayItem, String name, ResourceLocation background, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return Advancement.Builder.advancement().display(displayItem, translate(name + ".title"), translate(name + ".desc"), background, AdvancementType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemStack displayItem, String name, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return builder(displayItem, name, (ResourceLocation) null, AdvancementType, showToast, announceToChat, hidden);
		}

		private Advancement.Builder builder(ItemLike displayItem, String name, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return builder(displayItem, name, (ResourceLocation) null, AdvancementType, showToast, announceToChat, hidden);
		}

		/*private AdvancementHolder builder(ItemLike displayItem, String name, ResourceLocation background, AdvancementType AdvancementType, boolean showToast, boolean announceToChat, boolean hidden)
		{
			return new AdvancementHolder(GoodNightSleep.locate(name), Advancement.Builder.advancement().display(displayItem, translate(name + ".title"), translate(name + ".desc"), background, AdvancementType, showToast, announceToChat, hidden).save(this.saver, GoodNightSleep.find(name)));
		}*/
	}
}
