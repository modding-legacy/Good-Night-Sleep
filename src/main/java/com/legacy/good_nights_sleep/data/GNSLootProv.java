package com.legacy.good_nights_sleep.data;

import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.blocks.GNSBedBlock;
import com.legacy.good_nights_sleep.blocks.natural.RainbowBerriesBlock;
import com.legacy.good_nights_sleep.registry.GNSBlocks;
import com.legacy.good_nights_sleep.registry.GNSEntityTypes;
import com.legacy.good_nights_sleep.registry.GNSItems;

import net.minecraft.advancements.critereon.EntityPredicate;
import net.minecraft.advancements.critereon.StatePropertiesPredicate;
import net.minecraft.core.HolderLookup;
import net.minecraft.core.HolderLookup.Provider;
import net.minecraft.core.WritableRegistry;
import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.loot.BlockLootSubProvider;
import net.minecraft.data.loot.LootTableProvider;
import net.minecraft.data.loot.packs.VanillaEntityLoot;
import net.minecraft.tags.EntityTypeTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.tags.TagKey;
import net.minecraft.util.ProblemReporter;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.flag.FeatureFlags;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.Enchantments;
import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.DoorBlock;
import net.minecraft.world.level.block.FlowerPotBlock;
import net.minecraft.world.level.block.SlabBlock;
import net.minecraft.world.level.block.state.properties.BedPart;
import net.minecraft.world.level.block.state.properties.DoubleBlockHalf;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.ValidationContext;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.entries.LootPoolEntryContainer;
import net.minecraft.world.level.storage.loot.entries.TagEntry;
import net.minecraft.world.level.storage.loot.functions.ApplyBonusCount;
import net.minecraft.world.level.storage.loot.functions.EnchantedCountIncreaseFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.parameters.LootContextParamSets;
import net.minecraft.world.level.storage.loot.predicates.BonusLevelTableCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemBlockStatePropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemEntityPropertyCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemRandomChanceCondition;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;

public class GNSLootProv extends LootTableProvider
{
	public GNSLootProv(DataGenerator gen, CompletableFuture<HolderLookup.Provider> lookup)
	{
		super(gen.getPackOutput(), Set.of(), List.of(new LootTableProvider.SubProviderEntry(GNSBlockLoot::new, LootContextParamSets.BLOCK), new LootTableProvider.SubProviderEntry(GNSEntityLoot::new, LootContextParamSets.ENTITY)), lookup);
	}

	@Override
	protected void validate(WritableRegistry<LootTable> writableregistry, ValidationContext validationcontext, ProblemReporter.Collector problemreporter$collector)
	{
		writableregistry.listElements().forEach(lootTable -> lootTable.value().validate(validationcontext.setContextKeySet(lootTable.value().getParamSet()).enterElement("{" + lootTable.key().location() + "}", lootTable.key())));
	}

	private static class GNSEntityLoot extends VanillaEntityLoot implements LootPoolUtil
	{
		public GNSEntityLoot(Provider registries)
		{
			super(registries);
		}

		@Override
		public void generate()
		{
			HolderLookup.RegistryLookup<EntityType<?>> entityReg = this.registries.lookupOrThrow(Registries.ENTITY_TYPE);

			this.add(GNSEntityTypes.UNICORN, LootTable.lootTable().withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(Items.LEATHER).apply(SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 2.0F))).apply(EnchantedCountIncreaseFunction.lootingMultiplier(this.registries, UniformGenerator.between(0.0F, 1.0F))))));
			/*this.add(GNSEntityTypes.GUMMY_BEAR, LootTable.lootTable());*/
			this.add(GNSEntityTypes.BABY_CREEPER, LootTable.lootTable().withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(Items.GUNPOWDER).apply(SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 2.0F))).apply(EnchantedCountIncreaseFunction.lootingMultiplier(this.registries, UniformGenerator.between(0.0F, 1.0F))))).withPool(LootPool.lootPool().add(TagEntry.expandTag(ItemTags.CREEPER_DROP_MUSIC_DISCS)).when(LootItemEntityPropertyCondition.hasProperties(LootContext.EntityTarget.ATTACKER, EntityPredicate.Builder.entity().of(entityReg, EntityTypeTags.SKELETONS)))));

			this.add(GNSEntityTypes.HEROBRINE, LootTable.lootTable().withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(GNSItems.negatite).apply(SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 2.0F))))));
			this.add(GNSEntityTypes.TORMENTER, LootTable.lootTable().withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(GNSItems.necrum).apply(SetItemCountFunction.setCount(UniformGenerator.between(2.0F, 3.0F))))));
		}

		@SuppressWarnings("unused")
		private LootPool.Builder lootingPool(ItemLike item, int min, int max, int minLooting, int maxLooting)
		{
			return basicPool(item, min, max).apply(EnchantedCountIncreaseFunction.lootingMultiplier(this.registries, UniformGenerator.between(minLooting, maxLooting)));
		}

		@Override
		protected Stream<EntityType<?>> getKnownEntityTypes()
		{
			return BuiltInRegistries.ENTITY_TYPE.stream().filter(e -> BuiltInRegistries.ENTITY_TYPE.getKey(e).getNamespace().contains(GoodNightSleep.MODID));
		}
	}

	private static class GNSBlockLoot extends BlockLootSubProvider
	{
		protected GNSBlockLoot(HolderLookup.Provider lookup)
		{
			super(Set.of(), FeatureFlags.REGISTRY.allFlags(), lookup);
		}

		private float[] DEFAULT_SAPLING_DROP_RATES = new float[] { 0.05F, 0.0625F, 0.083333336F, 0.1F };

		@Override
		protected void generate()
		{
			HolderLookup.RegistryLookup<Enchantment> registrylookup = this.registries.lookupOrThrow(Registries.ENCHANTMENT);

			blocks().forEach(block ->
			{
				if (block == GNSBlocks.short_dream_grass)
					this.add(block, this.dropRainbowSeeds(block));
				else if (block == GNSBlocks.short_nightmare_grass)
					this.add(block, createGrassDrops(block));
				else if (block == GNSBlocks.prickly_nightmare_grass)
					this.add(block, createGrassDrops(block));
				else if (block == GNSBlocks.dream_grass_block)
					silkOrElse(block, GNSBlocks.dream_dirt);
				else if (block == GNSBlocks.nightmare_grass_block)
					silkOrElse(block, Blocks.DIRT);
				else if (block == GNSBlocks.dream_farmland)
					dropOther(block, GNSBlocks.dream_dirt);
				else if (block == GNSBlocks.delusion_stone)
					silkOrElse(block, GNSBlocks.delusion_cobblestone);
				else if (block == GNSBlocks.dream_leaves)
					add(block, (b) -> leaves(b, GNSBlocks.dream_sapling, Items.STICK));
				else if (block == GNSBlocks.candy_leaves)
					add(block, (b) -> leaves(b, GNSBlocks.candy_sapling, Items.STICK));
				else if (block == GNSBlocks.diamond_leaves)
					add(block, (b) -> leaves(b, GNSBlocks.dream_sapling, Items.STICK));
				else if (block instanceof SlabBlock)
					add(block, createSlabItemTable(block));
				else if (block == GNSBlocks.candy_ore)
					add(block, (b) -> createSilkTouchDispatchTable(b, applyExplosionDecay(b, LootItem.lootTableItem(GNSItems.candy).apply(SetItemCountFunction.setCount(UniformGenerator.between(1.0F, 3.0F))).apply(ApplyBonusCount.addUniformBonusCount(registrylookup.getOrThrow(Enchantments.FORTUNE))))));
				else if (block == GNSBlocks.fossilized_necrum)
					add(block, (b) -> createSilkTouchDispatchTable(b, applyExplosionDecay(b, LootItem.lootTableItem(GNSItems.necrum).apply(SetItemCountFunction.setCount(UniformGenerator.between(1.0F, 3.0F))).apply(ApplyBonusCount.addUniformBonusCount(registrylookup.getOrThrow(Enchantments.FORTUNE))))));
				else if (block == GNSBlocks.positite_ore)
					add(block, (b) -> createOreDrop(b, GNSItems.positite));
				else if (block == GNSBlocks.negatite_ore)
					add(block, (b) -> createOreDrop(b, GNSItems.negatite));
				else if (block == GNSBlocks.zitrite_ore)
					add(block, (b) -> createOreDrop(b, GNSItems.raw_zitrite));
				else if (block == GNSBlocks.spectrite_ore)
					add(block, (b) -> dropRainbow(b));
				else if (block == GNSBlocks.present)
					add(block, (b) -> dropPresent(b));
				else if (block instanceof DoorBlock)
					add(block, (b) -> createSinglePropConditionTable(b, DoorBlock.HALF, DoubleBlockHalf.LOWER));
				else if (block instanceof FlowerPotBlock)
					dropPottedContents(block);
				else if (block == GNSBlocks.delusion_lapis_ore)
					add(block, (b) -> createSilkTouchDispatchTable(block, applyExplosionDecay(block, LootItem.lootTableItem(Items.LAPIS_LAZULI).apply(SetItemCountFunction.setCount(UniformGenerator.between(4.0F, 9.0F))).apply(ApplyBonusCount.addOreBonusCount(registrylookup.getOrThrow(Enchantments.FORTUNE))))));
				else if (block == GNSBlocks.delusion_coal_ore)
					add(block, (b) -> createOreDrop(block, Items.COAL));
				else if (block == GNSBlocks.hope_mushroom_block)
					add(block, (b) -> createMushroomBlockDrop(b, GNSBlocks.hope_mushroom));
				else if (block == GNSBlocks.despair_mushroom_block)
					add(block, (b) -> createMushroomBlockDrop(b, GNSBlocks.despair_mushroom));
				else if (block instanceof GNSBedBlock)
					add(block, (bed) ->
					{
						return createSinglePropConditionTable(bed, GNSBedBlock.PART, BedPart.HEAD);
					});
				else if (block instanceof RainbowBerriesBlock)
				{
					LootItemCondition.Builder growthCondition = LootItemBlockStatePropertyCondition.hasBlockStateProperties(block).setProperties(StatePropertiesPredicate.Builder.properties().hasProperty(RainbowBerriesBlock.AGE, ((RainbowBerriesBlock) block).getMaxAge()));

					if (block == GNSBlocks.rainbow_berries)
						this.add(block, (b) -> crop(growthCondition, b, GNSItems.rainbow_berries, GNSItems.rainbow_seeds));
				}
				else
					dropSelf(block);
			});
		}

		// @formatter:off
		protected LootTable.Builder dropRainbow(Block block)
		{
			return LootTable.lootTable().withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(GNSItems.raw_spectrite).setWeight(50)))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(Items.RAW_GOLD).setWeight(10)))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(Items.RAW_IRON).setWeight(10)))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(Items.REDSTONE).setWeight(10).apply(SetItemCountFunction.setCount(UniformGenerator.between(1.0F, 4.0F)))))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(Items.EMERALD).setWeight(1)))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(GNSItems.candy).setWeight(20).apply(SetItemCountFunction.setCount(UniformGenerator.between(1.0F, 3.0F))))));
		}
		
		protected LootTable.Builder dropPresent(Block block)
		{
			return LootTable.lootTable().withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(GNSItems.spectrite_ingot).setWeight(30)))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(Items.GOLD_INGOT).setWeight(10)))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(Items.IRON_INGOT).setWeight(10)))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(Items.REDSTONE).setWeight(10).apply(SetItemCountFunction.setCount(UniformGenerator.between(1.0F, 4.0F)))))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(Items.EMERALD).setWeight(5)))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(GNSItems.candy).setWeight(30).apply(SetItemCountFunction.setCount(UniformGenerator.between(1.0F, 3.0F)))))
					.add(LootItem.lootTableItem(block).when(this.hasSilkTouch()).otherwise(LootItem.lootTableItem(GNSItems.positite).setWeight(5))));
		}
		// @formatter:on

		@Override
		protected Iterable<Block> getKnownBlocks()
		{
			return blocks()::iterator;
		}

		private Stream<Block> blocks()
		{
			return BuiltInRegistries.BLOCK.stream().filter(b -> BuiltInRegistries.BLOCK.getKey(b).getNamespace().equals(GoodNightSleep.MODID) && (b.asItem() != Blocks.AIR.asItem() || b instanceof FlowerPotBlock));
		}

		@SuppressWarnings("unused")
		private void droppingSeedTag(Block block, TagKey<Item> tag)
		{
			HolderLookup.RegistryLookup<Enchantment> registrylookup = this.registries.lookupOrThrow(Registries.ENCHANTMENT);

			this.add(block, createShearsDispatchTable(block, applyExplosionDecay(block, (TagEntry.expandTag(tag).when(LootItemRandomChanceCondition.randomChance(0.125F))).apply(ApplyBonusCount.addUniformBonusCount(registrylookup.getOrThrow(Enchantments.FORTUNE), 2)))));
		}

		protected LootTable.Builder dropRainbowSeeds(Block block)
		{
			HolderLookup.RegistryLookup<Enchantment> registrylookup = this.registries.lookupOrThrow(Registries.ENCHANTMENT);

			return createShearsDispatchTable(block, applyExplosionDecay(block, LootItem.lootTableItem(GNSItems.rainbow_seeds).when(LootItemRandomChanceCondition.randomChance(0.125F)).apply(ApplyBonusCount.addUniformBonusCount(registrylookup.getOrThrow(Enchantments.FORTUNE), 2))));
		}

		private void silkOrElse(Block withSilk, ItemLike without)
		{
			this.add(withSilk, (b) -> createSingleItemTableWithSilkTouch(b, without));
		}

		private LootTable.Builder leaves(Block block, ItemLike sapling, ItemLike stick)
		{
			HolderLookup.RegistryLookup<Enchantment> registrylookup = this.registries.lookupOrThrow(Registries.ENCHANTMENT);

			return createSilkTouchOrShearsDispatchTable(block, applyExplosionCondition(block, LootItem.lootTableItem(sapling)).when(BonusLevelTableCondition.bonusLevelFlatChance(registrylookup.getOrThrow(Enchantments.FORTUNE), DEFAULT_SAPLING_DROP_RATES))).withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1)).when(this.hasShears().invert()).add(applyExplosionDecay(block, LootItem.lootTableItem(stick).apply(SetItemCountFunction.setCount(UniformGenerator.between(1.0F, 2.0F)))).when(BonusLevelTableCondition.bonusLevelFlatChance(registrylookup.getOrThrow(Enchantments.FORTUNE), 0.02F, 0.022222223F, 0.025F, 0.033333335F, 0.1F))));
		}

		@SuppressWarnings("unused")
		private LootTable.Builder leavesFruit(Block block, ItemLike sapling, ItemLike stick, ItemLike fruit)
		{
			HolderLookup.RegistryLookup<Enchantment> registrylookup = this.registries.lookupOrThrow(Registries.ENCHANTMENT);

			float baseChance = 0.05F;
			float[] fortuneChances = new float[] { 1.11111114F, 1.25F, 1.6666668F, 5.0F };
			return leaves(block, sapling, stick).withPool(LootPool.lootPool().setRolls(ConstantValue.exactly(1)).when(this.hasShears().invert().and(this.doesNotHaveSilkTouch())).add(applyExplosionCondition(block, LootItem.lootTableItem(fruit)).when(BonusLevelTableCondition.bonusLevelFlatChance(registrylookup.getOrThrow(Enchantments.FORTUNE), baseChance, baseChance * fortuneChances[0], baseChance * fortuneChances[1], baseChance * fortuneChances[2], baseChance * fortuneChances[3]))));
		}

		@SuppressWarnings("unused")
		private LootTable.Builder crop(LootItemCondition.Builder growthCondition, Block block, ItemLike food)
		{
			return crop(growthCondition, block, food, food);
		}

		private LootTable.Builder crop(LootItemCondition.Builder growthCondition, Block block, ItemLike food, ItemLike seed)
		{
			HolderLookup.RegistryLookup<Enchantment> registrylookup = this.registries.lookupOrThrow(Registries.ENCHANTMENT);

			LootPool.Builder seedPool = LootPool.lootPool().add(LootItem.lootTableItem(seed).apply(ApplyBonusCount.addBonusBinomialDistributionCount(registrylookup.getOrThrow(Enchantments.FORTUNE), 0.5714286F, 3).when(growthCondition)));
			LootPool.Builder foodPool = LootPool.lootPool().when(growthCondition).add(LootItem.lootTableItem(food).apply(ApplyBonusCount.addBonusBinomialDistributionCount(registrylookup.getOrThrow(Enchantments.FORTUNE), 0.5714286F, 1)));

			return applyExplosionDecay(block, LootTable.lootTable().withPool(seedPool).withPool(foodPool));
		}
	}

	/**
	 * Interface with basic loot table generators
	 * 
	 * @author David
	 *
	 */
	public interface LootPoolUtil
	{
		/**
		 * Creates a table from the given loot pools.
		 * 
		 * @param pools
		 * @return
		 */
		default LootTable.Builder tableOf(List<LootPool.Builder> pools)
		{
			LootTable.Builder table = LootTable.lootTable();
			pools.forEach(pool -> table.withPool(pool));
			return table;
		}

		/**
		 * Creates a table from the given loot pool.
		 * 
		 * @param pool
		 * @return
		 */
		default LootTable.Builder tableOf(LootPool.Builder pool)
		{
			return LootTable.lootTable().withPool(pool);
		}

		/**
		 * Creates a loot pool with the given item. Gives an amount between the min and
		 * max.
		 * 
		 * @param item
		 * @param min
		 * @param max
		 * @return
		 */
		default LootPool.Builder basicPool(ItemLike item, int min, int max)
		{
			return LootPool.lootPool().add(basicEntry(item, min, max));
		}

		/**
		 * Creates a loot pool with the given item. Will only give one item.
		 * 
		 * @param item
		 * @return
		 */
		default LootPool.Builder basicPool(ItemLike item)
		{
			return LootPool.lootPool().add(basicEntry(item));
		}

		/**
		 * Creates a loot pool that will give a random item from the list.
		 * 
		 * @param items
		 * @return
		 */
		default LootPool.Builder randItemPool(List<ItemLike> items)
		{
			return poolOf(items.stream().map((i) -> basicEntry(i)).collect(Collectors.toList()));
		}

		/**
		 * Creates a loot pool with multiple entries. One of these entries will be
		 * picked at random each time the pool rolls.
		 * 
		 * @param lootEntries
		 * @return
		 */
		default LootPool.Builder poolOf(List<LootPoolEntryContainer.Builder<?>> lootEntries)
		{
			LootPool.Builder pool = LootPool.lootPool();
			lootEntries.forEach(entry -> pool.add(entry));
			return pool;
		}

		/**
		 * Creates a loot entry for the given item. Gives an amount between the min and
		 * max.
		 * 
		 * @param item
		 * @param min
		 * @param max
		 * @return
		 */
		default LootItem.Builder<?> basicEntry(ItemLike item, int min, int max)
		{
			return basicEntry(item).apply(SetItemCountFunction.setCount(UniformGenerator.between(min, max)));
		}

		/**
		 * Creates a loot entry for the given item. Will only give one item.
		 * 
		 * @param item
		 * @return
		 */
		default LootItem.Builder<?> basicEntry(ItemLike item)
		{
			return LootItem.lootTableItem(item);
		}
	}
}
