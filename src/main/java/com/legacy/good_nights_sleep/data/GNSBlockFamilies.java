package com.legacy.good_nights_sleep.data;

import static com.legacy.good_nights_sleep.registry.GNSBlocks.*;

import java.util.ArrayList;
import java.util.List;

import net.minecraft.data.BlockFamily;
import net.minecraft.data.BlockFamily.Builder;
import net.minecraft.world.level.block.Block;

public class GNSBlockFamilies
{
	private static final List<BlockFamily> FAMILIES = new ArrayList<>();

	// @formatter:off
	public static final BlockFamily DREAM = family(dream_planks)
			.button(dream_button)
			.fence(dream_fence)
			.fenceGate(dream_fence_gate)
			.pressurePlate(dream_pressure_plate)
			.sign(dream_sign, dream_wall_sign)
			.slab(dream_slab)
			.stairs(dream_stairs)
			.door(dream_door)
			.trapdoor(dream_trapdoor)
			.recipeGroupPrefix("wooden")
			.recipeUnlockedBy("has_planks").getFamily();
	
	public static final BlockFamily WHITE = family(white_planks)
			.button(white_button)
			.fence(white_fence)
			.fenceGate(white_fence_gate)
			.pressurePlate(white_pressure_plate)
			.sign(white_sign, white_wall_sign)
			.slab(white_slab)
			.stairs(white_stairs)
			.door(white_door)
			.trapdoor(white_trapdoor)
			.recipeGroupPrefix("wooden")
			.recipeUnlockedBy("has_planks").getFamily();
	
	public static final BlockFamily DEAD = family(dead_planks)
			.button(dead_button)
			.fence(dead_fence)
			.fenceGate(dead_fence_gate)
			.pressurePlate(dead_pressure_plate)
			.sign(dead_sign, dead_wall_sign)
			.slab(dead_slab)
			.stairs(dead_stairs)
			.door(dead_door)
			.trapdoor(dead_trapdoor)
			.recipeGroupPrefix("wooden")
			.recipeUnlockedBy("has_planks").getFamily();
	
	public static final BlockFamily BLOOD = family(blood_planks)
			.button(blood_button)
			.fence(blood_fence)
			.fenceGate(blood_fence_gate)
			.pressurePlate(blood_pressure_plate)
			.sign(blood_sign, blood_wall_sign)
			.slab(blood_slab)
			.stairs(blood_stairs)
			.door(blood_door)
			.trapdoor(blood_trapdoor)
			.recipeGroupPrefix("wooden")
			.recipeUnlockedBy("has_planks").getFamily();

	public static final BlockFamily DELUSION_STONE = family(delusion_stone)
			.button(delusion_button)
			.pressurePlate(delusion_pressure_plate)
			.slab(delusion_stone_slab)
			.stairs(delusion_stone_stairs)
			.getFamily();
	
	public static final BlockFamily DELUSION_COBBLESTONE = family(delusion_cobblestone)
			.slab(delusion_cobblestone_slab)
			.stairs(delusion_cobblestone_stairs)
			.wall(delusion_cobblestone_wall)
			.getFamily();
	
	public static final BlockFamily DELUSION_STONE_BRICK = family(delusion_stone_bricks)
			.slab(delusion_stone_brick_slab)
			.stairs(delusion_stone_brick_stairs)
			.wall(delusion_stone_brick_wall)
			.getFamily();
	// @formatter:on

	private static Builder family(Block block)
	{
		var builder = new BlockFamily.Builder(block);

		FAMILIES.add(builder.getFamily());

		return builder;
	}

	public static List<BlockFamily> getFamilies()
	{
		return FAMILIES;
	}
}