package com.legacy.good_nights_sleep.data;

import static com.legacy.good_nights_sleep.registry.GNSBlocks.*;

import java.util.concurrent.CompletableFuture;

import com.legacy.good_nights_sleep.registry.GNSItems;

import net.minecraft.core.HolderLookup;
import net.minecraft.data.PackOutput;
import net.minecraft.world.level.ItemLike;
import net.neoforged.neoforge.common.data.DataMapProvider;
import net.neoforged.neoforge.registries.datamaps.builtin.Compostable;
import net.neoforged.neoforge.registries.datamaps.builtin.NeoForgeDataMaps;

public class GNSDataMapProv extends DataMapProvider
{
	public GNSDataMapProv(PackOutput packOutput, CompletableFuture<HolderLookup.Provider> lookupProvider)
	{
		super(packOutput, lookupProvider);
	}


	@Override
	protected void gather(HolderLookup.Provider provider)
	{
		add(0.3F, candy_leaves, dream_leaves, diamond_leaves, short_dream_grass, short_nightmare_grass, prickly_nightmare_grass, dream_sapling, candy_sapling);
		add(0.4F, GNSItems.rainbow_seeds, GNSItems.rainbow_berries);
		add(0.65F, cyan_flower, orange_flower, lollipop_bush, hope_mushroom, despair_mushroom, dead_flower);
		add(0.85F, hope_mushroom_block, despair_mushroom_block);
	}

	@SuppressWarnings("deprecation")
	private void add(float value, ItemLike... items)
	{
		var compBuilder = this.builder(NeoForgeDataMaps.COMPOSTABLES);

		for (ItemLike i : items)
			compBuilder.add(i.asItem().builtInRegistryHolder().getKey(), new Compostable(value), false);
	}
}