package com.legacy.good_nights_sleep.data.dev;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.data.GNSSoundProv;

import net.neoforged.neoforge.common.data.SoundDefinitionsProvider;

/**
 * Prevents Forge from throwing an exception during datagen if a file isn't
 * present for a sound definition. More info in {@link GNSSoundProv}
 */
@Mixin(SoundDefinitionsProvider.class)
public class SoundDefinitionsProviderMixin
{
	@Inject(at = @At("HEAD"), method = "validate", cancellable = true, remap = false)
	private final void validate(CallbackInfo callback)
	{
		if (!GNSSoundProv.VALIDATE)
		{
			GoodNightSleep.LOGGER.warn("Sound definition provider set to ignore validation, definitons with missing files are allowed.");
			callback.cancel();
		}
	}
}
