package com.legacy.good_nights_sleep.util;

import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import com.legacy.good_nights_sleep.GoodNightSleep;

import net.minecraft.core.Holder;
import net.minecraft.core.HolderGetter;
import net.minecraft.core.Registry;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.resources.ResourceKey;

/**
 * Stand-in for Structure Gel's Registrar.Pointer. The mod is not updated at the
 * time of development, and it was easier to do this than rewrite everything.
 */
public class Pointer<T>
{
	private final Function<BootstrapContext<?>, T> instance;

	private final ResourceKey<T> resourceKey;
	private final ResourceKey<Registry<T>> registryKey;

	public Pointer(ResourceKey<Registry<T>> registry, String name, Function<BootstrapContext<?>, T> instance)
	{
		this.instance = instance;

		this.resourceKey = ResourceKey.create(registry, GoodNightSleep.locate(name));
		this.registryKey = registry;
	}

	public Pointer(ResourceKey<Registry<T>> registry, String name, Supplier<T> instance)
	{
		this(registry, name, c -> instance.get());
	}

	public ResourceKey<T> getKey()
	{
		return this.resourceKey;
	}

	public Optional<Holder.Reference<T>> getHolder(BootstrapContext<?> bootstrapContext)
	{
		return this.getHolder(bootstrapContext.lookup(this.registryKey));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Optional<Holder.Reference<T>> getHolder(HolderGetter<?> holderGetter)
	{
		return holderGetter.get((ResourceKey) this.getKey());
	}

	public Function<BootstrapContext<?>, T> getInstance()
	{
		return this.instance;
	}
}