package com.legacy.good_nights_sleep.util;

import java.util.function.Function;

import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.chunk.ChunkGenerator;
import net.minecraft.world.level.dimension.DimensionType;
import net.minecraft.world.level.dimension.LevelStem;
import net.minecraft.world.level.levelgen.NoiseGeneratorSettings;

/**
 * Stand-in for Structure Gel's DimensionRegistrar. The mod isn't updated at the
 * time of development, and it was easier than gutting the dimension class.
 */
public class DimensionHolder
{
	private final Pointer<DimensionType> dimensionType;
	private final Pointer<NoiseGeneratorSettings> noiseSettings;
	private final Pointer<LevelStem> levelStem;
	private final ResourceKey<Level> levelKey;

	public DimensionHolder(ResourceLocation registryName, Function<BootstrapContext<?>, DimensionType> dimensionType, Function<BootstrapContext<?>, NoiseGeneratorSettings> noiseSettings, Function<BootstrapContext<?>, ChunkGenerator> chunkGeneratorFactory)
	{
		this.dimensionType = new Pointer<DimensionType>(Registries.DIMENSION_TYPE, registryName.getPath(), dimensionType);
		this.noiseSettings = new Pointer<NoiseGeneratorSettings>(Registries.NOISE_SETTINGS, registryName.getPath(), noiseSettings);
		this.levelStem = new Pointer<LevelStem>(Registries.LEVEL_STEM, registryName.getPath(), c -> new LevelStem(this.dimensionType.getHolder(c).get(), chunkGeneratorFactory.apply(c)));
		this.levelKey = ResourceKey.create(Registries.DIMENSION, registryName);
	}

	public Pointer<DimensionType> getType()
	{
		return this.dimensionType;
	}

	public Pointer<NoiseGeneratorSettings> getNoiseSettings()
	{
		return this.noiseSettings;
	}

	public Pointer<LevelStem> getLevelStem()
	{
		return this.levelStem;
	}

	public ResourceKey<Level> getLevelKey()
	{
		return this.levelKey;
	}
}