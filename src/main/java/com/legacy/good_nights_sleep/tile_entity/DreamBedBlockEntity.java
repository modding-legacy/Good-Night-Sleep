package com.legacy.good_nights_sleep.tile_entity;

import com.legacy.good_nights_sleep.registry.GNSBlockEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.core.HolderLookup;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.Connection;
import net.minecraft.network.protocol.game.ClientboundBlockEntityDataPacket;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class DreamBedBlockEntity extends BlockEntity
{
	public DreamBedBlockEntity(BlockPos pos, BlockState state)
	{
		super(GNSBlockEntityTypes.DREAM_BED.get(), pos, state);
	}

	@Override
	public CompoundTag getUpdateTag(HolderLookup.Provider registries)
	{
		CompoundTag tag = new CompoundTag();
		this.saveAdditional(tag, registries);
		return tag;
	}

	@Override
	public void handleUpdateTag(CompoundTag tag, HolderLookup.Provider registries)
	{
		this.loadWithComponents(tag, registries);
	}

	@Override
	public final ClientboundBlockEntityDataPacket getUpdatePacket()
	{
		return ClientboundBlockEntityDataPacket.create(this);
	}

	@Override
	public void onDataPacket(Connection net, ClientboundBlockEntityDataPacket packet, HolderLookup.Provider registries)
	{
		this.loadWithComponents(packet.getTag(), registries);
	}
}