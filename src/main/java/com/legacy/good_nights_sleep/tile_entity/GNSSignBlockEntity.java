package com.legacy.good_nights_sleep.tile_entity;

import com.legacy.good_nights_sleep.registry.GNSBlockEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.SignBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class GNSSignBlockEntity extends SignBlockEntity
{
	public GNSSignBlockEntity(BlockPos pos, BlockState state)
	{
		super(pos, state);
	}
	
	@Override
	public BlockEntityType<?> getType()
	{
		return GNSBlockEntityTypes.SIGN.get();
	}
}
