package com.legacy.good_nights_sleep.tile_entity;

import com.legacy.good_nights_sleep.registry.GNSBlockEntityTypes;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.HangingSignBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class GNSHangingSignBlockEntity extends HangingSignBlockEntity
{

	public GNSHangingSignBlockEntity(BlockPos pos, BlockState state)
	{
		super(pos, state);
	}

	@Override
	public BlockEntityType<?> getType()
	{
		return GNSBlockEntityTypes.HANGING_SIGN.get();
	}
}
