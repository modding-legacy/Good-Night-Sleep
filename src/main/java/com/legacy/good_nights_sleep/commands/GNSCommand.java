package com.legacy.good_nights_sleep.commands;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.capabillity.DreamPlayer;
import com.legacy.good_nights_sleep.registry.GNSDimensions;
import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.arguments.IntegerArgumentType;
import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;

import net.minecraft.commands.CommandSourceStack;
import net.minecraft.commands.Commands;
import net.minecraft.commands.arguments.TimeArgument;
import net.minecraft.network.chat.Component;

public class GNSCommand
{
	public static void register(CommandDispatcher<CommandSourceStack> dispatcher)
	{
		// @formatter:off
		LiteralArgumentBuilder<CommandSourceStack> command = Commands.literal(GoodNightSleep.MODID);

		var setProgression = Commands.literal("remaining_time").requires(GNSCommand::isOp);
		setProgression.then(Commands.literal("add").then(Commands.argument("time", TimeArgument.time()).executes(context -> setTime(context, IntegerArgumentType.getInteger(context, "time"), false))));
		setProgression.then(Commands.literal("remove").then(Commands.argument("time", TimeArgument.time()).executes(context -> setTime(context, IntegerArgumentType.getInteger(context, "time"), true))));
		command.then(setProgression);
		
		dispatcher.register(command);
		// @formatter:on
	}

	private static boolean isOp(CommandSourceStack source)
	{
		return source.hasPermission(2);
	}

	private static int setTime(CommandContext<CommandSourceStack> context, int time, boolean isRemoving)
	{
		try
		{
			DreamPlayer player = DreamPlayer.get(context.getSource().getPlayerOrException());

			if (context.getSource().getPlayer() != null)
			{
				if (!GNSDimensions.inSleepDimension(player.getPlayer()))
				{
					context.getSource().sendSuccess(() -> Component.literal("Player is not inside a dream dimension."), true);
					return 0;
				}

				long newTime = player.getEnteredDreamTime() + (isRemoving ? -time : time);
				boolean capped = false;

				if (!isRemoving && newTime > player.getPlayer().level().getGameTime())
				{
					capped = true;
					newTime = player.getPlayer().level().getGameTime();
				}

				player.setEnteredDreamTime(newTime);
				player.syncDataToClient();

				boolean capFinal = capped;
				context.getSource().sendSuccess(() -> Component.literal("Modified dream time for " + context.getSource().getPlayer().getName().getString() + (capFinal ? " (Limit reached, capped at the maximum.)" : "")), true);
				return 1;
			}
			else
			{
				context.getSource().sendSuccess(() -> Component.literal("That command needs to be run by a player."), true);
				return 0;
			}
		}
		catch (CommandSyntaxException e)
		{
			context.getSource().sendSuccess(() -> Component.literal("That command needs to be run by a player."), true);
			return 0;
		}
	}
}
