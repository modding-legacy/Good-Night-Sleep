package com.legacy.good_nights_sleep.network;

import com.legacy.good_nights_sleep.GoodNightSleep;

import net.minecraft.core.BlockPos;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.phys.Vec3;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.fml.common.EventBusSubscriber;
import net.neoforged.fml.common.EventBusSubscriber.Bus;
import net.neoforged.neoforge.common.util.FakePlayer;
import net.neoforged.neoforge.network.event.RegisterPayloadHandlersEvent;
import net.neoforged.neoforge.network.registration.PayloadRegistrar;
import net.neoforged.neoforge.server.ServerLifecycleHooks;

@EventBusSubscriber(bus = Bus.MOD)
public class PacketHandler
{
	@SubscribeEvent
	public static void registerPayloads(RegisterPayloadHandlersEvent event)
	{
		PayloadRegistrar registrar = event.registrar(GoodNightSleep.PACKET_VERSION);

		// Server -> Client
		registrar.playToClient(SyncPlayerPayload.TYPE, SyncPlayerPayload.STREAM_CODEC, SyncPlayerPayload::handler);
	}

	/**
	 * Server -> Client player passed
	 */
	public static void sendToClient(CustomPacketPayload packet, ServerPlayer serverPlayer)
	{
		if (!(serverPlayer instanceof FakePlayer))
			serverPlayer.connection.send(packet);
	}

	/**
	 * Server -> Clients in same world
	 */
	public static void sendToClients(CustomPacketPayload packet, ServerLevel level)
	{
		level.players().forEach(player -> sendToClient(packet, player));
	}

	/**
	 * Server -> Clients in same world within 256 blocks
	 */
	public static void sendToClients(CustomPacketPayload packet, ServerLevel level, BlockPos pos)
	{
		sendToClients(packet, level, pos, 256);
	}

	/**
	 * Server -> Clients in same world within range
	 */
	public static void sendToClients(CustomPacketPayload packet, ServerLevel level, BlockPos pos, int range)
	{
		Vec3 centerPos = Vec3.atCenterOf(pos);
		level.players().forEach(player ->
		{
			if (centerPos.distanceTo(player.position()) <= range)
				sendToClient(packet, player);
		});
	}

	/**
	 * Server -> Clients in every level
	 */
	public static void sendToClients(CustomPacketPayload packet)
	{
		MinecraftServer server = ServerLifecycleHooks.getCurrentServer();
		if (server != null)
			server.getPlayerList().getPlayers().forEach(player -> sendToClient(packet, player));
	}

	/**
	 * Client to server
	 *
	 * @param packet
	 */
	public static void sendToServer(CustomPacketPayload packet)
	{
		net.minecraft.client.Minecraft.getInstance().getConnection().send(packet);
	}
}
