package com.legacy.good_nights_sleep.network;

import com.legacy.good_nights_sleep.GoodNightSleep;
import com.legacy.good_nights_sleep.capabillity.DreamPlayer;

import net.minecraft.network.RegistryFriendlyByteBuf;
import net.minecraft.network.codec.StreamCodec;
import net.minecraft.network.protocol.common.custom.CustomPacketPayload;
import net.neoforged.api.distmarker.Dist;
import net.neoforged.api.distmarker.OnlyIn;
import net.neoforged.fml.loading.FMLEnvironment;
import net.neoforged.neoforge.network.handling.IPayloadContext;

public record SyncPlayerPayload(DreamPlayer attachment) implements CustomPacketPayload
{
	public static final Type<SyncPlayerPayload> TYPE = new Type<>(GoodNightSleep.locate("sync_player"));

	public static final StreamCodec<RegistryFriendlyByteBuf, SyncPlayerPayload> STREAM_CODEC = StreamCodec.composite(DreamPlayer.STREAM_CODEC, SyncPlayerPayload::attachment, SyncPlayerPayload::new);

	@Override
	public Type<SyncPlayerPayload> type()
	{
		return TYPE;
	}

	public static void handler(SyncPlayerPayload packet, IPayloadContext context)
	{
		if (FMLEnvironment.dist == Dist.CLIENT)
			context.enqueueWork(() -> handlePacket(packet));

	}

	@OnlyIn(Dist.CLIENT)
	private static void handlePacket(SyncPlayerPayload packet)
	{
		net.minecraft.client.Minecraft mc = net.minecraft.client.Minecraft.getInstance();

		if (mc.player != null)
			DreamPlayer.get(mc.player).copyFrom(packet.attachment);
	}
}