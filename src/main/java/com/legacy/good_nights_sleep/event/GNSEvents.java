package com.legacy.good_nights_sleep.event;

import com.legacy.good_nights_sleep.GNSConfig;
import com.legacy.good_nights_sleep.blocks.natural.DreamFarmlandBlock;
import com.legacy.good_nights_sleep.registry.GNSBlocks;
import com.legacy.good_nights_sleep.registry.GNSDimensions;
import com.legacy.good_nights_sleep.world.GNSTeleporter;

import net.minecraft.Util;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.animal.Sheep;
import net.minecraft.world.entity.animal.horse.AbstractHorse;
import net.minecraft.world.entity.animal.horse.SkeletonHorse;
import net.minecraft.world.entity.animal.horse.ZombieHorse;
import net.minecraft.world.entity.monster.Phantom;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BedBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.event.entity.living.FinalizeSpawnEvent;
import net.neoforged.neoforge.event.entity.player.PlayerInteractEvent;
import net.neoforged.neoforge.event.level.BlockEvent;

public class GNSEvents
{
	@SubscribeEvent
	public static void onPlayerRightClickEntity(PlayerInteractEvent.EntityInteract event)
	{
		Player player = event.getEntity();

		if ((event.getTarget() instanceof ZombieHorse || event.getTarget() instanceof SkeletonHorse) && player.level().dimension().equals(GNSDimensions.nightmareKey()))
		{
			if (!((AbstractHorse) event.getTarget()).isTamed() && player.getMainHandItem().isEmpty() && player.getOffhandItem().isEmpty())
			{
				player.swing(InteractionHand.MAIN_HAND);
				player.startRiding(event.getTarget());
				event.setCancellationResult(InteractionResult.SUCCESS);
				event.setCanceled(true);
			}
		}
	}

	@SubscribeEvent
	public static void onLivingCheckSpawn(FinalizeSpawnEvent event)
	{
		// maybe might need to be cancelled as well
		if (event.getEntity() instanceof Phantom phantom && phantom.level().dimension().equals(GNSDimensions.nightmareKey()) && !GNSConfig.WORLD.allowNightmarePhantoms())
			event.setSpawnCancelled(true);

		if (event.getEntity() instanceof Sheep sheep && sheep.level().dimension().equals(GNSDimensions.dreamKey()))
			sheep.setColor(Util.getRandom(DyeColor.values(), sheep.getRandom()));
	}

	@SubscribeEvent
	public static void onPlayerRightClickBlock(PlayerInteractEvent.RightClickBlock event)
	{
		if (!(event.getEntity() instanceof ServerPlayer player) || !(event.getLevel() instanceof ServerLevel level))
			return;

		BlockState state = level.getBlockState(event.getPos());

		if (!level.isClientSide && state.getBlock() instanceof BedBlock && GNSDimensions.inSleepDimension(player))
		{
			player.swing(InteractionHand.MAIN_HAND, true);
			event.setCanceled(true);
			// get the player's bed spawn, otherwise go for the world spawn
			BlockPos pos = player.getRespawnPosition() != null ? player.getRespawnPosition() : level.getSharedSpawnPos();
			GNSTeleporter.changeDimension(Level.OVERWORLD, player, pos);
		}

		// Debug to see if any blocks aren't tagged with what they can be mined with
		/*if (!level.isClientSide)
		{
			List<String> blocksWithoutTool = new ArrayList<>();
			BuiltInRegistries.BLOCK.stream().map(b -> b.builtInRegistryHolder()).filter(h -> h.key().location().getNamespace().equals(GoodNightSleep.MODID)).forEach(h ->
			{
				if (!h.is(BlockTags.MINEABLE_WITH_PICKAXE) && !h.is(BlockTags.MINEABLE_WITH_SHOVEL) && !h.is(BlockTags.MINEABLE_WITH_AXE) && !h.is(BlockTags.MINEABLE_WITH_HOE) && !h.is(BlockTags.SWORD_EFFICIENT))
					blocksWithoutTool.add(h.unwrapKey().map(k -> k.location().toString()).orElse("null"));
			});
			if (!blocksWithoutTool.isEmpty())
				System.out.println("Blocks without a tool: \n" + String.join("\n", blocksWithoutTool));
		}*/
	}

	@SubscribeEvent
	public static void onFarmlandTrample(BlockEvent.FarmlandTrampleEvent event)
	{
		BlockState state = event.getLevel().getBlockState(event.getPos());
		if (state.is(GNSBlocks.dream_farmland) && event.getLevel() instanceof Level l)
		{
			DreamFarmlandBlock.turnToDreamDirt(event.getEntity(), state, l, event.getPos());
			event.setCanceled(true);
		}
	}
}