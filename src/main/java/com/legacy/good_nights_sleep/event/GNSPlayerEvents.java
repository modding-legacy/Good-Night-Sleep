package com.legacy.good_nights_sleep.event;

import com.legacy.good_nights_sleep.GNSConfig;
import com.legacy.good_nights_sleep.blocks.GNSBedBlock;
import com.legacy.good_nights_sleep.blocks.util.ToolCompat;
import com.legacy.good_nights_sleep.capabillity.DreamPlayer;

import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.player.Player.BedSleepingProblem;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.common.ItemAbilities;
import net.neoforged.neoforge.event.entity.EntityJoinLevelEvent;
import net.neoforged.neoforge.event.entity.living.LivingDeathEvent;
import net.neoforged.neoforge.event.entity.player.CanContinueSleepingEvent;
import net.neoforged.neoforge.event.entity.player.CanPlayerSleepEvent;
import net.neoforged.neoforge.event.entity.player.PlayerEvent.PlayerChangedDimensionEvent;
import net.neoforged.neoforge.event.entity.player.PlayerWakeUpEvent;
import net.neoforged.neoforge.event.level.BlockEvent.BlockToolModificationEvent;
import net.neoforged.neoforge.event.tick.EntityTickEvent;

public class GNSPlayerEvents
{
	@SubscribeEvent
	public static void onEntityDeath(LivingDeathEvent event)
	{
		if (event.getEntity() instanceof Player p)
			DreamPlayer.get(p).onDeath();
	}

	@SubscribeEvent
	public static void onEntityUpdate(EntityTickEvent.Post event)
	{
		if (event.getEntity() instanceof Player p)
			DreamPlayer.get(p).serverTick();
	}

	@SubscribeEvent
	public static void onEntityJoin(EntityJoinLevelEvent event)
	{
		if (event.getEntity() instanceof ServerPlayer sp)
			DreamPlayer.get(sp).syncDataToClient();
	}

	@SubscribeEvent
	public static void onEntityChangeDimension(PlayerChangedDimensionEvent event)
	{
		if (event.getEntity() instanceof ServerPlayer sp)
			DreamPlayer.get(sp).syncDataToClient();
	}

	@SubscribeEvent
	public static void onWakeUp(PlayerWakeUpEvent event)
	{
		// Handle the teleportation if relevant
		if (event.getEntity().isSleepingLongEnough())
			event.getEntity().getSleepingPos().ifPresent(pos -> GNSBedBlock.onWakeUp(event, event.getEntity(), event.getEntity().level().getBlockState(pos), pos));
	}

	@SubscribeEvent
	public static void canKeepSleeping(CanContinueSleepingEvent event)
	{
		// Should likely always be true regardless of config option in this case.
		// Not sure if the daytime changing would wake the player up or not.
		if (event.getProblem() == BedSleepingProblem.NOT_POSSIBLE_NOW)
		{
			event.getEntity().getSleepingPos().ifPresent(pos ->
			{
				if (event.getEntity().level().getBlockState(pos).getBlock() instanceof GNSBedBlock)
					event.setContinueSleeping(true);
			});
		}
	}

	@SubscribeEvent
	public static void canSleep(CanPlayerSleepEvent event)
	{
		// Allows the player to sleep during the day.
		// The mod has always been like this so theres a config option instead.
		if (event.getProblem() == BedSleepingProblem.NOT_POSSIBLE_NOW && !GNSConfig.WORLD.limitSleepTime() && event.getLevel().getBlockState(event.getPos()).getBlock() instanceof GNSBedBlock)
			event.setProblem(null);
	}

	@SubscribeEvent
	public static void onToolUse(final BlockToolModificationEvent event)
	{
		var toolAction = event.getItemAbility();
		if (toolAction == ItemAbilities.AXE_STRIP)
		{
			BlockState finalState = event.getFinalState();
			Block result = ToolCompat.AXE_STRIPPING.get(finalState.getBlock());
			if (result != null)
				event.setFinalState(result.withPropertiesOf(finalState));
		}
		else if (toolAction == ItemAbilities.HOE_TILL)
		{
			Block result = ToolCompat.HOE_TILLING.get(event.getFinalState().getBlock());
			if (result != null)
				event.setFinalState(result.defaultBlockState());
		}
	}
}
