package com.legacy.good_nights_sleep.blocks;

import org.jetbrains.annotations.Nullable;

import com.legacy.good_nights_sleep.registry.GNSBlocks;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.ARGB;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.ScheduledTickAccess;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Rotation;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.BlockStateProperties;
import net.minecraft.world.level.block.state.properties.BooleanProperty;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.FluidState;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.phys.Vec3;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.VoxelShape;

public class RainbowBlock extends Block
{
	private static final int MAX_DISTANCE = PotOfGoldBlock.MAX_DISTANCE;

	public static final EnumProperty<Direction.Axis> AXIS = BlockStateProperties.HORIZONTAL_AXIS;

	protected static final VoxelShape X_AABB = Block.box(0.0D, 0.0D, 6.0D, 16.0D, 16.0D, 10.0D);
	protected static final VoxelShape Z_AABB = Block.box(6.0D, 0.0D, 0.0D, 10.0D, 16.0D, 16.0D);

	/*
	 * Corner Types: 0 = none, 1 = starting corner, 2 = ending corner Side Types: 0
	 * = starting, 1 = top, 2 = ending
	 * 
	 * Update 12/16/24: god this is old and awful but I'm gonna pretend I didn't see it and leave it alone
	 */
	public static final IntegerProperty CORNER_TYPE = IntegerProperty.create("corner_type", 0, 2);
	public static final IntegerProperty SIDE_TYPE = IntegerProperty.create("side_type", 0, 2);
	public static final IntegerProperty DISTANCE = IntegerProperty.create("distance", 0, MAX_DISTANCE);
	public static final BooleanProperty DECAY = BooleanProperty.create("decay");

	public RainbowBlock(Block.Properties properties)
	{
		super(properties);
		this.registerDefaultState(this.stateDefinition.any().setValue(AXIS, Direction.Axis.X).setValue(CORNER_TYPE, 0).setValue(SIDE_TYPE, 0).setValue(DECAY, false));
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context)
	{
		switch ((Direction.Axis) state.getValue(AXIS))
		{
		case Z:
			return Z_AABB;
		case X:
		default:
			return X_AABB;
		}
	}

	@Override
	public Integer getBeaconColorMultiplier(BlockState state, LevelReader reader, BlockPos pos, BlockPos beaconPos)
	{
		if (reader instanceof Level level)
		{
			int offsetAmount = 360;
			int speed = 10;
			int zOffset = pos.getZ() % offsetAmount;
			if (zOffset < 0)
				zOffset += offsetAmount;
			zOffset *= speed;

			int xOffset = pos.getX() % offsetAmount;
			if (xOffset < 0)
				xOffset += offsetAmount;
			xOffset *= speed;

			long gameTime = (level.getGameTime() + Long.MAX_VALUE) + zOffset + xOffset;

			float h = (gameTime % 360) / 360.0F;
			if (h < 0)
				h += 1.0F;
			int rgb = Mth.hsvToRgb(h, 1.0F, 0.9F);

			return ARGB.color(new Vec3(ARGB.red(rgb) / 255.0F, ARGB.green(rgb) / 255.0F, ARGB.blue(rgb) / 255.0F));
		}
		return super.getBeaconColorMultiplier(state, reader, pos, beaconPos);
	}

	@Override
	public BlockState updateShape(BlockState state, LevelReader level, ScheduledTickAccess tickAccess, BlockPos currentPos, Direction facing, BlockPos facingPos, BlockState facingState, RandomSource rand)
	{
		int i = getDistance(state, facingState) + 1;

		if (i != 1 || state.getValue(DISTANCE) != i)
			tickAccess.scheduleTick(currentPos, this, 1);

		return state;
	}

	private static int getDistance(BlockState state, BlockState neighbor)
	{
		if (neighbor.is(GNSBlocks.pot_of_gold))
			return 0;

		return neighbor.hasProperty(AXIS) && neighbor.getValue(AXIS) == state.getValue(AXIS) && neighbor.hasProperty(DISTANCE) ? neighbor.getValue(DISTANCE) : MAX_DISTANCE;
	}

	@Override
	public void destroy(LevelAccessor level, BlockPos pos, BlockState state)
	{
		decayNeighbors(state.setValue(DECAY, true), level, pos);
	}

	@Override
	public void onDestroyedByPushReaction(BlockState state, Level level, BlockPos pos, Direction pushDirection, FluidState fluid)
	{
		decayNeighbors(state.setValue(DECAY, true), level, pos);
		super.onDestroyedByPushReaction(state, level, pos, pushDirection, fluid);
	}

	private static boolean decayNeighbors(BlockState state, LevelAccessor level, BlockPos pos)
	{
		if (state.getValue(DISTANCE) <= 1 && !level.getBlockState(pos.below()).is(GNSBlocks.pot_of_gold) || state.getValue(DECAY))
		{
			BlockPos.MutableBlockPos neighborPos = new BlockPos.MutableBlockPos();

			for (Direction direction : Direction.values())
			{
				if (direction.getAxis() == state.getValue(AXIS) || direction.getAxis() == Axis.Y)
				{
					neighborPos.setWithOffset(pos, direction);

					BlockState neighbor = level.getBlockState(neighborPos);
					if (neighbor.hasProperty(AXIS) && neighbor.getValue(AXIS) == state.getValue(AXIS))
					{
						level.setBlock(neighborPos, neighbor.setValue(DECAY, true), 3);
						level.scheduleTick(neighborPos, state.getBlock(), 1);
					}
				}
			}

			return true;
		}

		return false;
	}

	private static BlockState updateDistance(BlockState state, LevelAccessor level, BlockPos pos)
	{
		int i = MAX_DISTANCE;
		if (decayNeighbors(state, level, pos))
			return state.setValue(DISTANCE, i).setValue(DECAY, true);

		BlockPos.MutableBlockPos neighborPos = new BlockPos.MutableBlockPos();

		for (Direction direction : Direction.values())
		{
			// only on matching axis, as well as up and down
			// this should prevent neighboring rainbows from other pots from sharing
			// distance in most cases
			if (direction.getAxis() == state.getValue(AXIS) || direction.getAxis() == Axis.Y)
			{
				neighborPos.setWithOffset(pos, direction);

				int neighborDistance = getDistance(state, level.getBlockState(neighborPos));
				i = Math.min(i, Math.min(state.getValue(DISTANCE), neighborDistance) + 1);

				if (i == 1)
					break;
			}
		}

		return state.setValue(DISTANCE, i);
	}

	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource random)
	{
		BlockState newState = updateDistance(state, level, pos);
		level.setBlock(pos, newState, 2);

		if (state.getValue(DISTANCE) >= MAX_DISTANCE || state.getValue(DECAY))
			level.destroyBlock(pos, false);
	}

	@Override
	public void entityInside(BlockState state, Level level, BlockPos pos, Entity entity)
	{
	}

	@Override
	protected ItemStack getCloneItemStack(LevelReader level, BlockPos pos, BlockState state, boolean includeData)
	{
		return ItemStack.EMPTY;
	}

	@Override
	public @Nullable PushReaction getPistonPushReaction(BlockState state)
	{
		return PushReaction.DESTROY;
	}

	@Override
	public BlockState rotate(BlockState state, Rotation rot)
	{
		switch (rot)
		{
		case COUNTERCLOCKWISE_90:
		case CLOCKWISE_90:
			switch ((Direction.Axis) state.getValue(AXIS))
			{
			case Z:
				return state.setValue(AXIS, Direction.Axis.X);
			case X:
				return state.setValue(AXIS, Direction.Axis.Z);
			default:
				return state;
			}
		default:
			return state;
		}
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(AXIS, CORNER_TYPE, SIDE_TYPE, DISTANCE, DECAY);
	}
}