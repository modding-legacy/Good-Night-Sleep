package com.legacy.good_nights_sleep.blocks.util;

import java.util.HashMap;
import java.util.Map;

import com.legacy.good_nights_sleep.registry.GNSBlocks;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;

public class ToolCompat
{
	public static final Map<Block, Block> AXE_STRIPPING = new HashMap<>();
	public static final Map<Block, Block> HOE_TILLING = new HashMap<>();

	static
	{
		axeStripping(GNSBlocks.dream_log, GNSBlocks.stripped_dream_log);
		axeStripping(GNSBlocks.white_log, GNSBlocks.stripped_white_log);
		axeStripping(GNSBlocks.dead_log, GNSBlocks.stripped_dead_log);
		axeStripping(GNSBlocks.blood_log, GNSBlocks.stripped_blood_log);
		axeStripping(GNSBlocks.dream_wood, GNSBlocks.stripped_dream_wood);
		axeStripping(GNSBlocks.white_wood, GNSBlocks.stripped_white_wood);
		axeStripping(GNSBlocks.dead_wood, GNSBlocks.stripped_dead_wood);
		axeStripping(GNSBlocks.blood_wood, GNSBlocks.stripped_blood_wood);

		hoeTilling(GNSBlocks.dream_grass_block, GNSBlocks.dream_farmland);
		hoeTilling(GNSBlocks.dream_dirt, GNSBlocks.dream_farmland);
		hoeTilling(GNSBlocks.nightmare_grass_block, Blocks.FARMLAND);
	}

	public static void init()
	{
	}
	
	static void axeStripping(Block log, Block stripped)
	{
		AXE_STRIPPING.put(log, stripped);
	}

	static void hoeTilling(Block dirt, Block farmland)
	{
		HOE_TILLING.put(dirt, farmland);
	}
}
