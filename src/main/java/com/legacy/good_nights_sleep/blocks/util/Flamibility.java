package com.legacy.good_nights_sleep.blocks.util;

import static com.legacy.good_nights_sleep.registry.GNSBlocks.*;

import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.FireBlock;

public class Flamibility
{

	public static void init()
	{
		FireBlock fire = (FireBlock) Blocks.FIRE;

		fire.setFlammable(dream_log, 5, 5);
		fire.setFlammable(dream_wood, 5, 5);
		fire.setFlammable(stripped_dream_log, 5, 5);
		fire.setFlammable(stripped_dream_wood, 5, 5);
		fire.setFlammable(dream_planks, 5, 20);
		fire.setFlammable(dream_slab, 5, 20);
		fire.setFlammable(dream_stairs, 5, 20);
		fire.setFlammable(dream_fence, 5, 20);
		fire.setFlammable(dream_fence_gate, 5, 20);

		fire.setFlammable(white_log, 5, 5);
		fire.setFlammable(white_wood, 5, 5);
		fire.setFlammable(stripped_white_log, 5, 5);
		fire.setFlammable(stripped_white_wood, 5, 5);
		fire.setFlammable(white_planks, 5, 20);
		fire.setFlammable(white_slab, 5, 20);
		fire.setFlammable(white_stairs, 5, 20);
		fire.setFlammable(white_fence, 5, 20);
		fire.setFlammable(white_fence_gate, 5, 20);

		fire.setFlammable(dream_leaves, 30, 60);
		fire.setFlammable(candy_leaves, 30, 60);
		fire.setFlammable(diamond_leaves, 30, 60);
		
		fire.setFlammable(cyan_flower, 60, 100);
		fire.setFlammable(orange_flower, 60, 100);
		fire.setFlammable(lollipop_bush, 60, 100);
		fire.setFlammable(short_dream_grass, 60, 100);
	}

}
