package com.legacy.good_nights_sleep.blocks;

import com.legacy.good_nights_sleep.tile_entity.GNSSignBlockEntity;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.WallSignBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.properties.WoodType;

public class GNSWallSignBlock extends WallSignBlock
{
	public GNSWallSignBlock(Properties prop, WoodType woodType)
	{
		super(woodType, prop);
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new GNSSignBlockEntity(pos, state);
	}
}
