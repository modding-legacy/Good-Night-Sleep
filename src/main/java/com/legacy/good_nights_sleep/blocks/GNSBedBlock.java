package com.legacy.good_nights_sleep.blocks;

import com.legacy.good_nights_sleep.capabillity.DreamPlayer;
import com.legacy.good_nights_sleep.registry.GNSBlocks;
import com.legacy.good_nights_sleep.registry.GNSDimensions;
import com.legacy.good_nights_sleep.tile_entity.DreamBedBlockEntity;
import com.legacy.good_nights_sleep.world.GNSTeleporter;

import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.util.RandomSource;
import net.minecraft.util.StringRepresentable;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.DyeColor;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.BedBlock;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.BlockHitResult;
import net.neoforged.neoforge.event.entity.player.PlayerWakeUpEvent;

public class GNSBedBlock extends BedBlock
{
	/*public static final MapCodec<GNSBedBlock> CODEC = RecordCodecBuilder.mapCodec(instance -> instance.group(DreamType.CODEC.fieldOf("dream").forGetter(GNSBedBlock::getDreamType), propertiesCodec()).apply(instance, GNSBedBlock::new));*/

	public GNSBedBlock(Block.Properties properties)
	{
		super(DyeColor.WHITE, properties);
	}

	public DreamType getDreamType()
	{
		return this == GNSBlocks.luxurious_bed ? DreamType.DREAM : this == GNSBlocks.wretched_bed ? DreamType.NIGHTMARE : DreamType.RANDOM;
	}

	@Override
	public InteractionResult useWithoutItem(BlockState state, Level level, BlockPos pos, Player player, BlockHitResult hit)
	{
		return super.useWithoutItem(state, level, pos, player, hit);
	}

	public static void onWakeUp(PlayerWakeUpEvent event, Player player, BlockState bedState, BlockPos sleepPos)
	{
		if (bedState.getBlock() instanceof GNSBedBlock bed)
		{
			bed.setBedOccupied(bedState, player.level(), sleepPos, player, false);
			bed.travelToDream(player, bed.getDreamType());
		}
	}

	private void travelToDream(Player player, DreamType dreamType)
	{
		if (!(player instanceof ServerPlayer serverPlayer))
			return;

		var ow = Level.OVERWORLD;
		ResourceKey<Level> sleepDim = dreamType.getDreamDestination(player.getRandom());
		ResourceKey<Level> transferDimension = player.level().dimension().equals(ow) ? sleepDim : ow;

		// Update entry time when entering a dream
		if (transferDimension != Level.OVERWORLD)
		{
			DreamPlayer d = DreamPlayer.get(serverPlayer);

			d.setEnteredDreamTime(serverPlayer.level().getGameTime());
			d.syncDataToClient();
		}

		try
		{
			ServerLevel overworldLevel = player.getServer().getLevel(ow);
			BlockPos pos = overworldLevel.getSharedSpawnPos();

			if (serverPlayer.getRespawnPosition() != null)
				pos = serverPlayer.getRespawnPosition();

			GNSTeleporter.changeDimension(transferDimension, player, pos);
		}
		catch (NullPointerException e)
		{
			e.printStackTrace();
		}
	}

	@Override
	public BlockEntity newBlockEntity(BlockPos pos, BlockState state)
	{
		return new DreamBedBlockEntity(pos, state);
	}

	private enum DreamType implements StringRepresentable
	{
		DREAM("dream"), NIGHTMARE("nightmare"), RANDOM("random");

		/*public static final Codec<DreamType> CODEC = StringRepresentable.fromEnum(DreamType::values);*/

		private String name;

		private DreamType(String name)
		{
			this.name = name;
		}

		public ResourceKey<Level> getDreamDestination(RandomSource rand)
		{
			return switch (this)
			{
			case DREAM -> GNSDimensions.dreamKey();
			case NIGHTMARE -> GNSDimensions.nightmareKey();
			default -> rand.nextBoolean() ? GNSDimensions.dreamKey() : GNSDimensions.nightmareKey();
			};
		}

		@Override
		public String getSerializedName()
		{
			return this.name;
		}
	}
}