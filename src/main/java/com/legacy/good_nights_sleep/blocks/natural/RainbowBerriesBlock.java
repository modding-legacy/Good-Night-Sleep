package com.legacy.good_nights_sleep.blocks.natural;

import com.legacy.good_nights_sleep.registry.GNSItems;

import net.minecraft.world.level.ItemLike;
import net.minecraft.world.level.block.BeetrootBlock;

public class RainbowBerriesBlock extends BeetrootBlock
{
	public RainbowBerriesBlock(Properties props) 
	{
		super(props);
	}
	
	@Override
	protected ItemLike getBaseSeedId()
    {
        return GNSItems.rainbow_seeds;
    }
}
