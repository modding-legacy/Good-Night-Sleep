package com.legacy.good_nights_sleep.blocks.natural;

import javax.annotation.Nullable;

import com.legacy.good_nights_sleep.registry.GNSBlocks;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.util.RandomSource;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.FarmBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;

public class DreamFarmlandBlock extends FarmBlock
{
	public DreamFarmlandBlock(Block.Properties builder)
	{
		super(builder);
	}

	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		if (!state.canSurvive(level, pos))
			turnToDreamDirt((Entity) null, state, level, pos);
	}

	@Override
	public void randomTick(BlockState state, ServerLevel level, BlockPos pos, RandomSource rand)
	{
		int i = state.getValue(MOISTURE);

		if (!isNearWater(level, pos) && !level.isRainingAt(pos.above()))
		{
			if (i > 0)
				level.setBlock(pos, state.setValue(MOISTURE, Integer.valueOf(i - 1)), 2);
			else if (!shouldMaintainFarmland(level, pos))
				turnToDreamDirt((Entity) null, state, level, pos);
		}
		else if (i < 7)
			level.setBlock(pos, state.setValue(MOISTURE, Integer.valueOf(7)), 2);
	}

	public static void turnToDreamDirt(@Nullable Entity entity, BlockState state, Level level, BlockPos pos)
	{
		BlockState blockstate = pushEntitiesUp(state, GNSBlocks.dream_dirt.defaultBlockState(), level, pos);
		level.setBlockAndUpdate(pos, blockstate);
		level.gameEvent(GameEvent.BLOCK_CHANGE, pos, GameEvent.Context.of(entity, blockstate));
	}
}