package com.legacy.good_nights_sleep.blocks.natural;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.animal.horse.SkeletonHorse;
import net.minecraft.world.entity.animal.horse.ZombieHorse;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.monster.Enemy;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.state.BlockState;

public class PricklyGrassBlock extends GNSTallGrassBlock
{
	public PricklyGrassBlock(Properties properties)
	{
		super(properties);
	}

	@Override
	public boolean canBeReplaced(BlockState state, BlockPlaceContext useContext)
	{
		return true;
	}

	@Override
	public void entityInside(BlockState state, Level level, BlockPos pos, Entity entity)
	{
		if (level instanceof ServerLevel sl && !(entity instanceof Enemy || entity instanceof ItemEntity || entity instanceof ZombieHorse || entity instanceof SkeletonHorse))
			entity.hurtServer(sl, entity.damageSources().cactus(), 1.0F);
	}
}