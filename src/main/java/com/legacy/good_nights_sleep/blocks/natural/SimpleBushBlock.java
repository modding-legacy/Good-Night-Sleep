package com.legacy.good_nights_sleep.blocks.natural;

import com.mojang.serialization.MapCodec;

import net.minecraft.world.level.block.BushBlock;

public class SimpleBushBlock extends BushBlock
{
	public static final MapCodec<SimpleBushBlock> CODEC = simpleCodec(SimpleBushBlock::new);

	public SimpleBushBlock(Properties props)
	{
		super(props);
	}

	@Override
	protected MapCodec<? extends BushBlock> codec()
	{
		return CODEC;
	}

}
