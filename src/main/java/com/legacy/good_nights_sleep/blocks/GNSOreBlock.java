package com.legacy.good_nights_sleep.blocks;

import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.util.valueproviders.UniformInt;
import net.minecraft.world.level.block.DropExperienceBlock;
import net.minecraft.world.level.block.state.BlockBehaviour;

public class GNSOreBlock extends DropExperienceBlock
{
	public GNSOreBlock(BlockBehaviour.Properties properties)
	{
		super(ConstantInt.of(0), properties);
	}

	public GNSOreBlock(Properties properties, int minXp, int maxXp)
	{
		super(UniformInt.of(minXp, maxXp), properties);
	}
}
