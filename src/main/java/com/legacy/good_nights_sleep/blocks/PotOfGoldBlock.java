package com.legacy.good_nights_sleep.blocks;

import org.jetbrains.annotations.Nullable;

import com.legacy.good_nights_sleep.registry.GNSBlocks;
import com.legacy.good_nights_sleep.registry.GNSSounds;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction.Axis;
import net.minecraft.core.particles.ColorParticleOption;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.ARGB;
import net.minecraft.util.Mth;
import net.minecraft.util.RandomSource;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.HorizontalDirectionalBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.StateDefinition;
import net.minecraft.world.level.block.state.properties.EnumProperty;
import net.minecraft.world.level.block.state.properties.IntegerProperty;
import net.minecraft.world.level.material.PushReaction;
import net.minecraft.world.level.pathfinder.PathComputationType;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.shapes.BooleanOp;
import net.minecraft.world.phys.shapes.CollisionContext;
import net.minecraft.world.phys.shapes.Shapes;
import net.minecraft.world.phys.shapes.VoxelShape;

public class PotOfGoldBlock extends Block
{
	protected static final int MAX_DISTANCE = 100;

	private static final VoxelShape INSIDE = box(2.0D, 4.0D, 2.0D, 14.0D, 16.0D, 14.0D);
	protected static final VoxelShape SHAPE = Shapes.join(Shapes.block(), Shapes.or(box(0.0D, 0.0D, 4.0D, 16.0D, 3.0D, 12.0D), box(4.0D, 0.0D, 0.0D, 12.0D, 3.0D, 16.0D), box(2.0D, 0.0D, 2.0D, 14.0D, 3.0D, 14.0D), INSIDE), BooleanOp.ONLY_FIRST);

	public static final IntegerProperty PLACEMENTS = IntegerProperty.create("placements", 0, MAX_DISTANCE);
	public static final EnumProperty<Direction> FACING = HorizontalDirectionalBlock.FACING;

	public PotOfGoldBlock(Block.Properties properties)
	{
		super(properties);
		this.registerDefaultState(this.stateDefinition.any().setValue(PLACEMENTS, 0).setValue(FACING, Direction.NORTH));
	}

	@Override
	public void animateTick(BlockState state, Level level, BlockPos pos, RandomSource rand)
	{
		if (level.isClientSide)
		{
			if (level.getBlockState(pos.above()).is(GNSBlocks.rainbow))
			{
				for (int i = 0; i < 1 + rand.nextInt(2); ++i)
				{
					float x = pos.getX() + 0.25F + (rand.nextFloat() * 0.5F);
					float z = pos.getZ() + 0.25F + (rand.nextFloat() * 0.5F);
					float scale = 0.5F;

					level.addParticle(ParticleTypes.FIREWORK, x, pos.getY() + 0.9F, z, (x - (pos.getX() + 0.6F)) * scale, 0.05F + (rand.nextFloat() * 0.2F), (z - (pos.getZ() + 0.5F)) * scale);
				}
			}

			float x = pos.getX() + 0.25F + (rand.nextFloat() * 0.5F);
			float z = pos.getZ() + 0.25F + (rand.nextFloat() * 0.5F);

			level.addParticle(ColorParticleOption.create(ParticleTypes.ENTITY_EFFECT, ARGB.color(Mth.floor(38.25F), 0xf1c957)), x, pos.getY() + 0.9F, z, 1, 1, 1);
		}
	}

	@Override
	public VoxelShape getShape(BlockState state, BlockGetter worldIn, BlockPos pos, CollisionContext context)
	{
		return SHAPE;
	}

	@Override
	public VoxelShape getInteractionShape(BlockState state, BlockGetter worldIn, BlockPos pos)
	{
		return INSIDE;
	}

	@Override
	public InteractionResult useWithoutItem(BlockState state, Level level, BlockPos pos, Player player, BlockHitResult hit)
	{
		if (level instanceof ServerLevel sl)
		{
			player.addEffect(new MobEffectInstance(MobEffects.REGENERATION, 180, 0, false, true));
			sl.playSound(null, pos, GNSSounds.BLOCK_POT_OF_GOLD_USE, SoundSource.BLOCKS);
			sl.sendParticles(ColorParticleOption.create(ParticleTypes.ENTITY_EFFECT, ARGB.color(100, MobEffects.REGENERATION.value().getColor())), pos.getX() + 0.5F, pos.getY() + 0.9F, pos.getZ() + 0.5F, 20, 0.3F, 0.1F, 0.3F, 1);
		}

		return InteractionResult.SUCCESS;
	}

	@Override
	public void setPlacedBy(Level level, BlockPos pos, BlockState state, LivingEntity placer, ItemStack stack)
	{
		level.scheduleTick(pos, this, 1);
	}

	@Override
	public BlockState getStateForPlacement(BlockPlaceContext context)
	{
		return this.defaultBlockState().setValue(FACING, context.getHorizontalDirection());
	}

	@Override
	public void tick(BlockState state, ServerLevel level, BlockPos pos, RandomSource random)
	{
		if (state.hasProperty(PLACEMENTS) && state.hasProperty(FACING) && state.getValue(PLACEMENTS) < 100)
		{
			if (placeRainbows(state.getValue(PLACEMENTS), level, pos, state, state.getValue(FACING)))
			{
				level.setBlock(pos, state.setValue(PLACEMENTS, state.getValue(PLACEMENTS) + 1), 2);
				level.scheduleTick(pos, this, 1);
			}
			else
				level.setBlock(pos, state.setValue(PLACEMENTS, MAX_DISTANCE), 2);
		}
	}

	@Override
	public @Nullable PushReaction getPistonPushReaction(BlockState state)
	{
		return PushReaction.BLOCK;
	}

	@Override
	protected boolean isPathfindable(BlockState state, PathComputationType pathComputationType)
	{
		return false;
	}

	private static boolean placeRainbows(int iteration, Level level, BlockPos pos, BlockState state, Direction dir)
	{
		if (!(level instanceof ServerLevel sl))
			return false;

		if (iteration == 0)
			return true;
		Axis axis = dir.getAxis();

		boolean flipped = dir == Direction.NORTH || dir == Direction.EAST;
		int maxHeight = 20, maxLength = 40;

		// for (int iteration = 1; iteration < MAX_DISTANCE; ++iteration)

		BlockPos placementPos = null;
		{
			if (iteration <= maxHeight)
			{
				if (level.getBlockState(pos.above(iteration)).isAir())
				{
					BlockState rainbow = GNSBlocks.rainbow.defaultBlockState().setValue(RainbowBlock.AXIS, axis).setValue(RainbowBlock.SIDE_TYPE, flipped ? 2 : 0);

					if (iteration == maxHeight)
						rainbow = rainbow.setValue(RainbowBlock.CORNER_TYPE, flipped ? 2 : 1);

					level.setBlock(placementPos = pos.above(iteration), rainbow.setValue(RainbowBlock.DISTANCE, iteration), 3);
				}
				else
					iteration = MAX_DISTANCE + 1;
			}

			int currentLength = iteration - maxHeight;

			if (iteration > maxHeight && currentLength <= maxLength)
			{
				if (level.getBlockState(pos.above(maxHeight).relative(dir, currentLength)).isAir())
				{
					BlockState rainbow = GNSBlocks.rainbow.defaultBlockState().setValue(RainbowBlock.AXIS, axis).setValue(RainbowBlock.SIDE_TYPE, 1);

					if (currentLength == maxLength)
						rainbow = rainbow.setValue(RainbowBlock.CORNER_TYPE, flipped ? 1 : 2);

					level.setBlock(placementPos = pos.above(maxHeight).relative(dir, currentLength), rainbow.setValue(RainbowBlock.DISTANCE, iteration), 3);
				}
				else
					iteration = MAX_DISTANCE + 1;
			}

			if (currentLength > maxLength)
			{
				int heightDecrease = iteration - (maxHeight + maxLength);

				if (level.getBlockState(pos.above(maxHeight - heightDecrease).relative(dir, maxLength)).isAir())
				{
					BlockState rainbow = GNSBlocks.rainbow.defaultBlockState().setValue(RainbowBlock.AXIS, axis).setValue(RainbowBlock.SIDE_TYPE, flipped ? 0 : 2);
					level.setBlock(placementPos = pos.above(maxHeight - heightDecrease).relative(dir, maxLength), rainbow.setValue(RainbowBlock.DISTANCE, iteration), 3);
				}
				else
					iteration = MAX_DISTANCE + 1;
			}

		}

		if (placementPos != null)
		{
			sl.sendParticles(ParticleTypes.FIREWORK, placementPos.getX() + 0.5F, placementPos.getY() + 0.5F, placementPos.getZ() + 0.5F, 10, 0.5F, 0.5F, 0.5F, 0.3F);

			if (iteration % 2 == 0)
				sl.playSound(null, placementPos, GNSSounds.BLOCK_RAINBOW_APPEAR, SoundSource.BLOCKS, 5, sl.getRandom().nextBoolean() ? (sl.getRandom().nextBoolean() ? 1.1F : 1.2F) : 1F);

			return true;
		}

		return false;
	}

	@Override
	protected void createBlockStateDefinition(StateDefinition.Builder<Block, BlockState> builder)
	{
		builder.add(PLACEMENTS, FACING);
	}
}